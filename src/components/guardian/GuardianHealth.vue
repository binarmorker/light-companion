<script setup lang="ts">
import { computed, ref, watch, nextTick } from 'vue'
import { Guardian, Subclass, Ability, DamageType, Feature } from '@/models/destiny'
import { getSubclass } from '@/models/destinySubclasses'
import { getDiceAverageOrMaxOnFirstLevel, getAbilityModifier, getDamageTypeName, getHealthPoints, appendExplosive } from '@/utils'
import { elementColors, getColor } from '@/utils/color'
import DialogWindow from '@/components/DialogWindow.vue'
import ListingHeader from '@/components/ListingHeader.vue'
import DialogButton from '@/components/DialogButton.vue'
import DamageFormula from '@/components/DamageFormula.vue'
import DialogTabs from '@/components/DialogTabs.vue'

enum HealthPointTab {
  Damage = 0,
  Healing = 1,
  Manual = 2
}

const emit = defineEmits(['update'])
const props = defineProps<{ character: Guardian, readonly?: boolean }>()
const activeTab = ref(HealthPointTab.Damage)

const subclass = computed<Subclass | null>(() => props.character?.subclass as never && getSubclass(props.character!.subclass!))
const health = ref<InstanceType<typeof DialogWindow>>()
const formula = ref<InstanceType<typeof DialogWindow>>()

const maxHitPoints = computed(() => props.character?.maxHitPoints
  ?? (subclass.value && getHealthPoints(subclass.value.shieldDie, getAbilityModifier(props.character!, Ability.Constitution), props.character!.level))
  ?? 1)
const maxShieldPoints = computed(() => props.character?.maxShieldPoints
  ?? (subclass.value && getDiceAverageOrMaxOnFirstLevel(subclass.value.shieldDie, props.character!.level))
  ?? 0)
const hasShield = computed(() => props.character?.shieldPoints && maxShieldPoints.value)
const maxOvershieldPoints = computed(() => props.character?.maxOvershieldPoints
  ?? (subclass.value && props.character!.level * 5)
  ?? 5)
const hasOvershield = computed(() => props.character?.overshieldPoints && maxOvershieldPoints.value)

const defaultSource = { damageType: DamageType.Kinetic, amount: 0, multiplier: 1 }
const damageSources = ref<{ damageType: DamageType, amount: number, multiplier: number }[]>([
  { ...defaultSource }
])
const validDamageSources = computed(() => damageSources.value.filter(({ amount }) => amount))
const totalDamage = computed(() => damageSources.value.reduce((sum, { amount, multiplier }) => sum + Math.floor(amount * multiplier), 0))
const damageToOvershield = computed(() => hasOvershield.value ? Math.min(props.character.overshieldPoints, totalDamage.value) : 0)
const newOvershield = computed(() => hasOvershield.value ? Math.max(props.character.overshieldPoints - totalDamage.value, 0) : 0)
const damageToShield = computed(() => hasShield.value ? Math.min(props.character.shieldPoints, totalDamage.value - damageToOvershield.value) : 0)
const newShield = computed(() => hasShield.value ? Math.max(props.character.shieldPoints - (totalDamage.value - damageToOvershield.value), 0) : 0)
const damageToHealth = computed(() => Math.min(props.character.hitPoints, totalDamage.value - damageToOvershield.value - damageToShield.value))
const newHealth = computed(() => Math.max(props.character.hitPoints - (totalDamage.value - damageToOvershield.value - damageToShield.value), 0))

const healing = ref(0)
const healingType = ref<'health'|'shield'|'all'>('shield')
const healingToShield = computed(() => healingType.value === 'shield' && healing.value
  ? Math.min(maxShieldPoints.value - props.character.shieldPoints, healing.value)
  : 0)
const healingToHealth = computed(() => (healingType.value === 'all' || healingType.value === 'health') && healing.value
  ? Math.min(maxHitPoints.value - props.character.hitPoints, healing.value)
  : 0)
const healingToAll = computed(() => healingType.value === 'all' 
  ? Math.min(maxShieldPoints.value - props.character.shieldPoints, healing.value - healingToHealth.value)
  : 0)
const shieldDiceFeature = computed(() => subclass.value ? ({
  _type: 'Feature',
  name: `${props.character.name} rolls for shield points`,
  level: 0,
  description: '',
  attackFormula: `[light]d${subclass.value.shieldDie} + [lightMod]`
} as Feature) : null)

function resetHealth() {
  healing.value = 0
  healingType.value = 'shield'
  activeTab.value = HealthPointTab.Damage
  damageSources.value = [{ ...defaultSource }]
  health.value?.hide()
  emit('update')
}

function applyDamage() {
  const values = [newOvershield.value, newShield.value, newHealth.value].slice()
  if (damageToOvershield.value) props.character.overshieldPoints = values[0]
  if (damageToShield.value) props.character.shieldPoints = values[1]
  if (damageToHealth.value) props.character.hitPoints = values[2]
  resetHealth()
}

function applyHealing() {
  const values = [healingToHealth.value, healingToShield.value, healingToAll.value].slice()
  if (healingToHealth.value) props.character.hitPoints += values[0]
  if (healingToShield.value) props.character.shieldPoints += values[1]
  if (healingToAll.value) props.character.shieldPoints += values[2]
  resetHealth()
}

function healFully() {
  props.character.hitPoints = maxHitPoints.value
  resetHealth()
}

function applyOvershield() {
  props.character.overshieldPoints = maxOvershieldPoints.value
  resetHealth()
}

function update(event: Event) {
  emit('update', event)
}

function onRoll(_: boolean, result: { formula: string, evaluated: string, value: number, type?: DamageType }[]) {
  formula.value?.hide()
  healing.value = result[0].value
  healingType.value = 'shield'
  health.value?.show()
}

function setDefaultValues(result: { formula: string, evaluated: string, value: number, type?: DamageType }[]) {
  damageSources.value = []
  
  if (result.some(x => x.type !== undefined && x.type < 9000)) {
    for (const damage of result) {
      const damageType = damage.type || DamageType.Kinetic
      const multiplier = appendExplosive(props.character.weaknesses).includes(damageType) ? 2
        : appendExplosive(props.character.resistances).includes(damageType) ? 0.5
        : appendExplosive(props.character.immunities).includes(damageType) ? 0
        : 1
      damageSources.value.push({ amount: damage.value, damageType, multiplier })
    }
  } else if (result.some(x => x.type !== undefined && x.type > 9000)) {
    activeTab.value = HealthPointTab.Healing

    for (const damage of result) {
      healing.value += damage.value

      if (damage.type === DamageType.Healing) {
        healingType.value = 'all'
      } else if (damage.type === DamageType.HealthHealing) {
        healingType.value = 'health'
      } else if (damage.type === DamageType.ShieldHealing) {
        healingType.value = 'shield'
      }
    }
  }
}

defineExpose({
  setDefaultValues
})

watch(healing, () => {
  if (healing.value < 0) healing.value = 0
})
watch(damageSources, () => {
  if (damageSources.value.some(({ amount }) => amount < 0)) {
    damageSources.value = damageSources.value.map(x => x.amount < 0 ? { ...x, amount: 0 } : x)
  }
}, { deep: true })
</script>

<template>
  <div>
    <DialogTabs :tabs="{
      damage: 'DAMAGE',
      healing: 'HEALING',
      manual: 'MANUAL'
    }" :active-tab="activeTab">
      <template #damage>
        <ListingHeader :headers="{
          'DAMAGE': 'w-1/6',
          'TYPE': 'w-1/2',
          'RESISTANCE': 'w-1/3'
        }">
          <button @click="() => damageSources.push({ ...defaultSource })" class="text-zinc-400 h-6 w-6 -mr-1 flex justify-center items-center text-4xl rounded-2xl hover:bg-zinc-600">&plus;</button>
        </ListingHeader>
        <div class="flex items-center mb-2" v-for="(source, i) in damageSources" :key="i">
          <input min="0" type="number" v-model="source.amount" class="text-right px-2 bg-zinc-800 w-1/6 cursor-pointer h-8 border-zinc-500 border-2 border-r-0" />
          <select v-model="source.damageType" class="bg-zinc-800 w-1/2 text-sm px-2 cursor-pointer h-8 border-zinc-500 border-2 border-r-0">
            <optgroup label="Generic">
              <option :value="DamageType.Kinetic">{{ getDamageTypeName(DamageType.Kinetic) }}</option>
              <option :value="DamageType.Poison">{{ getDamageTypeName(DamageType.Poison) }}</option>
              <option :value="DamageType.Psychic">{{ getDamageTypeName(DamageType.Psychic) }}</option>
            </optgroup>
            <optgroup label="Light">
              <option :value="DamageType.Light">{{ getDamageTypeName(DamageType.Light) }}</option>
              <option :value="DamageType.Arc">{{ getDamageTypeName(DamageType.Arc) }}</option>
              <option :value="DamageType.Solar">{{ getDamageTypeName(DamageType.Solar) }}</option>
              <option :value="DamageType.Void">{{ getDamageTypeName(DamageType.Void) }}</option>
            </optgroup>
            <optgroup label="Darkness">
              <option :value="DamageType.Darkness">{{ getDamageTypeName(DamageType.Darkness) }}</option>
              <option :value="DamageType.Stasis">{{ getDamageTypeName(DamageType.Stasis) }}</option>
              <option :value="DamageType.Strand">{{ getDamageTypeName(DamageType.Strand) }}</option>
              <!-- <option :value="DamageType.Luster">{{ getDamageTypeName(DamageType.Luster) }}</option> -->
            </optgroup>
            <optgroup label="Explosive Generic">
              <option :value="DamageType.ExplosiveKinetic">{{ getDamageTypeName(DamageType.ExplosiveKinetic) }}</option>
              <option :value="DamageType.ExplosivePoison">{{ getDamageTypeName(DamageType.ExplosivePoison) }}</option>
              <option :value="DamageType.ExplosivePsychic">{{ getDamageTypeName(DamageType.ExplosivePsychic) }}</option>
            </optgroup>
            <optgroup label="Explosive Light">
              <option :value="DamageType.ExplosiveLight">{{ getDamageTypeName(DamageType.ExplosiveLight) }}</option>
              <option :value="DamageType.ExplosiveArc">{{ getDamageTypeName(DamageType.ExplosiveArc) }}</option>
              <option :value="DamageType.ExplosiveSolar">{{ getDamageTypeName(DamageType.ExplosiveSolar) }}</option>
              <option :value="DamageType.ExplosiveVoid">{{ getDamageTypeName(DamageType.ExplosiveVoid) }}</option>
            </optgroup>
            <optgroup label="Explosive Darkness">
              <option :value="DamageType.ExplosiveDarkness">{{ getDamageTypeName(DamageType.ExplosiveDarkness) }}</option>
              <option :value="DamageType.ExplosiveStasis">{{ getDamageTypeName(DamageType.ExplosiveStasis) }}</option>
              <option :value="DamageType.ExplosiveStrand">{{ getDamageTypeName(DamageType.ExplosiveStrand) }}</option>
              <!-- <option :value="DamageType.ExplosiveLuster">{{ getDamageTypeName(DamageType.ExplosiveLuster) }}</option> -->
            </optgroup>
          </select>
          <select v-model="source.multiplier" class="bg-zinc-800 w-1/3 text-sm px-2 cursor-pointer h-8 border-zinc-500 border-2">
            <option :value="1">Normal</option>
            <option :value="0.5">Resistant</option>
            <option :value="0">Immune</option>
            <option :value="2">Vulnerable</option>
          </select>
          <div class="w-7 flex justify-end">
            <button @click="() => damageSources.splice(i, 1)" class="text-zinc-400 -mr-1.5 h-6 w-6 flex justify-center items-center text-4xl rounded-2xl hover:bg-zinc-600">&times;</button>
          </div>
        </div>
        <div v-if="validDamageSources.length">
          <span class="float-left mr-1">Total: </span>
          <p class="flex flex-wrap gap-x-1">
            <template v-for="(source, i) in validDamageSources" :key="i">
              <span class="relative inline-flex gap-1">
                <img class="w-4 invert absolute opacity-100 animate-ping mt-1" src="@/assets/explosive.svg" v-if="source.damageType >= 900" />
                <img class="w-4 invert" src="@/assets/kinetic.svg" v-if="(source.damageType - DamageType.Kinetic) % 900 === 0" />
                <img class="w-4 invert" src="@/assets/poison.svg" v-if="(source.damageType - DamageType.Poison) % 900 === 0" />
                <img class="w-4 invert" src="@/assets/psychic.svg" v-if="(source.damageType - DamageType.Psychic) % 900 === 0" />
                <img class="w-4 invert-[10%]" src="@/assets/light.svg" v-if="(source.damageType - DamageType.Light) % 900 === 0" />
                <img class="w-4 invert-[40%]" src="@/assets/darkness.svg" v-if="(source.damageType - DamageType.Darkness) % 900 === 0" />
                <img class="w-4" :style="elementColors.arc.filter" src="@/assets/arc.svg" v-if="(source.damageType - DamageType.Arc) % 900 === 0" />
                <img class="w-4" :style="elementColors.solar.filter" src="@/assets/solar.svg" v-if="(source.damageType - DamageType.Solar) % 900 === 0" />
                <img class="w-4" :style="elementColors.void.filter" src="@/assets/void.svg" v-if="(source.damageType - DamageType.Void) % 900 === 0" />
                <img class="w-4" :style="elementColors.strand.filter" src="@/assets/strand.svg" v-if="(source.damageType - DamageType.Strand) % 900 === 0" />
                <img class="w-4" :style="elementColors.stasis.filter" src="@/assets/stasis.svg" v-if="(source.damageType - DamageType.Stasis) % 900 === 0" />
                <img class="w-4" :style="elementColors.luster.filter" src="@/assets/luster.svg" v-if="(source.damageType - DamageType.Luster) % 900 === 0" />
                <span class="font-semibold" :style="{ color: getColor(source.damageType) }">{{ Math.floor(source.amount * source.multiplier) }}</span>
                <i :style="{ color: getColor(source.damageType) }">{{ getDamageTypeName(source.damageType) }} damage</i>
                <span class="text-xs text-zinc-400 leading-6" v-if="source.multiplier === 2">(WEAK, &times;2)</span>
                <span class="text-xs text-zinc-400 leading-6" v-if="source.multiplier === 0.5">(RES., &divide;2)</span>
                <span class="text-xs text-zinc-400 leading-6" v-if="source.multiplier === 0">(IMM., &times;0)</span>
                <span v-if="validDamageSources.length > 1 && i < validDamageSources.length - 1">&nbsp;&plus;&nbsp;</span>
                <span v-if="i === validDamageSources.length - 1">&nbsp;&equals;&nbsp;</span>
              </span>
            </template>
            <strong class="font-semibold">{{ totalDamage }}</strong>
          </p>
        </div>
        <p class="flex flex-wrap gap-x-1" v-if="totalDamage && validDamageSources.length && damageToOvershield">
          <img class="h-6 invert" src="@/assets/overshield.svg" />
          <span>{{ character.overshieldPoints }} overshield points</span>
          <span>&nbsp;&minus;&nbsp;</span>
          <span>{{ damageToOvershield }}</span>
          <span>&nbsp;&equals;&nbsp;</span>
          <strong class="font-semibold">{{ newOvershield }}</strong>
          <i v-if="!newOvershield" class="text-sm text-zinc-400 leading-6">(Overshield depleted)</i>
        </p>
        <p class="flex flex-wrap gap-x-1" v-if="totalDamage && validDamageSources.length && damageToShield">
          <img class="h-6 invert" src="@/assets/shield.svg" />
          <span>{{ character.shieldPoints }} shield points</span>
          <span>&nbsp;&minus;&nbsp;</span>
          <span>{{ damageToShield }}</span>
          <span>&nbsp;&equals;&nbsp;</span>
          <strong class="font-semibold">{{ newShield }}</strong>
          <i v-if="!newShield" class="text-sm text-zinc-400 leading-6">(Shield popped)</i>
        </p>
        <p class="flex flex-wrap gap-x-1" v-if="totalDamage && validDamageSources.length && damageToHealth">
          <img class="h-6 invert" src="@/assets/health.svg" />
          <span>{{ character.hitPoints }} health points</span>
          <span>&nbsp;&minus;&nbsp;</span>
          <span>{{ damageToHealth }}</span>
          <span>&nbsp;&equals;&nbsp;</span>
          <strong class="font-semibold">{{ newHealth }}</strong>
          <i v-if="!newHealth" class="text-sm text-zinc-400 leading-6">(Guardian down)</i>
        </p>
        <p class="flex flex-wrap gap-x-1" v-if="totalDamage > damageToOvershield + damageToShield + damageToHealth">
          <span>Left-over: </span>
          <span class="font-semibold">{{ totalDamage - (damageToOvershield + damageToShield + damageToHealth) }}</span>
          <i v-if="totalDamage >= character.hitPoints * 4" class="text-sm text-zinc-400 leading-6">(Disintegrated!)</i>
          <i v-else class="text-sm text-zinc-400 leading-6">(That's a lot of damage!)</i>
        </p>
        <form method="dialog" class="flex justify-end gap-2 mt-2 w-full">
          <DialogButton color="success" @click="applyDamage">APPLY</DialogButton>
          <DialogButton>CLOSE</DialogButton>
        </form>
      </template>
      <template #healing>
        <ListingHeader :headers="{
          'HEALING': 'w-1/3',
          'TYPE': 'w-2/3'
        }" />
        <div class="flex h-6 mt-2 mb-2 items-center">
          <input min="0" type="number" v-model="healing" class="text-right mt-1 mb-1 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2 border-r-0" />
          <select v-model="healingType" class="bg-zinc-800 w-2/3 text-sm px-2 cursor-pointer h-8 border-zinc-500 border-2">
            <option value="shield">Shield points</option>
            <option value="health">Health points</option>
            <option value="all">Hit points (health &rightarrow; shield)</option>
          </select>
        </div>
        <p class="flex flex-wrap gap-x-1" v-if="healingToHealth">
          <img class="h-6 invert" src="@/assets/health.svg" />
          <span>{{ character.hitPoints }} health points</span>
          <span>&nbsp;&plus;&nbsp;</span>
          <span>{{ healingToHealth }}</span>
          <span>&nbsp;&equals;&nbsp;</span>
          <strong class="font-semibold">{{ Math.min(character.hitPoints + healingToHealth, maxHitPoints) }}</strong>
          <i v-if="character.hitPoints + healingToHealth >= maxHitPoints" class="text-sm text-zinc-400 leading-6">(Fully healed)</i>
        </p>
        <p class="flex flex-wrap gap-x-1" v-if="healingToAll || healingToShield">
          <img class="h-6 invert" src="@/assets/shield.svg" />
          <span>{{ character.shieldPoints }} shield points</span>
          <span>&nbsp;&plus;&nbsp;</span>
          <span>{{ healingToAll || healingToShield }}</span>
          <span>&nbsp;&equals;&nbsp;</span>
          <strong class="font-semibold">{{ Math.min(character.shieldPoints + (healingToAll || healingToShield), maxShieldPoints) }}</strong>
          <i v-if="character.shieldPoints + (healingToAll || healingToShield) >= maxShieldPoints" class="text-sm text-zinc-400 leading-6">(Fully recharged)</i>
        </p>
        <form method="dialog" class="flex justify-end gap-2 mt-3 w-full flex-wrap">
          <DialogButton @click="healFully">HEAL</DialogButton>
          <DialogButton @click.prevent="formula?.show">RECHARGE SHIELD</DialogButton>
          <DialogButton @click="applyOvershield">GAIN OVERSHIELD ({{ maxOvershieldPoints }}/{{ maxOvershieldPoints }})</DialogButton>
          <DialogButton color="success" @click="applyHealing">APPLY</DialogButton>
          <DialogButton>CLOSE</DialogButton>
        </form>
      </template>
      <template #manual>
        <ListingHeader :headers="{
          '': 'w-1/3',
          'AMOUNT': 'w-1/3',
          'MAXIMUM': 'w-1/3'
        }" />
        <div class="flex h-6 mt-2 mb-1 gap-2 items-center">
          <small class="text-xs text-zinc-400 w-1/3 text-right">HEALTH</small>
          <input min="0" :max="maxHitPoints" type="number" @input="update" v-model="character.hitPoints" class="text-right mt-1 mb-2 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2" />
          <input min="0" type="number" @input="update" :placeholder="maxHitPoints.toString()" v-model="character.maxHitPoints" class="text-right mt-1 mb-2 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2" />
        </div>
        <div class="flex h-6 mt-3 mb-1 gap-2 items-center">
          <small class="text-xs text-zinc-400 w-1/3 text-right">SHIELD</small>
          <input min="0" :max="maxShieldPoints" type="number" @input="update" v-model="character.shieldPoints" class="text-right mt-1 mb-2 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2" />
          <input min="0" type="number" @input="update" :placeholder="maxShieldPoints.toString()" v-model="character.maxShieldPoints" class="text-right mt-1 mb-2 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2" />
        </div>
        <div class="flex h-6 mt-3 mb-1 gap-2 items-center">
          <small class="text-xs text-zinc-400 w-1/3 text-right">OVERSHIELD</small>
          <input min="0" :max="maxOvershieldPoints" type="number" @input="update" v-model="character.overshieldPoints" class="text-right mt-1 mb-2 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2" />
          <input min="0" type="number" @input="update" :placeholder="maxOvershieldPoints.toString()" v-model="character.maxOvershieldPoints" class="text-right mt-1 mb-2 px-2 bg-zinc-800 w-1/3 cursor-pointer h-8 border-zinc-500 border-2" />
        </div>
        <form method="dialog" class="flex justify-end gap-2 mt-3 w-full">
          <DialogButton>CLOSE</DialogButton>
        </form>
      </template>
    </DialogTabs>
    <Teleport to="body">
      <DialogWindow key="guardian-health-dice" class="max-w-[30rem]" v-if="!readonly" ref="formula">
        <DamageFormula :character="character" :subclass="subclass" :item="shieldDiceFeature!" show-all-formula simple-roll @roll="onRoll" />
        <form method="dialog" class="flex justify-end gap-2 mt-2 w-full">
          <DialogButton>CLOSE</DialogButton>
        </form>
      </DialogWindow>
    </Teleport>
  </div>
</template>