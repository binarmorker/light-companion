<script setup lang="ts">
import { computed, inject, ref, onBeforeUnmount, ComputedRef } from 'vue'
import { Ability, Formula, Guardian, Ghost, Subclass, Item, DamageType } from '@/models/destiny'
import { CharacterInfo, parseFormula } from '@/utils/formulaParser'
import { directive, directiveHtml, Directive } from 'micromark-extension-directive'
import { CompileContext } from 'micromark-util-types'
import { getAbilityModifier, getProficiencyBonus, getDamageTypeName, getAbilityName } from '@/utils'
import { micromark } from 'micromark'
import { rollDice } from '@/utils/diceRoll'
import DialogButton from '@/components/DialogButton.vue'

const sendDice = inject('ChatSendDice', undefined) as ComputedRef<(context: string|null, data: { formula: string, value: number, type?: DamageType }[], sender?: string) => Promise<void> | null>|undefined

const props = defineProps<{
  character?: Guardian|Ghost,
  savingCharacter?: Guardian,
  subclass?: Subclass|null,
  item: Formula,
  showAllFormula?: boolean,
  showSaveFormula?: boolean,
  attackWarning?: string,
  noBonus?: boolean,
  simpleRoll?: boolean
}>()
const emit = defineEmits(['beforeRoll', 'roll'])
const rolling = ref(false)
const proficiencyBonus = computed(() => props.character ? getProficiencyBonus(props.character) : 0)
const itemProficiency = computed(() => (props.item as Item).proficient !== false)
const itemAbility = computed(() => (props.item as Item).ability)
const characterLight = computed<CharacterInfo>(() => ({
  level: props.character?.level ?? 0,
  light: proficiencyBonus.value - 1,
  lightMod: props.character && props.subclass ? getAbilityModifier(props.character, props.subclass!.lightModifier) : 0,
  prof: itemProficiency.value ? proficiencyBonus.value : 0,
  mod: props.character && itemAbility.value !== undefined ? getAbilityModifier(props.character, itemAbility.value) : 0,
  str: props.character?.abilities[Ability.Strength] ?? 0,
  dex: props.character?.abilities[Ability.Dexterity] ?? 0,
  con: props.character?.abilities[Ability.Constitution] ?? 0,
  int: props.character?.abilities[Ability.Intelligence] ?? 0,
  wis: props.character?.abilities[Ability.Wisdom] ?? 0,
  cha: props.character?.abilities[Ability.Charisma] ?? 0,
  strMod: props.character ? getAbilityModifier(props.character, Ability.Strength) : 0,
  dexMod: props.character ? getAbilityModifier(props.character, Ability.Dexterity) : 0,
  conMod: props.character ? getAbilityModifier(props.character, Ability.Constitution) : 0,
  intMod: props.character ? getAbilityModifier(props.character, Ability.Intelligence) : 0,
  wisMod: props.character ? getAbilityModifier(props.character, Ability.Wisdom) : 0,
  chaMod: props.character ? getAbilityModifier(props.character, Ability.Charisma) : 0
}))
const savingThrowDCs = computed(() => props.item?.savingThrowFormula?.map(x => +parseFormula(characterLight.value, x.dc, true, true)))
const attackBonus = ref('')
const damageBonus = ref('')

function descRef(this: CompileContext, directive: Directive) {
  if (directive.type !== 'textDirective') return false

  this.tag('<abbr')

  if (directive.attributes && 'title' in directive.attributes) {
    switch (directive.attributes.title) {
      case 'level': this.tag(` title="Your Character level"`); break
      case 'light': this.tag(` title="Your Light level"`); break
      case 'lightMod': this.tag(props.subclass ? ` title="Your ${getAbilityName(props.subclass!.lightModifier!)} modifier"` : 'title="Unknown modifier: [lightMod]"'); break
      case 'prof': this.tag(` title="Your Proficiency bonus"`); break
      case 'mod': this.tag(` title="Your ${getAbilityName(itemAbility.value!)} modifier"`); break
      case 'str': this.tag(` title="Your Strength score"`); break
      case 'dex': this.tag(` title="Your Dexterity score"`); break
      case 'con': this.tag(` title="Your Constitution score"`); break
      case 'int': this.tag(` title="Your Intelligence score"`); break
      case 'wis': this.tag(` title="Your Wisdom score"`); break
      case 'cha': this.tag(` title="Your Charisma score"`); break
      case 'strMod': this.tag(` title="Your Strength modifier"`); break
      case 'dexMod': this.tag(` title="Your Dexterity modifier"`); break
      case 'conMod': this.tag(` title="Your Constitution modifier"`); break
      case 'intMod': this.tag(` title="Your Intelligence modifier"`); break
      case 'wisMod': this.tag(` title="Your Wisdom modifier"`); break
      case 'chaMod': this.tag(` title="Your Charisma modifier"`); break
      default: this.tag(' title="Unknown modifier: ' + this.encode(directive.attributes.title) + '"'); break
    }
  }

  this.tag('>')
  this.raw(directive.label ||'')
  this.tag('</abbr>')
}

function applyMarkdown(markdown: string) {
  return micromark(markdown, {
    extensions: [directive()],
    htmlExtensions: [directiveHtml({ descRef })]
  })
}

async function roll(data: { formula: string, type?: DamageType }[], params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
  rolling.value = true
  emit('beforeRoll')

  if (!sendDice?.value) return

  const result = await rollDice(data, params)
  let name = 'name' in props.item ? props.item.name as string : null
  if (name && params?.isCritical) name += ' (Critical hit)'
  if (name && params?.isAdvantage) name += ' (Advantage)'
  if (name && params?.isDisadvantage) name += ' (Disadvantage)'
  if (!name && params?.isCritical) name = 'Critical hit'
  if (!name && params?.isAdvantage) name = 'Advantage'
  if (!name && params?.isDisadvantage) name = 'Disadvantage'

  await sendDice.value(name, result, (props.character && !('id' in props.character)) ? 'ghost' : 'character')
  attackBonus.value = ''
  damageBonus.value = ''
  emit('roll', /* isAttack */ data.every(d => d.type === undefined), result)
  rolling.value = false
}

async function rollSave(index: number, data: { formula: string, type?: DamageType }[], params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
  rolling.value = true
  emit('beforeRoll')

  if (!sendDice?.value) return

  const result = await rollDice(data, params)
  
  let name = ('name' in props.item ? `Saving for ${props.item.name}` : `Saving throw`)
  if (name && params?.isAdvantage) name += ' (Advantage)'
  if (name && params?.isDisadvantage) name += ' (Disadvantage)'

  if (result[0].value >= (savingThrowDCs.value?.[index] ?? 0)) {
    name += ' - SUCCESS'
  } else {
    name += ' - FAILURE'
  }

  await sendDice.value(name, result, (props.character && !('id' in props.character)) ? 'ghost' : 'character')
  attackBonus.value = ''
  damageBonus.value = ''
  emit('roll', /* isAttack */ data.every(d => d.type === undefined), result)
  rolling.value = false
}

onBeforeUnmount(() => {
  attackBonus.value = ''
  damageBonus.value = ''
})
</script>

<template>
  <template v-if="showAllFormula && !rolling">
    <div class="flex flex-col" v-if="item?.attackFormula">
      <div class="grid grid-cols-3 items-baseline">
        <small class="text-right pr-2 text-zinc-400 text-xs">FORMULA</small>
        <span class="formula col-span-2" v-html="applyMarkdown(parseFormula(characterLight, item.attackFormula))" />
      </div>
      <div class="grid grid-cols-3 items-baseline" v-if="!noBonus">
        <small class="text-right pr-2 text-zinc-400 text-xs">BONUS</small>
        <input class="col-span-2 mt-1 mb-2 px-2 bg-zinc-800 cursor-pointer h-8 border-zinc-500 border-2" v-model="attackBonus" />
      </div>
      <p v-if="attackWarning" class="text-center text-yellow-500">{{ attackWarning }}</p>
      <div class="flex gap-1" v-if="!!sendDice">
        <DialogButton
          block
          :color="attackWarning ? 'warning' : 'default'"
          @click="() => roll([{ formula: parseFormula(characterLight, item.attackFormula!, true) }], { bonus: attackBonus })"
        >ROLL</DialogButton>
        <DialogButton
          block
          :color="attackWarning ? 'warning' : 'default'"
          @click="() => roll([{ formula: parseFormula(characterLight, item.attackFormula!, true) }], { bonus: attackBonus, isAdvantage: true })"
        >ADVANTAGE</DialogButton>
        <DialogButton
          block
          :color="attackWarning ? 'warning' : 'default'"
          @click="() => roll([{ formula: parseFormula(characterLight, item.attackFormula!, true) }], { bonus: attackBonus, isDisadvantage: true })"
        >DISADVANTAGE</DialogButton>
      </div>
    </div>
    <hr class="border-t-2 border-zinc-400 my-2" v-if="item?.attackFormula && item?.damage?.length" />
    <div class="flex flex-col" v-if="item?.damage?.length">
      <template v-for="(damage, i) in item.damage" :key="i">
        <div class="grid grid-cols-3 items-baseline">
          <small class="text-right pr-2 text-zinc-400 text-xs">DAMAGE FORMULA</small>
          <span class="col-span-2">
            <span class="formula" v-html="`${applyMarkdown(parseFormula(characterLight, damage.formula))} ${getDamageTypeName(damage.type)}`" />
          </span>
        </div>
        <div class="grid grid-cols-3 items-baseline" v-if="!noBonus">
          <small class="text-right pr-2 text-zinc-400 text-xs">BONUS</small>
          <input class="col-span-2 mt-1 mb-2 px-2 bg-zinc-800 cursor-pointer h-8 border-zinc-500 border-2" v-model="damageBonus" />
        </div>
        <div class="flex gap-1" v-if="!!sendDice">
          <DialogButton
            block
            @click="() => roll([{ formula: parseFormula(characterLight, damage.formula, true), type: damage.type }], { bonus: damageBonus })"
          >ROLL</DialogButton>
          <DialogButton
            block
            @click="() => roll([{ formula: parseFormula(characterLight, damage.formula, true), type: damage.type }], { bonus: damageBonus, isCritical: true })"
          >CRITICAL</DialogButton>
        </div>
      </template>
    </div>
  </template>
  <template v-else-if="showSaveFormula && !rolling">
    <div class="flex flex-col" v-if="item?.savingThrowFormula?.length">
      <template v-for="(save, i) in item.savingThrowFormula" :key="i">
        <div class="grid grid-cols-3 items-baseline">
          <small class="text-right pr-2 text-zinc-400 text-xs">SAVING THROW FORMULA</small>
          <span class="col-span-2">
            <span class="formula" v-html="`DC ${parseFormula(characterLight, save.dc, true, true)} ${getAbilityName(save.ability)} saving throw`" />
          </span>
        </div>
        <div class="grid grid-cols-3 items-baseline" v-if="!noBonus">
          <small class="text-right pr-2 text-zinc-400 text-xs">BONUS</small>
          <input class="col-span-2 mt-1 mb-2 px-2 bg-zinc-800 cursor-pointer h-8 border-zinc-500 border-2" v-model="damageBonus" />
        </div>
        <div class="flex gap-1" v-if="!!sendDice && savingCharacter">
          <DialogButton
            block
            @click="() => rollSave(i, [{ formula: parseFormula(characterLight, `1d20+${getAbilityModifier(savingCharacter!, save.ability)}`, true) }], { bonus: damageBonus })"
          >ROLL</DialogButton>
          <DialogButton
            block
            :color="attackWarning ? 'warning' : 'default'"
            @click="() => rollSave(i, [{ formula: parseFormula(characterLight, `1d20+${getAbilityModifier(savingCharacter!, save.ability)}`, true) }], { bonus: damageBonus, isAdvantage: true })"
          >ADVANTAGE</DialogButton>
          <DialogButton
            block
            :color="attackWarning ? 'warning' : 'default'"
            @click="() => rollSave(i, [{ formula: parseFormula(characterLight, `1d20+${getAbilityModifier(savingCharacter!, save.ability)}`, true) }], { bonus: damageBonus, isDisadvantage: true })"
          >DISADVANTAGE</DialogButton>
        </div>
      </template>
    </div>
  </template>
  <template v-else-if="!rolling">
    <template v-for="(damage, i) in item?.damage" :key="i">
      <span class="formula" v-html="`${applyMarkdown(parseFormula(characterLight, damage.formula))} ${getDamageTypeName(damage.type)}`" />
      <template v-if="i < (item.damage?.length ?? 0) - 1"> / </template>
    </template>
  </template>
  <template v-else>
    <!-- <span>Rolling...</span> -->
  </template>
</template>
