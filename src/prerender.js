import express from 'express'
import fs from 'node:fs/promises'
import puppeteer from 'puppeteer'
import dotenv from 'dotenv'
dotenv.config()

const prerender = async () => {
  // Create a simple Express server that prints "Hello, World"
  const app = express()
  app.use(express.static('./dist'))
  const server = app.listen() // Take a random available port
  
  // Launch Puppeteer and navigate to the Express server
  const browser = await puppeteer.launch({ slowMo: 500, headless: "new" })

  const indexPage = await browser.newPage()
  await indexPage.goto(`http://localhost:${server.address().port}`)
  const indexContent = await indexPage.content()
  await fs.writeFile('./dist/index.html', indexContent)
  
  // Cleanup 
  await browser.close()
  server.close()
}

prerender()
