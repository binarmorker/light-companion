import { Random, browserCrypto } from 'random-js'
import { DamageType, DiceRoll } from '@/models/destiny'

export class SimpleDiceRoller {
  replaceAdvDis: (result: string, isAdvantage: boolean) => string

  constructor(replaceAdvDisFunc: (result: string, isAdvantage: boolean) => string) {
    this.replaceAdvDis = replaceAdvDisFunc
  }

  async rollDiceFormula(random: Random, formula: string, type?: DamageType, params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
    let formulaResult = formula

    for (const [match] of formula.matchAll(/(\d+d\d+(?:d\d+)?)/g)) {
      // eslint-disable-next-line prefer-const
      let [amount, dice, drop] = match.split('d')
      if (params?.isCritical) amount = `${Number(amount) * 2}`

      if (params?.isAdvantage || params?.isDisadvantage) {
        const adjustedAmount = amount === '1' ? 2 : Number(amount)
        const amountResult = random.dice(Number(dice), adjustedAmount)
        formulaResult = formulaResult.replace(match, params.isAdvantage
          ? `max(${amountResult.map(x => `(${x})`).join(', ')})`
          : `min(${amountResult.map(x => `(${x})`).join(', ')})`)
      } else {
        const amountResult = random.dice(Number(dice), Number(amount))
        const droppedResult = drop ? amountResult.sort((a, b) => b - a).slice(0, -Number(drop)) : amountResult
        formulaResult = formulaResult.replace(match, droppedResult.map(x => `(${x})`).join(' + '))
      }
    }

    if (type === undefined) return { formula, evaluated: formulaResult, value: eval(this.replaceAdvDis(formulaResult, !!params?.isAdvantage)) as number }
    return { formula, evaluated: formulaResult, value: eval(this.replaceAdvDis(formulaResult, !!params?.isAdvantage)) as number, type }
  }

  async rollDice(data: { formula: string, type?: DamageType }[], params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
    const result: Promise<DiceRoll>[] = []

    let cryptoSupported = true
    try { browserCrypto.next() } catch (_) { cryptoSupported = false }
    const random = cryptoSupported ? new Random(browserCrypto) : new Random

    // eslint-disable-next-line prefer-const
    for (let { formula, type } of data) {
      formula += ((data.length === 1 && params?.bonus) ? ` + ${params.bonus}` : '')
      const diceResult = this.rollDiceFormula(random, formula, type, params)
      result.push(diceResult)
    }

    if (data.length > 1 && params?.bonus) {
      const diceResult = this.rollDiceFormula(random, params.bonus, undefined, params)
      result.push(diceResult)
    }

    return Promise.all(result)
  }
}
