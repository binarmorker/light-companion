import { Random, browserCrypto } from 'random-js'
import { DamageType, DiceRoll } from '@/models/destiny'
import DiceBox from '@3d-dice/dice-box'

export class ThreeDiceRoller {
  replaceAdvDis: (result: string, isAdvantage: boolean) => string
  Dice: DiceBox
  isInitialized: Promise<DiceBox>
  timeout?: NodeJS.Timeout

  constructor(replaceAdvDisFunc: (result: string, isAdvantage: boolean) => string) {
    this.replaceAdvDis = replaceAdvDisFunc
    this.Dice = new DiceBox({
      container: '#dicebox',
      assetPath: '/assets/',
      scale: 6,
      startingHeight: 50,
      mass: 3,
      gravity: 1.5
    })
    this.isInitialized = this.Dice.init()
    document.addEventListener('mousedown', () => {
      this.resetDice()
    })
  }

  transpose<T>(matrix: T[][]) {
    const rows = matrix.length
    const cols = matrix[0].length
    const grid: T[][] = []

    for (let j = 0; j < cols; j++) {
      grid[j] = Array(rows)
    }

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        grid[j][i] = matrix[i][j]
      }
    }

    return grid
  }

  async rollDiceFormula(random: Random, formula: string, type?: DamageType, params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
    let formulaResult = formula
    const formulaPromises: Promise<void>[] = []

    for (const [match] of formula.matchAll(/(\d+d\d+(?:d\d+)?)/g)) {
      // eslint-disable-next-line prefer-const
      let [amount, dice, drop] = match.split('d')
      if (params?.isCritical) amount = `${Number(amount) * 2}`
      
      if ([4, 6, 8, 10, 12, 20].includes(Number(dice))) {
        if (params?.isAdvantage || params?.isDisadvantage) {
          const adjustedAmount = amount === '1' ? 2 : Number(amount)
          formulaPromises.push(this.Dice.add(`${adjustedAmount}d${dice}`).then(amountResult => {
            formulaResult = params.isAdvantage
              ? formulaResult.replace(match, `max(${amountResult.map(x => `(${x.value})`).join(', ')})`)
              : formulaResult.replace(match, `min(${amountResult.map(x => `(${x.value})`).join(', ')})`)
          }))
        } else {
          formulaPromises.push(this.Dice.add(`${amount}d${dice}`).then(amountResult => {
            const droppedResult = drop ? amountResult.sort((a, b) => b.value - a.value).slice(0, -Number(drop)) : amountResult
            formulaResult = formulaResult.replace(match, droppedResult.map(x => `(${x.value})`).join(' + '))
          }))
        }
      } else if (dice === '100') {
        if (params?.isAdvantage || params?.isDisadvantage) {
          const adjustedAmount = amount === '1' ? 2 : Number(amount)
          formulaPromises.push(Promise.all([this.Dice.add(`${adjustedAmount}d100`), this.Dice.add(`${adjustedAmount}d10`)]).then(amountResult => {
            const transposedResult = this.transpose(amountResult)
            formulaResult = params.isAdvantage
              ? formulaResult.replace(match, `max(${transposedResult.map(([x, y]) => `(${x.value + y.value})`).join(', ')})`)
              : formulaResult.replace(match, `min(${transposedResult.map(([x, y]) => `(${x.value + y.value})`).join(', ')})`)
          }))
        } else {
          formulaPromises.push(Promise.all([this.Dice.add(`${amount}d100`), this.Dice.add(`${amount}d10`)]).then(amountResult => {
            const transposedResult = this.transpose(amountResult)
            formulaResult = formulaResult.replace(match, transposedResult.map(([x, y]) => `(${x.value + y.value})`).join(' + '))
          }))
        }
      } else {
        if (params?.isAdvantage || params?.isDisadvantage) {
          const adjustedAmount = amount === '1' ? 2 : Number(amount)
          const amountResult = random.dice(Number(dice), adjustedAmount)
          formulaPromises.push(Promise.resolve().then(() => {
            formulaResult = params.isAdvantage
              ? formulaResult.replace(match, `max(${amountResult.map(x => `(${x})`).join(', ')})`)
              : formulaResult.replace(match, `min(${amountResult.map(x => `(${x})`).join(', ')})`)
          }))
        } else {
          const amountResult = random.dice(Number(dice), Number(amount))
          formulaPromises.push(Promise.resolve().then(() => {
            formulaResult = formulaResult.replace(match, amountResult.map(x => `(${x})`).join(' + '))
          }))
        }
      }
    }

    await Promise.all(formulaPromises)
    return { formula, evaluated: formulaResult, value: eval(this.replaceAdvDis(formulaResult, !!params?.isAdvantage)) as number, type }
  }

  resetDice() {
    const diceBoxCanvas = document.getElementById('app')

    if (diceBoxCanvas && window.getComputedStyle(diceBoxCanvas).display !== 'none') {
      this.Dice?.hide()
    }
  }

  async rollDice(data: { formula: string, type?: DamageType }[], params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
    const result: Promise<DiceRoll>[] = []
  
    let cryptoSupported = true
    try { browserCrypto.next() } catch (_) { cryptoSupported = false }
    const random = cryptoSupported ? new Random(browserCrypto) : new Random

    await this.isInitialized
  
    this.resetDice()
    this.Dice?.show()
  
    // eslint-disable-next-line prefer-const
    for (let { formula, type } of data) {
      formula += ((data.length === 1 && params?.bonus) ? ` + ${params.bonus}` : '')
      const diceResult = this.rollDiceFormula(random, formula, type, params)
      result.push(diceResult)
    }
  
    if (data.length > 1 && params?.bonus) {
      const diceResult = this.rollDiceFormula(random, params.bonus, undefined, params)
      result.push(diceResult)
    }
  
    const promiseResult = await Promise.all(result)
  
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      this.resetDice()
      this.Dice?.clear()
    }, 5000)

    return promiseResult
  }
}
