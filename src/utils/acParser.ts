const modifierRegex = /(?:(\d+)\s*)?(?:([+-])\s*)?(?:(dex|con|wis)\s*mod)(?:\s*\(max\s*(\d+)\))?/gi

export function parseAc(character: { dex: number, con: number, wis: number }, formula: string) {
  if (/^\d+$/.test(formula)) return parseInt(formula)
  
  let baseAC = 0
  let total = 0
  let foundNumber = false

  const initialNumber = formula.match(/^(\d+)/)
  if (initialNumber) {
    baseAC = parseInt(initialNumber[1])
    foundNumber = true
  }

  let match
  let hadModifier = false

  while ((match = modifierRegex.exec(formula)) !== null) {
    const modifierName = match[3].toLowerCase()

    if (modifierName === 'dex' || modifierName === 'con' || modifierName === 'wis') {
      hadModifier = true
      let modifier = character[modifierName]

      if (match[4]) {
        modifier = Math.min(modifier, parseInt(match[4]))
      }

      if (match[2] === '-') {
        modifier = -modifier
      }

      total += modifier
    }
  }

  return hadModifier || foundNumber ? baseAC + total : baseAC
}
