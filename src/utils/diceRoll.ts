import { SimpleDiceRoller } from './simpleDiceRoll'
import { ThreeDiceRoller } from './3dDiceRoll'
import { DamageType } from '@/models/destiny'

let fastEnough: boolean | undefined = localStorage.getItem('fastEnough') ? localStorage.getItem('fastEnough') === 'true' : undefined
let diceRoller: ThreeDiceRoller|SimpleDiceRoller|undefined

export function speedtest() {
  return new Promise<boolean>(resolve => {
    if (fastEnough) return resolve(fastEnough)

    let target = new Date().getTime(), count = 0, slow = 0
    
    const iid = setInterval(function () {
      const actual = new Date().getTime()
      target += 100
      
      if ((actual - target) > 100) {
        if ((++slow) > 5) {
          clearInterval(iid)
          console.log(`This computer is too slow. We counted ${count} times before it slowed down more than half a second.`)
          fastEnough = false
          localStorage.setItem('fastEnough', 'false')
          return resolve(fastEnough)
        }
      } else if (slow > 0){
        slow--
      }
      
      count++
      target = actual

      if (count >= 50) {
        clearInterval(iid)
        console.log(`This computer is fast enough. We counted ${count} times without it slowing down more than half a second.`)
        fastEnough = true
        localStorage.setItem('fastEnough', 'true')
        return resolve(fastEnough)
      }
    }, 100)
  })
}

function replaceAdvDis(result: string, isAdvantage: boolean) {
  const [match, numbers] = result.match(/(?:max|min)\((.+)\)/) ?? []
  if (!numbers) return result
  const values = numbers.split(',').map(x => Number(x.trim().replace(/[()]/g, '')))
  if (!match || !values.length) return result
  const value = isAdvantage ? Math.max(...values) : Math.min(...values)
  return result.replace(match, `${value}`)
}

export async function rollDice(data: { formula: string, type?: DamageType }[], params?: { bonus?: string, isAdvantage?: boolean, isDisadvantage?: boolean, isCritical?: boolean }) {
  // const fastEnough = await speedtest()

  if (!diceRoller) {
    // diceRoller = fastEnough ? new ThreeDiceRoller(replaceAdvDis) : new SimpleDiceRoller(replaceAdvDis)
    diceRoller = new SimpleDiceRoller(replaceAdvDis)
  }

  return diceRoller.rollDice(data, params)
}