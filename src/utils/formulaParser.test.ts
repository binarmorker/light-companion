import { parseFormula } from './formulaParser'

describe('testing formula parser', () => {
  const character = {
    level: 10,
    light: 3,
    lightMod: 6,
    prof: 4,
    mod: 1,
    str: 15,
    dex: 13,
    con: 12,
    int: 8,
    wis: 14,
    cha: 10,
    strMod: 2,
    dexMod: 1,
    conMod: 1,
    intMod: -1,
    wisMod: 2,
    chaMod: 0
  }

  test('basic formula should return itself', () => {
    expect(parseFormula(character, '2d6')).toBe('2d6')
  })

  test('variable formula should return matching descriptor', () => {
    expect(parseFormula(character, '2d4 + [mod]')).toBe('2d4 + :descRef[1]{title="mod"}')
  })

  test('math formula should return calculated form', () => {
    expect(parseFormula(character, '(1 + (([level] + 1) / 6))d8')).toBe('2d8')
  })

  test('complex math formula with nested variable should return calculated form with no descriptor', () => {
    expect(parseFormula(character, '1d((2 + (([level] - 1) / 4)) * 2)')).toBe('1d8')
  })

  test('math formula with nested and unnested variables should return calculated form and one matching descriptor', () => {
    expect(parseFormula(character, '(1 + [light])d10 + [lightMod]')).toBe('4d10 + :descRef[6]{title="lightMod"}')
  })

  test('invalid characters should be ignored', () => {
    expect(parseFormula(character, 'h5e46d7')).toBe('546d7')
  })

  test('invalid variable should result in matching descriptor', () => {
    expect(parseFormula(character, '14d7 - [red]')).toBe('14d7 - :descRef[0]{title="red"}')
  })

  test('invalid nested variable should be equal to zero', () => {
    expect(parseFormula(character, '((((2 * [what]))))d6')).toBe('0d6')
  })

  test('incorrectly closed variable should be ignored', () => {
    expect(parseFormula(character, '3d6 + [test')).toBe('3d6')
  })

  test('multiple variable formula should return matching descriptors', () => {
    expect(parseFormula(character, '2d4 + [mod] + [dexMod]')).toBe('2d4 + :descRef[1]{title="mod"} + :descRef[1]{title="dexMod"}')
  })
})
