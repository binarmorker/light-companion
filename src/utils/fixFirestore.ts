import { cert, initializeApp } from 'firebase-admin/app'
import { getFirestore } from 'firebase-admin/firestore'
import { Guardian, Session, /* Item, Ghost */ } from '../models/destiny'
// import { uuid4, MersenneTwister19937 } from 'random-js'

// const randomEngine = MersenneTwister19937.autoSeed()

const app = initializeApp({
  credential: cert('./src/utils/light-companion-a5286aedfcc2.json')
})
const db = getFirestore(app)

async function processCharacters() {
  const docs = await db.collection('characters').get()
  const promises: Promise<unknown>[] = []
  docs.forEach(character => {
    const data = character.data() as Guardian
    const newCharacter: Guardian = {
      ...data,
      privacy: 'private'
      /* items: data.items.map((item: Item) => ({
        ...item,
        uuid: item.uuid || uuid4(randomEngine)
      })), */
      // weapons: [],
      // armor: [],
      // tools: [],
      // resistances: [],
      // weaknesses: [],
      // immunities: [],
      // conditionImmunities: [],
      // ghost: JSON.parse(JSON.stringify(new Ghost))
    }
    // console.log('characters/' + character.id, newCharacter)
    promises.push(db.doc('characters/' + character.id).update(newCharacter as never))
  })
  
  await Promise.all(promises)
}

async function processSessions() {
  const docs = await db.collection('sessions').get()
  const promises: Promise<unknown>[] = []
  docs.forEach(session => {
    const data = session.data() as Session
    const newSession: Session = {
      ...data,
      privacy: 'private'
    }
    // console.log('sessions/' + session.id, newSession)
    promises.push(db.doc('sessions/' + session.id).update(newSession as never))
  })
  
  await Promise.all(promises)
}

processCharacters()
processSessions()