export type CharacterInfo = {
  level: number,
  light: number,
  lightMod: number,
  prof: number,
  mod: number,
  str: number,
  dex: number,
  con: number,
  int: number,
  wis: number,
  cha: number,
  strMod: number,
  dexMod: number,
  conMod: number,
  intMod: number,
  wisMod: number,
  chaMod: number
}

type TokenType = 'number' | 'string' | 'operator' | 'reference' | 'bracket' | 'whitespace' | 'error'
type NodeType = 'number' | 'string' | 'operator' | 'reference' | 'group'

type Token = {
  value: string,
  type: TokenType
}

type Node = {
  type: NodeType,
  value?: string,
  innerNodes?: Node[]
}

type StreamFunctions = {
  peek: () => string | null,
  match: (pattern: RegExp, consume: boolean) => string | null,
  next: () => string | null,
  isEnd: () => boolean,
  getPreviousToken: () => Token | null
}

export function tokenIsValue(token: Token | null) {
  if (!token) return false
  if (token.type === 'number') return true
  if (token.type === 'bracket' && (token.value === ')' || token.value === ']')) return true
  return false
}

function syntaxTokenizer(stream: StreamFunctions): TokenType {
  const peek = stream.peek()
  const previousToken = stream.getPreviousToken()
  const previousTokenType = previousToken ? previousToken.type : null

  if (stream.match(/^[-]?\d*\.?\d+/, false)) {
    if (peek === '-' && previousTokenType !== 'operator' && tokenIsValue(previousToken)) {
      // if this number is starting with a minus and there is no previous operator, then we need to be treating this as an operator instead
      stream.next()
      return 'operator'
    }

    stream.match(/^[-]?\d*\.?\d+/, true)
    return 'number'
  }

  if (peek && ['*', '-', '+', '/'].indexOf(peek) > -1) {
    stream.next()
    return 'operator'
  }

  if (previousTokenType === 'bracket' && previousToken?.value === '[' && stream.match(/^[^[\]]+(?=\])/, true)) {
    return 'reference'
  }

  if (peek && [')', ']', '(', '['].indexOf(peek) > -1) {
    stream.next()
    return 'bracket'
  }

  if (stream.match(/^d/, true)) return 'string'

  if (stream.match(/^ +/, true)) return 'whitespace'

  stream.next()
  return 'error'
}

function lexer(formula: string, syntaxTokenizer: (stream: StreamFunctions) => TokenType) {
  let position = 0
  const tokens: Token[] = []

  const isEnd = () => position >= formula.length

  const peek = () => {
    if (isEnd()) return null
    return formula.substring(position, position + 1)
  }

  const match = (pattern: RegExp, consume: boolean) => {
    const restOfFormula = formula.substring(position)
    const match = restOfFormula.match(pattern)
    if (!match || match.length > 1) return null
    if (consume) position += match[0].length
    return match[0] ||null
  }

  const next = () => {
    position++
    return peek()
  }

  const getPreviousToken = () => {
    for (let i = tokens.length - 1; i >= 0; i--) {
      if (tokens[i].type !== 'whitespace') return tokens[i]
    }
    return null
  }

  while (!isEnd()) {
    const startingPosition = position
    const tokenType = syntaxTokenizer({ peek, match, next, isEnd, getPreviousToken })
    if (startingPosition === position) throw new Error('Tokenizer did not move forward')
    const token = {
      value: formula.substring(startingPosition, position),
      type: tokenType
    }
    tokens.push(token)
  }

  return tokens
}

const operators = {
  '+': (parameters: string[]) => Number(parameters[0]) + Number(parameters[1]),
  '-': (parameters: string[]) => Number(parameters[0]) - Number(parameters[1]),
  '/': (parameters: string[]) => Math.floor(Number(parameters[0]) / Number(parameters[1])),
  '*': (parameters: string[]) => Number(parameters[0]) * Number(parameters[1])
}

function executeOperator(operator: keyof typeof operators, parameters: string[]) {
  const method = operators[operator]
  if (method) return `${method(parameters)}`
  return ''
}

function addNode(nodes: Node[], node: Node) {
  const lastNode = nodes[nodes.length - 1]

  if (lastNode?.type === 'operator' && lastNode.innerNodes && lastNode.innerNodes.length < 2) {
    lastNode.innerNodes.push(node)
  } else {
    nodes.push(node)
  }
}

function getInnerTokens(tokens: Token[]) {
  const innerTokens: Token[] = []
  const innerBrackets: string[] = []
  let i: number

  for (i = 0; i < tokens.length; i++) {
    if (tokens[i].type === 'bracket') {
      if (tokens[i].value === ')' && innerBrackets[innerBrackets.length - 1] === '(') {
        innerBrackets.pop()

        if (!innerBrackets.length) {
          i++
          break
        }
      }

      if (tokens[i].value === '(') {
        innerBrackets.push(tokens[i].value)

        if (i === 0) {
          continue
        }
      }
    }

    innerTokens.push(tokens[i])
  }

  return { tokens: innerTokens, i: i - 1 }
}

function createNodesFromTokens(tokens: Token[]) {
  const nodes: Node[] = []
  const nonWhitespaceTokens = tokens.filter(token => token.type !== 'whitespace')

  for (let i = 0; i < nonWhitespaceTokens.length; i++) {
    const token = nonWhitespaceTokens[i]

    if (['number', 'string'].indexOf(token.type) > -1) {
      addNode(nodes, {
        type: token.type as 'number' | 'string',
        value: token.value
      })
    }

    if (token.type === 'operator') {
      // const lastNode = nodes[nodes.length - 1]
      const lastNode = nodes.pop()

      if (lastNode) nodes.push({
        type: 'operator',
        value: token.value,
        innerNodes: [lastNode]
      })
    }

    if (token.type === 'reference') {
      addNode(nodes, {
        type: 'reference',
        value: token.value,
      })
    }

    if (token.type === 'bracket' && token.value === '(') {
      const innerTokenResult = getInnerTokens(nonWhitespaceTokens.slice(i))
      i += innerTokenResult.i
      const innerTokens = innerTokenResult.tokens

      addNode(nodes, {
        type: 'group',
        innerNodes: innerTokens ? createNodesFromTokens(innerTokens) : undefined,
      })
    }
  }

  return nodes
}

function evaluateNode(node: Node, input: CharacterInfo, evaluateAll: boolean, collapseOperators: boolean, insideGroup?: boolean): string {
  if (!node) return ''

  if (node.type === 'operator') {
    const parameters = node.innerNodes?.map(x => evaluateNode(x, input, evaluateAll, collapseOperators, insideGroup)) || []
    return insideGroup || collapseOperators ? executeOperator(node.value as keyof typeof operators, parameters) : parameters.join(` ${node.value} `)
  } else if (node.type === 'group') {
    return node.innerNodes?.map(x => evaluateNode(x, input, evaluateAll, collapseOperators, true)).join() || ''
  } else if (node.type === 'reference') {
    return insideGroup || evaluateAll ? `${input[node.value as keyof CharacterInfo] || 0}` : `:descRef[${input[node.value as keyof CharacterInfo] || 0}]{title="${node.value}"}`
  } else if (node.type === 'number' || node.type === 'string') {
    return node.value || ''
  }

  return ''
}

function evaluateTokens(tokens: Token[], input: CharacterInfo, evaluateAll: boolean, collapseOperators: boolean) {
  // console.log('Tokens: ', tokens)
  const nodes = createNodesFromTokens(tokens)
  // console.log('Nodes: ', nodes)
  let result = ''

  for (const node of nodes) {
    result += evaluateNode(node, input, evaluateAll, collapseOperators)
    // console.log('Result: ', result)
  }

  return result
}

export function parseFormula(character: CharacterInfo, formula: string, evaluateAll?: boolean, collapseOperators?: boolean) {
  const tokens = lexer(formula, syntaxTokenizer)
  return evaluateTokens(tokens, character, !!evaluateAll, !!collapseOperators)
}
