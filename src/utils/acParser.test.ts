import { parseAc } from './acParser'

describe('testing ac parser', () => {
  const character = {
    dex: 3,
    con: 2,
    wis: 5
  }

  test('single number should return itself', () => {
    expect(parseAc(character, '15')).toBe(15)
  })

  test('only modifier should return itself', () => {
    expect(parseAc(character, 'Dex mod')).toBe(3)
  })

  test('one modifier should be added to value', () => {
    expect(parseAc(character, '10 + Dex mod')).toBe(13)
  })

  test('two modifiers should be added to value', () => {
    expect(parseAc(character, '10 + Con mod + Dex mod')).toBe(15)
  })

  test('lowercase modifier should be added to value', () => {
    expect(parseAc(character, '12 + wis mod')).toBe(17)
  })

  test('subtracted modifier should be removed from value', () => {
    expect(parseAc(character, '12 - Wis mod')).toBe(7)
  })

  test('partially matched modifier should not be added to value', () => {
    expect(parseAc(character, '12 + Wis')).toBe(12)
  })

  test('modifier with max not reached should be itself', () => {
    expect(parseAc(character, '12 + Dex mod (max 4)')).toBe(15)
  })

  test('modifier with max reached should have be the maximum', () => {
    expect(parseAc(character, '12 + Wis mod (max 4)')).toBe(16)
  })

  test('unmatched modifier should not be added to value', () => {
    expect(parseAc(character, '10 + Red')).toBe(10)
  })
})
