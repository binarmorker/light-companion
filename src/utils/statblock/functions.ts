import { AbilityScores, Action, HitPoints, LairAction, LegendaryAction, SpellcastingInfo, SpellLevelGroup, StatBlock, UltraAction, UltraHitPoints } from './types'

export const extractModifier = (str: string): number => {
  const match = str.match(/\(([+-]\d+)\)/)
  return match ? parseInt(match[1]) : 0
}

export const parseAbilityScores = (line: string): AbilityScores => {
  const scores = line.split('|').filter(s => s.trim()).map(s => s.trim())
  const result: Partial<AbilityScores> = {}
  ;['STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA'].forEach((ability, index) => {
    const [score, modifier] = scores[index].split(' ')
    result[ability.toLowerCase() as keyof AbilityScores] = {
      score: parseInt(score),
      modifier: extractModifier(modifier)
    }
  })
  return result as AbilityScores
}

export const parsePoints = (text: string): { average: number, formula: string } => {
  const match = text.match(/(\d+) \(([^)]+)\)/)
  if (!match) return { average: 0, formula: '' }
  return {
    average: parseInt(match[1]),
    formula: match[2]
  }
}

export const parseUltraHitPoints = (lines: string[]): UltraHitPoints => {
  const pools: HitPoints[] = []
  const poolLine = lines.find(line => line.includes('|') && !line.includes(':') && 
                                    line.match(/\d+/g))?.trim()
  
  if (poolLine) {
    const poolsData = poolLine.split('|')
      .filter(cell => cell.trim() && !cell.includes(':'))
      .map(cell => cell.trim())

    poolsData.forEach(pool => {
      const match = pool.match(/(\d+) \(([^)]+)\)/)
      if (match) {
        pools.push({
          average: parseInt(match[1]),
          formula: match[2]
        })
      }
    })
  }
  
  return { pools }
}

export const parseSpeed = (text: string): Record<string, number | string> => {
  const speeds: Record<string, number | string> = {}
  const parts = text.split(',').map(s => s.trim())
  
  parts.forEach(part => {
    if (part.includes('(')) {
      // Handle special cases like "35 ft. (can hover)"
      const [speedPart, special] = part.split('(').map(s => s.trim())
      const [value] = speedPart.split(' ')
      speeds['walk'] = parseInt(value)
      speeds['special'] = special.replace(')', '')
    } else {
      const speed = part.split(' ')
      const type = speed.length > 2 ? speed[0] : 'walk'
      const value = speed.length > 2 ? speed[1] : speed[0]
      speeds[type] = parseInt(value)
    }
  })
  return speeds
}

export const parseSavingThrows = (text: string): Record<string, number> => {
  const throws: Record<string, number> = {}
  text.split(',').forEach(save => {
    const [ability, modifier] = save.trim().split(' ')
    throws[ability] = parseInt(modifier)
  })
  return throws
}

export const parseCR = (text: string): StatBlock['challengeRating'] => {
  const individual = text.match(/Individual CR (\d+) \(([0-9,]+) XP\)/)
  const classification = text.match(/Classification CR (\d+) ([A-Za-z]+) \(([0-9,]+) XP\)/)
  
  return {
    individual: {
      rating: individual ? parseInt(individual[1]) : 0,
      xp: individual ? parseInt(individual[2].replace(',', '')) : 0
    },
    classification: classification ? {
      rating: parseInt(classification[1]),
      type: classification[2],
      xp: parseInt(classification[3].replace(',', ''))
    } : undefined
  }
}

export const parseUltraActions = (lines: string[]): UltraAction[] => {
  const ultraActions: UltraAction[] = []
  let currentAction: Partial<UltraAction> | null = null
  let readingEffects = false

  lines.forEach(line => {
    if (line.startsWith('***')) {
      if (currentAction) {
        ultraActions.push(currentAction as UltraAction)
      }
      currentAction = {
        name: line.match(/\*\*\*(.*?)\.\*\*\*/)?.[1] || '',
        description: line,
        effects: []
      }
    } else if (line.startsWith('-') && currentAction) {
      readingEffects = true
      currentAction.effects = currentAction.effects || []
      currentAction.effects.push(line.substring(1).trim())
    } else if (currentAction && line.trim()) {
      if (readingEffects) {
        if (currentAction.effects) {
          currentAction.effects.push(line.trim())
        }
      } else {
        currentAction.description += ' ' + line.trim()
      }
    }
  })

  if (currentAction) {
    ultraActions.push(currentAction as UltraAction)
  }

  return ultraActions
}

export const parseLairActions = (lines: string[]): LairAction[] => {
  const lairActions: LairAction[] = []
  let currentAction: Partial<LairAction> | null = null

  lines.forEach(line => {
    if (line.startsWith('On initiative count')) {
      if (currentAction) {
        lairActions.push(currentAction as LairAction)
      }
      const initiativeMatch = line.match(/On initiative count (\d+)/)
      currentAction = {
        description: line,
        initiative: initiativeMatch ? parseInt(initiativeMatch[1]) : undefined
      }
    } else if (line.startsWith('***') && currentAction) {
      currentAction.name = line.match(/\*\*\*(.*?)\.\*\*\*/)?.[1] || ''
    } else if (currentAction && line.trim()) {
      currentAction.description += ' ' + line.trim()
    }
  })

  if (currentAction) {
    lairActions.push(currentAction as LairAction)
  }

  return lairActions
}

export const parseAction = (text: string): Action => {
  const name = text.match(/\*\*\*(.*?)\.\*\*\*/)?.[1] || ''
  const attack = text.match(/\_([^_]+)\_/)?.[1]
  const toHit = text.match(/\+(\d+) to hit/)?.[1]
  const damage = text.match(/\_Hit:\_ (\d+) \((.*?)\) (\w+) damage/)
  const range = text.match(/scope ([^,]+)/)?.[1]
  const rechargeMatch = text.match(/recharge d(\d+) \[(\d+)\]/)
  
  return {
    name,
    type: attack,
    toHit: toHit ? parseInt(toHit) : undefined,
    damage: damage ? {
      average: parseInt(damage[1]),
      formula: damage[2],
      type: damage[3]
    } : undefined,
    range,
    recharge: rechargeMatch ? {
      dice: parseInt(rechargeMatch[1]),
      threshold: parseInt(rechargeMatch[2])
    } : undefined,
    description: text
  }
}

export const parseSpellcasting = (lines: string[]): SpellcastingInfo => {
  const spellcasting: SpellcastingInfo = {
    spellGroups: []
  }
  
  let currentGroup: SpellLevelGroup | null = null
  
  lines.forEach(line => {
    if (line.includes('level spellcaster')) {
      const levelMatch = line.match(/(\d+)\w+-level spellcaster/)
      const abilityMatch = line.match(/spellcasting ability is (\w+)/)
      const dcMatch = line.match(/spell save DC (\d+)/)
      const toHitMatch = line.match(/\+(\d+) to hit/)
      
      if (levelMatch) spellcasting.level = parseInt(levelMatch[1])
      if (abilityMatch) spellcasting.ability = abilityMatch[1]
      if (dcMatch) spellcasting.saveDC = parseInt(dcMatch[1])
      if (toHitMatch) spellcasting.toHit = parseInt(toHitMatch[1])
    } else if (line.startsWith('***')) {
      const [levelInfo, spellList] = line.split('::').map(s => s.trim())
      currentGroup = {
        level: levelInfo.replace(/\*/g, ''),
        spells: spellList.split(',').map(s => s.trim().replace(/\*/g, ''))
      }
      
      if (levelInfo.includes('level')) {
        const slotsMatch = levelInfo.match(/\((.*?)\)/)
        if (slotsMatch) currentGroup.slots = slotsMatch[1]
      }
      
      spellcasting.spellGroups.push(currentGroup)
    }
  })
  
  return spellcasting
}

export const parseLegendaryActions = (lines: string[]): LegendaryAction[] => {
  const legendaryActions: LegendaryAction[] = []
  let currentAction: Partial<LegendaryAction> | null = null
  
  lines.forEach(line => {
    if (line.startsWith('***')) {
      if (currentAction) {
        legendaryActions.push(currentAction as LegendaryAction)
      }
      const name = line.match(/\*\*\*(.*?)\.\*\*\*/)?.[1] || ''
      const costMatch = line.match(/\(costs (\d+) actions\)/)
      currentAction = {
        name,
        description: line,
        cost: costMatch ? parseInt(costMatch[1]) : 1
      }
    } else if (currentAction && line.trim()) {
      currentAction.description += ' ' + line.trim()
    }
  })
  
  if (currentAction) {
    legendaryActions.push(currentAction as LegendaryAction)
  }
  
  return legendaryActions
}
