export type Ability = {
  score: number
  modifier: number
}

export type AbilityScores = {
  str: Ability
  dex: Ability
  con: Ability
  int: Ability
  wis: Ability
  cha: Ability
}

export type Action = {
  name: string
  type?: string
  toHit?: number
  damage?: {
    formula: string
    average: number
    type: string
  }
  range?: string
  description?: string
  recharge?: {
    dice: number
    threshold: number
  }
}

export type UltraAction = {
  name: string
  description: string
  trigger?: string
  effects?: string[]
}

export type LairAction = {
  name?: string
  description: string
  initiative?: number
}

export type HitPoints = {
  average: number
  formula: string
}

export type UltraHitPoints = {
  pools: HitPoints[]
}

export type EnergyShields = {
  average: number
  formula: string
  type: string
}

export type SpellLevelGroup = {
  level: string
  slots?: string
  spells: string[]
}

export type SpellcastingInfo = {
  level?: number
  ability?: string
  saveDC?: number
  toHit?: number
  spellGroups: SpellLevelGroup[]
}

export type LegendaryAction = {
  name: string
  description: string
  cost?: number
}

export type StatBlock = {
  name: string
  size?: string
  type?: string
  armorClass: {
    value: number
    type?: string
  }
  energyShields?: EnergyShields
  hitPoints: HitPoints | UltraHitPoints
  speed: Record<string, number | string>
  abilityScores: AbilityScores
  savingThrows?: Record<string, number>
  skills?: Record<string, number>
  damageResistances?: string[]
  damageWeaknesses?: string[]
  damageImmunities?: string[]
  conditionImmunities?: string[]
  toolProficiencies?: string[]
  vehicleProficiencies?: string[]
  senses?: Record<string, string | number>
  languages?: string[]
  challengeRating: {
    individual: {
      rating: number
      xp: number
    }
    classification?: {
      rating: number
      type: string
      xp: number
    }
  }
  proficiencyBonus: number
  hackDC?: number
  features?: Record<string, string>
  actions: Action[]
  ultraActions?: UltraAction[]
  lairActions?: LairAction[]
  legendaryActions?: LegendaryAction[]
  spellcasting?: SpellcastingInfo
}
