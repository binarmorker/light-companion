import { parseAbilityScores, parseAction, parseCR, parseLairActions, parseLegendaryActions, parsePoints, parseSavingThrows, parseSpeed, parseSpellcasting, parseUltraActions, parseUltraHitPoints } from './functions'
import { Action, EnergyShields, StatBlock } from './types'

export function parseStatBlock(statblockText: string): StatBlock {
  const lines = statblockText.split('\n')
  
  let result: Partial<StatBlock> = {}
  let currentSection = ''
  let actions: Action[] = []
  let spellcastingLines: string[] = []
  let ultraActionLines: string[] = []
  let lairActionLines: string[] = []
  let legendaryActionLines: string[] = []
  let ultraHitPointLines: string[] = []
  let parsingUltraHP = false

  lines.forEach(line => {
    line = line.trim()
    if (!line) return

    if (line.startsWith('## ')) {
      result.name = line.replace('## ', '')
    } else if (line.startsWith('*Medium')) {
      const [size, type] = line.replace(/\*/g, '').split(' ')
      result.size = size
      result.type = type
    } else if (line.startsWith('**Armor Class**')) {
      const [value, type] = line.split('::')[1].trim().split(' (')
      result.armorClass = {
        value: parseInt(value),
        type: type ? type.replace(')', '') : undefined
      }
    } else if (line.startsWith('**Energy Shields**')) {
      result.energyShields = parsePoints(line.split('::')[1].trim()) as EnergyShields
    } else if (line.startsWith('***Ultra Health Point Pools:***')) {
      parsingUltraHP = true
      ultraHitPointLines = []
    } else if (parsingUltraHP && line.includes('|')) {
      ultraHitPointLines.push(line)
    } else if (line.startsWith('**Hit Points**')) {
      result.hitPoints = parsePoints(line.split('::')[1].trim())
    } else if (line.startsWith('**Speed**')) {
      parsingUltraHP = false
      if (ultraHitPointLines.length > 0) {
        result.hitPoints = parseUltraHitPoints(ultraHitPointLines)
      }
      result.speed = parseSpeed(line.split('::')[1].trim())
    } else if (line.startsWith('**Saving Throws**')) {
      result.savingThrows = parseSavingThrows(line.split('::')[1].trim())
    } else if (line.startsWith('**Languages**')) {
      result.languages = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.startsWith('**Tool Proficiencies**')) {
      result.toolProficiencies = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.startsWith('**Vehicle Proficiencies**')) {
      result.vehicleProficiencies = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.startsWith('**Damage Resistances**')) {
      result.damageResistances = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.startsWith('**Damage Weaknesses**')) {
      result.damageWeaknesses = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.startsWith('**Damage Immunities**')) {
      result.damageImmunities = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.startsWith('**Condition Immunities**')) {
      result.conditionImmunities = line.split('::')[1].trim().split(',').map(s => s.trim())
    } else if (line.includes('STR|DEX|CON|INT|WIS|CHA')) {
      // Skip the header line
    } else if (line.startsWith('|') && !line.includes('Proficiency') && line.includes('(')) {
      result.abilityScores = parseAbilityScores(line)
    } else if (line.startsWith('**Challenge**')) {
      result.challengeRating = parseCR(line.split('::')[1].trim())
    } else if (line.startsWith('|Proficiency')) {
      // Skip the header line
    } else if (line.startsWith('|+')) {
      const [prof, hack] = line.split('|').filter(s => s.trim())
      result.proficiencyBonus = parseInt(prof.trim())
      result.hackDC = hack.trim() !== '-' ? parseInt(hack.trim()) : undefined
    } else if (line.startsWith('### Actions')) {
      currentSection = 'actions'
    } else if (line.startsWith('### Spellcasting')) {
      currentSection = 'spellcasting'
    } else if (line.startsWith('### Legendary Actions')) {
      currentSection = 'legendaryActions'
    } else if (line.startsWith('### Ultra Actions')) {
      currentSection = 'ultraActions'
    } else if (line.startsWith('### Lair Actions')) {
      currentSection = 'lairActions'
    } else if (currentSection === 'actions' && line.startsWith('***')) {
      actions.push(parseAction(line))
    } else if (currentSection === 'spellcasting') {
      spellcastingLines.push(line)
    } else if (currentSection === 'legendaryActions') {
      legendaryActionLines.push(line)
    } else if (currentSection === 'ultraActions') {
      ultraActionLines.push(line)
    } else if (currentSection === 'lairActions') {
      lairActionLines.push(line)
    }
  })

  result.actions = actions
  if (spellcastingLines.length > 0) result.spellcasting = parseSpellcasting(spellcastingLines)
  if (legendaryActionLines.length > 0) result.legendaryActions = parseLegendaryActions(legendaryActionLines)
  if (ultraActionLines.length > 0) result.ultraActions = parseUltraActions(ultraActionLines)
  if (lairActionLines.length > 0) result.lairActions = parseLairActions(lairActionLines)

  return result as StatBlock
}
