import { extractModifier, parseAbilityScores, parsePoints } from './functions'

describe('testing extract modifier', () => {
  test('zero should return zero', () => {
    expect(extractModifier('10 (0)')).toBe(0)
  })
  
  test('positive should return itself', () => {
    expect(extractModifier('13 (+1)')).toBe(1)
  })
  
  test('negative should return itself', () => {
    expect(extractModifier('6 (-2)')).toBe(-2)
  })
  
  test('only number should return 0', () => {
    expect(extractModifier('12')).toBe(0)
  })
  
  test('invalid characters should return 0', () => {
    expect(extractModifier('gt')).toBe(0)
  })
})

describe('testing parse ability scores', () => {
  test('normal ability scores should return themselves', () => {
    const scores = parseAbilityScores('|10 (0)|14 (+2)|16 (+3)|8 (-1)|20 (+5)|13 (+1)|')
    expect(scores).toMatchObject({
      str: { score: 10, modifier: 0 },
      dex: { score: 14, modifier: 2 },
      con: { score: 16, modifier: 3 },
      int: { score: 8, modifier: -1 },
      wis: { score: 20, modifier: 5 },
      cha: { score: 13, modifier: 1 }
    })
  })
  
  test('extreme ability scores should return themselves', () => {
    const scores = parseAbilityScores('|20 (+5)|24 (+7)|1 (-5)|8 (-1)|20 (+5)|13 (+1)|')
    expect(scores).toMatchObject({
      str: { score: 20, modifier: 5 },
      dex: { score: 24, modifier: 7 },
      con: { score: 1, modifier: -5 },
      int: { score: 8, modifier: -1 },
      wis: { score: 20, modifier: 5 },
      cha: { score: 13, modifier: 1 }
    })
  })

  test('unmatching modifiers should return themselves', () => {
    const scores = parseAbilityScores('|10 (+2)|14 (-4)|16 (+2)|8 (-3)|20 (+1000)|13 (+1)|')
    expect(scores).toMatchObject({
      str: { score: 10, modifier: 2 },
      dex: { score: 14, modifier: -4 },
      con: { score: 16, modifier: 2 },
      int: { score: 8, modifier: -3 },
      wis: { score: 20, modifier: 1000 },
      cha: { score: 13, modifier: 1 }
    })
  })
})

describe('testing parse points', () => {
  test('average and formula should return valid object', () => {
    expect(parsePoints('18 (4d4+6)')).toMatchObject({
      average: 18,
      formula: '4d4+6'
    })
  })
})
