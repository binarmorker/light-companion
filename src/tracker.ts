import Tracker from '@openreplay/tracker'

export let tracker: Tracker

export function createTracker() {
  try {
    tracker = new Tracker({
      projectKey: 'XQw22FTWGBvyPzISZKbo',
      ingestPoint: 'https://mon.binar.ca/ingest'
    })
    tracker?.start()
  } catch (_) {
    // noop
  }
}
