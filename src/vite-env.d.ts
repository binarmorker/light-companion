/// <reference types="vite/client" />

interface Window {
  dataLayer: { [key: string, val: never]: unknown }[],
  __EMULATED_USER_ID__: ?string
}

interface ComponentCustomProperties {
  $bus: {
    on (eventName: string, fn: (data?: never) => void)
    off (eventName: string, fn: (data?: never) => void)
    trigger (eventName: string, data?: never)
  }
}

interface HTMLElement {
  hidePopover: (evt?: MouseEvent = undefined) => void
}

// Inject types for the dicebox library
declare module '@3d-dice/dice-box' {
  export default class DiceBox {
    constructor(config: DiceConfig)
    init(): Promise<DiceBox>
    clear(): DiceBox
    hide(): DiceBox
    show(): DiceBox
    add(formula: string): Promise<DiceRollResultArray>
    roll(formula: string): Promise<DiceRollResultArray>
  }
}

type DiceConfig = {
  container: string,
  assetPath: string,
  scale: number,
  startingHeight: number,
  mass: number,
  gravity: number
}

type DiceRollResult = {
  groupId: number,
  rollId: number,
  sides: number,
  theme: string,
  themeColor: string
  value: number,
}

type DiceRollResultArray = DiceRollResult[]
