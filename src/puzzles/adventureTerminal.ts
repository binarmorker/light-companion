import { BobbyTCrewAdventure } from './adventure/bobbytcrew'
import { DonotAdventure } from './adventure/donot'
import { Jason7Adventure } from './adventure/jason-7'
import { JayceAdventure } from './adventure/jayce'
import { LexyAdventure } from './adventure/lexy'
import { Nexus6Adventure } from './adventure/nexus-6'
import { ZankemAdventure } from './adventure/zankem'
import { FauxTerm } from './fauxTerm'

function getAdventure(userId: string) {
  if (userId === 'R18G8LHh0SfHeLY1TI5LvnlS6rH3' && window.__EMULATED_USER_ID__) userId = window.__EMULATED_USER_ID__

  switch (userId) {
    case 'hHNYmUxJPOTNNt2ippB58Uz7xbW2': return new ZankemAdventure() // Nicolas
    case 'ZnaIuD5DOYX7NjmbpJbr9q0N2Gd2': return new Jason7Adventure() // Ian
    case 'CeS5jTWBXiVtdieOHpXxPPYeGgq1': return new LexyAdventure() // Carolanne
    case '20qXkC67l6V3rk6gLyHK3US7Kjx2': return new DonotAdventure() // Ralph
    case '92VAuNpqeVdSJs6gwfPMRL8PucU2': return new BobbyTCrewAdventure() // Bastien
    case 'Bn4z87N9TKccmmO5sBEePXWami53': return new Nexus6Adventure() // Miguel
    case 'hffTGTWeEuURun4FPTxgOTOWQqO2': return new JayceAdventure() // Samuel
    default: return null
  }
}

export class AdventureTerm extends FauxTerm {
  constructor(el: HTMLElement, userId: string) {
    const adventure = getAdventure(userId)

    if (!adventure) throw new Error('Invalid parameters.')

    super({
      el,
      cwd: `{bold}{white}Your choice `,
      leaderChar: '>{/white}{/bold}',
      cmd (argv: string[], argc: number) {
        const index = parseInt(argv[0], 10) - 1

        if (index === 6 && adventure.isCompleted) {
          return adventure.successMessage
        }

        if (!adventure.selectChoice(index)) {
          return `{red}Invalid choice{/red}\n\n{white}${adventure.showText()}{/white}\n`
        }

        return `{white}${adventure.showText()}{/white}\n`
      },
      coreCmds: {},
      tags: ['red', 'green', 'white', 'bold', 'blue', 'reset']
    })

    this.writeToBuffer(this.renderStdOut(`{white}${adventure.showText()}{/white}\n`))
    this.renderTerm()
  }
}
