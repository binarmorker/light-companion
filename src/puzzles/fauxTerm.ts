export type FauxTermConfig = {
  el: HTMLElement,
  initialMessage?: string,
  initialLine?: string,
  leaderChar?: string,
  cwd?: string,
  tags?: string[],
  cmd?: (argv: string[], argc: number) => string,
  maxBufferLength?: number,
  maxCommandHistory?: number,
  autoFocus?: boolean,
  coreCmds?: Record<string, (argv: string[], argc: number) => string>,
  onCommand?: (cmd: string, res: string) => void
}

export class FauxTerm {
  term: HTMLElement
  termBuffer: string
  lineBuffer: string
  leaderChar: string
  cwd: string
  tags: string[]
  processCommand: ((argv: string[], argc: number) => string)|null
  maxBufferLength: number
  commandHistory: string[]
  currentCommandIndex = -1
  maxCommandHistory: number
  autoFocus: boolean
  coreCmds: Record<string, (argv: string[], argc: number) => string>
  fauxInput: HTMLTextAreaElement
  onCommand: ((cmd: string, res: string) => void)|null

  constructor (config: FauxTermConfig) {
    this.term = config.el || document.getElementById('term')
    this.termBuffer = config.initialMessage || ''
    this.lineBuffer = config.initialLine || ''
    this.leaderChar = config.leaderChar || '$'
    this.cwd = config.cwd || '~/'
    this.tags = config.tags || ['red', 'green', 'white', 'bold', 'blue']
    this.processCommand = config.cmd || null
    this.maxBufferLength = config.maxBufferLength || 8192
    this.commandHistory = []
    this.maxCommandHistory = config.maxCommandHistory || 100
    this.autoFocus = config.autoFocus || false
    this.coreCmds = {
      'clear': this.clear.bind(this)
    }
    this.onCommand = config.onCommand || null

    this.fauxInput = document.createElement('textarea')
    this.fauxInput.className = 'faux-input'
    document.body.appendChild(this.fauxInput)

    if (this.autoFocus) {
      this.fauxInput.focus()
    }
    
    this.term.addEventListener('click', () => {
      this.fauxInput.focus()
      this.term.classList.add('term-focus')
    })
    this.fauxInput.addEventListener('keydown', (event: KeyboardEvent) => {
      this.acceptInput(event)
    })
    this.fauxInput.addEventListener('blur', () => {
      this.term.classList.remove('term-focus')
    })

    this.renderTerm()
  }

  getLeader() {
    return `${this.renderStdOut(this.cwd + this.leaderChar)} `
  }

  renderTerm() {
    const bell = '<span class="bell"></span>'
    const ob = this.termBuffer + this.getLeader() + this.lineBuffer
    this.term.innerHTML = ob
    this.term.innerHTML += bell
    this.term.scrollTop = this.term.scrollHeight
  }

  writeToBuffer(str: string) {
    this.termBuffer += str

    // Stop the buffer getting massive.
    if (this.termBuffer.length > this.maxBufferLength) {
      const diff = this.termBuffer.length - this.maxBufferLength
      this.termBuffer = this.termBuffer.substring(diff)
    }
  }

  renderStdOut(str: string) {
    const max = this.tags.length

    for (let i = 0; i < max; i++) {
      const start = new RegExp('{' + this.tags[i] + '}', 'g')
      const end = new RegExp('{/' + this.tags[i] + '}', 'g')
      str = str.replace(start, '<span class="' + this.tags[i] + '">')
      str = str.replace(end, '</span>')
    }

    return str
  }

  clear() {
    this.termBuffer = ''
    return ''
  }

  isCoreCommand(line: string) {
    return !!this.coreCmds[line]
  }

  coreCommand(argv: string[], argc: number) {
    const cmd = argv[0]
    return this.coreCmds[cmd](argv, argc)
  }

  processLine() {
    // Dispatch command
    let stdout = this.lineBuffer
    const line = this.lineBuffer,
          argv = line.split(' '),
          argc = argv.length
    const cmd = argv[0]

    this.lineBuffer += '\n'
    this.writeToBuffer(this.getLeader() + this.lineBuffer)
    this.lineBuffer = ''

    // If it's not a blank line.
    if (cmd !== '') {

      // If the command is not registered by the core.
      if (!this.isCoreCommand(cmd)) {

        // User registered command
        if (this.processCommand) {
          stdout = this.processCommand(argv, argc)
        } else {
          stdout = '{white}{bold}' + cmd + '{/bold}{/white}: command not found\n'
        }
      } else {
        // Execute a core command
        stdout = this.coreCommand(argv, argc)
      }

      // If an actual command happened.
      if (stdout === 'error') {
        stdout = '{white}{bold}' + cmd + '{/bold}{/white}: command not found\n'
      }

      stdout = this.renderStdOut(stdout)
      this.writeToBuffer(stdout)

      this.addLineToHistory(line)
      this.onCommand?.(cmd, stdout.replace(/(<([^>]+)>)/ig, ''))
    }

    this.renderTerm()
  }

  addLineToHistory(line: string) {
    this.commandHistory.unshift(line)
    this.currentCommandIndex = -1

    if (this.commandHistory.length > this.maxCommandHistory) {
      // console.log('reducing command history size')
      // console.log(this.commandHistory.length)
      const diff = this.commandHistory.length - this.maxCommandHistory
      this.commandHistory.splice(this.commandHistory.length - 1, diff)
      // console.log(this.commandHistory.length)
    }
  }

  isInputKey(keyCode: number) {
    const inputKeyMap = [32, 190, 192, 189, 187, 220, 221, 219, 222, 186, 188, 191]
    return inputKeyMap.indexOf(keyCode) > -1
  }

  toggleCommandHistory(direction: number) {
    const max = this.commandHistory.length - 1
    let newIndex = this.currentCommandIndex + direction

    if (newIndex < -1) newIndex = -1
    if (newIndex >= this.commandHistory.length) newIndex = max

    if (newIndex !== this.currentCommandIndex) {
      this.currentCommandIndex = newIndex
    }

    if (newIndex > -1) {
      // Change line to something from history.
      this.lineBuffer = this.commandHistory[newIndex]
    } else {
      // Blank line...
      this.lineBuffer = ''
    }
  }

  acceptInput(e: KeyboardEvent) {
    e.preventDefault()

    this.fauxInput.value = ''

    if (e.keyCode >= 48 && e.keyCode <= 90 || this.isInputKey(e.keyCode)) {
      if (!e.ctrlKey) {
        // Character input
        this.lineBuffer += e.key
      } else {
        // Hot key input? I.e Ctrl+C
      }
    } else if (e.keyCode === 13) {
      this.processLine()
    } else if (e.keyCode === 9) {
      this.lineBuffer += '\t'
    } else if (e.keyCode === 38) {
      this.toggleCommandHistory(1)
    } else if (e.keyCode === 40) {
      this.toggleCommandHistory(-1)
    }
    else if (e.key === 'Backspace') {
      this.lineBuffer = this.lineBuffer.substring(0, this.lineBuffer.length - 1)
    }

    this.renderTerm()
  }
}
