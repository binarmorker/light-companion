import { FauxTerm } from './fauxTerm'

export function vannetCommands (type: 'vex'|'exo'|'other') {
  const hexText = [
    '54 68 65 20 54 72 61 76 65 6C 65 72 20 6D 6F 76 65 64 20 61 63 72 6F 73 73 20 74 68 65 20 66 61 63 65 20 6F 66 20 74 68 65 20 69 72 6F 6E 20 77 6F 72 6C 64 2E 20 49 74 20 6F 70 65 6E 65 64 20 74 68 65 20 65 61 72 74 68 20 61 6E 64 20 73 74 69 74 63 68 65 64 20 73 68 75 74 20 74 68 65 20 73 6B 79 2E 20 49 74 20 6D 61 64 65 20 6C 69 66 65 20 70 6F 73 73 69 62 6C 65 2E 20 49 6E 20 74 68 65 73 65 20 74 68 69 6E 67 73 20 74 68 65 72 65 20 69 73 20 61 6C 77 61 79 73 20 73 79 6D 6D 65 74 72 79 2E 20 44 6F 20 79 6F 75 20 75 6E 64 65 72 73 74 61 6E 64 3F 20 54 68 69 73 20 69 73 20 6E 6F 74 20 74 68 65 20 62 65 67 69 6E 6E 69 6E 67 20 62 75 74 20 69 74 20 69 73 20 74 68 65 20 72 65 61 73 6F 6E 2E 54 68 65 20 47 61 72 64 65 6E 20 67 72 6F 77 73 20 69 6E 20 62 6F 74 68 20 64 69 72 65 63 74 69 6F 6E 73 2E 20 49 74 20 67 72 6F 77 73 20 69 6E 74 6F 20 74 6F 6D 6F 72 72 6F 77 20 61 6E 64 20 79 65 73 74 65 72 64 61 79 2E 20 54 68 65 20 72 65 64 20 66 6C 6F 77 65 72 73 20 62 6C 6F 6F 6D 20 66 6F 72 65 76 65 72 2E 54 68 65 72 65 20 61 72 65 20 67 61 72 64 65 6E 65 72 73 20 6E 6F 77 2E 20 54 68 65 79 20 63 61 6D 65 20 69 6E 74 6F 20 74 68 65 20 67 61 72 64 65 6E 20 69 6E 20 76 65 73 73 65 6C 73 20 6F 66 20 62 72 6F 6E 7A 65 20 61 6E 64 20 74 68 65 79 20 6D 6F 76 65 20 74 68 72 6F 75 67 68 20 74 68 65 20 67 72 6F 76 65 73 20 69 6E 20 72 69 76 65 72 73 20 6F 66 20 74 68 6F 75 67 68 74 2E 7F 7F 7F 7F 7F 7F 7F 4d 54 68 64 00 00 00 06 00 00 00 01 00 80 7F 7F 7F 7F 7F 7F 7F 67 61 72 64 65 6E 2E 6C 6F 67',
    `ARENA DESIGNATION: Anomaly
LOCATION: Mare Cognitum, Earth's Moon
Documents recovered on-site listed this research station only as "K1", although the location was hard to keep secret, given the intense electromagnetic fluctuations emanating from what City scholars have come to know as the Anomaly. Attempts to scan the Anomaly itself have proven futile, as the casing is constructed in a manner that defies modern techniques.

Reports suggest that those who spent time in proximity to the Anomaly reported symptoms of insomnia, some so severe they required hospitalization. It was the City's recommendation that only remote sensing equipment be used until such time that a full review of the existing data could be completed.

--. .- .-. -.. . -. .-.-.- .-.. --- --.`,
    `C Lydian-Mixolydian

The C Lydian-Mixolydian scale is a unique seven-note scale that combines elements of both the Lydian and Mixolydian modes. It is essentially a C major scale with a raised fourth (F#) and a lowered seventh (Bb). The notes in the C Lydian-Mixolydian scale are:

C - D - E - F# - G - A - Bb

This scale creates a distinctive sound that blends the bright, uplifting quality of the Lydian mode (with its raised fourth) and the bluesy, dominant feel of the Mixolydian mode (with its lowered seventh). The C Lydian-Mixolydian scale is often used in jazz, fusion, and progressive rock to add a unique flavor to melodies and improvisations.

C F# Bb _ C F# C _ C A D _ C F# F# _ C F# G _ C G A _ _ G A _ C G F# _ C G Bb _ C F# Bb _`,
    `+++++========----------------------==---------------------=*++=-------=====+++++++
++++==========-------------==++++****#####+=+###%%%####++####%%*===----=====++++++
++++==========---------=**###################%#######%#####%%%%%%*---=======++++++
+++++======----------+#######################%%###%%##%%###%%%%%%#=--=======++++++
+++++=====--===++*+==*#*#%####################%####%%%%%%##%%###%#+--=======++++++
++++=====--=%*########%%%%%%########%#########%%#####%%%%%%#%#%#%%*----========+++
*+======-=**########%#%%%%%%%%###%##%%%%#######%#######%%%%%%###%%%=----========++
+======--=#########%%%%%%%%%%%%%####%%%%#######%######%%%%%%%%%#%%#*-----======+++
+======--=*###%%%%%%%%%%%%%#%%%%%%%#%%#%###%%%%%#######%%%%%%%%%#%%#=----========+
++=====--=*%%%%%%%#%%%%%%%%##%%%%%%%%%#%#######%@##%%%%#%%%%%%%%%%%%=------=====++
#*+===--==*%%%#%%%#%%%@%%%%%#%%%%%%%%%%%%%%##%%##%%%%%%%%%%%%%%%%%%%%==------==+++
%%#+=====+#%%%%%%%#%%%@%%%%%%%%%#%%%%%%%%%%#%%%%%%%%%%#%%%%%%%%%@@@@%%%-----=====+
%%%%##*=*#%%%%%%%%%#%@%% WE ARE WATCHING // FROM AFAR %%%%%%%%%%%@%@%%%%%**=======
%%%#%##%##%%##%%%##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%@%%%%%%%%%%%%%%#%%@%@%%%%%%#+====+
%########%%%##%%%%%%@%%%%%%%%%%%%%%%%%%%%%%%%%@@@@@@@%@@@@@@@@%%%%%%%%%%%%%%%%##**
%%%%%%%%%%%%%%%%%%%@%%%%%%%%%%%%%%%%%%%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%@@@@@@@
@@@@@@@@@@%@#@%@@@@%%%%%%%%%%@@@@@@@@@@@@@#@@@@@@@@@@@@@@@@@@@@@@@%#+*@@@@=@%%@%%%
@@@@@@@@@@@@%%%%%@%@@@@%%%%%%*+#=+#=@@@@@@#*#%%@%%%%#****#****####*#**+*+***######
%#**+###%%#+=+####**=+#+#%#*#*==+=**==*###@##***###*##*#***=****######+****=######
#######*#*####+###%%%#*##*%#%**+##%########%%##*#*+==#+#++###*#%#**++*####%%%+%#-+
%#%%#*=-####*#+**++++######%%%%#########%#*%%##*+-+#*#####%%###%%##%%%%%%##%%%%%%%
#%#%@#%%%*=***#%%#**##%##################*=#**#*#*=*##*=+#*#%###*%%%%+%%%%*%#%%%%%
%%%#%%%--****%*++=*+*####+*######+++****##*%#*%%%%%%##%++#*+###*%%%####%%%###+#%%%
###**#%%%**#%%%%%%***#######%%*%%#%#%#+%*+%#%###%%%%%%%##%%%%%####%%#%#%%%%%%==-#%
%##%#%+%%%%%%#%%%%%%%%%%%%%%%%%%#%%%#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%##%%%%#=#%`
  ]

  function help() {
    return 'VANNET SHIP COMMUNICATION INTERFACE\n' +
           '========================\n' +
           '- help: List commands\n' +
           '- cd: Change directories\n' +
           '- ls: List files\n' +
           '- view: View the contents of a file\n' +
           '- clear: Empty terminal output\n'
  }

  function ls(directory: string) {
    if (!directory) directory = '.'
    if (['.', '/', './', '..', '../'].includes(directory)) {
      return 'Directory Listing of ' + directory + '\n' +
             '======================= ====\n' +
             'cd.run                 | 755\n' +
             'clear.run              | 755\n' +
             'help.run               | 755\n' +
             'ls.run                 | 755\n' +
             (type === 'other' ? 'anomaly.log            | 644\n' : '') +
             (type === 'vex' ? 'scale.log              | 644\n' : '') +
             (type === 'exo' ? '7F7F7F.log             | 644\n' : '') +
             'view.run               | 755\n'
    }

    return 'Unknown directory\n'
  }

  function cd() {
    return 'Unknown directory\n'
  }

  function view(file: string) {
    if (file === '7F7F7F.log') return `${hexText[0]}\nEND OF FILE\n`
    if (file === 'anomaly.log') return `${hexText[1]}\nEND OF FILE\n`
    if (file === 'scale.log') return `${hexText[2]}\nEND OF FILE\n`
    if (file === 'garden.log') return `${hexText[3]}\nEND OF FILE\n`
    return 'Unknown file\n'
  }

  return {
    help,
    ls,
    cd,
    view
  }
}

export class VanNetTerm extends FauxTerm {
  constructor(el: HTMLElement, userId: string) {
    if (userId === 'R18G8LHh0SfHeLY1TI5LvnlS6rH3' && window.__EMULATED_USER_ID__) userId = window.__EMULATED_USER_ID__

    const vexCharacters = [
      'hHNYmUxJPOTNNt2ippB58Uz7xbW2', // Nicolas
      '20qXkC67l6V3rk6gLyHK3US7Kjx2', // Ralph
    ]
    const exoCharacters = [
      'Bn4z87N9TKccmmO5sBEePXWami53', // Miguel
      'ZnaIuD5DOYX7NjmbpJbr9q0N2Gd2', // Ian
    ]

    let type: 'vex'|'exo'|'other' = 'other'
    if (exoCharacters.includes(userId)) type = 'exo'
    if (vexCharacters.includes(userId)) type = 'vex'
    const commands = vannetCommands(type)

    super({
      el,
      cwd: `{blue}${location.search.replace('?c=', '').replace(/[%20|.]/g, '').toLowerCase()}@vannet[encrypted] {white}`,
      cmd (argv: string[], argc: number) {
        if (argv[0].endsWith('.run')) {
          return 'Use the command without .run to execute it.\n'
        }

        switch (argv[0]) {
          case 'help':
            return commands.help()
          case 'ls':
            return commands.ls(argv[1])
          case 'cd':
            return commands.cd()
          case 'view':
            return commands.view(argv[1])
        }

        return 'error'
      },
      tags: ['red', 'green', 'white', 'bold', 'blue']
    })
  }
}
