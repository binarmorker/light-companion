export type AdventureChoice = {
  name: (adventure: Adventure) => string,
  prompt?: (adventure: Adventure) => string,
  choices?: AdventureChoice[]
  condition?: (adventure?: Adventure) => boolean,
  event?: (adventure?: Adventure) => void
}

export class Adventure {
  choices: AdventureChoice
  currentStep: number[]

  constructor(choices: AdventureChoice) {
    this.choices = choices
    this.currentStep = []
  }

  goto(...indexes: number[]) {
    this.currentStep = indexes
  }

  advanceStep(index: number) {
    this.goto(...this.currentStep, index)
  }

  getChoice() {
    if (this.currentStep.length) {
      let currentChoice: AdventureChoice|undefined = this.choices

      for (const index of this.currentStep) {
        currentChoice = currentChoice?.choices?.[index]
      }

      return currentChoice
    } else {
      return this.choices
    }
  }

  showText() {
    const choice = this.getChoice()

    if (!choice) return 'Invalid choice'

    const choices = choice.choices?.filter(x => x.condition?.(this) !== false).map((x, i) =>  `{blue}${i + 1}. ${x.name(this)}{/blue}`) || []

    return `${choice.prompt?.(this)}\n\n${choices.join('\n')}\n`
  }

  selectChoice(index: number) {
    const currentChoice = this.getChoice()
    const choices = currentChoice?.choices

    if (!choices) return false

    const selectedChoice = choices.filter(x => x.condition?.(this) !== false)[index]

    if (!selectedChoice) return false

    if (selectedChoice.condition?.(this) !== false) {
      const trueIndex = choices.indexOf(selectedChoice)
      this.advanceStep(trueIndex)
      selectedChoice.event?.(this)
      return selectedChoice
    }

    return false
  }
}
