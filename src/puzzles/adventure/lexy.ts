import { Adventure } from './adventure'

export class LexyAdventure extends Adventure {
  endingMessage = ''
  successMessage = `{green}YOU WILL FIND SALVATION IN THE VALLEYS OF THE GARDEN, WHERE PLANTS OF DARKNESS BLOOM AND LIGHT HAS NO PURPOSE. SAVE THIS MELODY AND BE SAVED:\n\nBb A E G F# D E C{/green}\n\nYour Awoken self is at war with the light, and the dark. Find comfort on the line between the two, and blur them.\n`
  hasGivenName = false
  hasAskedCousin = false
  hasAskedCity = false
  fakeNameCounter = 0
  wasteOfTime = 0
  hasTalkedToMerchant = false
  isCompleted = (localStorage.getItem('lexy-adventure-complete-2024-07') === 'true') || false

  reset() {
    this.endingMessage = ''
    this.hasGivenName = false
    this.hasAskedCousin = false
    this.hasAskedCity = false
    this.fakeNameCounter = 0
    this.wasteOfTime = 0
    this.hasTalkedToMerchant = false
  }

  constructor() {
    super({
      name: () => 'Lexy\'s Adventure',
      prompt: () => 'You start dreaming of a beautiful city of purple amethysts, blue sapphires and white rocks. The grass is green and the land is mountainous and vast. The city\'s entrance is welcoming, but empty except for a single female awoken looking at you at the side of the gate.',
      choices: [
        {
          name: () => 'Ending',
          prompt: () => this.endingMessage,
          condition: () => !!this.endingMessage.length,
          choices: [
            {
              name: () => 'Restart',
              event: () => {
                this.reset()
                this.goto()
              }
            }
          ]
        },
        {
          name: () => 'Approach the awoken',
          prompt: () => this.hasAskedCousin ? '"This place is the birthplace of the Awoken. Those who are born here are Reefborn—they are brothers and sisters. Those who are born on Earth are Earthborn, and they are called cousins."' : this.hasAskedCity ? '"This place is the birthplace of the Awoken. It is called the Dreaming City."' : '"Happy to see you, sister. What is your name?"',
          choices: [
            {
              name: () => 'Tell your real name',
              prompt: () => '"Welcome, Lexy. This is the city of the Awoken, accessed from the Reef. You may not be familiar to this place, but the Queen wanted you to see it."',
              choices: [
                {
                  name: () => 'Enter the city',
                  prompt: () => 'You enter and see lush gardens and ornate roads, walked by dozens of Awoken just like you. They are talking amongst each other, or going from a market store to another, but they are lively. Can the same be said of you?',
                  choices: [
                    {
                      name: () => 'Go to the market',
                      prompt: () => 'You arrive to the markets and a merchant hails you.',
                      choices: [
                        {
                          name: () => 'Approach the merchant',
                          prompt: () => this.hasTalkedToMerchant ? '"So, what else would you like to purchase?"' : '"Hi! What would you like to buy today?"',
                          choices: [
                            {
                              name: () => 'Buy an apple',
                              prompt: () => '"Sure, have you got any Glimmer? There you go, one apple. You should go look around a bit!"',
                              choices: [
                                {
                                  name: () => 'Say your thanks',
                                  event: () => {
                                    this.hasTalkedToMerchant = true
                                    this.wasteOfTime++
                                    this.goto(1, 0, 0, 0, 0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Buy a Tincture of Queensfoil',
                              prompt: () => '"Hmmm, it is not for sale. It is a gift from the Queen. Maybe you should visit her!"',
                              choices: [
                                {
                                  name: () => 'Say sorry',
                                  event: () => {
                                    this.hasTalkedToMerchant = true
                                    this.wasteOfTime++
                                    this.goto(1, 0, 0, 0, 0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Buy a Combat Bow',
                              prompt: () => '"No weapons in the city. Are you crazy? You\'re not a Corsair, you need no combat bow."',
                              choices: [
                                {
                                  name: () => 'Say sorry',
                                  event: () => {
                                    this.hasTalkedToMerchant = true
                                    this.wasteOfTime++
                                    this.goto(1, 0, 0, 0, 0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Go back',
                              event: () => {
                                this.wasteOfTime++
                                this.goto(1, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Ignore him',
                          event: () => {
                            this.wasteOfTime++
                            this.goto(1, 0, 0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Go to the gardens',
                      prompt: () => 'A gardener is taking care of the flowers',
                      choices: [
                        {
                          name: () => 'Ask what the flowers are',
                          prompt: () => 'The gardener turns their head towards you. "Those are Asphodelia", they say. "A species of red flower native to another garden. They are a symbol of conflict, of the fragility of life."',
                          event: () => {
                            this.wasteOfTime++
                          },
                          choices: [
                            {
                              name: () => 'Go back',
                              event: () => {
                                this.goto(1, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Go back',
                          event: () => {
                            this.wasteOfTime++
                            this.goto(1, 0, 0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Go to the castle',
                      prompt: () => 'You appraoch the towering building in the middle of the city and begin to see gorgeous statues of creatures you have never seen before. The flowers are blooming well on the road leading to the castle\'s gate. As you approach, you see 2 Queen\'s Guards inside the door arch.',
                      choices: [
                        {
                          name: () => 'Greet them',
                          prompt: () => 'They greet you by doing a slight salute, then going back to their guarding posture. They seem to know who you are and allowing you to enter.',
                          choices: [
                            {
                              name: () => 'Enter the castle',
                              prompt: () => {
                                let iritated = ''

                                switch (this.wasteOfTime) {
                                  case 0: iritated = ''; break
                                  case 1: iritated = ' I hope you had fun visiting.'; break
                                  case 2: iritated = ' You took some time in the city I see.'; break
                                  case 3: iritated = ' I see you really did take your time.'; break
                                  case 4: iritated = ' Were you not eager to see me?'; break
                                  case 5: iritated = ' Why did it take so long to come to me?'; break
                                  case 6: iritated = ' I was wondering when you would arrive.'; break
                                  default: iritated = ' You made me wait way too long for your visit.'; break
                                }

                                return `As you begin to enter, other guards begin to follow you and escort you to the Queen's quarters. You enter, and the Queen greets you: "Welcome, Lexy.${iritated}"`
                              },
                              choices: [
                                {
                                  name: () => 'Greet the Queen back',
                                  prompt: () => `"${this.fakeNameCounter > 0 ? 'Why were you so eager to lie to my Corsair at the city gates I wonder?"' : 'So, what do you want from me today?"'}`,
                                  choices: [
                                    {
                                      name: () => 'I did not mean it',
                                      prompt: () => '"I believe you, Lexy."',
                                      condition: () => this.fakeNameCounter > 0,
                                      choices: [
                                        {
                                          name: () => 'Say your thanks',
                                          event: () => {
                                            this.fakeNameCounter = 0
                                            this.goto(1, 0, 0, 2, 0, 0, 0)
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      name: () => 'I would like to know how I can please you, my Queen',
                                      prompt: () => '"I ask nothing of you but to please the city. Or maybe are you asking for something more... involved?"',
                                      condition: () => !this.fakeNameCounter,
                                      choices: [
                                        {
                                          name: () => 'Yes, I would like something more involved.',
                                          event: () => {
                                            this.endingMessage = '"It is good to hear, Lexy. I need you in the Black Garden soon. You will know more when the time comes." You turn around after doing your reverence, and you begin to hear a faint voice in your ears. You feel dizzy, your vision becomes blurry, and the whisper becomes clearer and clearer. All of a sudden, you see nothing but hear a single word from a voice unknown: {green}-Seven-{/green}'
                                            this.isCompleted = true
                                            localStorage.setItem('lexy-adventure-complete-2024-07', 'true')
                                            this.goto(0)
                                          }
                                        },
                                        {
                                          name: () => 'No, I will do as you say.',
                                          event: () => {
                                            this.endingMessage = '"It is good to hear, Lexy. Now go, my people need your help. You will know what to do in time." You turn around after doing your reverence, and you begin to hear a faint voice in your ears. You feel dizzy, your vision becomes blurry, and the whisper becomes many whispers. They intensify. All of a sudden, you see nothing but hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                            this.goto(0)
                                          }
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  name: () => 'Stay silent',
                                  prompt: () => `"Not very talkative I see. ${this.fakeNameCounter > 0 ? 'Why were you so eager to lie to my Corsair at the city gates I wonder?"' : 'Did you have any inquiries?"'}`,
                                  choices: [
                                    {
                                      name: () => 'I did not mean it',
                                      prompt: () => '"I believe you, Lexy."',
                                      condition: () => this.fakeNameCounter > 0,
                                      choices: [
                                        {
                                          name: () => 'Say your thanks',
                                          event: () => {
                                            this.fakeNameCounter = 0
                                            this.goto(1, 0, 0, 2, 0, 0, 0)
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      name: () => 'I would like to know how I can please you, my Queen',
                                      prompt: () => '"I ask nothing of you but to please the city. Or maybe are you asking for something more... involved?"',
                                      condition: () => !this.fakeNameCounter,
                                      choices: [
                                        {
                                          name: () => 'Yes, I would like something more involved.',
                                          event: () => {
                                            this.endingMessage = '"It is good to hear, Lexy. I need you in the Black Garden soon. You will know more when the time comes." You turn around after doing your reverence, and you begin to hear a faint voice in your ears. You feel dizzy, your vision becomes blurry, and the whisper becomes clearer and clearer. All of a sudden, you see nothing but hear a single word from a voice unknown: {green}-Seven-{/green}'
                                            this.isCompleted = true
                                            localStorage.setItem('lexy-adventure-complete-2024-07', 'true')
                                            this.goto(0)
                                          }
                                        },
                                        {
                                          name: () => 'No, I will do as you say.',
                                          event: () => {
                                            this.endingMessage = '"It is good to hear, Lexy. Now go, my people need your help. You will know what to do in time." You turn around after doing your reverence, and you begin to hear a faint voice in your ears. You feel dizzy, your vision becomes blurry, and the whisper becomes many whispers. They intensify. All of a sudden, you see nothing but hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                            this.goto(0)
                                          }
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  name: () => 'Flee',
                                  event: () => {
                                    this.endingMessage = 'You start running away and exit the castle. You continue outside the city and towards a white fog, losing track of your surroundings. After a short while, a galaxy form in front of you and the ground slowly gives way from under your feet. You are falling, but you don\'t feel the direction in which you are falling. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                    this.goto(0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Go back',
                              event: () => {
                                this.wasteOfTime++
                                this.goto(1, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Go back',
                          event: () => {
                            this.wasteOfTime++
                            this.goto(1, 0, 0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Go to the houses',
                      prompt: () => 'You arrive in the viscinity of multiple Awoken houses, much of which are closed. Some have their front door open and the inhabitants are talking between them from their front porch.',
                      choices: [
                        {
                          name: () => 'Approach one of them',
                          prompt: () => '"Hey there!" says one of the Awoken, interrupting their conversation. "Are you new here? I have not seen you around."',
                          choices: [
                            {
                              name: () => 'Tell them who you are',
                              prompt: () => 'Oh hi Lexy, nice to meet you sister. I\'d suggest you don\'t go down that route, something bad happened there a few moments ago I\'m sure. Stay safe out there!',
                              choices: [
                                {
                                  name: () => 'Go back',
                                  event: () => {
                                    this.wasteOfTime++
                                    this.goto(1, 0, 0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Tell them you are new',
                              prompt: () => 'Oh, mysterious! I\'d suggest you don\'t go down that route, something bad happened there a few moments ago I\'m sure. Stay safe out there!',
                              choices: [
                                {
                                  name: () => 'Go back',
                                  event: () => {
                                    this.wasteOfTime++
                                    this.goto(1, 0, 0)
                                  }
                                }
                              ]
                            }
                          ]
                        },
                        {
                          name: () => 'Continue down the road',
                          prompt: () => 'Down the road, you finally approach one house that is seemingly smaller and more worn down than the others. The front door is open, but nobody appears to be there.',
                          choices: [
                            {
                              name: () => 'Enter the house',
                              prompt: () => 'Upon entering, you begin to notice a strong smell of blood. After a short investigation, you realize the house is a crime scene: a body is visible on the bed of the only bedroom of the house, stabbed multiple times. You hear a sound outside, and as you turn around, a small group of Corsairs barge in and begin to interrogate you. "What did you do to this man?"',
                              choices: [
                                {
                                  name: () => 'I did not do it!',
                                  prompt: () => '"I\'m sure an honest citizen would know better than to enter a stranger\'s house and go check in the bedroom. Corsairs! Execute him!"',
                                  choices: [
                                    {
                                      name: () => 'Plead',
                                      event: () => {
                                        this.endingMessage = 'Before you know it, the Corsairs have drawn their weapons and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                        this.goto(0)
                                      }
                                    }
                                  ]
                                },
                                {
                                  name: () => 'What is happening here?',
                                  prompt: () => '"You sure do. An honest citizen would know better than to enter a stranger\'s house and go check in the bedroom. Corsairs! Execute him!"',
                                  choices: [
                                    {
                                      name: () => 'Plead',
                                      event: () => {
                                        this.endingMessage = 'Before you know it, the Corsairs have drawn their weapons and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                        this.goto(0)
                                      }
                                    }
                                  ]
                                },
                                {
                                  name: () => 'I\'m innocent!',
                                  prompt: () => '"I\'m sure an honest citizen would know better than to enter a stranger\'s house and go check in the bedroom. Corsairs! Execute him!"',
                                  choices: [
                                    {
                                      name: () => 'Plead',
                                      event: () => {
                                        this.endingMessage = 'Before you know it, the Corsairs have drawn their weapons and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                        this.goto(0)
                                      }
                                    }
                                  ]
                                },
                                {
                                  name: () => 'Who is that?',
                                  prompt: () => '"A loyal and brave citizen. Now, killer, prepare to pay the price of your crimes. Corsairs! Execute him!"',
                                  choices: [
                                    {
                                      name: () => 'Plead',
                                      event: () => {
                                        this.endingMessage = 'Before you know it, the Corsairs have drawn their weapons and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                        this.goto(0)
                                      }
                                    }
                                  ]
                                },
                                {
                                  name: () => 'I want to see the Queen',
                                  prompt: () => '"You killer! You will not see a minute more of your life. Corsairs! Execute him!"',
                                  choices: [
                                    {
                                      name: () => 'Plead',
                                      event: () => {
                                        this.endingMessage = 'Before you know it, the Corsairs have drawn their weapons and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                                        this.goto(0)
                                      }
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              name: () => 'Go back',
                              event: () => {
                                this.wasteOfTime++
                                this.goto(1, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Go back',
                          event: () => {
                            this.wasteOfTime++
                            this.goto(1, 0, 0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Exit the city',
                    }
                  ]
                },
                {
                  name: () => 'Flee',
                  event: () => {
                    this.endingMessage = 'You start running towards a white fog, losing track of your surroundings. After a short while, a galaxy form in front of you and the ground slowly gives way from under your feet. You are falling, but you don\'t feel the direction in which you are falling. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                    this.goto(0)
                  }
                }
              ]
            },
            {
              name: () => 'Tell a fake name',
              prompt: () => this.fakeNameCounter > 0 ? '"The Queen will not see you unless you tell your real name."' : '"I know this is not your real name, because this is not the name my Queen tells me should enter this place. You should be forever banned from entering the city, should the Queen not show mercy!"',
              event: () => {
                this.fakeNameCounter++
              },
              choices: [
                {
                  name: () => 'Say you\'re sorry and tell your real name',
                  prompt: () => '"I can only extend the simple courtesy of pity, but the Queen might not. Please visit her at your own risk."',
                  event: () => {
                    this.hasGivenName = true
                  },
                  choices: [
                    {
                      name: () => 'Enter the city',
                      event: () => {
                        this.goto(1, 0, 0)
                      }
                    }
                  ]
                },
                {
                  name: () => 'Say you\'re sorry but don\'t reveal your name',
                  event: () => {
                    this.fakeNameCounter++

                    if (this.fakeNameCounter > 5) {
                      this.endingMessage = 'Before you know it, the Awoken has drawn her weapon and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                      this.goto(0)
                    } else {
                      this.goto(1, 1)
                    }
                  }
                },
                {
                  name: () => 'Insist it is your real name',
                  event: () => {
                    this.fakeNameCounter++

                    if (this.fakeNameCounter > 5) {
                      this.endingMessage = 'Before you know it, the Awoken has drawn her weapon and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                      this.goto(0)
                    } else {
                      this.goto(1, 1)
                    }
                  }
                }
              ]
            },
            {
              name: () => 'Ask why you are being called "sister"',
              condition: () => !this.hasAskedCousin,
              event: () => {
                this.hasAskedCousin = true
                this.goto(1)
              }
            },
            {
              name: () => 'Ask where you are',
              condition: () => !this.hasAskedCity,
              event: () => {
                this.hasAskedCity = true
                this.goto(1)
              }
            }
          ]
        },
        {
          name: () => 'Ignore her and enter the city',
          prompt: () => '"You may only enter once you have told me your name."',
          choices: [
            {
              name: () => 'Enter the city anyway',
              event: () => {
                this.endingMessage = 'Before you know it, the Awoken has drawn her weapon and you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                this.goto(0)
              }
            },
            {
              name: () => 'Approach the Awoken',
              event: () => {
                this.goto(1)
              }
            },
            {
              name: () => 'Draw your weapon',
              event: () => {
                this.endingMessage = 'You draw your weapon and the Awoken does the same, but faster. Before you know it, you are pierced by bullets in your arms and torso. The light begins to fade from your vision. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
                this.goto(0)
              }
            }
          ]
        },
        {
          name: () => 'Flee',
          event: () => {
            this.endingMessage = 'You start running towards a white fog, losing track of your surroundings. After a short while, a galaxy form in front of you and the ground slowly gives way from under your feet. You are falling, but you don\'t feel the direction in which you are falling. You hear a faint voice in your ears trying to whisper something, but can\'t make out the words. You feel dizzy, and the whisper becomes many whispers. They intensify. All of a sudden, you hear a single word. You hear it with all the might with which it was pronounced, as if the whispers suddenly got along and said it in unison. {red}-Salvation-{/red}'
            this.goto(0)
          }
        }
      ]
    })
  }
}