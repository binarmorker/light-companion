import { Adventure } from './adventure'

export class DonotAdventure extends Adventure {
  endingMessage = ''
  successMessage = `{green}YOU WILL FIND SALVATION IN THE VALLEYS OF THE GARDEN, WHERE PLANTS OF DARKNESS BLOOM AND LIGHT HAS NO PURPOSE. SAVE THIS MELODY AND BE SAVED:\n\nBb A E G F# D E C{/green}\n\nYour Vex mind will find dissonance in the light and find redemption destroying it, like its peers have done in the Garden.\n`
  hasSeenImminentMind = false
  hasSeenPrimevalMind = false
  hasSeenEschatonMind = false
  isPast = false
  isFuture = false
  isBeginning = true
  isCompleted = (localStorage.getItem('donot-adventure-complete-2024-07') === 'true') || false

  reset() {
    this.endingMessage = ''
    this.hasSeenImminentMind = false
    this.hasSeenPrimevalMind = false
    this.hasSeenEschatonMind = false
    this.isPast = false
    this.isFuture = false
    this.isBeginning = true
  }

  constructor() {
    const leftMostLever = {
      name: () => 'Activate the left-most lever',
      prompt: () => 'As you activate the lever, the room around you becomes more and more lit, like the broken lights got repaired instantly. You feel as if new life got breathed into the room.',
      condition: () => !this.isPast && !(this.hasSeenImminentMind && this.hasSeenPrimevalMind && this.hasSeenEschatonMind),
      event: () => {
        this.isFuture = false
        this.isPast = true
      },
      choices: [
        {
          name: () => 'Exit the room',
          prompt: () => 'As you exit the room, the vegetation seems to be denser than before. The sky is clear and the energy is flowing around you. A fresh wind caresses your metallic hull.',
          choices: [
            {
              name: () => 'Go to the center of the garden',
              prompt: () => 'You go back to the center of the Garden.',
              choices: [
                {
                  name: () => 'Go north',
                  event: () => {
                    this.goto(2, 0)
                  }
                },
                {
                  name: () => 'Go west',
                  event: () => {
                    this.goto(2, 1)
                  }
                },
                {
                  name: () => 'Go east',
                  event: () => {
                    this.goto(2, 2)
                  }
                },
                {
                  name: () => 'Go south',
                  event: () => {
                    this.goto(2, 3)
                  }
                }
              ]
            }
          ]
        }
      ]
    }
  
    const centerLever = {
      name: () => 'Activate the center lever',
      prompt: () => `As you activate the lever, the room around you becomes mildly lit, like some of the lights were working and some not. You feel as if most life was ${this.isPast ? 'removed from' : 'returned to'} the room.`,
      condition: () => (this.isFuture || this.isPast) && !(this.hasSeenImminentMind && this.hasSeenPrimevalMind && this.hasSeenEschatonMind),
      event: () => {
        this.isFuture = false
        this.isPast = false
      },
      choices: [
        {
          name: () => 'Exit the room',
          prompt: () => 'As you exit the room, the vegetation seems to be sparser than before. The sky is dark and the energy is nowhere to be found. The stillness of the air is suffocating.',
          choices: [
            {
              name: () => 'Go to the center of the garden',
              prompt: () => 'You go back to the center of the Garden.',
              choices: [
                {
                  name: () => 'Go north',
                  event: () => {
                    this.goto(2, 0)
                  }
                },
                {
                  name: () => 'Go west',
                  event: () => {
                    this.goto(2, 1)
                  }
                },
                {
                  name: () => 'Go east',
                  event: () => {
                    this.goto(2, 2)
                  }
                },
                {
                  name: () => 'Go south',
                  event: () => {
                    this.goto(2, 3)
                  }
                }
              ]
            }
          ]
        }
      ]
    }
  
    const rightMostLever = {
      name: () => 'Activate the right-most lever',
      prompt: () => 'As you activate the lever, the room around you becomes more and more dark, like the working lights went out instantly. You feel as if all life got sucked out of the room.',
      condition: () => !this.isFuture && !(this.hasSeenImminentMind && this.hasSeenPrimevalMind && this.hasSeenEschatonMind),
      event: () => {
        this.isFuture = true
        this.isPast = false
      },
      choices: [
        {
          name: () => 'Exit the room',
          prompt: () => 'As you exit the room, the vegetation seems to be sparser than before. The sky is dark and the energy is nowhere to be found. The stillness of the air is suffocating.',
          choices: [
            {
              name: () => 'Go to the center of the garden',
              prompt: () => 'You go back to the center of the Garden.',
              choices: [
                {
                  name: () => 'Go north',
                  event: () => {
                    this.goto(2, 0)
                  }
                },
                {
                  name: () => 'Go west',
                  event: () => {
                    this.goto(2, 1)
                  }
                },
                {
                  name: () => 'Go east',
                  event: () => {
                    this.goto(2, 2)
                  }
                },
                {
                  name: () => 'Go south',
                  event: () => {
                    this.goto(2, 3)
                  }
                }
              ]
            }
          ]
        }
      ]
    }

    super({
      name: () => 'Donot\'s Adventure',
      prompt: () => 'You start dreaming of a beautiful garden, neatly separated by pillars of rock. A voice in your mind, or your mind itself, begins talking to you. It is unclear in its words and wanders in its dialogue.',
      choices: [
        {
          name: () => 'Ending',
          prompt: () => this.endingMessage,
          condition: () => !!this.endingMessage.length,
          choices: [
            {
              name: () => 'Restart',
              event: () => {
                this.reset()
                this.goto()
              }
            },
          ]
        },
        {
          name: () => 'Try to focus on the voice',
          prompt: () => 'The voice screams mindly caresses and cries angry thoughts. It is not intelligible and remains impossible to decipher.',
          choices: [
            {
              name: () => 'Try to focus on the voice',
              prompt: () => 'The voice screams mindly caresses and cries angry thoughts. It is not intelligible and remains impossible to decipher.',
              choices: [
                {
                  name: () => 'Try to focus on the voice',
                  prompt: () => 'The voice screams mindly caresses and cries angry thoughts. It is not intelligible and remains impossible to decipher.',
                  choices: [
                    {
                      name: () => 'Try to focus on the voice',
                      prompt: () => 'The voice materializes in front of you, as a simple Harpy. It speaks not in a language, but in a code. You understand the code perfectly.',
                      event: () => {
                        this.hasSeenImminentMind = true
                      },
                      choices: [
                        {
                          name: () => 'Introduce yourself to the Harpy',
                          prompt: () => 'The Harpy, immobile, responds to you in code again. You feel in yourself a query. It wants information.',
                          choices: [
                            {
                              name: () => 'Tell it about yourself',
                              prompt: () => 'The Harpy expulses a sound of contentment and tells you about itself. It is the Imminent Mind. It wants to warn you of what is to come.',
                              choices: [
                                {
                                  name: () => 'Tell it about the rest of your party',
                                  event: () => {
                                    this.goto(2, 0, 0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Tell it about your Vex companion, Zankem',
                              prompt: () => 'The Harpy seems to laugh and tells you about itself. It is the Imminent Mind. It wants to warn you of what is to come.',
                              choices: [
                                {
                                  name: () => 'Tell it about the rest of your party',
                                  event: () => {
                                    this.goto(2, 0, 0)
                                  }
                                }
                              ]
                            },
                            {
                              name: () => 'Tell it about the Light',
                              prompt: () => 'The Harpy spins in interest. It wants to know more.',
                              choices: [
                                {
                                  name: () => 'Show your Ghost',
                                  prompt: () => 'Your Ghost stays hidden, or is not there. You do not feel him near you, but don\'t long for him. You feel at peace with this knowledge that he might be elsewhere.',
                                  choices: [
                                    {
                                      name: () => 'Tell the Harpy more about the Light instead',
                                      event: () => {
                                        this.goto(2, 0, 0)
                                      }
                                    }
                                  ]
                                },
                                {
                                  name: () => 'Tell more about the Light',
                                  event: () => {
                                    this.goto(2, 0, 0)
                                  }
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      name: () => 'Try to ignore the voice',
                      event: () => {
                        this.goto(2)
                      }
                    }
                  ]
                },
                {
                  name: () => 'Try to ignore the voice',
                  event: () => {
                    this.goto(2)
                  }
                }
              ]
            },
            {
              name: () => 'Try to ignore the voice',
              event: () => {
                this.goto(2)
              }
            }
          ]
        },
        {
          name: () => 'Try to ignore the voice',
          prompt: () => 'You focus your attention on the garden and the voice goes away. The stillness of the Garden reminds you of simpler times, when purpose was not a concern. But you do not remember such a time, as it has never existed, and you have no memory of such a time.',
          choices: [
            {
              name: () => 'Go north',
              prompt: () => 'You see multiple rows of pillars, some cracked, some in beautiful shape. They are aligned with a mount in the distance, which you can see towering any structure or vegetation in the Garden.',
              choices: [
                {
                  name: () => 'Go forward',
                  prompt: () => `${!this.hasSeenImminentMind || !this.isBeginning ? 'While you move forward, you feel the scenery changing around you and closing in on the path you are taking. The more you walk, the more the branches, flowers and fresh earth give way to the rock beneath, smooth and carefully carved. You arrive at the top of the mount, or inside of it.' : 'You feel a tingly sensation through your metallic body, as if you were transmatted from your ship. You feel yourself displaced.' } You are in a circular room with lights in each cardinal direction and a pillar in the center.`,
                  event: () => {
                    this.isBeginning = false
                  },
                  choices: [
                    {
                      name: () => 'Approach the altar',
                      prompt: () => `On the altar are three levers, all of which are aligned on a line that seems to depict a chronology. You see what seems to be zeroes on the left side, and ones on the other. In the middle, a combination of them leaving much to be understood. A figure manifests behind the altar. ${this.isPast ? (this.hasSeenPrimevalMind ? 'It is the Primeval Mind again' : 'It is a Harpy. It presents itself as the Primeval Mind. It speaks in code, but you understand perfectly.') : this.isFuture ? (this.hasSeenEschatonMind ? 'It is the Eschaton Mind again' : 'It is a Harpy. It presents itself as the Eschaton Mind. It speaks in code, but you understand perfectly.') : (this.hasSeenImminentMind ? 'It is the Imminent Mind again' : 'It is a Harpy. It presents itself as the Imminent Mind. It speaks in code, but you understand perfectly.')}`,
                      choices: [
                        leftMostLever,
                        centerLever,
                        rightMostLever,
                        {
                          name: () => 'Ask the Imminent Mind what it wants',
                          prompt: () => 'The Imminent Mind responds that the present is inevitable. Whether you desire it or not, fate is imposed on you. It is curious however about your Light...',
                          condition: () => !this.isPast && !this.isFuture && !this.hasSeenImminentMind,
                          event: () => {
                            this.hasSeenImminentMind = true
                          },
                          choices: [
                            {
                              name: () => 'Check the altar',
                              condition: () => this.hasSeenPrimevalMind && this.hasSeenImminentMind && this.hasSeenEschatonMind,
                              event: () => {
                                this.endingMessage = 'As you look at the altar, its stone becomes all white. You look around to see a glass room with white ornaments. Upon exiting the room, glass shards replace the flowers and the rocks are now of the same white as the ornaments. Fractals replace the Garden\'s rivers, and the roots, plants and flowers are merely branches of the repeating mathematical pattern of the Vex. As the simulation grows in all directions, left, right, up, down, forward, backwards, past and future, your senses become agitated. The code in your mind becomes one with the Garden, and you hear musical notes. They repeat, endlessly, and you feel yourself faint while hearing a melody of {green}sevens{/green}... Of {green}sevens{/green}...'
                                this.isCompleted = true
                                localStorage.setItem('donot-adventure-complete-2024-07', 'true')
                                this.goto(0)
                              }
                            },
                            {
                              name: () => 'Check the altar',
                              condition: () => !(this.hasSeenPrimevalMind && this.hasSeenImminentMind && this.hasSeenEschatonMind),
                              event: () => {
                                this.goto(2, 0, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Ask the Primeval Mind what it wants',
                          prompt: () => 'The Primeval Mind responds that the past is to be explored, in memories, and studied, in litterature. Fate is determined by the actions and events of the past.',
                          condition: () => this.isPast && !this.hasSeenPrimevalMind,
                          event: () => {
                            this.hasSeenPrimevalMind = true
                          },
                          choices: [
                            {
                              name: () => 'Check the altar',
                              condition: () => this.hasSeenPrimevalMind && this.hasSeenImminentMind && this.hasSeenEschatonMind,
                              event: () => {
                                this.endingMessage = 'As you look at the altar, its stone becomes all white. You look around to see a glass room with white ornaments. Upon exiting the room, glass shards replace the flowers and the rocks are now of the same white as the ornaments. Fractals replace the Garden\'s rivers, and the roots, plants and flowers are merely branches of the repeating mathematical pattern of the Vex. As the simulation grows in all directions, left, right, up, down, forward, backwards, past and future, your senses become agitated. The code in your mind becomes one with the Garden, and you hear musical notes. They repeat, endlessly, and you feel yourself faint while hearing a melody of {green}sevens{/green}... Of {green}sevens{/green}...'
                                this.isCompleted = true
                                localStorage.setItem('donot-adventure-complete-2024-07', 'true')
                                this.goto(0)
                              }
                            },
                            {
                              name: () => 'Check the altar',
                              condition: () => !(this.hasSeenPrimevalMind && this.hasSeenImminentMind && this.hasSeenEschatonMind),
                              event: () => {
                                this.goto(2, 0, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Ask the Eschaton Mind what it wants',
                          prompt: () => 'The Eschaton Mind responds that the end is coming, at whatever rate it may be. But the end is predicatble, given we know all the variables that make fate.',
                          condition: () => this.isFuture && !this.hasSeenEschatonMind,
                          event: () => {
                            this.hasSeenEschatonMind = true
                          },
                          choices: [
                            {
                              name: () => 'Check the altar',
                              condition: () => this.hasSeenPrimevalMind && this.hasSeenImminentMind && this.hasSeenEschatonMind,
                              event: () => {
                                this.endingMessage = 'As you look at the altar, its stone becomes all white. You look around to see a glass room with white ornaments. Upon exiting the room, glass shards replace the flowers and the rocks are now of the same white as the ornaments. Fractals replace the Garden\'s rivers, and the roots, plants and flowers are merely branches of the repeating mathematical pattern of the Vex. As the simulation grows in all directions, left, right, up, down, forward, backwards, past and future, your senses become agitated. The code in your mind becomes one with the Garden, and you hear musical notes. They repeat, endlessly, and you feel yourself faint while hearing a melody of {green}sevens{/green}... Of {green}sevens{/green}...'
                                this.isCompleted = true
                                localStorage.setItem('donot-adventure-complete-2024-07', 'true')
                                this.goto(0)
                              }
                            },
                            {
                              name: () => 'Check the altar',
                              condition: () => !(this.hasSeenPrimevalMind && this.hasSeenImminentMind && this.hasSeenEschatonMind),
                              event: () => {
                                this.goto(2, 0, 0, 0)
                              }
                            }
                          ]
                        },
                        {
                          name: () => 'Exit the room',
                          prompt: () => 'As you try to exit the room, the altar beckons you back to accomplish something.',
                          choices: [
                            {
                              name: () => 'Go back to the altar',
                              event: () => {
                                this.goto(2, 0, 0, 0)
                              }
                            },
                            {
                              name: () => 'Exit anyway',
                              event: () => {
                                this.endingMessage = 'As you exit the room, you feel the way towards the Garden making you go further down, until the ground gives way. You fall into the infinite, butthe infinite feels like only a few seconds until you are trapped in an endless black you are unfamiliar with. Then, a cage of Vex fabrication traps you, and you see gunshots all around you. A vault in which you are now trapped, and Guardians fighting all around you everywhere. You pound on the edges of the cage, but can only hear yourself pounding and nothing from the outside. You see a huge Minotaur tear down the Guardians from each others, from time itself. Exerting vengeance. You feel your code being changed to a single digit, wiped from your mind. A single concept remains. {red}-Salvation-{/red}'
                                this.goto(0)
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      name: () => 'Exit the room',
                      prompt: () => 'As you try to exit the room, the altar beckons you back to accomplish something.',
                      choices: [
                        {
                          name: () => 'Go back to the altar',
                          event: () => {
                            this.goto(2, 0, 0, 0)
                          }
                        },
                        {
                          name: () => 'Exit anyway',
                          event: () => {
                            this.endingMessage = 'As you exit the room, you feel the way towards the Garden making you go further down, until the ground gives way. You fall into the infinite, butthe infinite feels like only a few seconds until you are trapped in an endless black you are unfamiliar with. Then, a cage of Vex fabrication traps you, and you see gunshots all around you. A vault in which you are now trapped, and Guardians fighting all around you everywhere. You pound on the edges of the cage, but can only hear yourself pounding and nothing from the outside. You see a huge Minotaur tear down the Guardians from each others, from time itself. Exerting vengeance. You feel your code being changed to a single digit, wiped from your mind. A single concept remains. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    }
                  ]
                },
                {
                  name: () => 'Go south-west',
                  event: () => {
                    this.goto(2, 1)
                  }
                },
                {
                  name: () => 'Go south-east',
                  event: () => {
                    this.goto(2, 2)
                  }
                },
                {
                  name: () => 'Go back',
                  prompt: () => 'You go back to the center of the Garden.',
                  choices: [
                    {
                      name: () => 'Go north',
                      event: () => {
                        this.goto(2, 0)
                      }
                    },
                    {
                      name: () => 'Go west',
                      event: () => {
                        this.goto(2, 1)
                      }
                    },
                    {
                      name: () => 'Go east',
                      event: () => {
                        this.goto(2, 2)
                      }
                    },
                    {
                      name: () => 'Go south',
                      event: () => {
                        this.goto(2, 3)
                      }
                    }
                  ]
                }
              ]
            },
            {
              name: () => 'Go west',
              prompt: () => this.isPast ? 'You see a beautiful lake of white fluid, overflowing and bubbling as new Vex come out of it in abundance. You watch them as they come out and instantly teleport away outside the Garden.' : this.isFuture ? 'You see an almost empty lake with a small pond of still white fluid at the bottom. What used to be a full reservoir of electric liquid is now an empty pit of long-gone potential.' : 'You see a beautiful lake of white fluid, reminding you of the liquid from which you are made. You never questionned it, and seeing so many here makes you feel like you should ask more questions.',
              choices: [
                {
                  name: () => 'Go forward',
                  prompt: () => this.isPast ? 'As you walk towards the lake, you feel the liquid grab at your feet lightly, wetting the metal on your frame and clinging to the footsteps you left behind. You arrive at an altar.' : this.isFuture ? 'As you walk towards the lake, you feel no connection to the liquid. It is stale, lifeless. You arrive at an altar.' : 'As you walk towards the lake, you see the liquid trying to reach out of the lake, bubbling slightly. You arrive at an altar.',
                  choices: [
                    {
                      name: () => 'Activate the left-most lever',
                      condition: () => !this.isPast,
                      event: () => {
                        this.isPast = true
                        this.isFuture = false
                        this.goto(2, 1)
                      }
                    },
                    {
                      name: () => 'Activate the center lever',
                      condition: () => this.isPast || this.isFuture,
                      event: () => {
                        this.isPast = false
                        this.isFuture = false
                        this.goto(2, 1)
                      }
                    }
                  ]
                },
                {
                  name: () => 'Go north-east',
                  event: () => {
                    this.goto(2, 0)
                  }
                },
                {
                  name: () => 'Go south-east',
                  event: () => {
                    this.goto(2, 3)
                  }
                },
                {
                  name: () => 'Go back',
                  prompt: () => 'You go back to the center of the Garden.',
                  choices: [
                    {
                      name: () => 'Go north',
                      event: () => {
                        this.goto(2, 0)
                      }
                    },
                    {
                      name: () => 'Go west',
                      event: () => {
                        this.goto(2, 1)
                      }
                    },
                    {
                      name: () => 'Go east',
                      event: () => {
                        this.goto(2, 2)
                      }
                    },
                    {
                      name: () => 'Go south',
                      event: () => {
                        this.goto(2, 3)
                      }
                    }
                  ]
                }
              ]
            },
            {
              name: () => 'Go east',
              prompt: () => this.isPast ? 'You watch into the distant Gardens and see fields of golden grain, lively red flowers and green leaves. The separations of the Garden are not visible at all under all the vegetation.' : this.isFuture ? 'You see desolated lads of rough dirt and desertified soil. The rock is visible throughout the Garden, as if left abandonned for thousands of years.' : 'You watch into the distant Gardens and see only fields of red flowers, manifesting thorns and more branches the more you look down in the crevisses separated by the giant walls of stone.',
              choices: [
                {
                  name: () => 'Go forward',
                  prompt: () => this.isPast ? 'As you walk among the vegetation, you feel yourself being watched from all sides. You continue navigating through the dense plantations until you bump into an altar.' : this.isFuture ? 'You walk on the desolate land, wanting to be reclaimed and used again. You feel a long lost presence that used to be there. You arrive at an altar.' : 'As you walk among the flowers, you feel yourself being watched. You continue navigating between the huge rivers of stone and emptyness until you find yourself trapped in a cul-de-sac. You arrive at an altar.',
                  choices: [
                    {
                      name: () => 'Activate the center lever',
                      condition: () => this.isPast || this.isFuture,
                      event: () => {
                        this.isPast = false
                        this.isFuture = false
                        this.goto(2, 2)
                      }
                    },
                    {
                      name: () => 'Activate the right-most lever',
                      condition: () => !this.isFuture,
                      event: () => {
                        this.isPast = false
                        this.isFuture = true
                        this.goto(2, 2)
                      }
                    }
                  ]
                },
                {
                  name: () => 'Go north-west',
                  event: () => {
                    this.goto(2, 0)
                  }
                },
                {
                  name: () => 'Go south-west',
                  event: () => {
                    this.goto(2, 3)
                  }
                },
                {
                  name: () => 'Go back',
                  prompt: () => 'You go back to the center of the Garden.',
                  choices: [
                    {
                      name: () => 'Go north',
                      event: () => {
                        this.goto(2, 0)
                      }
                    },
                    {
                      name: () => 'Go west',
                      event: () => {
                        this.goto(2, 1)
                      }
                    },
                    {
                      name: () => 'Go east',
                      event: () => {
                        this.goto(2, 2)
                      }
                    },
                    {
                      name: () => 'Go south',
                      event: () => {
                        this.goto(2, 3)
                      }
                    }
                  ]
                }
              ]
            },
            {
              name: () => 'Go south',
              prompt: () => 'You see a cylindrical pit in which you can barely see the bottom. The only light emanating from the hole is a faint collection of shining lights, slowly moving in a predictable rhythm.',
              choices: [
                {
                  name: () => 'Go forward',
                  prompt: () => 'As you advance, you find yourself at the edge of the immense schism. You see huge plants at the bottom emitting a few lights, waiting for you to land on them.',
                  choices: [
                    {
                      name: () => 'Jump in the hole',
                      event: () => {
                        this.endingMessage = 'As you fall into the hole, the flowers disappear from view and close upon themselves to show only the black emptyness beneath them. The infinite feels like only a few seconds until you are trapped in an endless black you are unfamiliar with. Then, a cage of Vex fabrication traps you, and you see gunshots all around you. A vault in which you are now trapped, and Guardians fighting all around you everywhere. You pound on the edges of the cage, but can only hear yourself pounding and nothing from the outside. You see a huge Minotaur tear down the Guardians from each others, from time itself. Exerting vengeance. You feel your code being changed to a single digit, wiped from your mind. A single concept remains. {red}-Salvation-{/red}'
                        this.goto(0)
                      }
                    },
                    {
                      name: () => 'Go back',
                      event: () => {
                        this.goto(2, 3)
                      }
                    }
                  ]
                },
                {
                  name: () => 'Go north-west',
                  event: () => {
                    this.goto(2, 1)
                  }
                },
                {
                  name: () => 'Go north-east',
                  event: () => {
                    this.goto(2, 2)
                  }
                },
                {
                  name: () => 'Go back',
                  prompt: () => 'You go back to the center of the Garden.',
                  choices: [
                    {
                      name: () => 'Go north',
                      event: () => {
                        this.goto(2, 0)
                      }
                    },
                    {
                      name: () => 'Go west',
                      event: () => {
                        this.goto(2, 1)
                      }
                    },
                    {
                      name: () => 'Go east',
                      event: () => {
                        this.goto(2, 2)
                      }
                    },
                    {
                      name: () => 'Go south',
                      event: () => {
                        this.goto(2, 3)
                      }
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
  }
}