import { Adventure } from './adventure'

export class Jason7Adventure extends Adventure {
  endingMessage = ''
  successMessage = `{green}YOU WILL FIND SALVATION IN THE VALLEYS OF THE GARDEN, WHERE PLANTS OF DARKNESS BLOOM AND LIGHT HAS NO PURPOSE. SAVE THIS MELODY AND BE SAVED:\n\nBb A E G F# D E C{/green}\n\nYour Exo mind is smothering your memories. Revel in the dark and find them anew.\n`
  isDonotAlive = true
  isZankemAlive = true
  hasTalkedToLexy = false
  isLexyAlive = true
  isJayceAlive = true
  isNexus6Alive = true
  isBobbyTCrewAlive = true
  isCompleted = (localStorage.getItem('jason-7-adventure-complete-2024-07') === 'true') || false

  reset() {
    this.endingMessage = ''
    this.isDonotAlive = true
    this.isZankemAlive = true
    this.hasTalkedToLexy = false
    this.isLexyAlive = true
    this.isJayceAlive = true
    this.isNexus6Alive = true
    this.isBobbyTCrewAlive = true
  }

  constructor() {
    super({
      name: () => 'Jason-7\'s Adventure',
      prompt: () => 'You start dreaming of a tower reaching to the sky. You see a lot of people in your way to reach it. In front of you are your two Vex companions: Donot and Zankem. They seem hostile and are about to draw their weapons.',
      choices: [
        {
          name: () => 'Ending',
          prompt: () => this.endingMessage,
          condition: () => !!this.endingMessage.length,
          choices: [
            {
              name: () => 'Restart',
              event: () => {
                this.reset()
                this.goto()
              }
            }
          ]
        },
        {
          name: () => 'Kill Donot',
          prompt: () => 'As you assassinate Donot bare-handed and crack his neck, Zankem attacks and you suffer heavy damage. You pick up a scout rifle.',
          condition: () => this.isDonotAlive,
          event: () => {
            this.isDonotAlive = false
            if (this.isZankemAlive) this.goto()
            else if (this.isLexyAlive) this.goto(4)
            else this.goto(4, 0, 0)
          }
        },
        {
          name: () => 'Kill Zankem',
          prompt: () => 'As you assassinate Zankem bare-handed and rip out his eye, Donot attacks and you suffer light damage. You pick up an auto rifle.',
          condition: () => this.isZankemAlive,
          event: () => {
            this.isZankemAlive = false
            if (this.isDonotAlive) this.goto()
            else if (this.isLexyAlive) this.goto(4)
            else this.goto(4, 0, 0)
          }
        },
        {
          name: () => 'Ignore them',
          event: () => {
            if (this.isZankemAlive && this.isDonotAlive) {
              this.endingMessage = 'Zankem and Donot both pull out their weapons and attack you. You suffer immense damage and cannot move. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
            } else if (this.isZankemAlive) {
              this.endingMessage = 'Zankem pulls out his weapon and attacks you. You suffer immense damage and cannot move. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
            } else {
              this.endingMessage = 'Donot pulls out his weapon and attacks you. You suffer immense damage and cannot move. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
            }
            this.goto(0)
          }
        },
        {
          name: () => 'Flee',
          prompt: () => this.hasTalkedToLexy ? 'She starts talking but only manages to say a few distraught, unintelligible words.' : 'You continue for a while in the snowy landscape and see a few familiar faces go by. You encounter Lexy, who is reaching for her weapon.',
          event: () => {
            if (!this.isLexyAlive) {
              this.goto(4, 0, 0)
            }
          },
          choices: [
            {
              name: () => 'Kill Lexy',
              prompt: () => 'You shoot with your newfound weapon and kill Lexy, letting her expire and say her last words: "I only wanted to please the Queen..."',
              event: () => {
                this.isLexyAlive = false
              },
              choices: [
                {
                  name: () => 'Continue towards the tower',
                  prompt: () => (this.isJayceAlive || this.isNexus6Alive || this.isBobbyTCrewAlive || this.isDonotAlive || this.isZankemAlive) ? 'A lot of familiar people are on your way to the tower. You are unable to proceed this way unless you fight them.': 'The path to the tower is a lot less crowded now...',
                  choices: [
                    {
                      name: () => 'Continue towards the tower',
                      prompt: () => 'You continue towards the tower and see a person you do not recognize looking at you from afar. You never made it this far before, so the sight startles you. The person raises their arm with a hand cannon in their hand.',
                      event: () => {
                        if (this.isJayceAlive || this.isNexus6Alive || this.isBobbyTCrewAlive) {
                          this.goto(4, 0, 0)
                        }
                      },
                      choices: [
                        {
                          name: () => 'Race towards the person',
                          event: () => {
                            this.endingMessage = 'The person pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Insult the person',
                          event: () => {
                            this.endingMessage = 'The person pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Try to bargain',
                          event: () => {
                            this.endingMessage = 'The person pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Tell the person you are lost',
                          event: () => {
                            this.endingMessage = 'The person shouts at you from a distance: "The reality is, Jason, we all are. We are all lost in this universe. The meaning of this life is naught, and you and your memories will die when coming back to this tower. The real Jason is dead the same way his memories are dead. You are only a shell of your former self. May we never meet again, Clovis\'s puppet."\nThe person\'s shape slightly warps as you move towards them. A bullet hits you from the back: Lexy killed you with a scout rifle from afar, while bleeding on the ground. You have misjudged your opponent, once again. Maybe next time... You will find more time to study... You hear one last word, coming as a long, slow whisper, from a voice unknown: {green}-Seven-{green}'
                            this.isCompleted = true
                            localStorage.setItem('jason-7-adventure-complete-2024-07', 'true')
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Ask about the tower',
                          event: () => {
                            this.endingMessage = 'The person pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Ask about this dream',
                          event: () => {
                            this.endingMessage = 'The person pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => `Go back to your other companion${this.isDonotAlive && this.isZankemAlive ? 's' : ''}`,
                      condition: () => this.isDonotAlive || this.isZankemAlive,
                      event: () => {
                        this.goto()
                      }
                    },
                    {
                      name: () => 'Fight Bobby T. Crew',
                      prompt: () => 'Bobby is badly hit by your weapon and barely keeps his stance. He throws around a snark remark towards you: "500 Glimmer you won\'t."',
                      condition: () => this.isBobbyTCrewAlive,
                      choices: [
                        {
                          name: () => 'Make a snark remark and kill him',
                          event: () => {
                            this.isBobbyTCrewAlive = false

                            if ((!this.isJayceAlive && !this.isNexus6Alive && !this.isBobbyTCrewAlive) && (this.isDonotAlive || this.isZankemAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Kill him without a word',
                          event: () => {
                            this.isBobbyTCrewAlive = false
                            
                            if ((!this.isJayceAlive && !this.isNexus6Alive && !this.isBobbyTCrewAlive) && (this.isDonotAlive || this.isZankemAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Spare him',
                          event: () => {
                            this.endingMessage = 'Bobby T. Crew raises his shotgun and kills you instantly. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Fight Jayce',
                      prompt: () => 'Jayce fails to your weapon and drops to their knees. He utters some choice words toward you and asks what it is that you wanted to achieve.',
                      condition: () => this.isJayceAlive,
                      choices: [
                        {
                          name: () => 'Make a snark remark and kill him',
                          event: () => {
                            this.isJayceAlive = false
                            
                            if ((!this.isJayceAlive && !this.isNexus6Alive && !this.isBobbyTCrewAlive) && (this.isDonotAlive || this.isZankemAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Kill him without a word',
                          event: () => {
                            this.isJayceAlive = false
                            
                            if ((!this.isJayceAlive && !this.isNexus6Alive && !this.isBobbyTCrewAlive) && (this.isDonotAlive || this.isZankemAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Spare him',
                          event: () => {
                            this.endingMessage = 'Jayce sneakily draws his sword and pierces your chest, killing you. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Fight Nexus-6',
                      prompt: () => 'Nexus-6 falls down and lies falteringly before you. You hear his voice cracking as his last words echo: "What did I do to deserve this?"',
                      condition: () => this.isNexus6Alive,
                      choices: [
                        {
                          name: () => 'Make a snark remark and kill him',
                          event: () => {
                            this.isNexus6Alive = false
                            
                            if ((!this.isJayceAlive && !this.isNexus6Alive && !this.isBobbyTCrewAlive) && (this.isDonotAlive || this.isZankemAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Kill him without a word',
                          event: () => {
                            this.isNexus6Alive = false
                            
                            if ((!this.isJayceAlive && !this.isNexus6Alive && !this.isBobbyTCrewAlive) && (this.isDonotAlive || this.isZankemAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Spare him',
                          event: () => {
                            this.endingMessage = 'While waiting for Nexus-6 to rise up, the cold engulfs you and you freeze to death. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              name: () => 'Talk to Lexy',
              condition: () => !this.hasTalkedToLexy,
              event: () => {
                this.hasTalkedToLexy = true
                this.goto(4)
              }
            },
            {
              name: () => 'Ignore her',
              event: () => {
                this.endingMessage = 'Lexy draws her submachine gun and shoots you in the chest, killing you. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                this.goto(0)
              }
            }
          ]
        }
      ]
    })
  }
}