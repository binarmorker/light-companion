import { Adventure } from './adventure'

export class Nexus6Adventure extends Adventure {
  endingMessage = ''
  successMessage = `{green}YOU WILL FIND SALVATION IN THE VALLEYS OF THE GARDEN, WHERE PLANTS OF DARKNESS BLOOM AND LIGHT HAS NO PURPOSE. SAVE THIS MELODY AND BE SAVED:\n\nBb A E G F# D E C{/green}\n\nYour Exo mind is smothering your memories. Revel in the dark and find them anew.\n`
  isLexyAlive = true
  isBasilAlive = true
  isDonotAlive = true
  isZankemAlive = true
  hasTalkedToJayce = false
  isJayceAlive = true
  isJason7Alive = true
  isBobbyTCrewAlive = true
  isCompleted = (localStorage.getItem('nexus-6-adventure-complete-2024-07') === 'true') || false

  reset() {
    this.endingMessage = ''
    this.isLexyAlive = true
    this.isBasilAlive = true
    this.isDonotAlive = true
    this.isZankemAlive = true
    this.hasTalkedToJayce = false
    this.isJayceAlive = true
    this.isJason7Alive = true
    this.isBobbyTCrewAlive = true
  }

  constructor() {
    super({
      name: () => 'Nexus-6\'s Adventure',
      prompt: () => 'You start dreaming of a tower reaching to the sky. You see a lot of people in your way to reach it. In front of you is your old fireteam: Lexy and Basil. They seem hostile and are about to draw their weapons.',
      choices: [
        {
          name: () => 'Ending',
          prompt: () => this.endingMessage,
          condition: () => !!this.endingMessage.length,
          choices: [
            {
              name: () => 'Restart',
              event: () => {
                this.reset()
                this.goto()
              }
            }
          ]
        },
        {
          name: () => 'Kill Lexy',
          prompt: () => 'As you assassinate Lexy bare-handed and crack her neck, Basil attacks and you suffer heavy damage. You pick up a scout rifle.',
          condition: () => this.isLexyAlive,
          event: () => {
            this.isLexyAlive = false
            if (this.isBasilAlive) this.goto()
            else if (this.isJayceAlive) this.goto(4)
            else this.goto(4, 0, 0)
          }
        },
        {
          name: () => 'Kill Basil',
          prompt: () => 'As you assassinate Basil bare-handed and punch him out, Lexy attacks and you suffer light damage. You pick up an auto rifle.',
          condition: () => this.isBasilAlive,
          event: () => {
            this.isBasilAlive = false
            if (this.isLexyAlive) this.goto()
            else if (this.isJayceAlive) this.goto(4)
            else this.goto(4, 0, 0)
          }
        },
        {
          name: () => 'Ignore them',
          event: () => {
            if (this.isLexyAlive && this.isBasilAlive) {
              this.endingMessage = 'Lexy and Basil both pull out their weapons and attack you. You suffer immense damage and cannot move. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
            } else if (this.isLexyAlive) {
              this.endingMessage = 'Lexy pulls out her weapon and attacks you. You suffer immense damage and cannot move. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
            } else {
              this.endingMessage = 'Basil pulls out his weapon and attacks you. You suffer immense damage and cannot move. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
            }
            this.goto(0)
          }
        },
        {
          name: () => 'Flee',
          prompt: () => this.hasTalkedToJayce ? 'He starts talking but only manages to say a few distraught, unintelligible words.' : 'You continue for a while in the snowy landscape and see a few familiar faces go by. You encounter Jayce, who is reaching for his weapon.',
          event: () => {
            if (!this.isJayceAlive) {
              this.goto(4, 0, 0)
            }
          },
          choices: [
            {
              name: () => 'Kill Jayce',
              prompt: () => 'You shoot with your newfound weapon and kill Jayce, letting him expire and say his last words: "I only wanted to please the Queen..."',
              event: () => {
                this.isJayceAlive = false
              },
              choices: [
                {
                  name: () => 'Continue towards the tower',
                  prompt: () => (this.isLexyAlive || this.isBasilAlive || this.isZankemAlive || this.isDonotAlive || this.isBobbyTCrewAlive) ? 'A lot of familiar people are on your way to the tower. You are unable to proceed this way unless you fight them.': 'The path to the tower is a lot less crowded now...',
                  choices: [
                    {
                      name: () => 'Continue towards the tower',
                      prompt: () => 'You continue towards the tower and see a hunter like you looking at you from afar. You never made it this far before, so the sight startles you. The hunter raises their arm with a hand cannon in their hand. You recognize them: it\'s Jason-7.',
                      event: () => {
                        if (this.isZankemAlive || this.isDonotAlive || this.isBobbyTCrewAlive) {
                          this.goto(4, 0, 0)
                        }
                      },
                      choices: [
                        {
                          name: () => 'Race towards Jason-7',
                          event: () => {
                            this.endingMessage = 'Jason-7 pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Shoot Jason-7',
                          event: () => {
                            this.endingMessage = 'Jason-7 pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Try to bargain',
                          event: () => {
                            this.endingMessage = 'Jason-7 pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Tell Jason-7 you are lost',
                          event: () => {
                            this.endingMessage = 'Jason-7 shouts at you from a distance: "The reality is, Nexus, we all are. We are all lost in this universe. The meaning of this life is naught, and you and your memories will die when coming back to this tower. The real Jason is dead the same way his memories are dead. You are only a shell of your former self. May we never meet again, Clovis\'s puppet."\nJason-7\'s shape slightly warps as you move towards them. A bullet hits you from the back: Lexy killed you with a scout rifle from afar, while bleeding on the ground. You have been betrayed by the ones you once called your friends... You hear one last word, coming as a long, slow whisper, from a voice unknown: -Seven-'
                            this.isCompleted = true
                            localStorage.setItem('nexus-6-adventure-complete-2024-07', 'true')
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Ask about the tower',
                          event: () => {
                            this.endingMessage = 'Jason-7 pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        },
                        {
                          name: () => 'Ask about this dream',
                          event: () => {
                            this.endingMessage = 'Jason-7 pulls the trigger and you immediately see black. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => `Go back to your other companion${this.isLexyAlive && this.isBasilAlive ? 's' : ''}`,
                      condition: () => this.isLexyAlive || this.isBasilAlive,
                      event: () => {
                        this.goto()
                      }
                    },
                    {
                      name: () => 'Fight Bobby T. Crew',
                      prompt: () => 'Bobby is badly hit by your weapon and barely keeps his stance. He throws around a snark remark towards you: "500 Glimmer you won\'t."',
                      condition: () => this.isBobbyTCrewAlive,
                      choices: [
                        {
                          name: () => 'Make a snark remark and kill him',
                          event: () => {
                            this.isBobbyTCrewAlive = false

                            if ((!this.isZankemAlive && !this.isDonotAlive && !this.isBobbyTCrewAlive) && (this.isLexyAlive || this.isBasilAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Kill him without a word',
                          event: () => {
                            this.isBobbyTCrewAlive = false
                            
                            if ((!!this.isZankemAlive && !this.isDonotAlive && !this.isBobbyTCrewAlive) && (this.isLexyAlive || this.isBasilAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Spare him',
                          event: () => {
                            this.endingMessage = 'Bobby T. Crew raises his shotgun and kills you instantly. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Fight Donot',
                      prompt: () => 'Donot fails to your weapon and drops to their knees. He utters some choice words toward you and asks what it is that you wanted to achieve.',
                      condition: () => this.isDonotAlive,
                      choices: [
                        {
                          name: () => 'Make a snark remark and kill him',
                          event: () => {
                            this.isDonotAlive = false
                            
                            if ((!this.isZankemAlive && !this.isDonotAlive && !this.isBobbyTCrewAlive) && (this.isLexyAlive || this.isBasilAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Kill him without a word',
                          event: () => {
                            this.isDonotAlive = false
                            
                            if ((!this.isZankemAlive && !this.isDonotAlive && !this.isBobbyTCrewAlive) && (this.isLexyAlive || this.isBasilAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Spare him',
                          event: () => {
                            this.endingMessage = 'Donot sneakily draws his dagger and pierces your chest, killing you. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    },
                    {
                      name: () => 'Fight Zankem',
                      prompt: () => 'Zankem falls down and lies falteringly before you. You hear his voice cracking as his last words echo: "What did I do to deserve this?"',
                      condition: () => this.isZankemAlive,
                      choices: [
                        {
                          name: () => 'Make a snark remark and kill him',
                          event: () => {
                            this.isZankemAlive = false
                            
                            if ((!this.isZankemAlive && !this.isDonotAlive && !this.isBobbyTCrewAlive) && (this.isLexyAlive || this.isBasilAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Kill him without a word',
                          event: () => {
                            this.isZankemAlive = false
                            
                            if ((!this.isZankemAlive && !this.isDonotAlive && !this.isBobbyTCrewAlive) && (this.isLexyAlive || this.isBasilAlive)) {
                              this.endingMessage = 'One of your old companions that you spared before shoots you in the back, making you bleed out and lose consciousness. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                              this.goto(0)
                            } else {
                              this.goto(4, 0, 0)
                            }
                          }
                        },
                        {
                          name: () => 'Spare him',
                          event: () => {
                            this.endingMessage = 'While waiting for Zankem to rise up, the cold engulfs you and you freeze to death. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                            this.goto(0)
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              name: () => 'Talk to Jayce',
              condition: () => !this.hasTalkedToJayce,
              event: () => {
                this.hasTalkedToJayce = true
                this.goto(4)
              }
            },
            {
              name: () => 'Ignore him',
              event: () => {
                this.endingMessage = 'Jayce draws his submachine gun and shoots you in the chest, killing you. The dream ends on a long, slow whisper, saying many unintelligible words. Then, a single word, stretched out but resonating. {red}-Salvation-{/red}'
                this.goto(0)
              }
            }
          ]
        }
      ]
    })
  }
}