import { FauxTerm } from './fauxTerm'

export function rasputinCommands () {
  const rasputinSequence = [
    0x4c,0x6e,0x20,0x74,0x6a,0x68,0x64,0x6d,0x65,0x20,
    0x6a,0x75,0x6a,0x20,0x61,0x75,0x20,0x65,0x63,0x61,
    0x76,0x2e,0x20,0x55,0x66,0x74,0x20,0x63,0x67,0x61,
    0x67,0x69,0x75,0x75,0x69,0x63,0x68,0x76,0x20,0x72,
    0x6a,0x73,0x77,0x63,0x6e,0x62,0x6d,0x79,0x63,0x65,
    0x2e,0x20,0x4d,0x63,0x20,0x67,0x78,0x61,0x66,0x72,
    0x67,0x77,0x20,0x67,0x79,0x6f,0x6d,0x79,0x76,0x2e,
  ]
  const players = [
    'hHNYmUxJPOTNNt2ippB58Uz7xbW2', // Nicolas
    'ZnaIuD5DOYX7NjmbpJbr9q0N2Gd2', // Ian
    'CeS5jTWBXiVtdieOHpXxPPYeGgq1', // Carolanne
    '20qXkC67l6V3rk6gLyHK3US7Kjx2', // Ralph
    '92VAuNpqeVdSJs6gwfPMRL8PucU2', // Bastien
    'Bn4z87N9TKccmmO5sBEePXWami53', // Miguel
    'hffTGTWeEuURun4FPTxgOTOWQqO2', // Samuel
  ]

  function decode() {
    let result = ''
    const key = 'rasputin'

    for (let i = 0, j = 0; i < rasputinSequence.length; i++) {
      const c = String.fromCharCode(rasputinSequence[i])

      if (rasputinSequence[i] >= 65 && rasputinSequence[i] <= 90) {
        const upperLetter = ((rasputinSequence[i] - 65) - (key[j % key.length].toUpperCase().charCodeAt(0) - 65) + 26) % 26
        result += String.fromCharCode(upperLetter + 65)
        j++
      } else if (rasputinSequence[i] >= 97 && rasputinSequence[i] <= 122) {
        const lowerLetter = ((rasputinSequence[i] - 97) - (key[j % key.length].toLowerCase().charCodeAt(0) - 97) + 26) % 26
        result += String.fromCharCode(lowerLetter + 97)
        j++
      } else {
        result += c
      }
    }

    return 'Message decrypted from keys [0..6] with cryptographic key "rasputin"\n' +
           '{green}>>>{/green} {bold}' + result + ' {/bold}{green}<<<{/green}\n'
  }

  function key(uid: string) {
    if (uid === 'R18G8LHh0SfHeLY1TI5LvnlS6rH3' && window.__EMULATED_USER_ID__) uid = window.__EMULATED_USER_ID__

    if (players.includes(uid)) {
      const index = players.indexOf(uid)
      const offset = index * 10
      return index + ': ' + rasputinSequence.slice(offset, offset + 10).map(x => x.toString(16)).join(', ') + '\n' +
             'You may need inputs from other users to continue...\n'
    }

    return 'Unauthorized Access\n'
  }

  function help() {
    return 'Auxiliary Codhook System\n' +
           '========================\n' +
           '- help: List commands\n' +
           '- cd: Change directories\n' +
           '- ls: List files\n' +
           '- view: View the contents of a file\n' +
           '- clear: Empty terminal output\n' +
           '- key: Get your user-assigned unique key\n' +
           '- decode: Decode synchronized user keys\n'
  }

  function ls(directory: string) {
    if (!directory) directory = '.'
    if (['.', '/', './', '..', '../'].includes(directory)) {
      return 'Directory Listing of ' + directory + '\n' +
             '======================= ====\n' +
             'cd.run                 | 755\n' +
             'clear.run              | 755\n' +
             'decode.run             | 755\n' +
             'help.run               | 755\n' +
             'key.run                | 755\n' +
             'ls.run                 | 755\n' +
             'rasputin.key           | 644\n' +
             'view.run               | 755\n'
    }

    return 'Unknown directory\n'
  }

  function cd() {
    return 'Unknown directory\n'
  }

  function view(file: string) {
    if (file === 'rasputin.key') return 'rasputin\nEND OF FILE\n'
    return 'Unknown file\n'
  }

  return {
    key,
    help,
    ls,
    cd,
    view,
    decode
  }
}

export class RasputinTerm extends FauxTerm {
  constructor(el: HTMLElement, userId: string) {
    const commands = rasputinCommands()

    super({
      el,
      cwd: '{green}guest@rasputin:/{white}',
      cmd (argv: string[], argc: number) {
        if (argv[0].endsWith('.run')) {
          return 'Use the command without .run to execute it.\n'
        }

        switch (argv[0]) {
          case 'help':
            return commands.help()
          case 'ls':
            return commands.ls(argv[1])
          case 'cd':
            return commands.cd()
          case 'view':
            return commands.view(argv[1])
          case 'decode':
            return commands.decode()
          case 'key':
            return commands.key(userId)
        }

        return 'error'
      },
      tags: ['red', 'green', 'white', 'bold', 'blue']
    })
  }
}
