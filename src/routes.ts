import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/pages/HomePage.vue'
import Guardian from '@/pages/GuardianPage.vue'
import Join from '@/pages/JoinPage.vue'
import Architect from '@/pages/ArchitectPage.vue'
import Terminal from './pages/TerminalPage.vue'
import Bestiary from '@/pages/BestiaryPage.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      name: 'Home',
      path: '/',
      component: Home
    },
    {
      name: 'Guardian',
      path: '/:id',
      component: Guardian
    },
    {
      name: 'Join',
      path: '/join/:id',
      component: Join
    },
    {
      name: 'Architect',
      path: '/architect',
      component: Architect
    },
    {
      name: 'Terminal',
      path: '/_',
      component: Terminal
    },
    {
      name: 'Bestiary',
      path: '/bestiary',
      component: Bestiary
    }
  ]
})

export default router
