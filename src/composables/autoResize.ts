import { onBeforeUnmount, onMounted, Ref } from 'vue'

export function useAutoResize(component: Ref) {
  if (!component) return

  const listener = () => {
    if (!component.value) return

    component.value.style.height = 'auto'
    component.value.style.height = component.value.scrollHeight + 'px'
  }

  onMounted(() => {
    listener()
    component.value?.addEventListener('input', listener)
  })
  onBeforeUnmount(() => {
    component.value?.removeEventListener('input', listener)
  })

  return { listener }
}

