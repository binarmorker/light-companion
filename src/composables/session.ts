import { Guardian, Session, SessionChatEntry, SessionEncounterEntry, UserData } from '@/models/destiny'
import { computed, ref, watch } from 'vue'
import { sessionsRef, usersRef } from '@/firebase'
import { addDoc, collection, deleteDoc, doc, orderBy, query, updateDoc } from 'firebase/firestore'
import { useCollection, useCurrentUser, useDocument } from 'vuefire'

const currentSession = ref<Session>()

export function useSession() {
  const user = useCurrentUser()
  const userDataRef = computed(() => user.value ? doc(usersRef, user.value?.uid) : null)
  const userData = useDocument<UserData>(userDataRef)
  const displayName = computed(() => userData.value?.displayName || user.value?.displayName)
  const sessionId = ref<string | null>(sessionStorage.getItem('light-session'))
  const sessionRef = computed(() => sessionId.value ? doc(sessionsRef, sessionId.value) : null)
  const currentSessionRef = useDocument<Session>(sessionRef, { reset: true })

  const sessionLink = computed(() => currentSession.value ? `https://light.binar.ca/join/${currentSession.value?.id}` : null)
  const currentPlayer = computed(() => user.value && currentSession.value && currentSession.value.members.find(x => x.player === user.value?.uid))
  const isCurrentAuthor = computed(() => user.value && currentSession.value && user.value.uid === currentSession.value.author)
  
  const chatRef = computed(() => sessionRef.value ? collection(sessionRef.value.firestore, sessionRef.value.path, 'chat') : null)
  const chatQuery = computed(() => user.value && chatRef.value ? query(chatRef.value, orderBy('date', 'desc')) : null)
  const chat = useCollection<SessionChatEntry>(chatQuery, { ssrKey: 'chat' })
  
  const encountersRef = computed(() => sessionRef.value ? collection(sessionRef.value.firestore, sessionRef.value.path, 'encounters') : null)
  const encounters = useCollection<SessionEncounterEntry>(encountersRef)

  async function create(name: string) {
    if (user.value) {
      const newDoc = await addDoc(sessionsRef, {
        name: name,
        privacy: 'private',
        author: user.value.uid,
        authorName: displayName.value,
        members: []
      } as Session)
      sessionStorage.setItem('light-session', newDoc.id)
      sessionId.value = newDoc.id
    }
  }
  
  async function join(id: string, character?: Guardian) {
    sessionStorage.setItem('light-session', id)
    sessionId.value = id

    if (user.value && sessionRef.value && currentSession.value && character) {
      const memberList = [
        ...currentSession.value.members.filter(x => x.player !== user.value?.uid),
        {
          player: user.value.uid,
          playerName: displayName.value as string,
          character: character.id
        }
      ]

      await updateDoc(sessionRef.value, {
        members: memberList
      })
    }
  }
  
  async function quit() {
    if (sessionRef.value && currentSession.value) {
      await updateDoc(sessionRef.value, {
        members: currentSession.value.members.filter(x => x.player !== user.value?.uid)
      })
      sessionStorage.removeItem('light-session')
      sessionId.value = null
    }
  }
  
  async function end() {
    if (sessionRef.value && currentSession.value && currentSession.value.author === user.value?.uid) {
      await updateDoc(sessionRef.value, {
        privacy: 'deleted'
      })
      sessionStorage.removeItem('light-session')
      sessionId.value = null
    }
  }

  async function sendChat(entry: SessionChatEntry) {
    if (chatRef.value && entry) {
      await addDoc(chatRef.value, entry)
    }
  }

  async function deleteChat(id: string) {
    if (chatRef.value && id) {
      const messageRef = doc(chatRef.value, id)
      await deleteDoc(messageRef)
    }
  }

  async function createEncounter(name: string) {
    if (user.value && sessionRef.value && encountersRef.value) {
      await addDoc(encountersRef.value, {
        name,
        monsters: []
      })
    }
  }

  async function updateEncounter(data: SessionEncounterEntry & { id: string }) {
    if (user.value && sessionRef.value && encountersRef.value) {
      const encounterRef = doc(encountersRef.value, data.id)
      await updateDoc(encounterRef, data)
    }
  }

  async function deleteEncounter(id: string) {
    if (user.value && sessionRef.value && encountersRef.value) {
      const encounterRef = doc(encountersRef.value, id)
      await deleteDoc(encounterRef)
    }
  }

  watch(currentSessionRef, value => {
    currentSession.value = value as Session
  })
  
  watch(() => currentSession.value?.privacy, async (value: 'private' | 'public' | 'deleted' | undefined) => {
    if (value === 'deleted') {
      await quit()
    }
  })

  return {
    currentSession,
    sessionLink,
    currentPlayer,
    isCurrentAuthor,
    chat,
    encounters,
    join,
    create,
    quit,
    end,
    sendChat,
    deleteChat,
    createEncounter,
    updateEncounter,
    deleteEncounter
  }
}
