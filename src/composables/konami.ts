import { ref } from 'vue'

const konamiSequence = ['ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'ArrowLeft', 'ArrowRight', 'b', 'a']
const konamiIndex = ref(0)

export function useKonamiCode() {
  const konamiCodeHandler = ref<() => void>()
  document.removeEventListener('keydown', konamiCodeListener)
  document.addEventListener('keydown', konamiCodeListener)
  
  function konamiCodeListener(event: KeyboardEvent) {
    if (event.key === konamiSequence[konamiIndex.value++]) {
      if (konamiIndex.value === konamiSequence.length) {
        konamiCodeHandler.value?.()
        konamiIndex.value = 0
      }
    } else {
      konamiIndex.value = 0
    }
  }

  return {
    onKonamiCode: (callback: () => void) => {
      konamiCodeHandler.value = callback
    }
  }
}
