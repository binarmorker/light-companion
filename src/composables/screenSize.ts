import { ref } from 'vue'

const isMobile = ref(false)
const screenWidth = ref(0)

function checkScreenSize() {
  screenWidth.value = window.innerWidth
  isMobile.value = screenWidth.value < 768
}

export function useScreenSize() {
  window.removeEventListener('resize', checkScreenSize)
  window.addEventListener('resize', checkScreenSize)

  checkScreenSize()

  return {
    isMobile,
    screenWidth
  }
}
