import { initializeApp } from 'firebase/app'
import { getFirestore, collection } from 'firebase/firestore'

export const firebaseApp = initializeApp({
  apiKey: "AIzaSyBwabNKXFvB2Vqujeu_EaAZziWvBJJXMSk",
  authDomain: "light-companion.firebaseapp.com",
  projectId: "light-companion",
  storageBucket: "light-companion.appspot.com",
  messagingSenderId: "731756372799",
  appId: "1:731756372799:web:c270e7fa03b323bd65298a",
  measurementId: "G-6KD9SNZ215"
})

const db = getFirestore(firebaseApp)

export const charactersRef = collection(db, 'characters')
export const sessionsRef = collection(db, 'sessions')
export const usersRef = collection(db, 'users')
