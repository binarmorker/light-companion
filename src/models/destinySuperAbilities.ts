import { Ability, ChargeType, DamageType, LightAbility, LightElement, SuperAbility } from '@/models/destiny'

export class BurningMaul implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.BurningMaul
  element = LightElement.Solar
  name = 'Burning Maul'
  description = `You wreath yourself in flames and summon a massive, fiery warhammer of solar Light into your free hands. This is the shape of your Hammer of Sol. While concentrating on your Hammer of Sol, the following effects apply: your base walking speed increases by 10 feet; you emit bright light in a 30-foot radius and dim light 15 feet beyond that; you have a resistance to bludgeoning, kinetic, piercing, slashing, and solar damage; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).
  
**Hammer Strike.** You can make a basic attack with your hammer. Make a melee Light attack roll attack against a target you can touch within 5 feet, and increase your Light attack modifier by an amount equal to your Light level for the attack. The target takes 1d6 + your Wisdom modifier + your Light level in solar damage on a hit. If you have the option to attack multiple times with the Attack action, this attack can replace one or more of them.

**Spinning Strike.** You use your action to whirl your hammer in a circle around you. Make a melee Light attack roll against all creatures within 5 feet of you (increase your Light attack modifier by an amount equal to your Light level for this). Any creature hit takes 2d6 solar damage, and creatures that are Gargantuan or smaller must make a Strength saving throw against your Light save DC or be knocked Prone.

**Flaming Whirlwind.** As an action you raise your hammer up and slam it on the ground, sending out a wave of solar energy which travels along the ground in a line up to 25 feet long. This line is cut short if it collides with a hostile creature or wall. All creatures within 5 feet of the end point of the line must make a Dexterity saving throw against your Light save DC (increase your Light save DC by an amount equal to your Light level for this). They take 3d8 + your Wisdom modifier + your Light level solar damage on a failed save, or half as much on a success.

**At Higher Levels.** The damage of Spinning Strike increases by 1d6, and the damage of Flaming Whirlwind increases by 1d8, for each Light level you are above first. The damage die size of Hammer Strike increases by one for each Light level you are above 2nd.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '1d(4+(2*[light])) + [lightMod] + [light]', type: DamageType.Solar },
    { formula: '(1+[light])d6', type: DamageType.Solar },
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class Forgemaster implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Forgemaster
  element = LightElement.Solar
  name = 'Forgemaster'
  description = `You wreath yourself in flames and summon a onehanded hammer of searing solar Light into a free hand. This is the shape of your Hammer of Sol. While concentrating on your Hammer of Sol, the following effects apply: your base walking speed increases by 10 feet; you emit bright light in a 30-foot radius and dim light 15 feet beyond that; you have a resistance to bludgeoning, kinetic, piercing, slashing, and solar damage; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).

**Light Hammer Strike.** You can make a basic attack with your hammer. Make a melee (5 feet) or ranged (20/60) Light attack roll attack against a target, and increase your Light attack modifier by an amount equal to your Light level for the attack. The target takes 1d6 + your Wisdom modifier + your Light level in explosive solar damage on a hit. If you have the option to attack multiple times with the Attack action, this attack can replace one or more of them.

**Suncharge.** As an action you charge up to 15 feet in a straight line, even moving through a hostile creature's space(s). All targets in your line must make a Strength saving throw (increase your Light save DC by an amount equal to your Light level for this). They take 3d10 + your Wisdom modifier + your Light level solar damage on a failed save, or half as much on a success. If a target is reduced to 0 hit points with the damage of this action it explodes, dealing 2d6 solar damage to all creatures other than yourself within 5 feet.

**Recall.** On your turn, if you are not holding your hammer, you can choose to cause your hammer to dissipate from its current location and immediately reform in one of your empty hands. The hammer takes nothing with it when it does this. If the hammer is ever more than 60 feet away from you, it dissipates automatically on its own, and you can choose to reform it on your turn.

**At Higher Levels.** The damage die size of Light Hammer Strike increases by one for each Light level you are above 2nd. The damage of Suncharge increases by 1d10 for the initial hit and 1d6 for the explosion for each Light level you are above first.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '1d(4+(2*[light])) + [lightMod] + [light]', type: DamageType.ExplosiveSolar },
    { formula: '(2+[light])d10 + [lightMod] + [light]', type: DamageType.Solar },
    { formula: '(1+[light])d6', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Strength }
  ]
}

export class ScorchedEarth implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.ScorchedEarth
  element = LightElement.Solar
  name = 'Scorched Earth'
  description = `You wreath yourself in flames and summon a hammer of searing solar Light into a free hand. This is the shape of your Hammer of Sol. While concentrating on your Hammer of Sol, the following effects apply: your base walking speed increases by 10 feet; you emit bright light in a 30-foot radius and dim light 15 feet beyond that; you have a resistance to bludgeoning, kinetic, piercing, slashing, and solar damage; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).

**Hammer Blast.** As an action, you throw your hammer at a location you can see within 60 feet, and it explodes on impact. All creatures within 5 feet of the location must make a Dexterity saving throw against your Light save DC (increase your Light save DC by an amount equal to your Light level for this). They take 3d8 + your Wisdom modifier + your Light level explosive solar damage on a failed save, or half as much on a success. You can also choose to create a Sunspot on the location of impact.

**Sunspot.** A Sunspot is a cylinder of solar Light 5 feet in diameter and 5 feet tall. It emits bright light in a 5-foot radius, and dim light 5 feet beyond that. Once you create a Sunspot, it lingers in place for the remaining duration of your super ability, whether you are concentrating on your super ability or not. You can choose to dissipate any number of your Sunspots on your turn, and at any time you can only have a number of Sunspots active equal to or less than your Light level. If a creature ends their turn within 5 feet of a Sunspot, they take 2d4 solar damage. This damage can invoke the effects of your Destroy Creature superclass feature.

**Recall.** On your turn, if you are not holding your hammer, you can choose to cause your hammer to dissipate from its current location and immediately reform in one of your empty hands. The hammer takes nothing with it when it does this. If the hammer is ever more than 60 feet away from you, it dissipates automatically on its own, and you can choose to reform it on your turn.

**At Higher Levels.** When you cast this super ability at a Light level of 2nd or higher, add 1d8 to the damage of Hammer Blast for each Light level above 1st, and add 1d4 to the damage your Sunspots cause creatures to take if they end their turn within 5 feet of them.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveSolar },
    { formula: '(1+[light])d4', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class FistOfHavocSuper implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.FistOfHavoc
  element = LightElement.Arc
  name = 'Fist of Havoc'
  description = `You summon a torrent of arc Light into yourself, turning your own body into the ultimate weapon. While concentrating on this super ability, the following effects apply: your base walking speed increases by 10 feet; you regain one melee ability charge at the start of each of your turns; you have a resistance to arc, bludgeoning, kinetic, piercing, and slashing damage; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).
  
**(Shockwave).** While concentrating on your Fist of Havoc, you can use your action to slam both of your fists into a hard surface that you share the same space with, such as a wall or the floor, to send out a shockwave of arc Light all around you. All creatures within 5 feet of you must make a Constitution saving throw against your Light save DC, with a bonus to your DC equal to your Light level. They take 4d8 + your Constitution modifier + your Light level in explosive arc damage on a failed save, or half as much on a success.
  
**At Higher Levels.** When you use Shockwave at a Light level of 2nd or higher, the damage increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(3+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveArc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
  ]
}

export class TectonicFistSuper implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.TectonicFist
  element = LightElement.Arc
  name = 'Tectonic Fist'
  description = `You gather an upwelling of arc Light into yourself, concentrating it into a single thunderous hammerblow. Choose a hard surface in the same space as you. You slam your fists into the surface, unleashing a massive discharge of arc Light. All creatures within 15 feet of the point you struck, other than yourself, must make a Constitution saving throw (increase your normal Light save DC by an amount equal to your Light level for this). On a failed save, creatures take 4d8 + your Constitution modifier + your Light level in explosive arc damage and become Stunned for 1 minute. On a success, they take half as much damage and are not Stunned. A Stunned creature can repeat the saving throw at the end of each of their turns, ending the effect on itself early on a success.

**At Higher Levels.** When you cast this super ability at a Light level of 2nd or higher, the damage increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Instantaneous'
  prerequisiteLevel = 3
  damage = [
    { formula: '(3+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveArc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
  ]
}

export class ThundercrashSuper implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Thundercrash
  element = LightElement.Arc
  name = 'Thundercrash'
  description = `Your Light swells within you, and you become a living missile carrying a payload of crackling arc Light. You fly up to 60 feet to reach a hard surface. This flight does not provoke attacks of opportunity. When you reach the surface you slam into it. All creatures other than yourself who are within 15 feet of you must make a Constitution saving throw against your Light save DC. Add a bonus to your DC equal to your Light level. On a failed save, creatures take 5d8 + your Constitution modifier + your Light level in explosive arc damage. On a success, they take half as much damage.
  
If you do not reach a hard surface by the end of your flight, your Light pitters out harmlessly.
  
**(Mega Impact).** The maximum CR of creatures this super ability affects via your superclass' Destroy Creature feature is increased by one. At 3rd level, you destroy creatures of CR ½ or less with the damage from your Thundercrash; at 5th level, you destroy creatures of CR 1 or less; and so on.
  
**At Higher Levels.** When you use Thundercrash at a Light level of 2nd or higher, add an additional 1d8 damage for each Light level you are above 1st. Beginning at 14th level, this super ability grants 120 feet of flying speed, instead of only 60.`
  castingTime = '1 action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Instantaneous'
  prerequisiteLevel = 3
  damage = [
    { formula: '(4+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveArc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
  ]
}

export class WardOfDawn implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.WardOfDawn
  element = LightElement.Void
  name = 'Ward of Dawn'
  description = `You extend your arms and project the Light within, creating a hollow sphere of void Light with a 10-foot radius centered on you. The outside shell of this sphere is semi-opaque, does not block sound, and though it is barely an inch thick, it is considered to be a full-cover barrier made of 6 feet of relic iron which blocks line of sight for any weapons, Light abilities, or spells that require being able to see the target, or for determining the area of an effect. However, creatures can freely pass through the shell of the Ward of Dawn as if it were not a barrier.

The shell of the sphere is an object made of pure Light that can be damaged and thus breached. The shell has an AC equal to 15 + your Charisma modifier and 50 hit points, succeeds on all Strength saving throws, fails on all Dexterity saving throws, and is immune to effects that would cause an Constitution, Intelligence, Wisdom, or Charisma saving throw. If you are concentrating on your Ward of Dawn when the shell blocks damage, you must make a concentration check against the damage in order to maintain your Ward of Dawn. You can forgo making this concentration check by subtracting the damage from the Ward of Dawn's hit points. If this reduces the Ward of Dawn's hit points to 0, your concentration ends after blocking the damage.

If at any time you are 100 feet or more away from the center of your Ward of Dawn, you immediately lose concentration on this Light ability.

**Armor of Light.** All creatures of your choice who are in the area of your Ward of Dawn when you cast it gain an overshield. If a creature starts their turn within the Ward of Dawn, or enters the area of the Ward of Dawn for the first time on a turn, you can choose to grant them an overshield. If you use this feature to grant an overshield to a creature that already has an overshield, but their overshield is not at maximum capacity, you restore their overshield to its maximum capacity.

A creature benefiting from this overshield loses it when they exit the Ward of Dawn.

**At Higher Levels.** When you cast Ward of Dawn at a Light level of 2nd or higher, the hit points your Ward of Dawn has increases by 50 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
}

export class BannerShield implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.BannerShield
  element = LightElement.Void
  name = 'Banner Shield'
  description = `You conjure a round shield of pure void Light into a free hand. While concentrating on this super ability, your base walking speed increases by 10 feet, and you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped). While concentrating on this super ability and holding the shield in at least one hand you have a bonus to your AC equal to your Charisma modifier, and you have a resistance to bludgeoning, kinetic, piercing, slashing, and void damage.

**Shield Bash.** As your action you can make an attack with the shield, which has a reach of 5 feet. When you make an attack with the shield, you make a melee Light attack roll. Increase your Light attack modifier by an amount equal to your Light level for this attack. On a hit, you deal 4d8 + your Charisma modifier + your Light level in void damage.

**Rally.** As an action you can hold your shield steady with two hands and focus your Light through it, creating an aura of void Light in a 10-foot radius around you. Until the start of your next turn, your speed is halved, and all creatures of your choice within the radius become Empowered (Stage 1) and receive a bonus to their AC equal to your Charisma modifier. This can stack with the bonus you already provide yourself.

**At Higher Levels.** Increase Shield Bash's damage by 1d8 for each Light level you are above 1st. When you cast Banner Shield at 11th level, it grants Empowered (Stage 2) instead, and when you cast it at 17th level, it grants Empowered (Stage 3).`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(3+[light])d8 + [lightMod] + [light]', type: DamageType.Void }
  ]
}

export class SentinelShield implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.SentinelShield
  element = LightElement.Void
  name = 'Sentinel Shield'
  description = `You conjure a round shield of pure void Light into a free hand. While concentrating on this super ability, your base walking speed increases by 10 feet, and you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped). While concentrating on this super ability and holding the shield in at least one hand you have a bonus to your AC equal to your Charisma modifier, and you have a resistance to bludgeoning, kinetic, piercing, slashing, and void damage.

**Shield Strike.** You can make a basic attack with the shield. Make a melee Light attack roll attack against a target you can touch within 5 feet, and increase your Light attack modifier by an amount equal to your Light level for the attack. The target takes 1d6 + your Charisma modifier + your Light level in void damage on a hit. If you have the option to attack multiple times with the Attack action, this attack can replace one or more of them.

**Forward Assault.** As your action, you throw your whole might behind the shield and lunge forward. Make a melee Light attack roll attack against a target you can touch within 5 feet, and increase your Light attack modifier by an amount equal to your Light level for the attack. On a hit the target takes 3d10 + your Charisma modifier + your Light level in void damage and must succeed on a Strength saving throw (increase your Light save DC by an amount equal to your Light level for this). The target is knocked Prone on a failed save.

**Shield Throw.** You can use your action to throw your shield in a 30-foot line, which can deflect off of hard surfaces and creatures, allowing the line to alter direction up to three times along its length. All creatures on the line must make a Dexterity saving throw (increase your Light save DC by an amount equal to your Light level for this). They take 3d8 + your Charisma modifier + your Light level in void damage on a failed save, or half as much on a success.

**Recall.** On your turn, if you are not holding your shield, you can use a free action to cause your shield to dissipate from its current location and immediately reform in one of your empty hands. The shield takes nothing with it when it does this. If the shield is ever more than 60 feet away from you, it dissipates automatically on its own.

**At Higher Levels.** Increase the damage of Forward Assault by 1d10 and the damage of Shield Throw by 1d8 for each Light level you are above 1st. The damage die size of Shield Strike increases by one for each Light level you are above 2nd.`
  castingTime = '1 action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '1d(4+(2*[light])) + [lightMod] + [light]', type: DamageType.Void },
    { formula: '(2+[light])d10 + [lightMod] + [light]', type: DamageType.Void },
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.Void },
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Strength },
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class ShadowshotSuper implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Shadowshot
  element = LightElement.Void
  name = 'Shadowshot'
  description = `You summon a recurve bow of pure void Light into your hands—your Dusk Bow—and shoot a single arrow from it. Make a ranged Light attack roll, and add a bonus to the total of your attack roll equal to your Light level. On a hit the target takes 2d8 + your Wisdom modifier + your Light level in void damage. Hit or miss, a void anchor spawns in the same space as your target, and remains in place while you are concentrating on this super ability.

For the duration, you can choose a number of creatures, up to 2 + your Wisdom modifier who are within 15 feet of the void anchor to become Tethered by the void anchor (see Conditions). You can only choose a creature to become Tethered if there is an opening at least 1 foot in diameter between the anchor and the creature. Large or smaller creatures who are Tethered by your void anchor also become Restrained. At any time on your turn, you can change which creatures are Tethered.

A creature Restrained by your void anchor may make a Charisma saving throw at the start of each of their turns. Increase the DC for this save by an amount equal to your Light level. If the creature succeeds, they may use up to 5 feet of their movement on their turn.

**Blood Bound.** If any creature that is Tethered by your void anchor takes damage from one source, all other creatures Tethered by your void anchor take 1d6 + your Light level in void damage.

**At Higher Levels.** Increase the damage of Shadowshot by 1d8 and increase the damage of the Blood Bound effect by 1d6 for each Light level you are above first.`
  castingTime = '1 action'
  range = '100/200'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '[light]d6 + [light]', type: DamageType.Void }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Charisma }
  ]
}

export class Radiance implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Radiance
  element = LightElement.Solar
  name = 'Radiance'
  description = `You summon up a great outpouring of your Light and engulf yourself in its flames, sprouting wings of solar fire from your shoulders and enveloping yourself in a brilliant golden aura. For the duration, you gain the following benefits:
- You shine bright light in a 30-foot radius and dim light 15 feet beyond that.
- You have a resistance to bludgeoning, fire, kinetic, piercing, slashing, and solar damage.
- You regain one spent grenade ability charge at the start of each of your turns, and casting a grenade that requires concentration does not end your concentration on Radiance or any other grenade you are concentrating on. All grenades you are concentrating on dissipate when you end concentration on Radiance.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
}

export class WellOfRadiance implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.WellOfRadiance
  element = LightElement.Solar
  name = 'Well of Radiance'
  description = `You conjure a sword of pure solar Light and stab it into the ground beneath you, projecting an aura of soothing Light in a 10-foot radius from the sword. For the duration, the area of your Well of Radiance is considered a Light zone, and it emits bright light in a 30-foot sphere centered on it, and sheds dim light 15 feet beyond that.

You can dissipate your Well of Radiance at any time (no action cost). You cannot cast this super ability if you are not standing on a solid surface. Finally, you cannot lose your concentration on this Light ability as a result of taking damage.

**Burst Effect.** Only on the turn that you cast this super ability, you can choose whether each creature in the area is healed or damaged by the Well of Radiance. If you choose to heal a creature, they recover 3d8 + your Charisma modifier + your Light level in hit points. If you choose to damage a creature, they must make a Constitution saving throw. Increase the DC of this saving throw by an amount equal to your Light level. They take 3d8 + your Charisma modifier + your Light level as solar damage on a failed save, or half as much on a success.

**Residual Effect.** For the remaining duration, you can allow creatures who end their turns within the Well of Radiance, or who enter the area for the first time on a turn, to recover hit points equal to 3d8 + your Charisma modifier + your Light level. A creature can only benefit from this healing once on any turn.

In addition, creatures of your choice who are within the area of the Well of Radiance become Empowered (Stage 1). This empowerment is lost if the creature leaves the area of the Well of Radiance.

**At Higher Levels.** When you cast this super ability at a Light level of 2nd or higher, the amount of healing or damage it does for Burst Effect increases by 1d8 for each Light level you are above 1st. For Residual Effect, the amount of healing the Well of Radiance supplies increases by 1d8 for each Light level you are above first. Finally, beginning at 11th Sunsinger level, your Well of Radiance grants Empowered (Stage 2) instead, and at 17th Sunsinger level, it grants Empowered (Stage 3).`
  castingTime = '1 bonus action'
  range = 'See description'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.Healing },
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
  ]
}

export class Dawnblade implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Dawnblade
  element = LightElement.Solar
  name = 'Dawnblade'
  description = `You unfurl solar wings and summon a flaming greatsword made of pure solar Light into your hands. For the duration, you: emit bright light in a 30-foot radius and dim light 10 feet beyond that; gain a flying speed equal to your base walking speed, and you can hover in place; cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).

**Blade Salvo.** As an action, you swing your sword and hurl a wave of solar Light from its edge toward a location you can see within 90 feet. All creatures within 5 feet of the location must make a Dexterity saving throw against your Light save DC, with a bonus to your DC equal to your Light level. They take 3d8 + your Charisma modifier + your Light level in explosive solar damage on a failed save, or half as much on a success.

**Armored Light.** Your AC increases by 2 while concentrating on Dawnblade.

**Phoenix Dive.** As a bonus action on your turn, you can rapidly descend up to 30 feet at the cost of only 5 feet of movement. If this causes you to become grounded, you can make a shield recharge roll. If there is a target beneath you, you cannot become grounded, but you can make a melee Light attack roll (add a bonus to the roll equal to your Light level). The target takes the damage of your Blade Salvo on a hit.

**At Higher Levels.** When you use Blade Salvo at a Light level of 2nd or higher, add 1d8 to the damage of Blade Salvo for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveSolar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class Stormtrance implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Stormtrance
  element = LightElement.Arc
  name = 'Stormtrance'
  description = `You summon and channel an arc storm, conducting its raw power through intense focus, wreathing yourself in arc Light. While concentrating on this super ability, the following effects apply: your base walking speed increases by 10 feet; you hover 1 foot above hard surfaces and still water; you have a resistance to arc, bludgeoning, kinetic, piercing, and slashing damage; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).

**Channel the Storm.** As an action, you can channel the wild lightning of the summoned arc storm through your own body and into your hands, directing it with your unshakeable will and shaping it with your Light. The channeled lightning dances from your fingertips in a 15-foot cone. All creatures in the cone must make a Constitution saving throw against your Light save DC (add a bonus to your DC equal to your Light level). They take 4d8 + your Wisdom modifier + your Light level in arc damage on a failed save, or half as much on a success.

**At Higher Levels.** When you use this super ability at a Light level of 2nd or higher, add 1d8 to its damage for each Light level above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(3+[light])d8 + [intMod] + [light]', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
  ]
}

export class ChaosReach implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.ChaosReach
  element = LightElement.Arc
  name = 'Chaos Reach'
  description = `You channel the storm within you into a concentrated beam of arc Light. Every creature in the area of your sweeping line must make a Dexterity saving throw against your Light save DC (increase your DC by an amount equal to your Light level for this). They take 4d8 + your Wisdom modifier + your Light level in arc damage on a failed save, or half as much on a success.

**At Higher Levels.** When you use this super ability at a Light level of 2nd or higher, add 1d8 to its damage for each Light level above 1st.`
  castingTime = '1 action'
  range = '80-foot sweeping line'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Instantaneous'
  prerequisiteLevel = 3
  damage = [
    { formula: '(3+[light])d8 + [intMod] + [light]', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class Landfall implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Landfall
  element = LightElement.Arc
  name = 'Landfall'
  description = `You unleash the full force of the storm within you. All creatures within 15 feet of you must make a Dexterity saving throw (increase the DC by an amount equal to your Light level). They take 4d8 + your Wisdom modifier + your Light level explosive arc damage on a failed save, or half as much on a success.

**At Higher Levels.** When you use this super ability at a Light level of 2nd or higher, add 1d8 to its damage for each Light level above 1st.`
  castingTime = '1 action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Instantaneous'
  prerequisiteLevel = 3
  damage = [
    { formula: '(3+[light])d8 + [lightMod] + [light]', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class NovaBomb implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.NovaBomb
  element = LightElement.Void
  name = 'Nova Bomb'
  description = `You form a spherical bolt of intensely concentrated void Light in your hands and hurl it at a hard surface within range. All creatures within 5 feet of the spot must make a Dexterity saving throw against your Light save DC (add a bonus to your DC equal to your Light level). They take 4d8 + your Intelligence modifier + your Light level explosive void damage on a failed save, or half as much on a success.

**Mortar.** There must be an opening within any obstruction, or a space above the obstruction, that is at least 10 feet wide in all directions for you to be able to throw your Nova Bomb as a mortar.

**At Higher Levels.** When you cast this super ability at a Light level of 2nd or higher, the damage increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '80 feet, mortar'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Instantaneous'
  prerequisiteLevel = 3
  damage = [
    { formula: '(3+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveVoid }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class NovaWarp implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.NovaWarp
  element = LightElement.Void
  name = 'Nova Warp'
  description = `You draw an intense concentration of void Light into your body, charging every atom of your being with roiling, explosive power. While concentrating on this super ability, the following effects apply: your base walking speed increases by 10 feet; you hover 1 foot above hard surfaces and still water; you have a resistance to bludgeoning, kinetic, piercing, slashing, and void damage; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).

**Void Eruption.** As an action on your turn, you can unleash an eruption of void energy all around you. Every creature within 10 feet of you, other than yourself, must make a Dexterity saving throw against your Light save DC (add a bonus to your DC equal to your Light level). On a failed save, they take 3d8 + your Intelligence modifier + your Light level explosive void damage. On a success, they take half as much.

**At Higher Levels.** When you cast this super ability at a Light level of 2nd or higher, the damage of Void Eruption increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(2+[light])d8 + [lightMod] + [light]', type: DamageType.ExplosiveVoid }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class Deadeye implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.Deadeye
  element = LightElement.Solar
  name = 'Deadeye'
  description = `You channel your Light into your weapon of choice and turn it into a Deadeye Golden Gun. You can take up to three shots with your Golden Gun, making a ranged firearm attack roll with your weapon. Add your Light level to your attack modifier for these shots. On a hit, the damage of your shot increases by 2d8, and you always deal solar damage instead of your weapon's normal damage.

The extended and maximum ranges of your weapon of choice are temporarily increased by half the weapon's normal extended and maximum ranges. Round down to the nearest multiple of 5 when you do this. For example, a weapon with scope ranges of 30/75/120 would have its ranges become 30/110/180 while it is a Deadeye Golden Gun.

Because your Golden Gun is channeled through a firearm, a shot with your Golden Gun can benefit from any trait, perk, or other source that targets weapon attacks, ranged weapon attacks, or firearm attacks, and damage from those sources is considered super ability damage for the purpose of Destroy Creature.

**Deadshot.** The maximum CR of creatures this super ability affects via your superclass' Destroy Creature feature is increased by one. At 3rd level, you destroy creatures of CR ½ or less with the damage from your Golden Gun; at 5th level, you destroy creatures of CR 1 or less; and so on.

**At Higher Levels.** The bonus damage a shot with your Golden Gun provides increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(1+[light])d8', type: DamageType.Solar }
  ]
}

export class SixShooter implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.SixShooter
  element = LightElement.Solar
  name = 'Six-shooter'
  description = `You channel your Light into your weapon of choice and turn it into a Six-Shooter Golden Gun. You can take up to six shots with your Golden Gun, making a ranged firearm attack roll with your weapon as normal. Add your Light level to your attack modifier for these shots. On a hit, the damage of your shot is increased by 1d6, and you always deal solar damage instead of your weapon's normal damage.

The effective range of your weapon of choice is temporarily increased by half the weapon's normal effective range, up to the weapon's extended range (a weapon's effective range cannot be greater than its extended range). Round down to the nearest multiple of 5 when you do this. For example, a firearm with scope ranges of 35/50/90 would see its ranges become 50/50/90.

Because your Golden Gun is channeled through a firearm, a shot with your Golden Gun can benefit from any trait, perk, or other source that targets ranged Light attacks, ranged weapon attacks, or firearm attacks, and damage from those sources is considered super ability damage for the purpose of Destroy Creature.

**At Higher Levels.** The bonus damage a shot with your Golden Gun provides increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '[light]d6', type: DamageType.Solar }
  ]
}

export class BladeBarrage implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.BladeBarrage
  element = LightElement.Solar
  name = 'Blade Barrage'
  description = `You summon three throwing knives made of pure solar Light, then hurl them at one or more targets within range. Each knife causes its target to make a Dexterity saving throw (increase your Light save DC by an amount equal to your Light level for this), taking 2d6 + your Charisma modifier + your Light level in explosive solar damage on a failed save, or half as much on a success. The knives strike simultaneously, and you can direct them to hit one target or several.

**At Higher Levels.** Add 1d6 to this super ability's base damage for every Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  damage = [
    { formula: '(1+[light])d6 + [lightMod] + [light]', type: DamageType.ExplosiveSolar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class RazorsEdge implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.RazorsEdge
  element = LightElement.Arc
  name = 'Razor\'s Edge'
  description = `You summon a one-handed shortsword of pure arc Light into an empty hand. While concentrating on this Light ability, your base walking speed increases by 10 feet, and you cannot hold any other objects in your hands (any objects you are holding when you cast this super ability are immediately dropped).

**Arc Blade.** As an action, you can make an attack with your Light shortsword (reach: 5 feet). Make a melee Light attack roll, and add a bonus to your Light attack modifier equal to your Light level. On a hit, the target takes 3d6 + your Intelligence modifier + your Light level in arc damage. Arc Blade can be used for Opportunity Strikes.

**Arc Wave.** As an action, you can send out a wave of arc energy in a 15-foot cone. All creatures in the area must make a Constitution saving throw against your Light save DC (add a bonus to your DC equal to your Light level). They take 4d6 + your Intelligence modifier + your Light level in arc damage on a failed save, or half as much on a success.

**At Higher Levels.** When you cast this super ability at 2nd Light level or higher, the damage of Arc Blade increases by 1d6 for each Light level you are above 1st, and the damage of Arc Wave  increases by 2d6 for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(2+[light])d6 + [lightMod] + [light]', type: DamageType.Arc },
    { formula: '(2+(2*[light]))d6 + [lightMod] + [light]', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
  ]
}

export class ArcStaff implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.ArcStaff
  element = LightElement.Arc
  name = 'Arc Staff'
  description = `You summon a quarterstaff of pure arc Light into your hands. For the duration, your base walking speed increases by 10 feet, and you cannot hold any other objects in your hands (any objects you are holding when you cast this super ability are immediately dropped).

**Arc Staff.** As an action, you can make an attack with this staff. Choose either one target within 10 feet of you, or two targets within 5 feet of you. Make a melee Light attack roll against each target, and add a bonus to your Light attack modifier equal to your Light level. A target takes 3d6 + your Intelligence modifier + your Light level in arc damage on a hit. Arc Staff can be used for Opportunity Strikes, but only against a single target.

**At Higher Levels.** When you cast this super ability at 2nd Light level or higher, the damage of Arc Staff increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(2+[light])d6 + [lightMod] + [light]', type: DamageType.Arc }
  ]
}

export class VanishingAct implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.VanishingAct
  element = LightElement.Arc
  name = 'Vanishing Arc'
  description = `You summon a one-handed shortsword of pure arc Light into an empty hand. While concentrating on this Light ability, your base walking speed increases by 10 feet, and you cannot hold any other objects in your hands (any objects you are holding when you cast this super ability are immediately dropped).

**Arc Blade.** As an action, you can make an attack with your Light shortsword (reach: 5 feet). Make a melee Light attack roll, and add a bonus to your Light attack modifier equal to your Light level. On a hit, the target takes 3d6 + your Intelligence modifier + your Light level in arc damage. Arc Blade can be used for Opportunity Strikes.

**Phantom Cloak.** When you cast this Light ability, you immediately gain active camouflage that persists while concentrating on Vanishing Act. Making an attack roll or causing another creature to make a saving throw does not end the active camouflage granted by this feature.

**At Higher Levels.** When you cast this super ability at 2nd Light level or higher, the damage of Arc Blade increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(2+[light])d6 + [lightMod] + [light]', type: DamageType.Arc }
  ]
}

export class MoebiusQuiverSuper implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.MoebiusQuiver
  element = LightElement.Void
  name = 'Moebius Quiver'
  description = `You summon a recurve bow of pure void Light into your hands—your Dusk Bow—and, as part of the same action, can shoot up to three arrows from it.

Make a ranged Light attack roll for each arrow, with a bonus to hit equal to your Light level. Each arrow deals 2d8 + your Wisdom modifier + your Light level in void damage on a hit. After hitting a target with an arrow, the target becomes Weakened for the next minute.

**At Higher Levels.** When you make an attack with Moebius Quiver at a Light level of 2nd or higher, its damage increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '100/200'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 17'
  duration = 'Instantaneous'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(1+[light])d8 + [lightMod] + [light]', type: DamageType.Void }
  ]
}

export class SpectralBladesSuper implements LightAbility {
  _type = 'LightAbility'
  id = SuperAbility.SpectralBlades
  element = LightElement.Void
  name = 'Spectral Blades'
  description = `You summon a pair of knives made of pure void Light into your empty hands. While concentrating on this super ability, the following effects apply: your base walking speed increases by 10 feet; you are under the effects of your Truesight focus action; you cannot hold any other weapons or objects in your hands (any weapons or objects you are holding when you cast this super ability are immediately dropped).

**Void Blade.** You can make a basic attack with your blades. Make a melee Light attack roll attack against a target you can touch within 5 feet, and increase your Light attack modifier by an amount equal to your Light level for the attack. The target takes 1d6 + your Wisdom modifier + your Light level in void damage on a hit. If you have the option to attack multiple times with the Attack action, this attack can replace one or more of them.

**Vanishing Strike.** As an action you strike out with both blades at once. Make a melee Light attack roll against a target within 5 feet, and increase your Light attack modifier by an amount equal to your Light level for the attack. The target takes 3d10 + your Wisdom modifier + your Light level in void damage on a hit. Hit or miss, you are granted active camouflage until the end of your next turn.

**At Higher Levels.** When you cast Spectral Blades at a Light level of 2nd or higher, the damage of Vanishing Strike increases by 1d10 for each Light level you are above 1st, and the damage die size of Void Blade increases by one for each Light level you are above 2nd.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Super
  recharge = 'd20, 18'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 3
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '1d(4+(2*[light])) + [lightMod] + [light]', type: DamageType.Void },
    { formula: '(2+[light])d10 + [lightMod] + [light]', type: DamageType.Void }
  ]
}

export const allSupers: LightAbility[] = [
  new BurningMaul,
  new Forgemaster,
  new ScorchedEarth,
  new FistOfHavocSuper,
  new TectonicFistSuper,
  new ThundercrashSuper,
  new WardOfDawn,
  new BannerShield,
  new SentinelShield,
  new Radiance,
  new WellOfRadiance,
  new Dawnblade,
  new Stormtrance,
  new ChaosReach,
  new Landfall,
  new NovaBomb,
  new NovaWarp,
  new Deadeye,
  new SixShooter,
  new BladeBarrage,
  new RazorsEdge,
  new ArcStaff,
  new VanishingAct,
  new ShadowshotSuper,
  new MoebiusQuiverSuper,
  new SpectralBladesSuper
]
export const superAbilityIndex = allSupers.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<SuperAbility, LightAbility>)
export const getSuperAbility = (id: SuperAbility, level: number) => (superAbilityIndex[id]?.prerequisiteLevel || 0) <= level ? superAbilityIndex[id] : null