import { WeaponPerks } from '@/models/destiny'

export class AcceleratedCoils implements WeaponPerks {
  id = 'Perks.AcceleratedCoils'
  name = 'Accelerated Coils'
  description = `The number of damage dice this weapon has is reduced by one, and it loses the Loading property.`
}

export class AdaptiveFrame implements WeaponPerks {
  id = 'Perks.AdaptiveFrame'
  name = 'Adaptive Frame'
  description = `Once on your turn, if you miss with an attack with this weapon, you can spend 5 shots to make an additional attack with this weapon.`
}

export class ArchersTempo implements WeaponPerks {
  id = 'Perks.ArchersTempo'
  name = 'Archer\'s Tempo'
  description = `If you hit a hostile creature with a shot from this weapon, you gain a bonus +1 to the attack roll of the next shot you take with this weapon, if that shot is made before the end of your next turn. This bonus can stack up to a maximum of +3, but is lost if you do not take a shot with this weapon on your turn.`
}

export class ArmorPiercingRounds implements WeaponPerks {
  id = 'Perks.ArmorPiercingRounds'
  name = 'Armor-Piercing Rounds'
  description = `The rounds fired from this weapon can pierce most barriers, but are blocked by 1 inch of metal, 2 inches of stone, or 3 feet of wood or dirt. These rounds can also damage all creatures in a line up to this weapon's effective range (while Aiming, up to extended range instead). In order to do this, your attack must have killed the previous target, and your attack roll must beat the AC of the subsequent target. Subsequent targets take the remaining damage of the previous target.`
}

export class AugmentedFocus implements WeaponPerks {
  id = 'Perks.AugmentedFocus'
  name = 'Augmented Focus'
  description = `The damage die size of this weapon increases by two.`
}

export class ArmyOfOne implements WeaponPerks {
  id = 'Perks.ArmyOfOne'
  name = 'Army of One'
  description = `If you kill a hostile creature with a shot from this weapon, you can make a melee ability recharge roll and a grenade ability recharge roll. Augmented Focus. The damage die size of this weapon increases by two.`
}

export class BattleRunner implements WeaponPerks {
  id = 'Perks.BattleRunner'
  name = 'Battle Runner'
  description = `If you kill a hostile creature with a shot from this weapon, your base walking speed increases by 10 feet for the next minute. The effect of this perk is lost early if you switch weapons or stop holding this weapon. The effect of this perk cannot stack with itself regardless of the number of weapons you are holding that have this perk.`
}

export class BlackPowder implements WeaponPerks {
  id = 'Perks.BlackPowder'
  name = 'Black Powder'
  description = `This weapon's Payload DC increases by 2.`
}

export class BoxBreathing implements WeaponPerks {
  id = 'Perks.BoxBreathing'
  name = 'Box Breathing'
  description = `This weapon gains an additional damage die, and it gains the Loading property. If you begin Aiming with this weapon on your turn, and you do not take any action or bonus action on your turn, you have advantage on any shots you take with this weapon on your following turn.`
}

export class BurstFrame implements WeaponPerks {
  id = 'Perks.BurstFrame'
  name = 'Burst Frame'
  description = `This weapon gains the Cumbersome property. As an action, you may spend 5 shots to launch a burst of energy. This burst uses this weapon's normal traits, but gains the payload property with an impact line of 20 feet, originating from you, instead of an impact radius.

This attack deals the damage type chosen in the weapon's Elemental Shot Capacity perk, not its normal damage type.`
}

export class CasterFrame implements WeaponPerks {
  id = 'Perks.CasterFrame'
  name = 'Caster Frame'
  description = `This weapon gains the Cumbersome property, and its weapon DC increases by 2. As an action, you may spend 5 shots to launch a bolt of energy from this weapon. Treat this attack as if the weapon had the Thrown (40/80) property, and if the attack hits, you can add an additional damage die to the attack's damage roll.

Alternatively, as an action you can spend 10 shots to launch a heavy bolt of energy from this weapon. Treat this attack as if the weapon had the Thrown (40/80) property, and if the attack hits, all other targets within 5 feet of the original target must make a Dexterity saving throw, taking half the damage of this weapon's hit on a failed save. In addition, the original target must make a Constitution saving throw at the start of its next turn. It takes half the damage of this weapon again on a failed save.

These attacks deal the damage type chosen in the weapon's Elemental Shot Capacity perk, not its normal damage type.`
}

export class ClusterBombs implements WeaponPerks {
  id = 'Perks.ClusterBombs'
  name = 'Cluster Bombs'
  description = `This weapon gains the Loading property, if it did not have it already. After resolving the payload effect of this weapon, all targets that failed their payload saving throw take an additional 1d8 explosive damage (same type as the weapon's damage type).`
}

export class CrowdControl implements WeaponPerks {
  id = 'Perks.CrowdControl'
  name = 'Crowd Control'
  description = `When you kill a hostile creature with a shot from this weapon, this weapon gains a bonus +2 to its damage rolls for the next minute.`
}

export class Demolitionist implements WeaponPerks {
  id = 'Perks.Demolitionist'
  name = 'Demolitionist'
  description = `If you kill a hostile creature with a shot from this weapon, you have advantage on grenade recharge rolls you make for the next minute. This benefit ends early if you are Incapacitated, if you regain a grenade ability charge, or if you stop holding this weapon.`
}

export class DualBarrel implements WeaponPerks {
  id = 'Perks.DualBarrel'
  name = 'Dual Barrel'
  description = `This weapon's Payload impact becomes a 5-foot diameter, its damage die size is reduced by two, and it loses the Loading property.`
}

export class Elemental implements WeaponPerks {
  id = 'Perks.Elemental'
  name = 'Elemental'
  description = `This weapon's damage type changes to one of the following: arc, solar, or void. You cannot alter this choice once made, and you cannot remove this perk once it has been applied.`
}

export class ElementalShotCapacity implements WeaponPerks {
  id = 'Perks.ElementalShotCapacity'
  name = 'Elemental Shot Capacity'
  description = `When you add this perk to your weapon choose one of the following damage types: arc, solar, void. You cannot alter this choice once made.

This weapon gains the Shot Capacity (20) property. When you deal damage with this weapon, you can spend 1 shot to deal the chosen damage type instead of this weapon's normal damage. Any time you make any attack with this weapon that results in spending a shot from this weapon's shot capacity, you can choose to deal the chosen damage type instead of this weapon's normal damage.`
}

export class EnduringEdge implements WeaponPerks {
  id = 'Perks.EnduringEdge'
  name = 'Enduring Edge'
  description = `This weapon's shot capacity increases by 10.`
}

export class EnhancedBattery implements WeaponPerks {
  id = 'Perks.EnhancedBattery'
  name = 'Enhanced Battery'
  description = `The damage die size of this weapon is increased by one, it gains a bonus +1 to damage rolls when you take a shot with it, and all ranges of this weapon are increased by 10 feet.`
}

export class Exhumed implements WeaponPerks {
  id = 'Perks.Exhumed'
  name = 'Exhumed'
  description = `You can take a shot with this weapon once on the turn that you are revived from death. You have advantage on your attack roll for that shot.`
}

export class ExplosiveRounds implements WeaponPerks {
  id = 'Perks.ExplosiveRounds'
  name = 'Explosive Rounds'
  description = `This weapon deals explosive-type damage. For example, if the weapon only dealt kinetic damage before, it would now deal explosive kinetic damage.`
}

export class ExtendedBarrel implements WeaponPerks {
  id = 'Perks.ExtendedBarrel'
  name = 'Extended Barrel'
  description = `The range of this weapon increases by 15 feet.`
}

export class FeatherweightBolt implements WeaponPerks {
  id = 'Perks.FeatherweightBolt'
  name = 'Featherweight Bolt'
  description = `This weapon gains the Automatic Fire property.`
}

export class FieldScout implements WeaponPerks {
  id = 'Perks.FieldScout'
  name = 'Field Scout'
  description = `If there are no creatures within 10 feet of you that you are aware of, you gain a bonus +2 to attack and damage rolls when you take a shot with this weapon.`
}

export class FinalRound implements WeaponPerks {
  id = 'Perks.FinalRound'
  name = 'Final Round'
  description = `Your critical hit range is increased by 1 when you take a shot with this weapon. If you take a shot with this weapon and score a critical hit, you can add a bonus damage die to this weapon's damage roll before accounting for the effects of the critical hit.`
}

export class FiringLine implements WeaponPerks {
  id = 'Perks.FiringLine'
  name = 'Firing Line'
  description = `When two or more allied creatures you are aware of are within 30 feet of you, you have a bonus +1 to attack and damage rolls with this weapon.`
}

export class FocusedFire implements WeaponPerks {
  id = 'Perks.FocusedFire'
  name = 'Focused Fire'
  description = `The range band of this weapon is increased, the effective range is reduced by 10 feet, and the extended range is increased by 10 feet. When you take a shot with this weapon while Aiming, the damage die size of this weapon is increased by one. Finally, if this weapon has the High Recoil property, this weapon loses the High Recoil property and gains the Finesse property instead.`
}

export class FullCourt implements WeaponPerks {
  id = 'Perks.FullCourt'
  name = 'Full Court'
  description = `If you take a shot with this weapon against a target within this weapon's extended range, this weapon gains an additional damage die in its damage roll for that attack. If the target is within this weapon's maximum range, this weapon gains two additional damage dice instead.`
}

export class FullDraw implements WeaponPerks {
  id = 'Perks.FullDraw'
  name = 'Full Draw'
  description = `This weapon's range band is increased, it gains the Loading and Finesse properties, its damage die size is increased by one, and it gains an additional damage die.`
}

export class HeadSeeker implements WeaponPerks {
  id = 'Perks.HeadSeeker'
  name = 'Head Seeker'
  description = `If you hit with a shot with this weapon while Aiming, and you roll a 1 or a 2 on any of this weapon's damage dice, you can re-roll those damage dice one time. You must use the new result, even if it's a 1 or a 2.`
}

export class HeavyFrame implements WeaponPerks {
  id = 'Perks.HeavyFrame'
  name = 'Heavy Frame'
  description = `This weapon gains the
    Cumbersome property, and its critical hit range is increased by 2. As your action, you can spend 5 shots to make a two-handed attack that, if it hits, gains two additional damage dice for its damage roll. In addition, on a hit with this powerful attack, the target must make a Strength saving throw or be knocked Prone.

This attack deals the damage type chosen in the weapon's Elemental Shot Capacity perk, not its normal damage type.`
}

export class HeavyPayload implements WeaponPerks {
  id = 'Perks.HeavyPayload'
  name = 'Heavy Payload'
  description = `The Payload radius of this weapon becomes 10 feet, and the damage die size decreases by one.`
}

export class HiddenHand implements WeaponPerks {
  id = 'Perks.HiddenHand'
  name = 'Hidden Hand'
  description = `When you take a shot with this weapon while Aiming, you gain a bonus +2 to this weapon's attack roll.`
}

export class HipFire implements WeaponPerks {
  id = 'Perks.HipFire'
  name = 'Hip Fire'
  description = `This weapon has a bonus +2 to attack rolls when you take a shot with it while you are not Aiming.`
}

export class HonedEdge implements WeaponPerks {
  id = 'Perks.HonedEdge'
  name = 'Honed Edge'
  description = `This weapon has a bonus +2 to attack rolls with it.`
}

export class ImpactCasing implements WeaponPerks {
  id = 'Perks.ImpactCasing'
  name = 'Impact Casing'
  description = `The weapon's damage die size decreases by one. When you take a shot with this weapon, you can choose a specific target within range and make an attack roll against it. On a hit the target becomes the origin for the Payload effect of this weapon, the target automatically fails their Payload saving throw, and you roll one additional damage die against the chosen target only (all other targets affected by the Payload effect take damage as normal). On a miss the target is still the origin for this weapon's Payload effect, but the target automatically succeeds on their Payload saving throw. Resolve the Payload effect for other targets within range as normal.`
}

export class JaggedEdge implements WeaponPerks {
  id = 'Perks.JaggedEdge'
  name = 'Jagged Edge'
  description = `This weapon's damage die size is increased by one, and it has a bonus +2 to its damage rolls.`
}

export class Javelin implements WeaponPerks {
  id = 'Perks.Javelin'
  name = 'Javelin'
  description = `All ranges of this weapon increase by 30 feet, and the damage die size of this weapon increases by one. This benefit applies after all other range increases or decreases are calculated for the weapon.`
}

export class Lightweight implements WeaponPerks {
  id = 'Perks.Lightweight'
  name = 'Lightweight'
  description = `If you start your turn while holding this weapon in your hands, your base walking speed increases by 5 feet until the end of your turn. You cannot gain the benefit of the Lightweight perk more than once regardless of how many weapons you are holding with this perk.`
}

export class LightweightFrame implements WeaponPerks {
  id = 'Perks.LightweightFrame'
  name = 'Lightweight Frame'
  description = `The damage die size of this weapon decreases by one. If this weapon has the High Recoil property, it loses the High Recoil property and gains the Finesse property instead.`
}

export class LuckInTheChamber implements WeaponPerks {
  id = 'Perks.LuckInTheChamber'
  name = 'Luck in the Chamber'
  description = `When you roll damage for a shot from this weapon, roll a d6. Your shot has a bonus +5 to damage if the d6 lands on a 5, or a bonus +6 to damage if it lands on 6.`
}

export class Mulligan implements WeaponPerks {
  id = 'Perks.Mulligan'
  name = 'Mulligan'
  description = `If you miss with a shot from this weapon on your turn, you can roll a d6. On a roll of 6, one shot is refunded to this weapon's magazine, and you can make an additional attack with this weapon.`
}

export class NaturalString implements WeaponPerks {
  id = 'Perks.NaturalString'
  name = 'Natural String'
  description = `This weapon's damage die size is decreased by one. Once on your turn, if you miss with a shot from this weapon, you can roll a d6. On a roll of 5-6, you can re-roll your attack roll for the missed shot. You must use the new roll.`
}

export class OneTwoPunch implements WeaponPerks {
  id = 'Perks.OneTwoPunch'
  name = 'One-two Punch'
  description = `If you hit a hostile creature with a shot from this weapon, the next melee attack you make before the end of your next turn has its damage increased by 1d6.`
}

export class OpeningShot implements WeaponPerks {
  id = 'Perks.OpeningShot'
  name = 'Opening Shot'
  description = `If you take a shot with this weapon against a creature that is unaware of your presence, or that has not taken a turn yet in the initiative order, you have a bonus +3 to the attack and damage rolls of that shot.`
}

export class Osmosis implements WeaponPerks {
  id = 'Perks.Osmosis'
  name = 'Osmosis'
  description = `When you spend a grenade ability charge while holding this weapon, you can choose to alter this weapon's damage type to match the type of Light you use. This change lasts for the next minute, but is lost early if you stop holding this weapon.`
}

export class Outlaw implements WeaponPerks {
  id = 'Perks.Outlaw'
  name = 'Outlaw'
  description = `When you take a shot with this weapon while Aiming, you have a bonus +2 to your attack roll. When you take a shot with this weapon while not Aiming, you have a bonus +2 to your damage roll.`
}

export class ParticleScattering implements WeaponPerks {
  id = 'Perks.ParticleScattering'
  name = 'Particle Scattering'
  description = `This weapon loses the Energy Projectiles property and its firearm ranges, its range band becomes Close, and it gains the Payload property with an impact cone instead of a radius. The impact cone is 15 feet, originating from you.`
}

export class PerfectBalance implements WeaponPerks {
  id = 'Perks.PerfectBalance'
  name = 'Perfect Balance'
  description = `This weapon has a bonus +2 to its attack roll when you take a shot with it, and it has the Finesse property.`
}

export class Persistence implements WeaponPerks {
  id = 'Perks.Persistence'
  name = 'Persistence'
  description = `When you take a shot with this weapon while Aiming, you can add a bonus +1 to hit when you take your next shot with this weapon, if that shot is taken before the end of your next turn. The effect of this perk can stack with itself to a maximum of +3, but is lost if you do not take a shot with this weapon on your turn, or if you end your turn without Aiming.`
}

export class PrecisionFrame implements WeaponPerks {
  id = 'Perks.PrecisionFrame'
  name = 'Precision Frame'
  description = `The range band of this weapon is increased, the extended and maximum ranges of this weapon increase by 10 feet each, and the damage die size of this weapon increases by one when you take a shot with it while Aiming. Finally, if this weapon has the High Recoil property, this weapon loses the High Recoil property and gains the Finesse property instead.`
}

export class Rangefinder implements WeaponPerks {
  id = 'Perks.Rangefinder'
  name = 'Rangefinder'
  description = `The scope values of this weapon increase by half of their current value. Round down to the nearest multiple of 5 when you do this. For example, a weapon with ranges of 35/50/90 would see its ranges become 50/75/135.`
}

export class RapidHit implements WeaponPerks {
  id = 'Perks.RapidHit'
  name = 'Rapid Hit'
  description = `If you hit a hostile creature with a shot from this weapon, you gain a bonus +1 to the damage roll of the next shot you take with this weapon if that shot is made before the end of your next turn and if it hits the same target as before. This bonus can stack up to a maximum of +5, but is lost if you do not take a shot with this weapon on your turn or if you make an attack against another target.`
}

export class RicochetRounds implements WeaponPerks {
  id = 'Perks.RicochetRounds'
  name = 'Ricochet Rounds'
  description = `When you take a shot with this weapon and miss, roll a d6. On a 5 or 6, you can choose one other target within 10 feet of your original target to attack instead, using your original attack roll. If your original attack roll would hit the new target, roll damage against the new target.`
}

export class RifledBarrel implements WeaponPerks {
  id = 'Perks.RifledBarrel'
  name = 'Rifled Barrel'
  description = `This weapon gains a bonus +2 to its damage rolls when you take a shot with it.`
}

export class SecretRound implements WeaponPerks {
  id = 'Perks.SecretRound'
  name = 'Secret Round'
  description = `This damage of this weapon is increased by 1d4.`
}

export class ShieldBreaker implements WeaponPerks {
  id = 'Perks.ShieldBreaker'
  name = 'Shield Breaker'
  description = `The damage die size of this weapon is increased by one if you take a shot against a target that has energy shield points. If a shot from this weapon reduces a target's energy shield points to 0, an explosion of energy erupts from the creature. It and all other targets within 5 feet of it take 1d6 damage of the same type as the elemental alignment of the target's energy shields. If no alignment is stated, the damage type is kinetic.`
}

export class ShootToLoot implements WeaponPerks {
  id = 'Perks.ShootToLoot'
  name = 'Shoot to Loot'
  description = `If you hit a small item with a shot from this weapon, one that weighs less than 7 lbs and is not being worn or carried, your Ghost can use its reaction to transmat that item into its memory using the smart rounds in this weapon. Your Ghost can do this even from within its pocket backpack, but can only transfer one item per shot. This weapon requires special ammunition that costs an additional 500 glimmer to purchase per magazine.`
}

export class ShotPackage implements WeaponPerks {
  id = 'Perks.ShotPackage'
  name = 'Shot Package'
  description = `All ranges of this weapon increase by 5 feet. You gain a bonus +1 to attack and damage rolls when you take a shot with this weapon.`
}

export class Snapshot implements WeaponPerks {
  id = 'Perks.Snapshot'
  name = 'Snapshot'
  description = `It costs 10 less feet of movement to begin Aiming with this weapon.`
}

export class StickyGrenades implements WeaponPerks {
  id = 'Perks.StickyGrenades'
  name = 'Sticky Grenades'
  description = `This weapon fires grenades that stick to the first hard surface they hit, including creatures and unmounted objects. After sticking to a surface, the first creature to enter the grenade's space causes the grenade to detonate. All creatures that share the same space as the grenade must make a Dexterity saving throw, taking the damage of this weapon on a failed save, or half as much on a success.

If you attempt to stick this weapon's grenades to a creature, make an attack roll like normal. On a hit the grenade detonates immediately and the creature automatically fails their Dexterity saving throw. On a miss, the grenade falls to the ground beneath the creature's feet and detonates like normal.

Grenades fired from this weapon will remain in place for up to 1 minute, or until they are detonated. If you reload this weapon, any active grenades detonate immediately.`
}

export class Supercharger implements WeaponPerks {
  id = 'Perks.Supercharger'
  name = 'Supercharger'
  description = `This weapon gains the Automatic Fire property, and a bonus +1 to its damage rolls.`
}

export class Surrounded implements WeaponPerks {
  id = 'Perks.Surrounded'
  name = 'Surrounded'
  description = `If you take a shot with this weapon when two or more hostile creatures are within 15 feet of you, you have advantage on your attack roll for that shot. You can only benefit from this perk once on your turn.`
}

export class Swashbuckler implements WeaponPerks {
  id = 'Perks.Swashbuckler'
  name = 'Swashbuckler'
  description = `When you deal 6 or more damage to a hostile creature from a shot with this weapon, you can make a melee ability recharge roll.`
}

export class TakeAKnee implements WeaponPerks {
  id = 'Perks.TakeAKnee'
  name = 'Take a Knee'
  description = `While Combat-Prone, this weapon has a bonus +1 to attack and damage rolls.`
}

export class TapTheTrigger implements WeaponPerks {
  id = 'Perks.TapTheTrigger'
  name = 'Tap the Trigger'
  description = `You have advantage on the first attack roll you make with this weapon when you take a shot with it on your turn, but only if you were already holding this weapon at the start of your turn. If you make more than one attack with this weapon on your turn, or if you stop holding this weapon before the start of your next turn, you lose the effect of this perk on your next turn.`
}

export class ThreatDetectorSights implements WeaponPerks {
  id = 'Perks.ThreatDetectorSights'
  name = 'Threat Detector Sights'
  description = `While you are Aiming
    and holding this weapon in your hands, you can use your action to grant yourself advantage on Wisdom (Perception) checks you make to detect the location of hostile creatures using the sights on this weapon. If you locate a creature in this way, and the creature in Invisible due to active camouflage, it is no longer considered Invisible to you until it breaks line-of-sight and takes the Hide action again.`
}

export class Tracking implements WeaponPerks {
  id = 'Perks.Tracking'
  name = 'Tracking'
  description = `When you take a shot with this weapon while Aiming, you can choose one target for the round to track. That creature becomes the impact point of the round, and it has disadvantage on its payload saving throw. If that creature uses a reaction that results in it moving, you can move the impact point to be within 5 feet of the tracked creature after they complete their reaction. If the creature ends its reaction behind cover, the impact point becomes that cover, which can affect the creature's saving throw like normal.`
}

export class TransmatHilt implements WeaponPerks {
  id = 'Perks.TransmatHilt'
  name = 'Transmat Hilt'
  description = `As a free action on your turn, you can recall the weapon from its current position into your hands. To do this, you must be within 60 feet of the weapon, and there must not be a barrier that prevents transmat.`
}

export class TrenchBarrel implements WeaponPerks {
  id = 'Perks.TrenchBarrel'
  name = 'Trench Barrel'
  description = `If you hit a hostile creature with a melee attack, until the end of your next turn, this weapon's gains a bonus +2 to damage rolls.`
}

export class UnderPressure implements WeaponPerks {
  id = 'Perks.UnderPressure'
  name = 'Under Pressure'
  description = `If you have 0 energy shield points, you have advantage on shots you take with this weapon.`
}

export class Unflinching implements WeaponPerks {
  id = 'Perks.Unflinching'
  name = 'Unflinching'
  description = `If you are Aiming with this weapon when you are forced to make a Strength or Dexterity saving throw, you can use your reaction to have advantage on the saving throw.`
}

export class VolatileLaunch implements WeaponPerks {
  id = 'Perks.VolatileLaunch'
  name = 'Volatile Launch'
  description = `The damage die size of this weapon increases by one.`
}

export class VorpalWeapon implements WeaponPerks {
  id = 'Perks.VorpalWeapon'
  name = 'Vorpal Weapon'
  description = `If you hit an Elite, Major, or Ultra creature with a shot from this weapon, this weapon gains an additional damage die for that shot.`
}

export class WaveGrenades implements WeaponPerks {
  id = 'Perks.WaveGrenades'
  name = 'Wave Grenades'
  description = `The range of this weapon becomes 60 feet (it loses its firearm ranges), it gains the Loading property, and it gains the Payload property with an impact line instead of radius. When you take a shot with this weapon, choose a space on a hard surface within range. The grenade fired by this weapon detonates on that surface and creates a 20 foot line across the surface, going in a direction away from you. This line ends early if the surface is less than 20 feet long. All creatures standing on the line must make a payload saving throw against the damage of this weapon.`
}

export class WhirlwindFrame implements WeaponPerks {
  id = 'Perks.WhirlwindFrame'
  name = 'Whirlwind Frame'
  description = `This weapon gains the Cumbersome property, and gains a +1 bonus to attack and damage rolls. As an action, you may spend 10 shots to perform a whirlwind attack. Make an attack against all targets within 5 feet of you. You do this by making a single attack roll and comparing the result to every affected target's AC. Make a separate damage roll for every target you hit.

This attack deals the damage type chosen in the weapon's Elemental Shot Capacity perk, not its normal damage type.`
}

export class ZenMoment implements WeaponPerks {
  id = 'Perks.ZenMoment'
  name = 'Zen Moment'
  description = `Every time you hit a hostile creature with a shot from this weapon while Aiming, you gain a bonus +1 to the attack rolls of shots you take with this weapon for the next minute. This bonus can stack up to a maximum of +3, but is lost if you take any damage.`
}

export class Thunderer implements WeaponPerks {
  id = 'Perks.Thunderer'
  name = 'Thunderer'
  description = `As an action you can take up to four shots with this weapon, however, before taking your shots you must make a DC 17 Strength saving throw. On a failed save, you can only take up to two shots with this action.`
}

export class ThePaleHorse implements WeaponPerks {
  id = 'Perks.ThePaleHorse'
  name = 'The Pale Horse'
  description = `This weapon has a bonus +2 to attack and damage rolls when you take a shot with it.`
}

export class Maverick implements WeaponPerks {
  id = 'Perks.Maverick'
  name = 'Maverick'
  description = `Aiming with this weapon costs 10 less feet of movement. This weapon's shots have a bonus +2 to attack and damage rolls.`
}

export class Firefly implements WeaponPerks {
  id = 'Perks.Firefly'
  name = 'Firefly'
  description = `If you are Aiming when you kill a creature with a shot from this weapon, that creature explodes. All other targets within 5 feet of the creature take 1d6 explosive kinetic damage.`
}

export class MementoMori implements WeaponPerks {
  id = 'Perks.MementoMori'
  name = 'Memento Mori'
  description = `When you kill a hostile creature with a shot from this weapon while Aiming, this weapon gains 4 charges for the next minute. When you take a shot with this weapon and hit, you can spend a charge to add an additional damage die to this weapon's damage roll.

If you spend a charge and the result of your attack invokes this weapon's Firefly perk, the Firefly perk does 2d6 explosive kinetic damage instead.

This weapon can have a maximum of 4 charges at a time.`
}

export class LightningGrenades implements WeaponPerks {
  id = 'Perks.LightningGrenades'
  name = 'Lightning Grenades'
  description = `The grenades fired from this weapon stick to the first hard surface they hit, including corporeal creatures. When you take a shot with this weapon, choose a hard surface you can see within range for the grenade to stick to. If you attempt to stick this weapon's grenades to a creature, make an attack roll like normal. On a hit the grenade deals damage and sticks to the creature. On a miss, the grenade deals no damage, falls to the ground beneath the creature's feet, and sticks there.

Grenades fired from this weapon will remain in place for up to 1 minute. If you reload this weapon, any active grenades are destroyed. A creature can use their action to remove a stuck grenade, and if the grenades take any damage (AC 19 mounted) they are destroyed.`
}

export class ArcTraps implements WeaponPerks {
  id = 'Perks.ArcTraps'
  name = 'Arc Traps'
  description = `When two or more grenades from this weapon are within 15 feet of each other, all grenades will link together to form a line of arc energy between themselves. Creatures who start their turn on this line, or who attempt to cross this line for the first time on a turn, must make a Constitution saving throw against the weapon DC. They take 5d8 arc damage on a failed save, or half as much on a success.`
}

export class HighVoltageNodes implements WeaponPerks {
  id = 'Perks.HighVoltageNodes'
  name = 'High-Voltage Nodes'
  description = `This weapon's DC increases by 2.`
}

export class Railgun implements WeaponPerks {
  id = 'Perks.Railgun'
  name = 'Railgun'
  description = `Damage caused by a shot from this weapon cannot be reduced due to the alignment of the target's energy shields.`
}

export class CompoundingForce implements WeaponPerks {
  id = 'Perks.CompoundingForce'
  name = 'Compounding Force'
  description = `If you hit a creature with a shot from this weapon, and that creature has energy shields, add an additional damage die to your damage roll for that shot.`
}

export class DisruptionBreak implements WeaponPerks {
  id = 'Perks.DisruptionBreak'
  name = 'Disruption Break'
  description = `If the damage from this weapon reduces a target's energy shield points to 0, the target must make a Constitution saving throw against your weapon DC, becoming vulnerable to kinetic damage for the next minute on a failed save. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Targets that succeed on their saving throw, or for whom the effect ends, become immune to the effect of this perk for 24 hours.`
}

export class Sunder implements WeaponPerks {
  id = 'Perks.Sunder'
  name = 'Sunder'
  description = `If you kill a hostile creature with a shot from this weapon, this weapon gains an additional damage die for the next minute. The effect of this perk can stack with itself up to three times, but is lost if you do not take a shot with this weapon on your turn, or if you stop holding this weapon.`
}

export class StringOfCurses implements WeaponPerks {
  id = 'Perks.StringOfCurses'
  name = 'String of Curses'
  description = `If you kill a hostile creature with a shot from this weapon, you can make a super ability recharge roll with a bonus +1 to the roll. The bonus can stack up to five times but is lost if you stop holding this weapon, if you recover a super ability charge, or if you complete a brief rest.`
}

export class Breakthrough implements WeaponPerks {
  id = 'Perks.Breakthrough'
  name = 'Breakthrough'
  description = `If you take a shot with this weapon against a target that has energy shield points, this weapon gains an additional damage die for that shot. The damage of this weapon always ignores the resistances granted by energy shield alignment, if any.`
}

export class FinalStand implements WeaponPerks {
  id = 'Perks.FinalStand'
  name = 'FinalStand'
  description = `When you are reduced to 0 hit points, you can use your reaction to drop to 1 hit point instead. You can't invoke this perk again until you complete a long rest.`
}

export class SaintsFists implements WeaponPerks {
  id = 'Perks.SaintsFists'
  name = 'Saint\'s Fists'
  description = `This weapon has a bonus +2 to attack rolls when you take a shot with it. If you hit a hostile creature with a shot from this weapon, the next melee ability you cast before the end of your next turn gains two additional damage dice in its damage roll, if applicable. If you hit a hostile creature with a melee ability that deals damage to the creature, this weapon's damage die size increases by 2 until the end of your next turn.`
}

export class VoidShotCapacity implements WeaponPerks {
  id = 'Perks.VoidShotCapacity'
  name = 'Void Shot Capacity'
  description = `This weapon gains the Shot Capacity (30) property and a +2 bonus to attack rolls. When you take the Attack action with this weapon, you can spend 1 shot to deal void damage on a hit, instead of slashing damage. You must spend this shot before making your attack roll.`
}

export class CrowsWings implements WeaponPerks {
  id = 'Perks.CrowsWings'
  name = 'Crow\'s Wings'
  description = `As an action you can spend 5 shots to launch a curve of void energy in a line up to 30 feet long. This line is cut short if it collides with a hostile creature or wall. The first target the line collides with must make a Dexterity saving throw against the weapon DC, taking 3d8 explosive void damage on a failed save, or half as much on a success. You must hold this weapon in two hands in order to use this action.`
}

export class CrowsReturn implements WeaponPerks {
  id = 'Perks.CrowsReturn'
  name = 'Crow\'s Return'
  description = `If you are holding this weapon when a creature you can see makes a ranged weapon attack against you, you can use your reaction to cause disadvantage on the attack. If the attack misses, or if you succeed on your saving throw (in the case of Payload saves), the damage of the Crow's Wings perk becomes 4d8 explosive void instead.`
}

export class AncientArcCore implements WeaponPerks {
  id = 'Perks.AncientArcCore'
  name = 'Ancient Arc Core'
  description = `This weapon gains the Shot Capacity (40) property and a +2 bonus to attack and damage rolls. When you take the Attack action with this weapon, you can spend 1 shot to deal arc damage on a hit, instead of slashing damage. You must spend this shot before making your attack roll.`
}

export class LiveByTheSword implements WeaponPerks {
  id = 'Perks.LiveByTheSword'
  name = 'Live by the Sword'
  description = `Once on your turn, if you kill a hostile creature with damage from this weapon, any Risen creatures of your choice (other than yourself) within 10 feet of you may make either a melee, grenade, or superclass recharge roll, their choice.`
}

export class SwordOfThunder implements WeaponPerks {
  id = 'Perks.SwordOfThunder'
  name = 'Sword of Thunder'
  description = `As an action you can spend 10 shots to launch a torus of arc energy with a 5-foot diameter in a line up to 120 feet long. This line is cut short if it collides with a hostile creature or wall. The first target the line collides with, as well as all creatures within 5 feet of it, must make a Constitution saving throw against the weapon DC, taking 4d8 arc damage on a failed save, or half as much on a success. You must hold this weapon in two hands in order to use this action.`
}

export class FiringMode implements WeaponPerks {
  id = 'Perks.FiringMode'
  name = 'Firing Mode'
  description = `Once on your turn, you can use a free action to change this weapon's active firing mode. You only benefit from the effect of the active firing mode, and only while holding this weapon in your hands.

- One Way. Your base walking speed increases by 5 feet.
- Or Another. Your AC increases by 1.`
}

export class TheFundamentals implements WeaponPerks {
  id = 'Perks.TheFundamentals'
  name = 'The Fundamentals'
  description = `You can use a free action to change the type of damage this weapon deals. You can choose between arc, solar, or void damage.`
}

export class IonicReturn implements WeaponPerks {
  id = 'Perks.IonicReturn'
  name = 'Ionic Return'
  description = `If a shot from this weapon reduces a target's energy shield points to 0, this weapon gains an additional damage die for the next minute.`
}

export class FourHeadedDog implements WeaponPerks {
  id = 'Perks.FourHeadedDog'
  name = 'Four-Headed Dog'
  description = `When you take a shot with this weapon while not Aiming, you make an attack against all targets in a 15-foot cone. You do this by making a single attack roll and comparing the result to every affected target's AC. Make a separate damage roll for every target you hit. Attacking in this way only consumes one shot from this weapon's magazine regardless of how many targets you hit.`
}

export class SpreadShotPackage implements WeaponPerks {
  id = 'Perks.SpreadShotPackage'
  name = 'Spread Shot Package'
  description = `When you take a shot with this weapon while Aiming, you ignore the effect of the Four-Headed Dog perk and make your attack like normal.`
}

export class FullBore implements WeaponPerks {
  id = 'Perks.FullBore'
  name = 'Full Bore'
  description = `Before you take a shot with this weapon, you can choose to go full bore and take a -5 penalty to your attack roll. If you hit, this weapon deals a bonus +10 damage.`
}

export class ColdFusion implements WeaponPerks {
  id = 'Perks.ColdFusion'
  name = 'Cold Fusion'
  description = `If you hit a creature with a shot from this weapon, that creature must succeed on a Constitution saving throw against the weapon DC or have their movement speed halved for the next mintue. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. A creature that succeeds on its saving throw, or for whom the effect ends, becomes immune to the effect of this perk for 24 hours.`
}

export class LongestWinter implements WeaponPerks {
  id = 'Perks.LongestWinter'
  name = 'Longest Winter'
  description = `This weapon deals an additional 2d6 arc damage to targets affected by the Cold Fusion perk.`
}

export class AncientVoidFuller implements WeaponPerks {
  id = 'Perks.AncientVoidFuller'
  name = 'Ancient Void Fuller'
  description = `This weapon gains the Shot Capacity (40) property and a +2 bonus to attack and damage rolls. When you take the Attack action with this weapon, you can spend 1 shot to deal void damage on a hit, instead of slashing damage. You must spend this shot before making your attack roll.`
}

export class DieByTheSword implements WeaponPerks {
  id = 'Perks.DieByTheSword'
  name = 'Die By The Sword'
  description = `Once on your turn, if you kill a hostile creature with damage from this weapon, you may recover 2d6 shield points.`
}

export class SupermassiveVortex implements WeaponPerks {
  id = 'Perks.SupermassiveVortex'
  name = 'Supermassive Vortex'
  description = `As an action you can spend 10 shots to perform a whirlwind attack that utilizes gravitational forces. All targets within 5 feet of you must make a Strength saving throw against the weapon DC, taking 4d10 void damage on a failed save, or half as much on a success. Also on a failed save, a target is knocked prone.`
}

export class DarkDeliverance implements WeaponPerks {
  id = 'Perks.DarkDeliverance'
  name = 'Dark Deliverance'
  description = `This weapon fires rockets full of void seeker orbs. When you take a shot with this weapon you can choose any point along the line of the shot for the rocket to break apart, releasing the void orbs it contains. If the void orbs are not released by the time the rocket reaches its maximum range, they are released automatically at maximum range.

Once released, the void orbs fall up to 40 feet directly downward, or until they hit the nearest creature or hard surface, whichever happens first. The orbs detonate when they complete their movement.

You resolve the Payload effect of this weapon whenever the orbs detonate. The center of the Payload effect is the center of the orbs' detonation point.`
}

export class DarkDescent implements WeaponPerks {
  id = 'Perks.DarkDescent'
  name = 'Dark Descent'
  description = `The damage of this weapon increases the farther the void orbs fall before detonating. For every 10 feet the orbs fall before detonating, the Payload radius increases by 5 feet, and the damage of the shot increases by 1d8.`
}

export class CloseTheGap implements WeaponPerks {
  id = 'Perks.CloseTheGap'
  name = 'Close The Gap'
  description = `As an action you can fire a highpowered laser from this gun. The laser has the Payload property with a 40-foot sweeping line instead of a radius. Creatures that fail their Payload saving throw take 4d8 solar damage, and creatures that succeed take half as much.`
}

export class ASymbol implements WeaponPerks {
  id = 'Perks.ASymbol'
  name = 'A Symbol'
  description = `If you end your turn holding this weapon in at least one hand, you are immune to being Frightened until the end of your next turn. If you were already Frightened, the condition is suspended for you while holding this weapon in at least one hand.`
}

export class Judgment implements WeaponPerks {
  id = 'Perks.Judgment'
  name = 'Judgment'
  description = `This weapon has a bonus +3 to attack rolls when you take a shot with it. When you hit a creature with a shot from this weapon, it becomes Weakened until the start of your next turn.`
}

export class Penance implements WeaponPerks {
  id = 'Perks.Penance'
  name = 'Penance'
  description = `Creatures hit with a shot from this weapon also gain a Penance Charge, to a max of 3 charges. On the attack that applies the third Penance Charge, the creature takes 5d6 explosive arc damage and it loses all Penance Charges. The creature also loses all Penance charges if you end your turn without attacking it with a shot from this weapon.`
}

export class DelayedGratification implements WeaponPerks {
  id = 'Perks.DelayedGratification'
  name = 'Delayed Gratification'
  description = `Shots from this weapon can deflect off of hard surfaces and tangible creatures, allowing the line of fire to alter direction up to three times along its length, to a maximum length of 60 feet. Targets are always considered to be within the effective range of this weapon when subject to the Payload effect from it.`
}

export class ThinTheHerd implements WeaponPerks {
  id = 'Perks.ThinTheHerd'
  name = 'Thin The Herd'
  description = `When you kill a hostile creature with a shot from this weapon, this weapon gains an additional damage die in its damage rolls for the next minute. This perk can stack with itself up to three times, to a maximum of three additional damage dice.`
}

export class SmartDriftControl implements WeaponPerks {
  id = 'Perks.SmartDriftControl'
  name = 'Smart Drift Control'
  description = `It costs 10 less feet of movement to begin Aiming with this weapon. The Payload DC of this weapon is increased by 2.`
}

export class WolfpackRounds implements WeaponPerks {
  id = 'Perks.WolfpackRounds'
  name = 'Wolfpack Rounds'
  description = `When the round fired from this weapon detonates, it unleashes a trio of Wolfpack Seekers centered on the space of detonation. First, resolve this weapon’s payload damage. Then choose up to 3 creatures within this weapon’s payload effect for the Wolfpack Seekers to target. You can choose multiple creatures, or you can choose the same creature multiple times. Each seeker deals an additional 1d8 explosive solar damage to its target.`
}

export class OverpenetrationRounds implements WeaponPerks {
  id = 'Perks.OverpenetrationRounds'
  name = 'Overpenetration Rounds'
  description = `The rounds fired from this weapon can pierce most barriers, but are blocked by 1 inch of metal, 2 inches of stone, or 3 feet of wood or dirt. These rounds can also damage all creatures in a line up to this weapon’s effective range (while Aiming, up to extended range instead). In order to do this, your attack must have killed the previous target, and your attack roll must beat the AC of the subsequent target. Subsequent targets take the remaining damage of the previous target.`
}

export class VolatileLight implements WeaponPerks {
  id = 'Perks.VolatileLight'
  name = 'Volatile Light'
  description = `When you take a shot with this weapon, you briefly shed bright light in a 5-foot radius, and dim light 10 feet beyond that. This illumination stops at the end of your turn. Additionally, if you take a shot with this weapon and miss, roll a d6. On a 4, 5, or 6, you can choose one other target within 30 feet of your original target to attack instead, using your original attack roll. If your original attack roll would hit the new target, roll damage against the new target. This weapon gains an additional damage die in its damage roll against the new target. If you roll a 1 on the d6, the new target is yourself.`
}

export class HoldingAces implements WeaponPerks {
  id = 'Perks.HoldingAces'
  name = 'Holding Aces'
  description = `For this weapon's Luck in the Chamber perk, you roll three d6s instead of only one, resolving each d6 as described by Luck in the Chamber.`
}

export class SpinningUp implements WeaponPerks {
  id = 'Perks.SpinningUp'
  name = 'Spinning Up'
  description = `You cannot take a shot with this weapon unless its barrels have begun spinning. As a bonus action, you can spend half your movement to begin spinning its barrels. The weapon's barrels remain spinning until the start of your next turn.`
}

export class ArmorOfTheColossus implements WeaponPerks {
  id = 'Perks.ArmorOfTheColossus'
  name = 'Armor Of The Colossus'
  description = `When you invoke the Spinning-Up perk, you also gain an overshield that lasts until the end of your next turn. While you have at least 1 point in this overshield, you have advantage on Strength saving throws, you cannot be knocked prone, and you have a vulnerability to arc damage. This overshield is lost early if you stop holding this weapon.`
}

export class ElegantDesign implements WeaponPerks {
  id = 'Perks.ElegantDesign'
  name = 'Elegant Design'
  description = `This weapon has a bonus +1 to attack and damage rolls when you take a shot with it.`
}

export class ChargeShot implements WeaponPerks {
  id = 'Perks.ChargeShot'
  name = 'Charge Shot'
  description = `When you take a shot with this weapon and miss, the target must make a Dexterity saving throw against the weapon DC, taking half the damage of your shot on a failed save.`
}

export class OverheatedRounds implements WeaponPerks {
  id = 'Perks.OverheatedRounds'
  name = 'Overheated Rounds'
  description = `Creatures that take damage from a shot from this weapon must also succeed on a Constitution saving throw against the weapon DC or begin Burning for the next minute. A Burning creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Those that succeed on their saving throw, or for whom the effect ends, become immune to the effect of this perk for 24 hours.`
}

export class Toasty implements WeaponPerks {
  id = 'Perks.Toasty'
  name = 'Toasty'
  description = `Creatures Burning from the Overheated Rounds perk take 1d6 solar damage at the start of each of their turns.`
}

export class ModularDesign implements WeaponPerks {
  id = 'Perks.ModularDesign'
  name = 'Modular Design'
  description = `Once on your turn, you can use a free action to change this weapon's active firing mode. This weapon only benefits from the effects of its active firing mode.

- Automatic. This weapon is considered an auto rifle, including all traits and for determining proficiency with it.
- Burst Fire. This weapon is considered a pulse rifle, including all traits and for determining proficiency with it.
- Semi-Auto. This weapon is considered a scout rifle, including all traits and for determining proficiency with it.`
}

export class PoisonArrows implements WeaponPerks {
  id = 'Perks.PoisonArrows'
  name = 'Poison Arrows'
  description = `If you hit a creature with a shot from this weapon, the creature must succeed on a Constitution saving throw against the weapon DC or become Poisoned for the next minute. Creatures Poisoned in this way 1d8 void damage at the start of each of their turns and can make a Constitution saving throw at the end of each of their turns, ending the effect on itself early on a success. Creatures that succeed on their saving throw, or for whom the effect ends, become immune to the effect of this perk for 24 hours.`
}

export class Hawkeye implements WeaponPerks {
  id = 'Perks.Hawkeye'
  name = 'Hawkeye'
  description = `If you take a shot with this weapon while Aiming, your critical hit range for that shot is increased by 1.`
}

export class LeMonarque implements WeaponPerks {
  id = 'Perks.LeMonarque'
  name = 'Le Monarque'
  description = `If you score a critical hit with a shot from this weapon, all creatures within 5 feet of the target are subject to the effects of the Poison Arrows perk.`
}

export class ShockBlast implements WeaponPerks {
  id = 'Perks.ShockBlast'
  name = 'Shock Blast'
  description = `The blast fired from this weapon can pierce most barriers, but is blocked by 1 inch of metal, 2 inches of stone, or 3 feet of wood or dirt. The blast can also damage all creatures up to the maximum range of this weapon, using a single attack roll against all potential targets.`
}

export class LongMarch implements WeaponPerks {
  id = 'Perks.LongMarch'
  name = 'Long March'
  description = `This weapon's on-board computer links up with your Guardian armor. You are aware of all creatures within 20 feet of you, and you do not have disadvantage on attack rolls you make against invisible creatures.`
}

export class RecoilCompensators implements WeaponPerks {
  id = 'Perks.RecoilCompensators'
  name = 'Recoil Compensators'
  description = `This weapon has a bonus +3 to attack rolls when you take a shot with it.`
}

export class ChainBowstring implements WeaponPerks {
  id = 'Perks.ChainBowstring'
  name = 'Chain Bowstring'
  description = `You have a bonus +2 to attack and damage rolls when you take a shot with this weapon, and the critical hit range of this weapon is increased by 1.`
}

export class LeviathansSigh implements WeaponPerks {
  id = 'Perks.LeviathansSigh'
  name = `Leviathan's Sigh`
  description = `If you hit a creature with a shot from this weapon, that creature must succeed on a Strength saving throw against the weapon DC or be knocked prone. If the creature is already prone and grounded, the creature also becomes Restrained by the arrow (escape DC equal to weapon DC).`
}

export class ReleaseTheWolves implements WeaponPerks {
  id = 'Perks.ReleaseTheWolves'
  name = 'Release The Wolves'
  description = `As an action, you can take up to three shots with this weapon. You must use your action on your next turn to reload this weapon before you can take a shot with it again.`
}

export class HowlOfThePack implements WeaponPerks {
  id = 'Perks.HowlOfThePack'
  name = 'Howl Of The Pack'
  description = `If you kill a hostile creature with a shot from this weapon, until the start of your next turn, Risen creatures who end their turn within 10 feet of you can make a shield recharge roll using your shield recharge formula instead of their own.`
}


/*

export class  implements WeaponPerks {
  id = 'Perks.'
  name = ''
  description = ``
}
*/

export const allWeaponPerks = [
  new AcceleratedCoils,
  new AdaptiveFrame,
  new ArchersTempo,
  new ArmorPiercingRounds,
  new AugmentedFocus,
  new ArmyOfOne,
  new BattleRunner,
  new BlackPowder,
  new BoxBreathing,
  new BurstFrame,
  new CasterFrame,
  new ClusterBombs,
  new CrowdControl,
  new Demolitionist,
  new DualBarrel,
  new Elemental,
  new ElementalShotCapacity,
  new EnduringEdge,
  new EnhancedBattery,
  new Exhumed,
  new ExplosiveRounds,
  new ExtendedBarrel,
  new FeatherweightBolt,
  new FieldScout,
  new FinalRound,
  new FiringLine,
  new FocusedFire,
  new FullCourt,
  new HeadSeeker,
  new HeavyFrame,
  new HeavyPayload,
  new HiddenHand,
  new HipFire,
  new HonedEdge,
  new ImpactCasing,
  new JaggedEdge,
  new Javelin,
  new Lightweight,
  new LightweightFrame,
  new LuckInTheChamber,
  new Mulligan,
  new NaturalString,
  new OneTwoPunch,
  new OpeningShot,
  new Osmosis,
  new Outlaw,
  new ParticleScattering,
  new PerfectBalance,
  new Persistence,
  new PrecisionFrame,
  new Rangefinder,
  new RapidHit,
  new RicochetRounds,
  new RifledBarrel,
  new SecretRound,
  new ShieldBreaker,
  new ShootToLoot,
  new Snapshot,
  new StickyGrenades,
  new Supercharger,
  new Surrounded,
  new Swashbuckler,
  new TakeAKnee,
  new TapTheTrigger,
  new ThreatDetectorSights,
  new Tracking,
  new TransmatHilt,
  new TrenchBarrel,
  new UnderPressure,
  new Unflinching,
  new VolatileLaunch,
  new VorpalWeapon,
  new WaveGrenades,
  new WhirlwindFrame,
  new ZenMoment,
  new Thunderer,
  new ThePaleHorse,
  new Maverick,
  new Firefly,
  new MementoMori,
  new LightningGrenades,
  new ArcTraps,
  new HighVoltageNodes,
  new Railgun,
  new CompoundingForce,
  new DisruptionBreak,
  new Sunder,
  new StringOfCurses,
  new Breakthrough,
  new FinalStand,
  new SaintsFists,
  new VoidShotCapacity,
  new CrowsWings,
  new CrowsReturn,
  new AncientArcCore,
  new LiveByTheSword,
  new SwordOfThunder,
  new FiringMode,
  new TheFundamentals,
  new IonicReturn,
  new FourHeadedDog,
  new SpreadShotPackage,
  new FullBore,
  new ColdFusion,
  new LongestWinter,
  new AncientVoidFuller,
  new DieByTheSword,
  new SupermassiveVortex,
  new DarkDeliverance,
  new DarkDescent,
  new CloseTheGap,
  new ASymbol,
  new Judgment,
  new Penance,
  new DelayedGratification,
  new ThinTheHerd,
  new SmartDriftControl,
  new WolfpackRounds,
  new OverpenetrationRounds,
  new VolatileLight,
  new HoldingAces,
  new SpinningUp,
  new ArmorOfTheColossus,
  new ElegantDesign,
  new ChargeShot,
  new OverheatedRounds,
  new Toasty,
  new ModularDesign,
  new PoisonArrows,
  new Hawkeye,
  new LeMonarque,
  new ShockBlast,
  new LongMarch,
  new RecoilCompensators,
  new ChainBowstring,
  new LeviathansSigh,
  new ReleaseTheWolves,
  new HowlOfThePack
]
