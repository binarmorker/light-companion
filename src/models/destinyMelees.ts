import { Ability, ChargeType, DamageType, LightAbility, LightElement, MeleeAbility } from '@/models/destiny'

export class ThrowingKnifeMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.ThrowingKnife
  element = LightElement.Solar
  name = 'Throwing Knife'
  description = `You use your Light to summon a small, lightweight knife into a free hand and attack with it. Make a ranged Light attack roll. Add a bonus to your attack roll equal to your Light level. On a hit, the target takes 3d6 solar damage. Hit or miss, the knife dissipates after.`
  castingTime = '1 action'
  range = '20/60 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '3d6', type: DamageType.Solar }
  ]
}

export class ElectrifyingStrikeMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.ElectrifyingStrike
  element = LightElement.Arc
  name = 'Electrifying Strike'
  description = `You strike out against a target you can touch. Make a melee Light attack roll. On a hit, the target takes 2d6 arc damage and becomes Electrified for the next minute. You only spend your melee ability charge if you successfully hit a target.

An Electrified creature can make a Constitution saving throw at the end of each of their turns to end the effect early. A creature who succeeds on their saving throw, or for which the effect ends, becomes immune to being Electrified in this way for 24 hours.

**At Higher Levels.** The damage of this Light ability increases by 1d6 at 5th level (3d6), 11th level (4d6), and 17th level (5d6).`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod]'
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d6', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class DisorientingBlowMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.DisorientingBlow
  element = LightElement.Arc
  name = 'Disorienting Blow'
  description = `You punch a target that you can touch. Make a melee Light attack roll. On a hit, the target takes 2d6 arc damage and must make a Constitution saving throw. On a failed save, they are Incapacitated until the end of their next turn. You only spend your melee ability charge if you successfully hit a target.

**At Higher Levels.** The damage of this Light ability increases by 1d6 at 5th level (3d6), 11th level (4d6), and 17th level (5d6).`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d6', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class BlinkMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.Blink
  element = LightElement.Arc
  name = 'Blink'
  description = `Starting at 2nd level, your practice attuning your movements to your Light endows you with the ability to close into melee range not merely by moving through space, but instantaneously translocating across it. As a bonus action you can spend your melee ability charge to teleport up to 10 feet to an unoccupied space you can see, taking all carried and worn equipment of your choice with you when you do.

You have a single melee ability charge. If you spend your melee ability charge, you regain it when you make a successful recharge roll (d6, 5-6) or complete a brief rest.`
  castingTime = '1 bonus action'
  range = '10 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
}

export class StormFistMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.StormFist
  element = LightElement.Arc
  name = 'Storm Fist'
  description = `You channel arc Light into your fist and swing at a target you can touch. Make a melee Light attack roll. On a hit, you deal 2d10 + your Constitution modifier in arc damage. You only spend your melee ability charge if you successfully hit a target.

**At Higher Levels.** As you gain Striker levels, the number of damage dice you roll for Storm Fist increases. It becomes 3d10 at 5th level, 4d10 at 11th level, and 5d10 at 17th level.`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod]'
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d10 + [conMod]', type: DamageType.Arc }
  ]
}

export class SmokeMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.Smoke
  element = LightElement.Void
  name = 'Smoke'
  description = 'You devise a clever, compact smoke bomb infused with a pinch of void Light, which you throw at a hard surface you can see within range. Upon impact, the puck breaks apart and releases an opaque cloud of green-black smoke that spreads to fill a sphere with a 10-foot radius. The cloud spreads around corners, and the area of the cloud is considered heavily obscured. A creature with darkvision can\'t see through the cloud, and nonmagical/non-paracausal sources of light can\'t illuminate it. Creatures of your choice can see through the cloud as if it were only a lightly obscured area. The cloud disperses at the start of your next turn.'
  castingTime = '1 action'
  range = '40 feet, mortar'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 6'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
}

export class DisintegrateMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.Disintegrate
  element = LightElement.Void
  name = 'Disintegrate'
  description = `You summon your void Light into your fist and strike out against a target you can touch. Make a melee Light attack roll. On a hit, you deal 2d10 + your Charisma modifier in void damage and gain an overshield that lasts for the next minute. You cannot make melee ability recharge rolls while you have this overshield. You only spend your melee ability charge if you successfully hit a target.

**At Higher Levels.** As you gain Defender levels, the number of damage dice you roll for Disintegrate increases. It becomes 3d10 at 5th level, 4d10 at 11th level, and 5d10 at 17th level.`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod]'
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d10 + [chaMod]', type: DamageType.Void }
  ]
}

export class SunstrikeMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.Sunstrike
  element = LightElement.Solar
  name = 'Sunstrike'
  description = `You coat your fist in the flames of your solar Light and strike out against a target you can touch. Make a melee Light attack roll. On a hit, you deal 2d10 + your Wisdom modifier in solar damage. You only spend your melee ability charge if you successfully hit a target.

**At Higher Levels.** As you gain Sunbreaker levels, the number of damage dice you roll for Sunstrike increases. It becomes 3d10 at 5th level, 4d10 at 11th level, and 5d10 at 17th level.`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod]'
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d10 + [wisMod]', type: DamageType.Solar }
  ]
}

export class EnergyDrainMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.EnergyDrain
  element = LightElement.Void
  name = 'Energy Drain'
  description = `You thrust out your hand, void Light reaching from your fingertips, ripping at a target within reach. The target must make a Dexterity saving throw against your Light save DC. They take 2d8 + your Intelligence modifier in void damage on a failed save, or half as much on a success.

**At Higher Levels.** The number of damage dice you roll for this Light ability increases as you reach higher Voidwalker levels. It becomes 3d8 at 5th level, 4d8 at 11th level, and 5d8 at 17th level.`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d8 + [intMod]', type: DamageType.Void }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class ScorchMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.Scorch
  element = LightElement.Solar
  name = 'Scorch'
  description = `You strike out with solar Light against a target within reach, and the target must make a Dexterity saving throw. On a failed save, the target takes 2d8 + your Charisma modifier in solar damage. On a successful save, they take half as much.

**At Higher Levels.** The number of damage dice you roll for this Light ability increases as you reach higher Sunsinger levels. It becomes 3d8 at 5th level, 4d8 at 11th level, and 5d8 at 17th level.`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d8 + [chaMod]', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class ThunderstrikeMelee implements LightAbility {
  _type = 'LightAbility'
  id = MeleeAbility.Thunderstrike
  element = LightElement.Arc
  name = 'Thunderstrike'
  description = `You thrust out your hand, and a bolt of arc Light leaps from your fingertips toward a target within reach. The target must make a Dexterity saving throw against your Light save DC. They take 2d8 + your Wisdom modifier in arc damage on a failed save, or half as much on a success.

**At Higher Levels.** The number of damage dice you roll for this Light ability increases as you reach higher Stormcaller levels. It becomes 3d8 at 5th level, 4d8 at 11th level, and 5d8 at 17th level.`
  castingTime = '1 action'
  range = '5 feet'
  cost = 1
  resource = ChargeType.Melee
  recharge = 'd6, 5'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + (([level] + 1) / 6))d8 + [wisMod]', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export const allMelees: LightAbility[] = [
  new ThrowingKnifeMelee,
  new ElectrifyingStrikeMelee,
  new DisorientingBlowMelee,
  new BlinkMelee,
  new StormFistMelee,
  new SmokeMelee,
  new DisintegrateMelee,
  new SunstrikeMelee,
  new EnergyDrainMelee,
  new ScorchMelee,
  new ThunderstrikeMelee
]
export const meleeIndex = allMelees.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<MeleeAbility, LightAbility>)
export const getMelee = (id: MeleeAbility, level: number) => (meleeIndex[id]?.prerequisiteLevel || 0) <= level ? meleeIndex[id] : null
