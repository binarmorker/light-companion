import { Ability, ChargeType, DamageType, LightAbility, OtherAbility, SubclassFeature } from '@/models/destiny'

export class AmplifiedHitsFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerAmplifiedHits
  name = 'Amplified Hits'
  description = 'When you make a successful attack roll, you can increase the damage of the attack by 1d6. At Higher Levels. The damage of this focus action becomes 1d8 at 5th Nightstalker level, 1d10 at 11th Nightstalker level, and finally, it becomes 1d12 at 17th Nightstalker level.'
  cost = 1
  resource = ChargeType.Subclass
  damage = [
    { formula: '1d((3 + (([level] + 1) / 6)) * 2)', type: DamageType.Kinetic }
  ]
}

export class FlatlineFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerFlatline
  name = 'Flatline'
  description = 'Your Light goes cold as the void. For the duration, you appear as a dead creature if scanned, and your heartbeat cannot be detected.'
  cost = 2
  resource = ChargeType.Subclass
  duration = 'Up to 10 minutes'
}

export class StudyFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerStudy
  name = 'Study'
  description = `As a bonus action, you learn key information about one target that you can see. Make a Wisdom (Perception) check with a DC equal to 8 + the CR of the target (round up to 1 for creatures of a CR less than 1). If the target is aware of your presence, the target can choose to contest your check with a Wisdom (Survival) or Charisma (Deception) check, target's choice. If you are seeing this target through a video source, you can use this focus action once for every 10 minutes of clear video recorded of the target.

If your Wisdom (Perception) check meets or exceeds the DC, or if it meets or exceeds the total for the target's contested check, you learn one of the following about the target:
- Armor class
- Current hit points
- Highest ability score
- Energy shield alignment
- Any one weakness, resistance, or immunity the
  target has
- One saving throw the target is proficient in, if any`
  cost = 1
  resource = ChargeType.Subclass
}

export class FarsightFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerFarsight
  name = 'Farsight'
  description = 'For the next minute, weapons with a scope have their scope values increase by half of their current value for you. Round down to the nearest multiple of 5 when you do this. For example, a weapon with scope ranges of 35/50/90 would see its ranges become 50/75/135 for you.'
  cost = 4
  resource = ChargeType.Subclass
}

export class AtTheReadyFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerAtTheReady
  name = 'At the Ready'
  description = 'As your action, pick a spot you can see within the extended range of a ranged weapon you are holding. Until the start of your next turn, whenever a creature moves within 15 feet of that spot, you can take a shot against them. You can only make one shot against a creature, and you can only take shots against a maximum number of creatures equal to your Wisdom modifier.'
  cost = 3
  resource = ChargeType.Subclass
  prerequisiteLevel = 5
}

export class PhantomStrideFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerPhantomStride
  name = 'Phantom Stride'
  description = 'Your base walking speed increases by 10 feet for the duration. You cannot benefit from this focus action more than once at a time.'
  cost = 4
  resource = ChargeType.Subclass
  prerequisiteLevel = 5
  duration = '10 minutes'
}

export class PredatorsEyeFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerPredatorsEye
  name = 'Predator\'s Eye'
  description = 'You grant yourself advantage on the first attack roll you make before the start of your next turn.'
  cost = 4
  resource = ChargeType.Subclass
  prerequisiteLevel = 5
}

export class StabilizeFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerStabilize
  name = 'Stabilize'
  description = 'You end an effect that is causing you to be Blinded, Burning, Charmed, Deafened, Frightened, or Poisoned.'
  cost = 4
  resource = ChargeType.Subclass
  prerequisiteLevel = 9
}

export class SpectersShroudFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerSpectersShroud
  name = 'Specter\'s Shroud'
  description = 'Duration: Concentration, up to 10 minutes As an action you can spend focus and exert your Light to dissociate out of the normal phase of space, becoming almost as unseen as the void itself. For the duration, you have a bonus +10 on Dexterity (Stealth) checks you make, creatures have disadvantage on attack rolls against you, and you can\'t be tracked except by paracausal means. You leave behind no physical tracks or other traces of your passage. You cannot benefit from this focus action more than once at a time.'
  cost = 8
  resource = ChargeType.Subclass
  prerequisiteLevel = 13
}

export class StunningBlowFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerStunningBlow
  name = 'Stunning Blow'
  description = 'As an action you make a single attack against a creature. On a hit, the creature must succeed on a Constitution saving throw (DC = 8 + your proficiency bonus + your Wisdom modifier). On a failed save, they are Stunned for the next minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success.'
  cost = 10
  resource = ChargeType.Subclass
  prerequisiteLevel = 17
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class RefreshFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerRefresh
  name = 'Refresh'
  description = 'You regain up to one melee, grenade, and superclass ability charge, and you can make a shield recharge roll. You must complete a long rest before you can use this focus action again.'
  cost = 16
  resource = ChargeType.Subclass
  prerequisiteLevel = 20
}

export class ShadestepFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerShadestep
  name = 'Shadestep'
  description = 'You spend focus to step into the shadows, teleporting to an unoccupied space you can see within 10 feet of you. You take all carried and worn equipment of your choice with you when you do this.'
  cost = 3
  resource = ChargeType.Subclass
}

export class TruesightFocus implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.NightstalkerTruesight
  name = 'Truesight'
  description = `You spend focus and, for the next minute, you gain Light-enhanced supernatural sight that allows you to see clearly in normal, paracausal, and magical darkness, and you can see all creatures and objects as ghostly outlines, even invisible ones. Furthermore, you automatically detect visual illusions and succeed on
  saving throws against them.

You can see in this way to a range of 60 feet. This sight can penetrate most barriers but is blocked by 1 foot of stone, 1 inch of common metal, a thin sheet of lead, or 3 feet of wood or dirt, as well as by any barriers that prevent divination.`
  cost = 2
  duration = 'Up to 1 minute'
  resource = ChargeType.Subclass
}

export class AlternatingCurrentAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerAlternatingCurrent
  name = 'Alternating Current'
  description = `On your first turn after rolling initiative, before taking any action or bonus action or using any movement, you can spend arc charges to re-roll your initiative once. You can choose to use either your original roll or the new roll. If you choose the new roll, this arc action costs you an additional 2 charges (you cannot choose this if you do not have enough charges). If the new roll is higher than your current roll, for the next minute, your base walking speed increases by 10 feet and the first 10 feet of movement on your turn does not provoke attacks of opportunity. If the new roll is less than your original roll, you have a bonus +2 to your AC for the next minute instead.

If you take the new roll, you continue with your current turn as normal. Your initiative changes to the new roll at the end of the round.`
  cost = 3
  resource = ChargeType.Subclass
  prerequisiteLevel = 13
}

export class BoltAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerBolt
  name = 'Bolt'
  description = `When you make a successful attack roll, you can increase the damage of the attack by 1d6 arc.

**At Higher Levels.** The damage of this arc action becomes 1d8 at 5th Stormcaller level, 1d10 at 11th Stormcaller level, and finally, it becomes 1d12 at 17th Stormcaller level.`
  cost = 1
  resource = ChargeType.Subclass
  damage = [
    { formula: '1d((3 + (([level] + 1) / 6)) * 2)', type: DamageType.Arc }
  ]
}

export class DischargeAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerDischarge
  name = 'Discharge'
  description = `When you roll damage for your melee ability, grenade ability, or super ability, you can automatically deal the maximum for that ability.`
  cost = 10
  resource = ChargeType.Subclass
  prerequisiteLevel = 20
}

export class InductionAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerInduction
  name = 'Induction'
  description = `Until the end of your turn, if you have the option to attack multiple times when you take the Attack action, your Thunderstrike can replace one or more of them.`
  cost = 2
  resource = ChargeType.Subclass
  prerequisiteLevel = 5
}

export class OverchargedAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerOvercharged
  name = 'Overcharged'
  description = `As an action, you can imbue any willing creature you can touch with a surge of arc Light. For the duration, the creature's speed is doubled, it has a bonus +2 to its AC and advantage on Dexterity saving throws, and it gains 10 temporary arc charges. These arc charges are kept in a separate pool. Once on each of its turns, the creature can spend a number of these temporary charges to take an additional action. The number of charges the creature spends is determined by the additional action the creature takes. A creature can only benefit from the Overcharged arc action once at a time.
- **1 Arc Charge**: Dash, Disengage, Hide, or Use an Object
- **2 Arc Charges**: Make one non-Loading weapon attack, Reload a Cumbersome or non-proficient weapon
- **3 Arc Charges**: Cast a melee or grenade Light ability, make one Loading weapon attack`
  cost = 5
  resource = ChargeType.Subclass
  duration = 'Concentration, up to 10 minutes'
  prerequisiteLevel = 9
}

export class OverrideAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerOverride
  name = 'Override'
  description = `You touch a digital lock, one that has not been equipped to resist Light interference, and override its function, allowing you to instantly unlock it.`
  cost = 2
  resource = ChargeType.Subclass
  prerequisiteLevel = 2
}

export class RechargeAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerRecharge
  name = 'Recharge'
  description = `You can spend a number of arc charges, up to your Wisdom modifier, to extend the life of a common battery or power cell, such as the kind found in tablet computers or handheld devices. The number of arc charges you spend determines the amount of battery life, in hours, you grant the device. Because of the paracausal nature of Light, you can use this arc action even on a non-rechargeable battery, though the battery becomes irrevocably destroyed when its Light-induced life ends.`
  resource = ChargeType.Subclass
}

export class RefreshAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerRefresh
  name = 'Refresh'
  description = `You regain up to one melee, grenade, and superclass ability charge, and you can make a shield recharge roll. You must complete a long rest before you can use this arc action again.`
  cost = 16
  resource = ChargeType.Subclass
  prerequisiteLevel = 20
}

export class WindswiftAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerWindswift
  name = 'Windswift'
  description = `You take the Dash, Disengage, or Dodge action as a bonus action.`
  cost = 4
  resource = ChargeType.Subclass
  prerequisiteLevel = 5
}

export class ZaplightAction implements LightAbility {
  _type = 'LightAbility'
  id = OtherAbility.StormcallerZaplight
  name = 'Zaplight'
  description = `You create a tiny orb of blue light no bigger than a small coin. For the next hour, this orb hovers over your shoulder and shines bright light in a 10-foot radius, and dim light 5 feet beyond that. You can dismiss this orb at any time (no action cost).`
  cost = 1
  resource = ChargeType.Subclass
  duration = '1 hour'
}

export class DefenseSpecialty implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.TitanDefense
  name = 'Defense'
  description = `While wearing Guardian armor, you have a bonus +1 to your AC.`
}

export class DualWielderSpecialty implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.TitanDualWielder
  name = 'Dual Wielder'
  description = `While dual-wielding, if you make an offhand attack that hits, you can add your ability modifier to the damage of your offhand weapon. You can also dual-wield a one-handed weapon with an agile weapon, though you can only use your agile weapon as your offhand weapon attack. See Chapter 7 for more information on dual-wielding.`
}

export class GeneralistSpecialty implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.TitanGeneralist
  name = 'Generalist'
  description = `When you are wielding one simple firearm or one simple melee weapon and no other weapons, you have a bonus +2 to damage rolls with that weapon.`
}

export class HeavyWeaponsExpertSpecialty implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.TitanHeavyWeaponsExpert
  name = 'Heavy Weapons Expert'
  description = `When you roll a 1 or 2 on a damage die for an attack you make with a martial weapon or firearm, you can reroll the die and must use the new roll, even if the new roll is a 1 or a 2.`
}

export class LoadedForBearSpecialty implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.TitanLoadedForBear
  name = 'Loaded for Bear'
  description = `You have a bonus +2 to attack rolls you make with weapons that have the Loading property.`
}

export class MasterAtArmsSpecialty implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.TitanMasterAtArms
  name = 'Master at Arms'
  description = `You have a bonus +1 to all weapon DCs calculated using your proficiency bonus, as well as payload DCs.`
}

export class AbyssalStalwartMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerAbyssalStalwart
  name = 'Abyssal Stalwart'
  description = `When you make a concentration check to maintain your Nova Warp, you can consider yourself proficient in Constitution saving throws for the check.`
  prerequisite = 'Lv.9, Nova Warp'
}

export class AngryMagicMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerAngryMagic
  name = 'Angry Magic'
  description = `Creatures have disadvantage on their saving throw against your Nova Bomb, if they do not have advantage.`
  prerequisite = 'Nova Bomb'
}

export class AnnihilateMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerAnnihilate
  name = 'Annihilate'
  description = `The radius of your Nova Bomb's impact increases by 5 feet.`
  prerequisite = 'Lv.11, Harbinger of Destruction'
}

export class AtMySummonsMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerAtMySummons
  name = 'At My Summons'
  description = `When you have to roll on the Chaos table, you can instead select which Chaos result will occur. You cannot do this again for the next seven days.`
  prerequisite = 'Lv.20, Harbinger of Chaos'
}

export class BlinkMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerBlink
  name = 'Blink'
  description = `You can use a bonus action on your turn to teleport to an unoccupied space you can see within 10 feet of you, taking all carried and worn equipment of your choice with you when you do. If you are at 9th Voidwalker level, the range is 15 feet instead.`
  prerequisite = 'Lv.2'
}

export class BombsAwayMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerBombsAway
  name = 'Bombs Away'
  description = `As a bonus action on your turn, you can regain a super ability charge. Once you invoke this mark, you must complete a long rest before you can invoke it again.`
  prerequisite = 'Lv.20, Harbinger of Destruction'
}

export class ChaosAccelerantMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerChaosAccelerant
  name = 'Chaos Accelerant'
  description = `You can spend a use of your Depths of the Void to increase your Light level by one for the next minute. You must complete a brief rest before you can invoke this Mark again.`
  prerequisite = 'Lv.3, Harbinger of Chaos'
}

export class ComprehensionOfTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerComprehensionOfTheVoid
  name = 'Comprehension of the Void'
  description = `You can read all writing, even if it is in a language you do not know.`
}

export class DarkMatterMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerDarkMatter
  name = 'Dark Matter'
  description = `If you reduce a hostile creature to 0 hit points with void damage, you can make a shield recharge roll. Once you benefit from this feature, you must wait until the start of your next turn to benefit from it again.`
  prerequisite = 'Lv.15, attuned to Atomic Breach'
}

export class DetectLightAndDarkMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerDetectLightAndDark
  name = 'Detect Light and Dark'
  description = `You gain the effects of the Sunsinger's Detect Light and Dark feature. Your refined sensitivity to the ebb andflow of the Light keeps you cognizant of subtle changes in the space around you. As an action, you can take one or two deep breaths, centering yourself and opening your awareness fully to the paracausal forces around you. For the next 10 minutes, you can determine what active modifiers there are in the area, if there are any weapons within 60 feet of you that can deal Light or Darkness damage, and you can sense if there are any creatures of the Light or the Darkness within the same range. You learn the creature's level (or individual CR) when you detect them in this way.`
}

export class EmbraceTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerEmbraceTheVoid
  name = 'Embrace the Void'
  description = `If you kill a hostile creature with the damage from your Nova Bomb, and you are attuned to either Surge or Devour, you can begin the effects of Surge or Devour (whichever you are attuned to) if you were not already benefiting from that feature. You do not spend a melee ability charge to do this.`
  prerequisite = 'Lv.9, Nova Bomb'
}

export class EyesOfTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerEyesOfTheVoid
  name = 'Eyes of the Void'
  description = `As a bonus action on your turn, you can grant yourself advantage on the next attack roll you make, provided you make that attack roll before the end of your next turn. You can do this a number of times equal to 1 + your Intelligence modifier (minimum of 1 use) before you need to take a long rest to regain all uses.`
  prerequisite = 'Lv.11, Harbinger of Destruction'
}

export class FeedTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerFeedTheVoid
  name = 'Feed the Void'
  description = `While Devour has duration, as an action on your turn, you can spend a grenade ability charge to make a shield recharge roll.`
  prerequisite = 'Lv.7, attuned to Devour'
}

export class GazeOfTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerGazeOfTheVoid
  name = 'Gaze of the Void'
  description = `You can see in all forms of darkness, including magical, nonmagical, and paracausal, to a range of 120 feet.`
}

export class GiftOfTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerGiftOfTheVoid
  name = 'Gift of the Void'
  description = `You can increase one ability score of your choice by 1. Like normal, you cannot increase an ability score above 20.`
  prerequisite = 'Lv.11'
}

export class GraduateStudentMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerGraduateStudent
  name = 'Graduate Student'
  description = `Choose two skills you are proficient in, or one skill you are proficient in and either a toolkit or a vehicle you are proficient with. You can double your proficiency bonus for both of your chosen proficiencies if they do not already benefit from this bonus.`
}

export class HandheldSupernovaMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerHandheldSupernova
  name = 'Handheld Supernova'
  description = `When you cast Energy Drain, you can spend a grenade ability charge to expel your void Light in a 15-foot cone instead. All creatures in the cone must make a Dexterity saving throw against your Light save DC. They take the damage of your Energy Drain as explosive void damage on a failed save, or half as much on a success. You do not spend a melee ability charge when you do this, but you must have a melee ability charge remaining in order to do this.`
  prerequisite = 'Lv.7, attuned to Atomic Breach'
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class IdentifyExoticMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerIdentifyExotic
  name = 'Identify Exotic'
  description = `As an action you can touch one exotic item and learn all its properties and perks.`
}

export class ItFollowsMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerItFollows
  name = 'It Follows'
  description = `Your Nova Bomb's damage die size is reduced by 2. After resolving the detonation of your Cataclysm Nova Bomb, it unleashes three axion seekers. These axion seekers follow the same rules, and do the same damage, as the axion seekers described in the Axion Bolt grenade. You must specify the target of each seeker as soon as they appear`
  prerequisite = 'Lv.15, Harbinger of Knowledge, attuned to Cataclysm'
}

export class InsatiableMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerInsatiable
  name = 'Insatiable'
  description = `Once on your turn, if you reduce a hostile creature to 0 hit points while Devour has duration, you can regain a spent grenade ability charge.`
  prerequisite = 'Lv.15, attuned to Devour, Feed the Void'
}

export class LongShotMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerLongShot
  name = 'Long Shot'
  description = `The range of your Nova Bomb increases by 200 feet.`
  prerequisite = 'Lv.3, Nova Bomb'
}

export class MasterOfLightMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerMasterOfLight
  name = 'Master of Light'
  description = `Your Light level is increased by one.`
  prerequisite = 'Lv.20'
}

export class OfferingToTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerOfferingToTheVoid
  name = 'Offering to the Void'
  description = `You can use your Depths of the Void feature to spend your super ability charge in order to regain all melee and grenade ability charges. You can only invoke this Mark of the Void if you have at least one use of your Depths of the Void feature remaining and at least one use of your super ability remaining, and you spend a use of each when you invoke this Mark.`
  prerequisite = 'Lv.15, Harbinger of Chaos'
}

export class OuroborosMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerOuroboros
  name = 'Ouroboros'
  description = `As a bonus action on your turn, you can spend one melee ability charge to regain a spent grenade ability charge, or one grenade ability charge to regain a spent melee ability charge. You must complete a brief rest before you can invoke this mark again.`
  prerequisite = 'Lv.9'
}

export class PantologistMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerPantologist
  name = 'Pantologist'
  description = `You can add half your proficiency bonus (rounded down) to any ability check you make that doesn't already benefit from your proficiency bonus.`
  prerequisite = 'Lv.3, Harbinger of Knowledge'
}

export class PeerlessMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerPeerless
  name = 'Peerless'
  description = `Your Intelligence score increases by 4, to a maximum of 24`
  prerequisite = 'Lv.20, Harbinger of Knowledge'
}

export class SuperlativeSurgeMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerSuperlativeSurge
  name = 'Superlative Surge'
  description = `Once on your turn, when you make an attack roll while your Surge has duration, you can add a d4 to your roll.`
  prerequisite = 'Lv.15, attuned to Surge'
}

export class SurgingProtectionMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerSurgingProtection
  name = 'Surging Protection'
  description = `Your AC can't be less than 16 while Surge has duration.`
  prerequisite = 'Lv.7, attuned to Surge'
}

export class TeachingsOfTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerTeachingsOfTheVoid
  name = 'Teachings of the Void'
  description = `You gain proficiency in any three languages, skills, tools, or vehicles of your choice, or combination thereof. Proficiency with a language means you can speak, read, and write in that language.`
}

export class UnboundMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerUnbound
  name = 'Unbound'
  description = `You are considered attuned to axion, scatter, and vortex grenades simultaneously. When you spend a grenade charge to cast a grenade, you choose which one you wish to cast. Finally, any grenade recharge value you have that's above 7 becomes 7.`
  prerequisite = 'Lv.2, Harbinger of Chaos'
}

export class VigilantScholarMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerVigilantScholar
  name = 'Vigilant Scholar'
  description = `You no longer need to sleep and can't be forced to sleep by any means. To gain the benefits of a long rest you can spend 4 hours doing light activity as if it were a short rest.`
}

export class VortexMasteryMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerVortexMastery
  name = 'Vortex Mastery'
  description = `You automatically succeed on concentration checks you make to maintain the vortex of either your Nova Bomb or Vortex Grenade.`
  prerequisite = 'Lv.11, Nova Bomb, attuned to Vortex'
}

export class WarpAmmoMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerWarpAmmo
  name = 'Warp Ammo'
  description = `The critical hit range of all weapon attacks you make is increased by one. If you are at 15th Voidwalker level, the critical hit range is increased by two instead.`
  prerequisite = 'Lv.3, Harbinger of Destruction, attuned to Surge'
}

export class WhispersOfTheVoidMark implements SubclassFeature {
  _type = 'Feature'
  level = OtherAbility.VoidwalkerWhispersOfTheVoid
  name = 'Whispers of the Void'
  description = `As an action you can grant yourself proficiency in one skill, tool, language, or vehicle for the next 10 minutes. You can invoke this Mark of the Void a number of times equal to your Intelligence modifier. When you complete a long rest, you regain all spent uses of this Mark of the Void.`
  prerequisite = 'Lv.3, Harbinger of Knowledge'
}

export const allSubclassAbilities: LightAbility[] = [
  new AmplifiedHitsFocus,
  new FlatlineFocus,
  new StudyFocus,
  new FarsightFocus,
  new AtTheReadyFocus,
  new PhantomStrideFocus,
  new PredatorsEyeFocus,
  new StabilizeFocus,
  new SpectersShroudFocus,
  new StunningBlowFocus,
  new RefreshFocus,
  new ShadestepFocus,
  new TruesightFocus,
  new AlternatingCurrentAction,
  new BoltAction,
  new DischargeAction,
  new InductionAction,
  new OverchargedAction,
  new OverrideAction,
  new RechargeAction,
  new RefreshAction,
  new WindswiftAction,
  new ZaplightAction
]
export const allSubclassFeatures: SubclassFeature[] = [
  new DefenseSpecialty,
  new DualWielderSpecialty,
  new GeneralistSpecialty,
  new HeavyWeaponsExpertSpecialty,
  new LoadedForBearSpecialty,
  new MasterAtArmsSpecialty,
  new AbyssalStalwartMark,
  new AngryMagicMark,
  new AnnihilateMark,
  new AtMySummonsMark,
  new BlinkMark,
  new BombsAwayMark,
  new ChaosAccelerantMark,
  new ComprehensionOfTheVoidMark,
  new DarkMatterMark,
  new DetectLightAndDarkMark,
  new EmbraceTheVoidMark,
  new EyesOfTheVoidMark,
  new FeedTheVoidMark,
  new GazeOfTheVoidMark,
  new GiftOfTheVoidMark,
  new GraduateStudentMark,
  new HandheldSupernovaMark,
  new IdentifyExoticMark,
  new ItFollowsMark,
  new InsatiableMark,
  new LongShotMark,
  new MasterOfLightMark,
  new OfferingToTheVoidMark,
  new OuroborosMark,
  new PantologistMark,
  new PeerlessMark,
  new SuperlativeSurgeMark,
  new SurgingProtectionMark,
  new TeachingsOfTheVoidMark,
  new UnboundMark,
  new VigilantScholarMark,
  new VortexMasteryMark,
  new WarpAmmoMark,
  new WhispersOfTheVoidMark
]
export const subclassAbilityIndex = allSubclassAbilities.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<OtherAbility, LightAbility>)
export const getSubclassAbility = (id: OtherAbility, level: number) => (subclassAbilityIndex[id]?.prerequisiteLevel || 0) <= level ? subclassAbilityIndex[id] : null
export const subclassFeatureIndex = allSubclassFeatures.reduce((a, x) => ({ ...a, [x.level]: x }), {} as Record<OtherAbility, SubclassFeature>)
export const getSubclassFeature = (id: OtherAbility) => subclassFeatureIndex[id]
