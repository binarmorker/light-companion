import { GuardianFoundation, Skill, ToolItemType } from '@/models/destiny'

export class AgentOfTheVanguardFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Agent Of The Vanguard',
      description: `As an agent of the Vanguard, you devote your time and resources to assist the organized Guardian leadership with their mission to protect and strengthen the Last City. You might be a field operative, performing strikes and patrols to keep the enemies of humanity at bay. Or maybe you are a scout, scavenging resources and gathering information to support Vanguard operations. You might even be an assassin, one of the few Guardians trusted to eliminate high-value targets swiftly and silently. The duties of a Vanguard agent are varied and often change over time.

Discuss with your Architect what your primary duty is as an agent of the Vanguard. Whenever the Vanguard has need of a Guardian to fulfill that duty, you can expect them to call on you as their go-to agent. Failure to comply with an order from the Vanguard may result in loss of esteem, privileges, or rank. Betrayal or abdication of your duties might result in Vanguard retaliation against you.

**Skill Proficiencies:** Persuasion and either Athletics or Acrobatics

**Additional Proficiencies:** Weaponsmithing toolkit, City common (language)`,
      skillOptions: [Skill.Persuasion, { choice: 1, skills: [Skill.Athletics, Skill.Acrobatics] }],
      proficiencies: [ToolItemType.WeaponsmithingTools],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Vanguard Connection',
          description: `You have ready access to standard equipment provided by the Vanguard, which you can borrow to assist in your Vanguard-sanctioned operations. If any of your equipment breaks, you can get it fixed when you visit the Tower. If you don't have a jumpship or sparrow, the Vanguard will provide temporary transportation at no charge. Finally, while operating in the field, if you encounter any other Vanguard agents or creatures who are allied with the Vanguard or the Last City, you can request aid from them. This aid may come in the form of shelter, glimmer, food, equipment, or help in combat, and may be limited depending on who exactly you encounter.`,
          source: 'Agent Of The Vanguard'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Officer',
          description: `As an officer of the Vanguard, you are a senior agent elevated into a position of command over other Guardians. The command may be small, such as control over a single fireteam, or you may be in charge of multiple fireteams as you direct their operations toward goals assigned to you by your superiors. Who you command may change over time. Regardless, those loyal to the Vanguard recognize your rank and will defer to you if you have authority to command them. You can invoke your rank to requisition equipment and supplies for Vanguard-sanctioned operations you perform, and you can purchase equipment, including exotic equipment, through the Vanguard at a small discount.`,
          source: 'Agent Of The Vanguard'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Ikora\'s Hidden',
          description: `Warlock Vanguard Ikora Rey oversees the Hidden, a secretive group of Guardians deployed as intelligence operatives for the City. Through long and especially distinguished service to the Vanguard, you have been inducted into this clandestine group. You may work exclusively as a Hidden agent, or you may operate under the guise of another position, perhaps as a regular Vanguard operative, perhaps as an apparently unaffiliated Guardian. You are entrusted with extremely sensitive intelligence and Vanguard secrets, which you must protect at all costs. Discuss with your Architect what your role is as a Hidden agent, and if you work undercover, what that cover is and what you must do to maintain it.
    
Whatever your cover or other activities may be, your first and highest commitment is to your duty as one of the Hidden. You report directly to Ikora, and the orders you receive from her take priority over anything else. Everything you do is to pursue your objectives as a Hidden agent, so work with your Architect to determine how your Hidden duties align with what you outwardly appear to be doing—especially if your fireteam members are not also Hidden, or are not aware of your being a Hidden agent.`,
          source: 'Agent Of The Vanguard'
        }
      ],
      source: 'Agent Of The Vanguard'
    }
  ]
  name = 'Agent of The Vanguard'
  id = 'agent-of-the-vanguard'
}

export class AwokenLoyalistFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Awoken Loyalist',
      description: `Many Awoken Guardians find their way to the Reefborn Awoken hoping to reconnect with their origins or seeking answers about their past lives. Even after death and resurrection, the otherworldly connection between Awoken is strong, and some Guardians come to feel more at home among their fellow Awoken than with humanity's other survivors in the Last City. Although generally the Reef prefers to keep Guardians at a distance, occasionally those Awoken who return as Lightbearers prepared to offer fealty to the Queen may find a place in her service.

**Skill Proficiencies:** Choose two from Arcana, History, and Insight

**Additional Proficiencies:** One alien language of your choice

**Equipment:** A sidearm`,
      skillOptions: [{ choice: 2, skills: [Skill.Arcana, Skill.History, Skill.Insight] }],
      source: 'Awoken Loyalist'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Save The Queen',
      description: `Whether or not you are on good terms with the Vanguard, your first and deepest loyalty is to the Awoken of the Reef, and Queen Mara Sov. You may even have sworn oaths to this effect. Your allegiance to the Reef comes with responsibilities and rewards depending on the nature and extent of your service. If you have only an informal commitment, you may be called upon to assist Corsair operations or provide aid to other Awoken agents. In turn, you can request assistance yourself, whether material or informational, from your Reef contacts. If your service has been long and faithful, and you have sworn fealty to the Queen, you may be accepted as an insider, and possibly even hold official title or rank among the Reefborn. In such a role, your responsibilities and duties are extensive, and the assistance you can request (or command) from the Awoken is accordingly greater.

Consult with your Architect to determine your standing among the Awoken, including the extent of your service and responsibilities, and the nature of your contacts and what they might be willing or able to do for you. Also keep in mind that Awoken Guardians are often recognized by Reefborn Awoken who knew them before they died—but taboo forbids speaking of it. Because of this and other reasons, Risen Awoken are likely to always remain out of place among the Reefborn. Consider how this tension may affect your position as a loyalist.`,
      source: 'Awoken Loyalist'
    }
  ]
  name = 'Awoken Loyalist'
  id = 'awoken-loyalist'
}

export class CriminalFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Criminal',
      description: `Despite what the Vanguard wants, not all Guardians are dedicated to protecting and uplifting humanity. Some Guardians are truly out for themselves, and are more concerned with securing their next payday than with ensuring humanity's survival. Others simply chafe under the imposition of the Vanguard's assumed authority, and would rather control their own lives and carry on with their own business, regardless of restrictions and regulations.

**Skill Proficiencies:** Deception, Stealth

**Additional Proficiencies:** Thieves' toolkit, one alien language of your choice`,
      skillOptions: [Skill.Deception, Skill.Stealth],
      proficiencies: [ToolItemType.ThievesTools],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Underworld Access',
          description: `You know at least one trustworthy contact in the criminal underbelly of the Sol system, and you can generally get in touch with this person easily. Your contact may be an insider in a crime syndicate, a purveyor of less-than-legal goods and services, or an infamous individual such as the Spider. Discuss the nature of your contact, and what your contact is willing to do for you, with your Architect. Regardless, you can almost always assume that working with your contact will come with a price, anything from plain glimmer to leveraging your talents for an objective the contact wishes to pursue.`,
          source: 'Criminal'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Pirate',
          description: `Supply and demand are perpetually uncertain under the constant threat of hostile aliens—but as a spacefaring entrepreneur, your business is founded on one unshakeable principle: there's always something good to steal. As a pirate, you make your living by recognizing opportunities to acquire goods in transit at weak points in the supply chain. Perhaps you steal from other humans (in which case you are probably generally reviled), but there are plenty of attractive targets in Cabal supply depots, Fallen caches, and other storehouses of humanity's enemies. For the benefit of your fireteam, or for your own needs, you can tap into these resources, or stashes you've left in place to permit hot merchandise to cool down. Discuss with your Architect what kinds of supplies or gear you may be able to obtain this way, and what you have to do to obtain it. Your underworld contact is probably another pirate, maybe a member of your current or former pirate crew, or a facilitator of your crew's operations. You probably also have access to jumpships and cargo ships belonging to your crew, if you don't have your own.
    
At your Architect's discretion, you may also have knowledge of the location of a huge cache, maybe a trove of Golden Age tech, another pirate's hidden loot, or the resting place of a wrecked treasure ship. Acquiring this loot could be one of your long-term goals, and may even be part of your motivation for joining a fireteam.`,
          source: 'Criminal'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Smuggler',
          description: `In a world of scarcity threatened on all sides by extrasolar invaders, there is great demand for certain commodities that can't easily be obtained legally. You fill the economic niche created by this state of affairs, facilitating the transfer and provision of goods not normally available in the open market. Your underworld contact is a (relatively) reliable fence who's willing to deal in stolen or contraband wares. Thanks to your web of dealings and contacts, you know how to acquire anything—and when you say anything, you mean anything. If there ever is something you don't know how to find, you know someone who does. Your professional expertise also entails a ready aptitude for finding places to keep things hidden, including on your own person. If you have a jumpship, you are able to rig it with concealed cargo compartments.`,
          source: 'Criminal'
        }
      ],
      source: 'Criminal'
    }
  ]
  name = 'Criminal'
  id = 'criminal'
}

export class CrucibleContenderFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Crucible Contender',
      description: `There are many areas of competition when it comes to the Crucible, covering everything from sweeping big team battles involving refurbished vehicles to small tournaments that only last a weekend. As a Crucible contender you have chosen to focus on a particular area of competition. You spend the majority of your time practicing and competing in the Crucible, and a not insignificant part of your free time is spent tracking the Crucible standings and tactical meta. You might still occasionally work for the Vanguard or for others, especially if others offer to employ your demonstrated combat capabilities—but your primary focus is the Crucible.
      
Though you may not actively be seeking fame and glory, you have discovered you are good enough to consistently win matches, and you enjoy at least a modicum of the privileges such success brings.

**Skill Proficiencies:** Intimidation and either Athletics or Acrobatics

**Additional Proficiencies:** Armorsmithing toolkit, weaponsmithing toolkit`,
      skillOptions: [Skill.Intimidation, { choice: 1, skills: [Skill.Athletics, Skill.Acrobatics] }],
      proficiencies: [ToolItemType.ArmorsmithingTools, ToolItemType.WeaponsmithingTools],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Rookie of the Year',
          description: `You can expect other people who follow the rankings of your chosen area of competition to have heard of you, though their opinion of you may not always be positive. Those who do have a favorable opinion of you are generally willing to provide you with free food and beverages for the chance to get to hang out with you for a while. You can also easily request private matches without much wait time, and should you decide to switch areas of competition you want to engage in, you'll generally find it easier to get in contact with mid-tier or higher competitors in your new area and possibly team up with them.`,
          source: 'Crucible Contender'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Champion',
          description: `After competing in the Crucible for a considerable time, you have finally achieved a championship title. You are considered the best of the best in your chosen area of battle, and being a champion means you've gained the attention of the City's weapon foundries, who are eager to sponsor Guardians such as yourself. Sponsorship means you can expect a large sum of glimmer when you sign the contract, and you can borrow from the arsenal of the foundry at no cost, though borrowing exotic weapons may still have an associated cost. What the foundry will require of you in return can vary and should be discussed with your Architect. Generally, you can expect that the foundry will require you to exclusively use their equipment when competing in the Crucible or appearing on broadcasts, and to appear in any advertisements the foundry records during the length of the contract.`,
          source: 'Crucible Contender'
        }
      ],
      source: 'Crucible Contender'
    }
  ]
  name = 'Crucible Contender'
  id = 'crucible-contender'
}

export class CryptarchFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Cryptarch',
      description: `While most Guardians only know cryptarchs as the people who decrypt engrams, you know that being a cryptarch is so much more. It is devoting your life to the pursuit of knowledge and the advancement of understanding, typically in one focused area of study, though particularly long-lived Guardian cryptarchs may collect numerous degrees and become experts in multiple disciplines. Unlike civilian cryptarchs, Guardian cryptarchs are in the position of being able to directly hunt for information they seek in the wilds, so it's very common for them to also serve as expedition leaders rather than staying inside the halls of Cryptarchy libraries.

When you choose this foundation, make sure to clarify with your Architect what your chosen area of study is and what that means to your character. You may have duties as a cryptarch that your Architect will expect you to perform in your downtime, such as attending classes to maintain your skill proficiencies, or recording the findings of your recent studies in the Archives.

**Skill Proficiencies:** Investigation, Insight

**Additional Proficiencies:** Choose any two toolkits or two languages, or one toolkit and one language

**Equipment:** Tablet computer`,
      skillOptions: [Skill.Investigation, Skill.Insight],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Scholar',
          description: `You can access the basic Cryptarch archives and can decode engrams at no cost. For whatever you don't know, you know how to contact someone who does know, be they in your own department or not. You can also borrow basic, unrestricted equipment from your department of study for predetermined lengths of time. For example, if your department is film and television, you can borrow gear such as cameras, microphones, and lighting equipment, but you may not be able to borrow the only remaining copy of an ancient film. Discuss the limits of what you can access with your Architect.`,
          source: 'Cryptarch'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Leading Expert',
          description: `After dedicating yourself to an area of study for some time you have become a foremost authority in that field. Other scholars and students defer to your knowledge and wisdom in your chosen area, and you have access to the Cryptarchy's restricted files and equipment related to your field. You also have a friend or two in the departments of other areas of study, and should you need something you can't access yourself, they may be able to quietly provide it for you.`,
          source: 'Cryptarch'
        }
      ],
      source: 'Cryptarch'
    }
  ]
  name = 'Cryptarch'
  id = 'cryptarch'
}

export class DeadOrbiterFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Dead Orbiter',
      description: `Because you recognize the fundamentally vulnerable situation of humanity's survivors on Earth, you joined Dead Orbit to be a part of the effort to restore the infrastructure necessary to realize the Golden Age aspiration of interstellar travel. Although as a Guardian you value the gift of the Light, you can't ignore that the Traveler's presence attracts one threat after another. Humanity can only hope to be safe by departing the cradle of Earth, and seeking new homes among the stars.

**Skill Proficiencies:** Investigation, Survival

**Additional Proficiencies:** Electronics toolkit, vehicle toolkit`,
      proficiencies: [ToolItemType.ElectronicsTools, ToolItemType.VehicleTools],
      skillOptions: [Skill.Investigation, Skill.Survival],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Satellite',
          description: `As a rank-and-file member of Dead Orbit, you contribute resources and effort to the faction's mission of salvaging and constructing ships, with the ultimate aim of building a fleet capable of journeys into deep space. Your membership may require you to donate glimmer or materials to the cause, or report information that may be pertinent to the leadership's objectives. You may also be called upon to participate in Dead Orbit operations, such as missions to secure salvage opportunities, interplanetary scouting throughout the Sol system, and humanitarian efforts to use the Dead Orbit fleet to provide evacuation or relocation transport.
    
In exchange for your service, your supervising arach and other contacts in Dead Orbit can provide you gear from the faction armory, access to equipment and salvaged technology that may not be easily available otherwise, and will sometimes offer information from Dead Orbit's wide-ranging intelligence-gathering that may be circumstantially useful to you.`,
          source: 'Dead Orbiter'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Ascending Orbit',
          description: `After long service to Dead Orbit, you have attained moderately high standing in the faction leadership, perhaps even as an arach, a rank blending the roles of chapter leader and ship captain. You may be involved in the organization or administration of Dead Orbit's operations, and you could also lead a group of faction operatives, or even command one of the sizable ships of the Dead Orbit fleet. Other Dead Orbit members heed your authority, and you have some pull within the faction to call for the fleet's support when you need it.
    
Consult with your Architect to decide under what circumstances and in what ways you can direct ships of the Dead Orbit fleet. The assistance they provide could range from minor things, like delivering supplies or providing transport for small groups, to major interventions such as conducting large salvage operations or full-scale evacuations. Your Architect will also help determine how much and what kind of intelligence you receive from other Dead Orbit leaders.`,
          source: 'Dead Orbiter'
        }
      ],
      source: 'Dead Orbiter'
    }
  ]
  name = 'Dead Orbiter'
  id = 'dead-orbiter'
}

export class EngineerFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Engineer',
      description: `The wonders of the Golden Age were enabled by the Traveler's mysterious powers of paracausality, but humanity's amazing technological achievements then—which the survivors of humanity still rely upon now—were all machines, when it comes down to it. Somebody has to know how to work those machines. As an engineer, you make it your business to know how technology works, rather than just use it without a second thought, like many Guardians do.

**Skill Proficiencies:** Technology, Investigation

**Additional Proficiencies:** Electronics toolkit`,
      skillOptions: [Skill.Technology, Skill.Investigation],
      proficiencies: [ToolItemType.ElectronicsTools],
      source: 'Engineer'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Glimmer Tinkerer',
      description: `Almost all technologies Guardians use rely on glimmer, so one of the best things anyone can do to improve the efficiency of any technological system is to eliminate wasteful glimmer usage. By careful refinement of glimmer-programming techniques, you have significantly streamlined the synthesis process. When you or your Ghost produce something by programming glimmer, reduce the glimmer cost by 10%.`,
      source: 'Engineer'
    }
  ]
  name = 'Engineer'
  id = 'engineer'
}

export class ExoSeekerFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Exo Seeker',
      description: `All Exos are bound by their unknown origin and uncertain former purpose. Some try to ignore the inescapable questions underlying existence as an Exo, but you are unable to set them aside. Whatever other commitments you have, your highest motivation is finding answers about your own nature as an Exo, and uncovering the lost history of Exos' existence.

**Skill Proficiencies:** History and either Investigation or Technology`,
      skillOptions: [Skill.History, { choice: 1, skills: [Skill.Investigation, Skill.Technology] }],
      source: 'Exo Seeker'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Tortured Dreams',
      description: `Almost every Exo knows the dream of a vast battlefield, an endless war, dying friends—and a dark place, somehow familiar, somewhere just beyond the horizon of recollection. Many Exos ignore these dreams, but you know they are seeds of the truth about your origin. In your quest to learn about these dreams, you have interviewed many other Exos, and developed a network of Exo acquaintances and contacts. Comparing your dreams has sharpened the sense of remembering something just below the surface of your awareness—but you don't yet know what it is.
      
You were resurrected in possession of an unidentified relic (perhaps a data-storage device you can't access, a book of notes you can't decipher, or an image of a place you don't recognize, yet feel drawn to) or an uncanny, vivid memory of a location (a Golden Age ruin, a derelict ship, or a hidden bunker)—or possibly both. This item or location features prominently in your recurring Exo nightmares, and you have heard other Exos mention it as well. Work with your Architect to determine what this item or location may be, how you can pursue learning about it, and what the ramifications of uncovering its secrets could be for you and your fireteam.`,
      source: 'Exo Seeker'
    }
  ]
  name = 'Exo Seeker'
  id = 'exo-seeker'
}

export class FreelancerFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Freelancer',
      description: `As a freelancer, you value nothing more than your individuality and right to determine the course of your own life. However, this does not mean you shun other organizations entirely. For example, you might cooperate loosely with the Vanguard because you don't want to end up an enemy of the City, or you might work with one of the City's factions to reap the benefits of their favor. What being a freelancer does mean is that you are, at all times, your number one client and most trusted friend. As far as you're concerned, the system is falling apart at the seams out there, and it's everyone for themself.

**Skill Proficiencies:** Perception, Persuasion

**Additional Proficiencies:** Medical toolkit, one alien language of your choice`,
      skillOptions: [Skill.Perception, Skill.Persuasion],
      proficiencies: [ToolItemType.MedicalTools],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Agent for Hire',
          description: `You're always on the prowl for a high-scoring job, and you know how to find such jobs throughout the Sol system using a web of contacts, most of which you can only trust as far as you can throw. These jobs are anything from Vanguard strike operations to bounties on wanted targets, including other Guardians. Whatever it is, you always know what you can do to get the biggest payday, and you can generally make sure you're at the top of the list of candidates, even if your means aren't always what others would consider above board.`,
          source: 'Freelancer'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Renegade',
          description: `As a renegade, you have taken the steps to swear off working with the Tower and the Last City. Perhaps the Tower Guardians were just too soft for your taste, or perhaps a threat you know of wasn't being taken seriously enough. Regardless of why you cut ties, your goals should still fall along the lines of opposing the Darkness—you aren't a Dredgen, after all. Though the Vanguard may not hold you in high regard, they might yet still reach out to you in times of need. You also have one trustworthy contact who can give you reliable information on the underworld of the Sol system and who can get you into and out of the Last City without being noticed.`,
          source: 'Freelancer'
        }
      ],
      source: 'Freelancer'
    }
  ]
  name = 'Freelancer'
  id = 'freelancer'
}

export class FutureWarCultistFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Future War Cultist',
      description: `You joined the Future War Cult because you recognize the common sense in its warning that humanity's survivors should prepare for coming and worsening conflict as the many threats to the Traveler and the Last City tighten around the Sol system. Despite the faction's reputation for secrecy and bizarre prognostication, you see the real good its members have done to make ready for battles others dismissed as unlikely and to aid those in danger from threats others didn't see coming.

**Skill Proficiencies:** Investigation, Insight

**Additional Proficiencies:** Weaponsmithing toolkit`,
      skillOptions: [Skill.Investigation, Skill.Insight],
      proficiencies: [ToolItemType.WeaponsmithingTools],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Si Vis Pacem',
          description: `Future War Cult calls upon all its members to contribute to their ongoing efforts to maintain and improve their own battle readiness, as well as to gather intelligence and sometimes conduct interventions informed by the forecasting and prognostication of the Cult's leadership. In return, members are equipped with weapons and armor, and are sometimes foretold of coming events that may be relevant to their current deployment. Your line of communication to the War Cult will occasionally direct you to complete certain objectives, and may offer insightful (if cryptic) information about what may be to come in your present situation. However, you are typically not at liberty to divulge your instructions or prophetic intelligence to nonmembers.`,
          source: 'Future War Cultist'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'War Prophet',
          description: `After years of diligent service and many tiers of initiation, you have become a fairly high-status member of Future War Cult's leadership. You may directly command a cell of subordinate members, or perhaps oversee a particular area of the War Cult's operations. Most significantly, you are permitted some access to the Device, an experimental Golden Age machine modeled on Vex gate technology, which the War Cult uses to temporally displace consciousness and thereby attempt to observe possible futures. You are also able to consult the CHASM records, which chronicle use of the Device since before the Collapse.
    
With both the Device and the CHASM records at your disposal, you are able to seek prophetic intelligence when you wish. Work with your Architect to determine what this intelligence is on a case-by-case basis. Your Architect may impose a limit on the number of times you can solicit this intelligence on a monthly basis.`,
          source: 'Future War Cultist'
        }
      ],
      source: 'Future War Cultist'
    }
  ]
  name = 'Future War Cultist'
  id = 'future-war-cultist'
}

export class InfluencerFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Influencer',
      description: `Guardian life is a subject of great fascination to the regular people of the Last City, and even to new Guardians, who haven't yet figured out what immortality is all about. Fortunately they have you to show them, if they give you a follow. In addition to your Vanguard duties or other Guardian occupations, you are a popular, trendsetting figure in VanNet forums and on the City's social networks.

**Skill Proficiencies:** Performance, Persuasion`,
      skillOptions: [Skill.Performance, Skill.Persuasion],
      source: 'Influencer'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Clout',
      description: `Your high follower count means you are well enough known to be recognized almost anywhere in the Last City, and very possibly by other Guardians off-world. While there's always a chance of running into a few haters, for the most part your followers are adoring and excited to spot you. They will reliably approach you in public and ask to capture images of themselves with you, and very commonly they will offer you drinks, food, and supplies. You may even be able to ask them collectively for help or favors. However, maintaining this popularity and goodwill is hard work. Your followers expect frequent posts, and if you fail to keep the nets up to date with your activity or don't get enough engagement, your follower count may drop, reducing the scope of your influence. Discuss with your Architect how to track this, and how it will affect you and your party.`,
      source: 'Influencer'
    }
  ]
  name = 'Influencer'
  id = 'influencer'
}

export class NewMonarchistFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: New Monarchist',
      description: `Frustrated with the Last City's government, which you feel is ineffectual, or romantically turning toward ancient tradition and its sense of nobility, you joined New Monarchy to support its project of replacing the Consensus with a single individual leader of incomparable moral authority. You believe such a king is necessary to unify humanity and reclaim the glory of the Golden Age.
      
**Skill Proficiencies:** Persuasion and your choice of either Deception of Intimidation`,
      skillOptions: [Skill.Persuasion, { choice: 1, skills: [Skill.Deception, Skill.Intimidation] }],
      featureOptions: [
        {
          _type: 'Feature',
          level: 0,
          name: 'Royalist',
          description: `Members of New Monarchy are expected to represent the faction's ideology in whatever circles they move, as well as commit some amount of time, effort, or resources to the faction. Your sponsor or leader in New Monarchy may call on you to perform tasks for the faction, to levy glimmer or other resources, or to invite you to represent the faction in some way. In return for your contributions, you have access to New Monarchy's stores of weapons, armor, and supplies, and can request loans or donations of equipment and resources to fulfill situational needs—especially when doing so reflects positively on the faction's image.`,
          source: 'New Monarchist'
        },
        {
          _type: 'Feature',
          level: 0,
          name: 'Kingmaker',
          description: `You have served New Monarchy well, whether steadfastly for a long time or with noteworthy demonstrations of your ability and largesse in a shorter period. Either way, you have secured respected standing in the faction leadership, and are well known to the executors. You have considerable influence within the faction, effectively enabling you to command other members of lower standing, and entitling you to draw upon New Monarchy's considerable material wealth and political influence. Work with your Architect to determine how often and to what extent you are able to call on these resources.
    
Additionally, your favored status in New Monarchy enables you to stand somewhat apart from the City's government and the Vanguard, if your interests should happen to diverge. In consultation with your Architect, you may receive information typically only available to the Consensus, and New Monarchy's leaders may offer political protection if you should run afoul of the Vanguard or other authorities.`,
          source: 'New Monarchist'
        }
      ],
      source: 'New Monarchist'
    }
  ]
  name = 'New Monarchist'
  id = 'new-monarchist'
}

export class PilgrimGuardianFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Pilgrim Guardian',
      description: `Whether you are a Titan old enough to have been a member of the actual Pilgrim Guard, the first Titan order, which preceded the City Age, or you came later to answer their old calling, you devote yourself to the protection of refugees traveling the many pilgrim roads of Earth. Perhaps a formative experience during your first journey to the Last City led you to this work, or perhaps you feel a particularly keen sympathy for the mortal survivors of humanity, still scattered in small pockets across the globe. Whatever your reasons, your paramount concern is helping the Lightless pass safely through the dangers of the wilds to reach the relative safety of the City.

**Skill Proficiencies:** Animal Handling, Survival, and either Medicine or Nature

**Additional Proficiencies:** Armorsmithing toolkit or weaponsmithing toolkit`,
      skillOptions: [Skill.AnimalHandling, Skill.Survival, { choice: 1, skills: [Skill.Medicine, Skill.Nature] }],
      source: 'Pilgrim Guardian'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Life on the Road',
      description: `Your devotion to protecting humanity's survivors means you spend most of your time on Earth, beyond the City walls. You live on the pilgrim roads, in the ruins of the Golden Age, and in the open wilds, and go wherever people seeking the City could use your help. Your familiarity with the wilds is as much of value to pilgrim caravans as your fighting prowess: after years of scouting, charting, and providing escort, you know most of Earth's terrain better than almost anyone alive. You often know of shortcuts, can assess the relative dangers of different routes, and are almost always able to find a safe place for a short rest.`,
      source: 'Pilgrim Guardian'
    }
  ]
  name = 'Pilgrim Guardian'
  id = 'pilgrim-guardian'
}

export class PilotFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: Pilot',
      description: `There's steering a ship, and there's being a pilot. You know the difference, and you're the latter. You can handle the stick on anything from a sluggish freight hauler to a hijacked Fallen skiff to a bleeding-edge tactical jumpship, and everything in between. You're happy just so long as you get to fly something.

**Skill Proficiencies:** Technology and either Investigation, Insight, or Performance

**Additional Proficiencies:** Vehicle toolkit`,
      skillOptions: [Skill.Technology, { choice: 1, skills: [Skill.Investigation, Skill.Insight, Skill.Performance] }],
      proficiencies: [ToolItemType.VehicleTools],
      source: 'Pilot'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Spacenoid',
      description: `However you acquired and used your piloting skills, your love of jumpships has led you to spend a great deal of time in space. You cannot become disoriented in microgravity or zero-gravity conditions. Your familiarity with interplanetary traffic and typical flight paths enables you to avoid certain unnatural hazards of space travel, like pirates and other ship-based attackers. Whether or not you own a jumpship of your own, your connections to other pilots probably enable you to find a ship in a pinch (at your Architect's discretion).`,
      source: 'Pilot'
    }
  ]
  name = 'Pilot'
  id = 'pilot'
}

export class SRLRacerFoundation implements GuardianFoundation {
  perks = [
    {
      _type: 'Feature',
      level: 0,
      name: 'Foundation: SRL Racer',
      description: `Sparrows were invented in the Golden Age for swift all-terrain transport, and have been the conveyance of choice for Lightbearers since the Dark Age, before Guardians were even called Guardians. For you, however, riding a sparrow isn't about getting around. You're a racer, and riding a sparrow is a state of being, a way of life—a fine art.

**Skill Proficiencies:** Acrobatics, Performance

**Additional Proficiencies:** Vehicle toolkit

**Equipment:** Sparrow`,
      skillOptions: [Skill.Acrobatics, Skill.Performance],
      proficiencies: [ToolItemType.VehicleTools],
      source: 'SRL Racer'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Sparrow Trickster',
      description: `Almost all Guardians know how to ride a sparrow, but you elevate it beyond a mere functional competency to a level of artful refinement few can match. You devote a considerable portion of your personal resources to maintain a precision-tuned, high-end sparrow, and you can ride it over anything. Terrain other Guardians would consider unsafe or even impassible is just another challenge for you to sail over, probably while pulling out-of-saddle tricks in the air. You can also comfortably ride at speeds most Guardians would find bowel-crushing, if they even had sparrows that could reach that kind of velocity without redlining and exploding. Off the racecourse, these special skills can be of use to your fireteam in certain situations.

You will need to work out with your Architect what it costs you to maintain your custom sparrow, and how much time you must devote to racing, if you participate professionally in the Sparrow Racing League.`,
      source: 'SRL Racer'
    }
  ]
  name = 'SRL Racer'
  id = 'srl-racer'
}

export const allFoundations: GuardianFoundation[] = [
  new AgentOfTheVanguardFoundation,
  new AwokenLoyalistFoundation,
  new CriminalFoundation,
  new CrucibleContenderFoundation,
  new CryptarchFoundation,
  new DeadOrbiterFoundation,
  new EngineerFoundation,
  new ExoSeekerFoundation,
  new FreelancerFoundation,
  new FutureWarCultistFoundation,
  new InfluencerFoundation,
  new NewMonarchistFoundation,
  new PilgrimGuardianFoundation,
  new PilotFoundation,
  new SRLRacerFoundation
]
export const foundationIndex = allFoundations.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<string, GuardianFoundation>)
export const allFoundationNames = allFoundations.map(x => ({ id: x.id, name: x.name }))
export const getFoundation = (id: string) => foundationIndex[id]
