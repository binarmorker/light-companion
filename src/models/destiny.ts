import { StatBlock } from '@/utils/statblock/types'
import { FieldValue } from 'firebase/firestore'

export enum Class {
  Titan = 0,
  Warlock = 1,
  Hunter = 2
}

export enum Race {
  Human = 0,
  Awoken = 1,
  Exo = 2,
  Eliksni = 3,
  Krill = 4,
  Vex = 5,
  Cabal = 6,
  Psion = 7,
  Taken = 8,
  Beast = 9,
  Construct = 10
}

export interface GuardianRace {
  perks: Feature[],
  race: Race,
  name: string,
  id: string
}

export interface GuardianFoundation {
  perks: FoundationFeature[],
  name: string,
  id: string
}

export enum NeutralElement {
  Kinetic = 0,
  Poison = 3,
  Psychic = 4
}

export enum LightElement {
  Solar = 10,
  Arc = 11,
  Void = 12,
}

export enum DarknessElement {
  Stasis = 20,
  Strand = 21,
  Luster = 22
}

export enum DamageType {
  Kinetic = NeutralElement.Kinetic,
  Light = 1,
  Darkness = 2,
  Poison = NeutralElement.Poison,
  Psychic = NeutralElement.Psychic,
  Solar = LightElement.Solar,
  Arc = LightElement.Arc,
  Void = LightElement.Void,
  Stasis = DarknessElement.Stasis,
  Strand = DarknessElement.Strand,
  Luster = DarknessElement.Luster,
  ExplosiveKinetic = 900 + NeutralElement.Kinetic,
  ExplosiveLight = 901,
  ExplosiveDarkness = 902,
  ExplosivePoison = 900 + NeutralElement.Poison,
  ExplosivePsychic = 900 + NeutralElement.Psychic,
  ExplosiveSolar = 900 + LightElement.Solar,
  ExplosiveArc = 900 + LightElement.Arc,
  ExplosiveVoid = 900 + LightElement.Void,
  ExplosiveStasis = 900 + DarknessElement.Stasis,
  ExplosiveStrand = 900 + DarknessElement.Strand,
  ExplosiveLuster = 900 + DarknessElement.Luster,
  ShieldHealing = 9997,
  HealthHealing = 9998,
  Healing = 9999
}

export enum ClassAbility {
  HuntersDodge = 200,
  Barricade = 201,
  RiftOfLight = 202
}

export enum GrenadeAbility {
  Arcbolt = 300,
  AxionBolt = 301,
  Firebolt = 302,
  Flashbang = 303,
  Flux = 304,
  Fusion = 305,
  Incendiary = 306,
  Lightning = 307,
  Magnetic = 308,
  Pulse = 309,
  Scatter = 310,
  Skip = 311,
  Solar = 312,
  Spike = 313,
  Storm = 314,
  Suppressor = 315,
  Swarm = 316,
  Thermite = 317,
  Tripmine = 318,
  Voidwall = 319,
  Vortex = 320
}

export enum MeleeAbility {
  Unarmed = 400,
  TitanUnarmed = 401,
  ThrowingKnife = 402,
  ElectrifyingStrike = 403,
  DisorientingBlow = 404,
  Blink = 405,
  Smoke = 406,
  StormFist = 407,
  Disintegrate = 408,
  Sunstrike = 409,
  EnergyDrain = 410,
  Scorch = 411,
  Thunderstrike = 412
}

export enum SuperAbility {
  Deadeye = 500,
  SixShooter = 501,
  BladeBarrage = 502,
  RazorsEdge = 503,
  ArcStaff = 504,
  VanishingAct = 505,
  Shadowshot = 506,
  MoebiusQuiver = 507,
  SpectralBlades = 508,
  FistOfHavoc = 509,
  TectonicFist = 510,
  Thundercrash = 511,
  WardOfDawn = 512,
  BannerShield = 513,
  SentinelShield = 514,
  BurningMaul = 515,
  Forgemaster = 516,
  ScorchedEarth = 517,
  NovaBomb = 518,
  NovaWarp = 519,
  Radiance = 520,
  WellOfRadiance = 521,
  Dawnblade = 522,
  Stormtrance = 523,
  ChaosReach = 524,
  Landfall = 525
}

export enum OtherAbility {
  NightstalkerAmplifiedHits = 600,
  NightstalkerFlatline = 601,
  NightstalkerStudy = 602,
  NightstalkerFarsight = 603,
  NightstalkerAtTheReady = 604,
  NightstalkerPhantomStride = 605,
  NightstalkerPredatorsEye = 606,
  NightstalkerStabilize = 607,
  NightstalkerSpectersShroud = 608,
  NightstalkerStunningBlow = 609,
  NightstalkerRefresh = 610,
  NightstalkerShadestep = 611,
  NightstalkerTruesight = 612,
  StormcallerAlternatingCurrent = 613,
  StormcallerBolt = 614,
  StormcallerDischarge = 615,
  StormcallerInduction = 616,
  StormcallerOvercharged = 617,
  StormcallerOverride = 618,
  StormcallerRecharge = 619,
  StormcallerRefresh = 620,
  StormcallerWindswift = 621,
  StormcallerZaplight = 622,
  TitanDefense = 623,
  TitanDualWielder = 624,
  TitanGeneralist = 625,
  TitanHeavyWeaponsExpert = 626,
  TitanLoadedForBear = 627,
  TitanMasterAtArms = 628,
  VoidwalkerAbyssalStalwart = 629,
  VoidwalkerAngryMagic = 630,
  VoidwalkerAnnihilate = 631,
  VoidwalkerAtMySummons = 632,
  VoidwalkerBlink = 633,
  VoidwalkerBombsAway = 634,
  VoidwalkerChaosAccelerant = 635,
  VoidwalkerComprehensionOfTheVoid = 636,
  VoidwalkerDarkMatter = 637,
  VoidwalkerDetectLightAndDark = 638,
  VoidwalkerEmbraceTheVoid = 639,
  VoidwalkerEyesOfTheVoid = 640,
  VoidwalkerFeedTheVoid = 641,
  VoidwalkerGazeOfTheVoid = 642,
  VoidwalkerGiftOfTheVoid = 643,
  VoidwalkerGraduateStudent = 644,
  VoidwalkerHandheldSupernova = 645,
  VoidwalkerIdentifyExotic = 646,
  VoidwalkerItFollows = 647,
  VoidwalkerInsatiable = 648,
  VoidwalkerLongShot = 649,
  VoidwalkerMasterOfLight = 650,
  VoidwalkerOfferingToTheVoid = 651,
  VoidwalkerOuroboros = 652,
  VoidwalkerPantologist = 653,
  VoidwalkerPeerless = 654,
  VoidwalkerSuperlativeSurge = 655,
  VoidwalkerSurgingProtection = 656,
  VoidwalkerTeachingsOfTheVoid = 657,
  VoidwalkerUnbound = 658,
  VoidwalkerVigilantScholar = 659,
  VoidwalkerVortexMastery = 660,
  VoidwalkerWarpAmmo = 661,
  VoidwalkerWhispersOfTheVoid = 662
}

export interface Formula {
  _type: string,
  attackFormula?: string,
  savingThrowFormula?: { dc: string, ability: Ability }[],
  damage?: { formula: string, type: DamageType }[]
}

export type DiceRoll = {
  formula: string,
  evaluated: string,
  value: number,
  type?: DamageType
}

export interface Effect extends Formula {
  name?: string,
  ac?: string,
  charges?: {
    type: ChargeType,
    value: number
  },
  languages?: string[],
  weaknesses?: DamageType[],
  resistances?: DamageType[],
  immunities?: DamageType[],
  conditionImmunities?: string[],
  proficiencies?: ItemType[]
}

export type SkillOption = {
  choice: number,
  skills: Skill[]
}

export interface Feature extends Effect {
  level: number,
  name: string,
  description: string,
  source?: string,
  skillOptions?: (Skill|SkillOption)[]
}

export interface FoundationFeature extends Feature {
  featureOptions?: Feature[]
}

export interface SubclassFeature extends Feature {
  abilityOptions?: (GrenadeAbility | MeleeAbility)[],
  abilities?: OtherAbility[],
  pathOptions?: SubclassPath[]
  featureOptions?: Feature[],
  prerequisite?: string
}

export type SubclassChargeType = 'ability'|'feature'

export type SubclassCharge = {
  name: string,
  type: SubclassChargeType,
  values: Record<number, number>
}

export enum ItemRarity {
  Common = 0,
  Uncommon = 1,
  Rare = 2,
  Legendary = 3,
  Exotic = 4
}

export enum WeaponItemType {
  SimpleWeapon = 10,
  MartialWeapon = 11,
  PrimaryWeapon = 12,
  HeavyWeapon = 13,
  RelicWeapon = 14,
  ExoticWeapon = 15
}

export enum MiscItemType {
  Armor = 20,
  Vehicle = 30,
  Toolkit = 40,
  Other = 90
}

export enum ArmorItemType {
  LightArmor = 2000,
  MediumArmor = 2001,
  HeavyArmor = 2002,
  GhostShell = 2003
}

export enum GenericItemType {
  Currency = 0,
  Clothing = 1,
  Consumable = 2,
  Trinket = 3,
  Generic = 4,
  Container = 5,
  ClassItem = 6,
}

export enum SimpleWeaponItemType {
  Dagger = 1000,
  ThrowingHammer = 1001
}

export enum MartialWeaponItemType {
  Greataxe = 1100,
  Broadsword = 1101,
  Longsword = 1102,
  HeavyMaul = 1103,
  Shortsword = 1104,
  Smallsword = 1105,
  Warhammer = 1106,
  CombatBow = 1107
}

export enum PrimaryWeaponItemType {
  AutoRifle = 1200,
  ScoutRifle = 1201,
  PulseRifle = 1202,
  HandCannon = 1203,
  Sidearm = 1204,
  SubmachineGun = 1205,
  TraceRifle = 1206,
  Other = 1207
}

export enum HeavyWeaponItemType {
  FusionRifle = 1300,
  GrenadeLauncher = 1301,
  LightMachineGun = 1302,
  LinearFusionRifle = 1303,
  RocketLauncher = 1304,
  Shotgun = 1305,
  SniperRifle = 1306
}

export enum VehicleItemType {
  LandVehicle = 3000,
  AirVehicle = 3001
}

export enum ToolItemType {
  ArmorsmithingTools = 4000,
  ClimbingTools = 4001,
  CookingTools = 4002,
  ElectronicsTools = 4003,
  MedicalTools = 4004,
  ScubaTools = 4005,
  SewingTools = 4006,
  ThievesTools = 4007,
  VehicleTools = 4008,
  WeaponsmithingTools = 4009,
  WhittlingTools = 4010
}

export type AllWeaponItemType = WeaponItemType | SimpleWeaponItemType | MartialWeaponItemType | PrimaryWeaponItemType | HeavyWeaponItemType
export type ItemType = MiscItemType | GenericItemType | AllWeaponItemType | ArmorItemType | VehicleItemType | ToolItemType

export type SubclassPath = {
  id: string,
  name: string,
  perks: SubclassFeature[],
  super: SuperAbility
}

export interface Subclass {
  id: string,
  class: Class,
  element: LightElement | DarknessElement,
  classAbility: ClassAbility,
  name: string,
  shieldDie: number,
  perks: SubclassFeature[],
  charges?: SubclassCharge,
  abilityProficiencies: Ability[],
  armor: ArmorItemType[],
  weapons: AllWeaponItemType[],
  tools: (VehicleItemType|ToolItemType)[],
  lightModifier: Ability,
  getPath(id: string|null): SubclassPath|null
}

export interface LightAbility extends Formula {
  id: ClassAbility | GrenadeAbility | MeleeAbility | SuperAbility | OtherAbility,
  element?: LightElement | DarknessElement,
  name: string,
  description: string,
  castingTime?: string,
  range?: string,
  cost?: number,
  resource: ChargeType,
  recharge?: string,
  duration?: string,
  prerequisiteLevel?: number
}

export type ItemSubtype = SimpleWeaponItemType | MartialWeaponItemType | PrimaryWeaponItemType | HeavyWeaponItemType | VehicleItemType

export enum ItemProperty {
  Agile = 1,
  Ammunition = 2,
  Cumbersome = 4,
  Elemental = 8,
  Finesse = 16,
  Heavy = 32,
  Loading = 64,
  OneHanded = 128,
  Ranged = 256,
  Reach = 512,
  Special = 1024,
  Thrown = 2048,
  TwoHanded = 4096,
  Versatile = 8192,
  AutomaticFire = 16384,
  EnergyProjectiles = 32768,
  HighRecoil = 65536,
  Payload = 131072
}

export enum ItemRange {
  Close = 0,
  Medium = 1,
  Long = 2
}

export enum Size {
  Tiny = 0,
  Small = 1,
  Medium = 2,
  Large = 3,
  Huge = 4,
  Gargantuan = 5
}

export interface Item extends Formula {
  id: string,
  uuid: string,
  rarity: ItemRarity,
  name: string,
  description?: string,
  type: ItemType | ItemSubtype,
  cost?: number,
  ac?: string,
  prerequisite?: string,
  conditions?: string[],
  memory?: [number, number],
  weight?: number,
  properties?: ItemProperty[],
  range?: [number, number],
  scope?: [number, number, number],
  scopeRange?: ItemRange,
  speed?: number,
  weightLimit?: number,
  shotCapacity?: number,
  currentShots?: number,
  linkedConsumable?: string,
  note?: string,
  quantity?: number,
  size?: Size,
  slot1?: WeaponPerks,
  slot2?: WeaponPerks,
  slot3?: WeaponPerks,
  slot1Choices?: WeaponPerks[],
  slot2Choices?: WeaponPerks[],
  slot3Choices?: WeaponPerks[],
  proficient?: boolean,
  equipped?: boolean,
  attuned?: boolean,
  ability?: Ability
}

export interface Action extends Formula {
  name: string,
  description: string,
  healing?: string,
  recharge?: string,
  cost?: number
}

export interface Spell extends Formula {
  name: string,
  level: number,
  note?: string,
  atIni?: boolean,
  atWill?: boolean
}

export interface Monster {
  id: string,
  name: string,
  race: Race,
  statblock: string,
  lore: string
}

export interface Enemy {
  id: string,
  name: string,
  size: Size,
  species: Race,
  speciesText: string,
  description: string,
  ac: number,
  armor: string,
  healthPoint?: number,
  healthPointRoll?: string,
  pools?: number,
  ultraHealthPoint?: number[],
  ultraHealthPointRoll?: string[],
  phase?: number,
  energyShield?: number,
  energyShieldRoll?: string,
  shieldAffinity?: LightElement[] | DarknessElement[] | NeutralElement[],
  speed?: number,
  climb?: number,
  swim?: number,
  burrow?: number,
  hover?: number,
  abilities: Record<Ability, number>,
  proficiencyBonus: number,
  hackDevice?: number,
  savingThrows?: Ability[],
  skills?: string,
  vehicleProficiency?: string,
  damageVulnerabilities?: DamageType[],
  damageResistances?: DamageType[],
  damageImmunities?: DamageType[],
  conditionImmunities?: string[],
  senses: string,
  languages?: string,
  crIndividual?: number,
  crClassification?: number,
  crText: string,
  features?: Feature[],
  actions?: Action[],
  reactions?: Action[],
  lairActions?: string,
  regionalEffect?: string,
  legendaryActionsAmount?: number,
  legendaryActions?: Action[],
  ultraActions?: Action[],
  enraged?: Action[],
  spellcasting?: Spell[],
  spellslot?: number[],
  spellAbility?: Ability,
  spellDC?: number,
  spellHit?: number,
  spellRecovery?: string
}

export interface WeaponPerks {
  id: string,
  name: string,
  description?: string
}

export enum Ability {
  Strength = 0,
  Dexterity = 1,
  Constitution = 2,
  Intelligence = 3,
  Wisdom = 4,
  Charisma = 5
}

export enum Skill {
  Acrobatics = 0,
  AnimalHandling = 1,
  Arcana = 2,
  Athletics = 3,
  Deception = 4,
  History = 5,
  Insight = 6,
  Intimidation = 7,
  Investigation = 8,
  Medicine = 9,
  Nature = 10,
  Perception = 11,
  Performance = 12,
  Persuasion = 13,
  Religion = 14,
  SleightOfHand = 15,
  Stealth = 16,
  Survival = 17,
  Technology = 18
}

export enum ProficiencyType {
  None = 0,
  Half = 1,
  Proficiency = 2,
  Expertise = 3
}

export enum ChargeType {
  Grenade = 0,
  Melee = 1,
  Super = 2,
  Class = 3,
  Subclass = 4
}

export enum Alignment {
  LawfulGood = 0,
  LawfulNeutral = 1,
  LawfulEvil = 2,
  NeutralGood = 3,
  TrueNeutral = 4,
  NeutralEvil = 5,
  ChaoticGood = 6,
  ChaoticNeutral = 7,
  ChaoticEvil = 8
}

export class Guardian {
  id = ''
  name = 'New character'
  privacy: 'private'|'public'|'deleted' = 'private'
  image = ''
  player = ''
  speed = ''
  race: string|null = null
  subclass: string|null = null
  subclassPath: string|null = null
  subclassFeatureChoices: Record<string, string|string[]> = {}
  meleeChoice: MeleeAbility|null = null
  grenadeChoice: GrenadeAbility|null = null
  faction: string|null = null
  alignment: Alignment|null = null
  conditions: string[] = []
  level = 1
  experience = 0
  hitPoints = 0
  maxHitPoints: number|null = null
  shieldPoints = 0
  maxShieldPoints: number|null = null
  overshieldPoints = 0
  maxOvershieldPoints: number|null = null
  languages = ''
  senses = ''
  weapons: AllWeaponItemType[] = []
  armor: ArmorItemType[] = []
  tools: (VehicleItemType|ToolItemType)[] = []
  resistances: DamageType[] = []
  weaknesses: DamageType[] = []
  immunities: DamageType[] = []
  conditionImmunities: string[] = []
  glimmer = 0
  engrams = 0
  inspiration = false
  deathSaveSuccesses = 0
  deathSaveFailures = 0
  items: Item[] = []
  abilities: Record<Ability, number> = {
    [Ability.Strength]: 10,
    [Ability.Dexterity]: 10,
    [Ability.Constitution]: 10,
    [Ability.Intelligence]: 10,
    [Ability.Wisdom]: 10,
    [Ability.Charisma]: 10
  }
  skillProficiencies: Record<Skill, ProficiencyType> = {
    [Skill.Acrobatics]: ProficiencyType.None,
    [Skill.AnimalHandling]: ProficiencyType.None,
    [Skill.Arcana]: ProficiencyType.None,
    [Skill.Athletics]: ProficiencyType.None,
    [Skill.Deception]: ProficiencyType.None,
    [Skill.History]: ProficiencyType.None,
    [Skill.Insight]: ProficiencyType.None,
    [Skill.Intimidation]: ProficiencyType.None,
    [Skill.Investigation]: ProficiencyType.None,
    [Skill.Medicine]: ProficiencyType.None,
    [Skill.Nature]: ProficiencyType.None,
    [Skill.Perception]: ProficiencyType.None,
    [Skill.Performance]: ProficiencyType.None,
    [Skill.Persuasion]: ProficiencyType.None,
    [Skill.Religion]: ProficiencyType.None,
    [Skill.SleightOfHand]: ProficiencyType.None,
    [Skill.Stealth]: ProficiencyType.None,
    [Skill.Survival]: ProficiencyType.None,
    [Skill.Technology]: ProficiencyType.None
  }
  charges: Record<ChargeType, number> = {
    [ChargeType.Grenade]: 0,
    [ChargeType.Melee]: 0,
    [ChargeType.Super]: 0,
    [ChargeType.Class]: 0,
    [ChargeType.Subclass]: 0
  }
  personalityTraits = ''
  ideals = ''
  bonds = ''
  flaws = ''
  appearance = ''
  bio = ''
  notes = ''
  gmNotes = ''
  ghost: Ghost

  constructor (userId: string) {
    this.player = userId
    this.ghost = new Ghost
  }
}

export class Ghost {
  name = 'New ghost'
  image = ''
  alignment: Alignment|null = null
  conditions: string[] = []
  level = 1
  experience = 0
  hitPoints = 0
  maxHitPoints: number|null = null
  languages = ''
  deathSaveSuccesses = 0
  deathSaveFailures = 0
  restorationPoints = 0
  abilities: Record<Ability, number> = {
    [Ability.Strength]: 10,
    [Ability.Dexterity]: 10,
    [Ability.Constitution]: 10,
    [Ability.Intelligence]: 10,
    [Ability.Wisdom]: 10,
    [Ability.Charisma]: 10
  }
  skillProficiencies: Record<Skill, ProficiencyType> = {
    [Skill.Acrobatics]: ProficiencyType.None,
    [Skill.AnimalHandling]: ProficiencyType.None,
    [Skill.Arcana]: ProficiencyType.None,
    [Skill.Athletics]: ProficiencyType.None,
    [Skill.Deception]: ProficiencyType.None,
    [Skill.History]: ProficiencyType.None,
    [Skill.Insight]: ProficiencyType.None,
    [Skill.Intimidation]: ProficiencyType.None,
    [Skill.Investigation]: ProficiencyType.None,
    [Skill.Medicine]: ProficiencyType.None,
    [Skill.Nature]: ProficiencyType.None,
    [Skill.Perception]: ProficiencyType.None,
    [Skill.Performance]: ProficiencyType.None,
    [Skill.Persuasion]: ProficiencyType.None,
    [Skill.Religion]: ProficiencyType.None,
    [Skill.SleightOfHand]: ProficiencyType.None,
    [Skill.Stealth]: ProficiencyType.None,
    [Skill.Survival]: ProficiencyType.None,
    [Skill.Technology]: ProficiencyType.None
  }
  personalityTraits = ''
}

export type UserData = {
  id?: string,
  displayName: string,
  isAdmin?: boolean
}

export type Session = {
  id?: string,
  name: string,
  privacy: 'private'|'public'|'deleted',
  author: string,
  authorName: string,
  members: {
    player: string,
    playerName: string,
    character: string
  }[]
}

export type SessionChatEntry = {
  id?: string,
  content?: string,
  author: string,
  authorName?: string,
  recipient?: string,
  recipientName?: string,
  data?: string,
  itemName?: string,
  type: 'message'|'item'|'action'|'dice'|'itemDesc'|'abilityDesc'|'featureDesc'
  date: FieldValue
}

export type SessionEncounterEntry = {
  name?: string,
  monsters?: StatBlock[]
}
