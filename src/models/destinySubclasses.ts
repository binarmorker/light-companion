import { SubclassFeature, Subclass, Class, LightElement, Ability, ArmorItemType, WeaponItemType,
  MartialWeaponItemType, HeavyWeaponItemType, MeleeAbility, SubclassPath, GrenadeAbility, SuperAbility,
  ClassAbility, VehicleItemType, ChargeType, OtherAbility, Feature, SubclassCharge, DamageType, Skill, ToolItemType } from '@/models/destiny'
import { getAllSkills } from '@/utils'

const LightGrenade: Feature = {
  _type: 'Feature',
  level: 2,
  name: 'Light Grenade',
  description: `Beginning at 2nd level you learn to form your Light into grenades, which you cast using a grenade ability charge. You have a single grenade ability charge.

Choose one type of Light grenade to attune yourself to. The options you can choose from are based on your class, as shown in the Grenade Options table below. You can alter your choice by practicing with your Light for 10 minutes, which can be done during a short or long rest. Once you have completed your practice, you are attuned to your new choice of Light grenade. If your practice is interrupted you do not become attuned to your new choice of Light grenade and must start the practice again.

When you spend a grenade charge to cast a Light grenade, you can only cast a grenade of the type you are attuned to.`,
  source: 'Risen'
}

const RisenClassFeatures: SubclassFeature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Background: Risen',
    description: `Chosen from the dead by the Traveler's Ghosts, Risen are those rare few able to wield the Light as a weapon. Risen are the only ones who can choose a Guardian class, and those who are Risen must choose a Guardian class as soon as they become Risen.

Some Risen have been dead for centuries; others, merely minutes. Regardless of how long you were dead, when your Ghost first resurrects you as a Risen creature, you lose all memory of your past life. Maybe you were a criminal, maybe you were a saint—it no longer matters. You are reborn into your second life with a blank slate.

…At least, more or less blank. It is not unheard of for some habits and skills to linger from your past life. When making your character, determine what sort of person you were in your past life and work with your Architect to determine what, if anything, carried over. If your race grants you a language, do you still remember that language? Do you have fragmentary memories of your past life, or any living relatives? It's also a good idea to determine when and where you died, and how your Ghost found you.

- **Proficiencies**: Risen language, RSL, and choose any one skill, toolkit, vehicle, or additional language to be proficient in. Your choice should be reflective of your experiences in your past life.
- **Equipment**: You have what equipment your Ghost is able to provide for you (see Chapter 3 for more).`,
    source: 'Risen'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Ghost',
    description: `As a Risen you have a Ghost, a tiny construct made by the Traveler, now your inseparable companion. Your Ghost is the conduit to your power as a Guardian. Without it, you cannot wield the Light or advance in any Guardian class.

Your Ghost is considered its own creature and has its own thoughts, feelings, opinions, and desires. See Chapter 3 for more information on how to create your Ghost, and what it can do.`,
    source: 'Risen'
  },
  {
    _type: 'Feature',
    level: 3,
    name: 'Super Ability',
    description: `At 3rd level you obtain your super ability option, determined by the class archetype you picked. For example, Gunslingers who choose the Showman archetype obtain the Blade Barrage super ability, while Voidwalkers who chose the Harbinger of Chaos archetype obtain the Nova Warp super ability.

Your super ability is included in your class' archetype description. You cast this super ability using a super ability charge. You can never have more than one super ability charge at a time.`,
    source: 'Risen'
  },
  {
    _type: 'Feature',
    level: 3,
    name: 'Destroy Creature',
    description: `Beginning at 3rd level, your Light is powerful enough to outright obliterate weaker foes. If you deal damage to a creature with your super ability, the creature is instantly destroyed if its individual challenge rating is at or below the value listed in the Destroy Creature table, with respect to your total Risen level.

| Risen Level | Creature CR  |
| :---------: | :----------- |
| 3rd	        | 1/4 or lower |
| 5th	        | 1/2 or lower |
| 8th	        | 1 or lower   |
| 11th        | 2 or lower   |
| 14th        | 3 or lower   |
| 17th        | 4 or lower   |`,
    source: 'Risen'
  }
]

const TitanClassFeatures: SubclassFeature[] = [
  ...RisenClassFeatures,
  {
    _type: 'Feature',
    level: 1,
    name: 'Titan\'s Natural Skill',
    description: 'You are proficient in the Athletics skill',
    skillOptions: [Skill.Athletics],
    source: 'Titan'
  },
  {
    _type: 'Feature',
    level: 1,
    name: 'Titan\'s Strength - Unarmed Strike',
    description: `As a Risen Titan, you are proficient in unarmed strikes, and you roll a d4 for the damage of your unarmed strikes. This damage die increases as your total Titan levels increase. It increases to 1d6 at 5th level, 1d8 at 9th level, 1d10 at 13th level, and 1d12 at 17th level.

Additionally, beginning at 2nd level, you can always choose to deal Light damage instead of bludgeoning damage whenever you hit with your unarmed strike. The type of Light damage you deal (arc, solar, or void) is determined by your Titan class (see your class' Light Affinity feature).`,
    source: 'Titan',
    attackFormula: '1d20 + [strMod] + [prof]',
    damage: [
      { formula: '1d((2 + (([level] - 1) / 4)) * 2) + [strMod]', type: DamageType.Kinetic }
    ]
  }
]

const WarlockClassFeatures: SubclassFeature[] = [
  ...RisenClassFeatures,
  {
    _type: 'Feature',
    level: 1,
    name: 'Warlock\'s Natural Skill',
    description: `You are proficient in the Arcana skill`,
    skillOptions: [Skill.Arcana],
    source: 'Warlock'
  },
  {
    _type: 'Feature',
    level: 1,
    name: 'Warlock\'s Prowess',
    description: `As a Risen Warlock, your control over the Light you channel allows you carefully craft the range of its effects. You can give a number of creatures equal to 1 + your Light ability modifier immunity against your Light abilities whenever you cast them. They take no damage and trigger no effects from your Light abilities. You can change which creatures gain this benefit on your turn. See your class' Light Affinity feature to determine your Light ability modifier.`,
    source: 'Warlock'
  },
  {
    _type: 'Feature',
    level: 1,
    name: 'Unarmed Strike',
    description: 'Instead of using a weapon to make a melee weapon attack, you can use an action to make an unarmed strike: a punch, kick, head-butt, or similar forceful blow (none of which count as weapons). On a hit, an unarmed strike deals bludgeoning damage equal to 1 + your Strength modifier. You are proficient with your unarmed strikes.',
    source: 'Warlock',
    attackFormula: '1d20 + [strMod] + [prof]',
    damage: [
      { formula: '1d1 + [strMod]', type: DamageType.Kinetic }
    ]
  }
]

const HunterClassFeatures: SubclassFeature[] = [
  ...RisenClassFeatures,
  {
    _type: 'Feature',
    level: 1,
    name: 'Hunter\'s Natural Skill',
    description: `You are proficient in the Survival skill`,
    skillOptions: [Skill.Survival],
    source: 'Hunter'
  },
  {
    _type: 'Feature',
    level: 1,
    name: 'Hunter\'s Instincts',
    description: `As a Risen Hunter, you can take the Disengage action as a bonus action on your turn.`,
    source: 'Hunter'
  },
  {
    _type: 'Feature',
    level: 1,
    name: 'Unarmed Strike',
    description: 'Instead of using a weapon to make a melee weapon attack, you can use an action to make an unarmed strike: a punch, kick, head-butt, or similar forceful blow (none of which count as weapons). On a hit, an unarmed strike deals bludgeoning damage equal to 1 + your Strength modifier. You are proficient with your unarmed strikes.',
    source: 'Hunter',
    attackFormula: '1d20 + [strMod] + [prof]',
    damage: [
      { formula: '1d1 + [strMod]', type: DamageType.Kinetic }
    ]
  }
]

export class Sunbreaker implements Subclass {
  static Devastator: SubclassPath = {
    id: 'devastator',
    name: 'Code of the Devastator',
    super: SuperAbility.BurningMaul,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Code of the Devastator',
        description: `Sunbreakers who practice the Code of the Devastator have chosen to refine themselves into instruments of raw power, determined to obliterate opposition with thunderous, earthshaking blows of a Light-forged warhammer. What they lack in subtlety, they more than make up for with their ability to flatten even the most daunting foes.`,
        source: 'Titan Sunbreaker Devastator'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'If I Had a Hammer',
        description: `At 3rd level, you are adept at the technique of coalescing your Light into a sturdy throwing hammer, and setting it alight with solar fire. You can use your action to spend your melee ability charge, constructing a one-handed hammer made out of your Light in a free hand. This hammer cannot be stored in a Ghost's memory, and it lasts until you choose to dissipate it (no action cost), until you die, or until you are ever more than 5 feet away from it for 1 minute or longer, at which point it dissipates on its own.

It is an action to make an attack with your Light-constructed hammer, and you make a Light attack roll even if you choose to throw the hammer (20/60 range). When you hit with your Light-constructed hammer, you deal the damage of your Sunstrike.

The maximum number of melee ability charges you have access to is reduced by one for each Lightconstructed hammer you have active from this feature. You cannot spend a melee ability charge you do not have access to, and you cannot make recharge rolls for it. When a Light-constructed hammer dissipates, you regain access to one melee ability charge, but you do not automatically regain that melee ability charge; you must recharge it as normal.`,
        source: 'Titan Sunbreaker Devastator'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Kindled Vigor',
        description: `At 6th level, the flame of your Light burns brightly, and kindles a surge in your strength. You can double your proficiency bonus in the Strength (Athletics) skill if it is not already benefiting from a source that doubles your proficiency bonus for it, and your weight limit is doubled.`,
        source: 'Titan Sunbreaker Devastator'
      },
      {
        _type: 'Feature',
        level: 10,
        name: 'Backdraft',
        description: `Beginning at 10th level, when you stoke the fire of your Light with the bellows of your will, it can propel you forward like a rush of hot wind. On your turn, you can choose to double your movement until the end of your turn. Once you use this feature, you can't use it again until you move 0 feet on one of your turns.`,
        source: 'Titan Sunbreaker Devastator'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Tireless Warrior',
        description: `At 15th level, taking your hammer in hand makes you feel dauntless, unwavering, and focused. You gain advantage on concentration checks to maintain your Hammer of Sol.`,
        source: 'Titan Sunbreaker Devastator'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Roaring Flames',
        description: `At 18th level, mastery of the Devastator's Code has taught you to make your Light fearsome. When you draw it forth as your hammer, it is a thing of terror to your enemies. On the turn you cast Hammer of Sol, all creatures hostile to you within the bright light of your Hammer of Sol must make a Wisdom saving throw against your Light save DC. On a failed save, they become Frightened of you for 1 minute, or until they take any damage. All attack rolls made against creatures Frightened in this way have advantage.`,
        source: 'Titan Sunbreaker Devastator',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Wisdom }
        ]
      }
    ]
  }
  static Fireforged: SubclassPath = {
    id: 'fireforged',
    name: 'Fireforged',
    super: SuperAbility.Forgemaster,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Code of the Fireforged',
        description: `The Fireforged are those among the Sunbreakers who train to become unwavering leaders and sharp tacticians. Adherents to this code learn to focus their Light both to weaken enemy defenses and bolster allies' resolve. They prefer to take initiative, bringing the hammer down before enemies can escape their scourging fire.`,
        source: 'Titan Sunbreaker Fireforged'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Warmth',
        description: `At 3rd level, the flames of your Light are a warming comfort to your comrades. Whenever a Risen creature within 15 feet of you receives any healing, the amount of healing they receive increases by 1d6 + your Wisdom modifier. The type of healing (health, shields, or hit points) is determined by the original source of healing you are bolstering.

The die you roll for this feature increases in size as you gain levels in this class. It becomes 1d8 at 9th Sunbreaker level, 1d10 at 13th Sunbreaker level, and then 1d12 at 17th Sunbreaker level.`,
        source: 'Titan Sunbreaker Fireforged'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Melting Point',
        description: `When you reach 6th level, you understand the forge's fire can be used both to create and to unmake, and you learn to apply your Light's fire to burn your enemies apart. If you hit a target with your Sunstrike melee ability, you can choose to cause the target to make a Constitution saving throw against your Light save DC. On a failed save, the target becomes Weakened for 1 minute. Targets Weakened in this way take an additional 1d8 solar damage when they take damage from any source. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success.`,
        source: 'Titan Sunbreaker Fireforged',
        damage: [
          { formula: '1d8', type: DamageType.Solar }
        ],
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      },
      {
        _type: 'Feature',
        level: 10,
        name: 'Tempered Metal',
        description: `Beginning at 10th level, the sparks cast from the blows of your Light are boons to you and your allies alike. Once on your turn, if you kill a creature hostile to you with the damage from any of your core Light abilities, you and all creatures of your choice within 15 feet of you gain a bonus 15 feet of movement until the start of your next turn.`,
        source: 'Titan Sunbreaker Fireforged'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Flameseeker',
        description: `At 15th level, you handle your Light-forged hammer with the easy readiness of long practice. The range of your super ability's Light Hammer Strike becomes 40/120, and your Suncharge now affects creatures in a 20-foot line.`,
        source: 'Titan Sunbreaker Fireforged'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Solaris Cordis',
        description: `At 18th level, you have tended the fires of your Light well, and now they burn high and hot. The radius of your Warmth and Tempered Metal features increases to 40 feet.`,
        source: 'Titan Sunbreaker Fireforged'
      }
    ]
  }
  static Siegebreaker: SubclassPath = {
    id: 'siegebreaker',
    name: 'Siegebreaker',
    super: SuperAbility.ScorchedEarth,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Code of the Siegebreaker',
        description: `When hope is dim and the outcome appears dire, the Sunbreakers send those who follow the Code of the Siegebreaker. Their scorching fire burns brightest when darkness seems overwhelming, and where others would turn away, they forge ahead. Their code calls for burning the very ground enemies stand upon, claiming the battlefield itself with fire.`,
        source: 'Titan Sunbreaker Siegebreaker'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Sculpt Light',
        description: `At 3rd level, your control over the Light you channel allows you carefully craft the range of its effects. You can give a number of creatures equal to 1 + your Light level immunity against your Light abilities whenever you cast them. They take no damage and trigger no effects from your Light abilities. You can change which creatures gain this benefit on your turn.`,
        source: 'Titan Sunbreaker Siegebreaker'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Sunburst',
        description: `At 6th level, you have refined the Light you gather into your fist when you strike, concentrating it to the point of bursting. If you hit a target with your Sunstrike melee ability, you can choose to create a Sunspot at your feet. While standing in the area of any Sunspot, you have advantage on melee, grenade, superclass recharge rolls. All Sunspots you make can invoke the effects of your Destroy Creature superclass feature.`,
        source: 'Titan Sunbreaker Siegebreaker'
      },
      {
        _type: 'Feature',
        level: 10,
        name: 'Cauterize',
        description: `Beginning at 10th level, you draw renewed fortitude from the ashes of each foe you cast into the pyre of your Light. If a creature hostile to you is reduced to 0 hit points with the damage from your core Light abilities or Sunspot, you can make a shield recharge roll. Once you use this feature, you must wait until the start of your next turn before you can use it again.`,
        source: 'Titan Sunbreaker Siegebreaker'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Firekeeper',
        description: `At 15th level, your code teaches you to give yourself over to the embrace of your own flames. If you start your turn within 5 feet of any of your Sunspots, you gain an overshield. If you already had an overshield, your overshield is restored to maximum capacity.`,
        source: 'Titan Sunbreaker Siegebreaker'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Endless Siege',
        description: `When you reach 18th level, as a master Siegebreaker, you have learned your strongest position is within the conflagration of your own fiery Light. While standing within 5 feet of any Sunspot, you have advantage on super ability recharge rolls. You can make super ability recharge rolls in this way even while concentrating on your super ability.`,
        source: 'Titan Sunbreaker Siegebreaker'
      }
    ]
  }

  id = 'sunbreaker'
  class = Class.Titan
  element = LightElement.Solar
  classAbility = ClassAbility.Barricade
  name = 'Titan Sunbreaker'
  shieldDie = 10
  perks = [
    ...TitanClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Sunbreaker',
      description: `As a Sunbreaker, you gain the following class features in addition to the features granted by the Titan superclass.      
### Health Points and Shields
- **Shield Die Size**: d10
- **Health Points at 1st level**: 10 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 10
- **Shield Points at higher levels**: 1d10 (or 6) for every level after 1st
### Proficiencies
- **Armor**: All armor
- **Weapons**: All weapons and firearms
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Strength, Wisdom
- **Skills (Choose 2)**: History, Insight, Intimidation, Perception, Persuasion, Religion, Survival, and Technology`,
      skillOptions: [{ choice: 2, skills: [Skill.History, Skill.Insight, Skill.Intimidation, Skill.Perception, Skill.Persuasion, Skill.Religion, Skill.Survival, Skill.Technology] }],
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Stamina Surge',
      description: `As a Sunbreaker, your Light was forged in the heat of your Order's rigorous trials, and your resolve tempered to unfailing strength. When others might fall, you press yourself to rise again. If you take damage that reduces you to half your shield points or less, but you still have at least 1 health point remaining, you can use your reaction to make a shield recharge roll. Alternatively, if you start your turn with half your shield points or less, you can use a bonus action to make a shield recharge roll. Once you invoke this feature, you must complete a short or long rest before you can use it again.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Combat Specialty',
      description: `Even at 1st level, you have a fighting specialization with a distinct style. Choose one of the following options. You can't select a Combat Specialty option more than once, even if you later get to choose again.`,
      source: 'Titan Sunbreaker',
      abilities: [
        OtherAbility.TitanDefense,
        OtherAbility.TitanDualWielder,
        OtherAbility.TitanGeneralist,
        OtherAbility.TitanHeavyWeaponsExpert,
        OtherAbility.TitanLoadedForBear,
        OtherAbility.TitanMasterAtArms
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Light Affinity',
      description: `Beginning at 2nd level, you have learned to harness the Light you possess, and you shape it in the form of solar Light. Wisdom is your Light ability score, and your Light Ability modifier is your Wisdom modifier. You use your Wisdom modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Wisdom saving throw.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Melee Ability',
      description: `At 2nd level, your Light gives you the ability to empower your unarmed strikes. You gain a new Light ability, the Sunstrike, which you cast using a melee ability charge. You have one melee ability charge. You regain spent melee ability charges when you complete a brief rest.`,
      source: 'Titan Sunbreaker',
      abilityOptions: [
        MeleeAbility.Sunstrike
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Titan Sunbreaker',
      abilityOptions: [
        GrenadeAbility.Fusion,
        GrenadeAbility.Incendiary,
        GrenadeAbility.Thermite
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Inner Flame',
      description: `At 2nd level, the Light you carry within you is forged into the Sunbreakers' fire, and you are initiated into its mysteries. You gain a number of Inner Flame uses equal to 1 + your Wisdom modifier (minimum of 1 use). When you spend a use of your Inner Flame, you can choose which of the following effects to cause. You have access to all effects at all times, unless another feature, condition, or other source specifies otherwise. You regain all your uses of Inner Flame when you complete a short rest.

**Flare.** When you are attacked by a creature within 30 feet of you that you can see, you can use your reaction to spend a use of your Inner Flame to cause disadvantage on all attacks that creature makes until the end of their turn.

**Smite.** When you hit with a weapon attack, or damage a target with a payload weapon attack, you can spend a use of Inner Flame to deal an additional 1d8 solar damage. The amount of damage this Inner Flame option deals increases as you advance in this class. It becomes 2d8 at 5th Sunbreaker level, 3d8 at 11th Sunbreaker level, and 4d8 at 17th Sunbreaker level.

**Soothe.** As a bonus action on your turn, you can touch a willing creature and spend a use of your Inner Flame to allow them to heal hit points equal to 1d10 + your Wisdom modifier. The number of hit points a creature can heal increases by 1d10 for each light level you are above first.`,
      source: 'Titan Sunbreaker',
      damage: [
        { formula: '(1 + (([level] + 1) / 6))d8', type: DamageType.Solar }
      ]
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Sunbreaker Code',
      description: `When you reach 3rd level, you choose a code that reflects your principles and the shape of the burning Light within you. Choose the Code of the Devastator, Code of the Siegebreaker, or the Code of the Fireforged, all detailed at the end of this class description. Your choice grants you features at 3rd, 6th, 10th, 15th, and 18th level.`,
      source: 'Titan Sunbreaker',
      pathOptions: [
        Sunbreaker.Devastator,
        Sunbreaker.Fireforged,
        Sunbreaker.Siegebreaker
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Hammerheaded',
      description: `At 7th level, you are sure of yourself and the fires of your Light. When you call upon your Hammer of Sol, you do so with unshakable poise. You are immune to being Frightened while concentrating on your Hammer of Sol. If you are Frightened when you cast your Hammer of Sol, the effect is suspended for the duration of your Hammer of Sol.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 11,
      name: 'Refined Flame',
      description: `At 11th level, with training and the practice of hard use, the flames of your Light are refined, and you gain deeper insight into the Sunbreaker ways. Choose one of the following features.`,
      featureOptions: [
        {
          _type: 'Feature',
          level: 11,
          name: 'Blinding Flare',
          description: `Whenever any creature is attacked within 30 feet of you, you can use your reaction to spend a use of your Inner Flame to cause disadvantage on the attack roll, and the attacker must make a Wisdom saving throw against your Light save DC. On a failed save, the attacker becomes Blinded for 1 minute. The attacker can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success.`,
          source: 'Titan Sunbreaker',
          savingThrowFormula: [
            { dc: '8 + [prof] + [lightMod]', ability: Ability.Wisdom }
          ]
        },
        {
          _type: 'Feature',
          level: 11,
          name: 'Wrathful Smite',
          description: `Each of your weapon attacks deals an additional 1d8 solar damage on a hit. This includes attacks on which you spend a use of your Inner Flame to invoke your regular Smite.`,
          source: 'Titan Sunbreaker',
          damage: [
            { formula: '1d8', type: DamageType.Solar }
          ]
        },
        {
          _type: 'Feature',
          level: 11,
          name: 'Revitalizing Soothe',
          description: `When you spend a use of Inner Flame on Soothe, the target gains an additional amount of shield points equal to 5 times your Light level.`,
          source: 'Titan Sunbreaker'
        }
      ],
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 14,
      name: 'Additional Combat Specialty',
      description: `By 14th level, you have become a battle-hardened veteran. Choose a second option from your Combat Specialty class feature.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Sunbreaker'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Fight Forever',
      description: `At 20th level, you are a master of the Sunbreaker Order's fighting arts. While you are concentrating on your Hammer of Sol, the following benefits apply:
- You have advantage on Light attack rolls you make against creatures in the bright light of your Hammer of Sol.
- Creatures who are within the bright light of your Hammer of Sol have disadvantage on any saving throws against any of your core Light abilities that deal solar damage.
- You can regain 10 hit points at the start of each of your turns.`,
      source: 'Titan Sunbreaker'
    }
  ]
  charges: SubclassCharge = {
    name: 'Combat Specialties',
    type: 'feature',
    values: {
      1: 1,
      2: 1,
      3: 1,
      4: 1,
      5: 1,
      6: 1,
      7: 1,
      8: 1,
      9: 1,
      10: 1,
      11: 1,
      12: 1,
      13: 1,
      14: 2,
      15: 2,
      16: 2,
      17: 2,
      18: 2,
      19: 2,
      20: 2
    }
  }
  abilityProficiencies = [
    Ability.Strength,
    Ability.Wisdom
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor,
    ArmorItemType.HeavyArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    WeaponItemType.MartialWeapon,
    WeaponItemType.PrimaryWeapon,
    WeaponItemType.HeavyWeapon
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle
  ]
  lightModifier = Ability.Wisdom

  getPath (id: string|null) {
    switch (id) {
      case Sunbreaker.Devastator.id: return Sunbreaker.Devastator
      case Sunbreaker.Fireforged.id: return Sunbreaker.Fireforged
      case Sunbreaker.Siegebreaker.id: return Sunbreaker.Siegebreaker
      default: return null
    }
  }
}

export class Striker implements Subclass {
  static Juggernaut: SubclassPath = {
    id: 'juggernaut',
    name: 'Juggernaut',
    super: SuperAbility.FistOfHavoc,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Juggernaut',
        description: `First to the fray and last to leave, you are the unstoppable blow that breaks the enemy line, and the unflappable counterstrike that clears the way for your allies. Your Fist of Havoc turns you into walking lightning, and your every attack becomes a veritable thunderstrike.`,
        source: 'Titan Striker Juggernaut'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Fist First',
        description: `Also at 3rd level, you embrace the old idea that a good offense is the best defense. When you hit a hostile creature with Storm Fist, you also grant yourself an overshield that lasts for up to one minute.`,
        source: 'Titan Striker Juggernaut'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Unwavering',
        description: `At 6th level, no opponent is ever too much for you to take on. When you invoke Reckless Engagement, you gain advantage on saving throws against being Charmed or Frightened until the start of your next turn. If you are already Charmed or Frightened when you invoke Reckless Engagement, the effect ends early for you.`,
        source: 'Titan Striker Juggernaut'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Immovable Stance',
        description: `Beginning at 11th level, your training develops exceptionally sure footing through exercises to resist being pushed. If an effect moves you against your will along the ground, you can use your reaction to remain where you are, and if the effect normally causes you to also fall Prone, you do not fall Prone.`,
        source: 'Titan Striker Juggernaut'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Way of The Fist',
        description: `By 15th level, you hold yourself in constant readiness to unleash the fury of your Light at an instant's notice. The recharge value of your Fist of Havoc is reduced by one. You cannot lose concentration on Fist of Havoc as a result of taking damage.`,
        source: 'Titan Striker Juggernaut'
      }
    ]
  }
  static Engineer: SubclassPath = {
    id: 'engineer',
    name: 'Engineer',
    super: SuperAbility.TectonicFist,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Engineer',
        description: `You choose to fight according to careful battle-plans, and even when you must strategize on the fly, your tactics are always considered and your blows are precisely placed. You train to channel your Light into the Tectonic Fist, to strike with overwhelming force in the single exact spot that will shape a battle according to your design.`,
        source: 'Titan Striker Engineer'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Shoulder Charge',
        description: `Also at 3rd level, you develop the technique of throwing your shoulder into a charging attack to knock even formidable enemies off balance. If, on one turn, you move at least 20 feet in a straight line toward a target that is no more than one size larger than you, and you immediately hit it with your Storm Fist, the target must succeed on a Strength saving throw against your Light save DC. On a failed save it is pushed back 10 feet, where it falls Prone.`,
        source: 'Titan Striker Engineer',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Strength }
        ]
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Handy Knack',
        description: `At 3rd level your work as a builder and fortifier makes you proficient with the Technology skill and the electronics toolkit, if you weren't already.`,
        skillOptions: [Skill.Technology],
        proficiencies: [ToolItemType.ElectronicsTools],
        source: 'Titan Striker Engineer'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Aftershocks',
        description: `At 6th level, you learn to use one application of your Light to bolster the next. If you damage a hostile creature with Storm Fist, your next grenade recharge roll is made with advantage.`,
        source: 'Titan Striker Engineer'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Grenadier',
        description: `Beginning at 11th level, deliberate practice shaping your explosive Light enables you to gain a second grenade ability charge. You cannot have more than two grenade ability charges. Furthermore, you automatically succeed on concentration checks you make to maintain Light grenades that you cast.`,
        source: 'Titan Striker Engineer',
        charges: {
          type: ChargeType.Grenade,
          value: 1
        }
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Tectonic Fist Attunement',
        description: `At 15th level, your training has advanced, and the strength of your Light has grown considerably. You learn one of the following ways to modify your Tectonic Fist. You can change your choice by practicing exercises with your Light, focusing on this feature exclusively for at least 4 hours (this practice can be done during a long rest). If you complete your practice, you lose the effects of your previous choice and gain the effects of your new choice.`,
        source: 'Titan Striker Engineer',
        featureOptions: [
          {
            _type: 'Feature',
            level: 15,
            name: 'Aftermath',
            description: `When you cast Tectonic Fist, you also create a sphere of wild arc Light that is 15 feet in diameter, centered on you. This sphere remains in place for the next minute, and it does not require concentration. Creatures of your choice who start their turn within the sphere, or who enter the sphere for the first time on a turn, must make a Dexterity saving throw. They take 2d8 arc damage on a failed save, or half as much on a success.`,
            source: 'Titan Striker Engineer',
            damage: [
              { formula: '2d8', type: DamageType.Arc }
            ],
            savingThrowFormula: [
              { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
            ]
          },
          {
            _type: 'Feature',
            level: 15,
            name: 'Terminal Velocity',
            description: `If you are airborne when you cast Tectonic Fist, and there is a hard surface no more than 60 feet directly beneath you, you can use your action to immediately fall to that hard surface and enact your Tectonic Fist. You do not take falling damage when you do this. Add 1d8 damage to your Tectonic Fist for every 20 feet you fall when you do this.`,
            source: 'Titan Striker Engineer',
            damage: [
              { formula: '1d8', type: DamageType.Arc }
            ]
          },
          {
            _type: 'Feature',
            level: 15,
            name: 'Controlled Demolition',
            description: `When you cast Tectonic Fist, only creatures of your choice are affected by it.`,
            source: 'Titan Stiker Engineer'
          }
        ]
      }
    ]
  }
  static Gladiator: SubclassPath = {
    id: 'gladiator',
    name: 'Gladiator',
    super: SuperAbility.Thundercrash,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Gladiator',
        description: `You are fully committed to the belief that a bigger punch is a better punch, and train accordingly to embody the singular, earth-shattering blow of an overwhelming lightning bolt. You hone your Light to perform the Thundercrash, literally making yourself into that bolt of lightning, to hurl in flight over and beyond any obstacles between your arc-charged fists and the spot you wish to strike.`,
        source: 'Titan Striker Gladiator'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Hammerblows',
        description: `At 3rd level, you really know how to knock someone down. Your unarmed strikes and Storm Fist attacks have their critical hit range increased by 1, and the recharge value of your Storm Fist is reduced by one.`,
        source: 'Titan Striker Gladiator'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Ballistic Slam',
        description: `When you reach 6th level, it occurs to you to let gravity do the work of setting up a big attack. If you are airborne you can use your action to cast Storm Fist to hurl yourself at an unoccupied space you can see. The space must exist on a hard surface, and it must be within a range equal to your airborne height. For example, if you are 10 feet off the ground, the space must be within 10 feet of you.

You instantly move to the unoccupied space, spending half a foot of movement per 1 foot you move in this way. You suffer no falling damage for doing this, and you do not land Prone. Upon impact, all other creatures within 5 feet of you must make a Dexterity saving throw against your Light save DC. They take the damage of your Storm Fist as explosive arc damage on a failed save, or half as much on a success. You spend a melee ability charge when you do this.
        
If you run out of movement before you make impact with the surface, your Light pitters out harmlessly.`,
        source: 'Titan Striker Gladiator',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
        ]
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Flexible Might',
        description: `At 11th level, you roll with the punches so well that they often roll right off you. When you are subjected to a Dexterity saving throw to prevent yourself from taking damage, you take no damage if you succeed on your saving throw, and you only take half damage if you fail.`,
        source: 'Titan Striker Gladiator'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Impact Conversion',
        description: `Starting at 15th level, your training enables a pathway for reuptake of some Light you expend through your advanced Light techniques. If you reduce a hostile creature to 0 hit points with the damage of your Thundercrash, you can regain a spent melee ability charge. For each creature you reduce to 0 hit points with the damage of your Storm Fist (including Ballistic Slam), you can make a super ability recharge roll with advantage.`,
        source: 'Titan Striker Gladiator'
      }
    ]
  }

  id = 'striker'
  class = Class.Titan
  element = LightElement.Arc
  classAbility = ClassAbility.Barricade
  name = 'Titan Striker'
  shieldDie = 8
  perks = [
    ...TitanClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Striker',
      description: `As a Striker, you gain the following class features in addition to the features granted by the Titan superclass.
### Health Points and Shields
- **Shield Die Size**: d8
- **Health Points at 1st level**: 8 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 8
- **Shield Points at higher levels**: 1d8 (or 5) for every level after 1st
### Proficiencies
- **Armor**: All armor
- **Weapons**: All weapons and firearms
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Strength, Constitution
- **Skills (Choose 2)**: Acrobatics, Animal Handling, History, Insight, Intimidation, Medicine, Perception, and Religion`,
      skillOptions: [{ choice: 2, skills: [Skill.Acrobatics, Skill.AnimalHandling, Skill.History, Skill.Insight, Skill.Intimidation, Skill.Medicine, Skill.Perception, Skill.Religion] }],
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Brawler',
      description: `You know to rely on your fists more than any other weapon. This mindset has granted you the following benefits:
- You can use your Strength or Dexterity modifier to determine the attack and damage rolls of your unarmed strikes. You must use the same modifier for both rolls.
- On your turn, if you take the Attack action, you can use your bonus action to make an unarmed strike.
- As a bonus action on your turn, you can move up to your speed toward a hostile creature you can see or hear. You must end this movement within 5 feet of the creature in order to do this.
- You are considered proficient with improvised weapons.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Lightning Reflexes',
      description: `You have an uncanny sense of your surroundings and know just when to react. You have advantage on Dexterity saving throws against effects you can see, such as when you can see a walker firing its main cannon at you, or when you can see a grenade about to detonate.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Light Affinity',
      description: `Beginning at 2nd level, you have learned to harness the Light you possess, and you shape it in the form of arc Light. Constitution is your Light ability score, and your Light Ability modifier is your Constitution modifier. You use your Constitution modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Constitution saving throw.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Melee Ability',
      description: `At 2nd level, your Light gives you the ability to empower your unarmed strikes. You acquire a new Light ability, the Storm Fist, which you cast using melee ability charges. You have one melee ability charge.`,
      source: 'Titan Striker',
      abilityOptions: [
        MeleeAbility.StormFist
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Titan Striker',
      abilityOptions: [
        GrenadeAbility.Flashbang,
        GrenadeAbility.Lightning,
        GrenadeAbility.Pulse
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Reckless Engagement',
      description: `When you reach 2nd level, you become confident in your own might, and remain aggressively selfassured in challenging combat situations. At the start of your turn you can choose to throw all caution to the wind and begin a Reckless Engagement. Doing so gives you advantage on melee weapon attack rolls, Close-range firearm attack rolls, and on Light attack rolls until the end of your turn, but all attack rolls against you have advantage until the start of your next turn.

In addition, your Light has adapted to keep you on your feet. Once on your turn, if you kill a hostile creature while under the effects of Reckless Engagement, you can make a shield recharge roll.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Striker Training',
      description: `At 3rd level, you choose a training discipline that shapes your abilities and reflects the role you prefer in combat. Choose the training of the Juggernaut, the Engineer, or the Gladiator, detailed at the end of this class description. Your choice grants you your super ability option, as well as features at 3rd, 6th, 11th, and 15th levels.`,
      source: 'Titan Striker',
      pathOptions: [
        Striker.Juggernaut,
        Striker.Engineer,
        Striker.Gladiator
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Spring in Your Step',
      description: `By the time you reach 7th level, you have grown used to striding swiftly to make sure you are always first to the fight. Your base walking speed increases by 10 feet.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 10,
      name: 'Enduring',
      description: `Starting at 10th level, you can push yourself to new limits, keeping yourself fighting despite grievous wounds. If you drop to 0 hit points and don't immediately lose your Light, you can make a DC 10 Constitution saving throw. If you succeed, you drop to 1 hit point instead.`,
      source: 'Titan Striker',
      savingThrowFormula: [
        { dc: '10', ability: Ability.Constitution }
      ]
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 14,
      name: 'Fight Response',
      description: `At 14th level, you're always ready for a fight. You have advantage on initiative rolls. In addition, if you are surprised at the start of combat and aren't Incapacitated, you can act normally on your first turn, but only if you cast a Striker class super ability before moving or taking any other action or bonus action on that turn.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 18,
      name: 'Strongest at Your Weakest',
      description: `By 18th level, your resolve allows you to strike back even harder after you take a devastating blow, rather than be knocked back on your heels. If a hostile creature reduces your energy shield points to 0, you can choose to grant yourself either a bonus to hit on the first attack roll you make before the end of your next turn, or a bonus to the DC of the first saving throw you cause a creature to make before the end of your next turn. The bonus for either of these is equal to your Constitution modifier.

You cannot invoke this feature again until you regain at least one energy shield point.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Striker'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Bring the Thunder',
      description: `At 20th level, your upwelling Light surges at the ready as easily as you flex your fist. When you roll initiative, if you don't have your super ability charge remaining, you can choose to regain your super ability charge. Once you use this feature, you must complete a long rest before you can use it again.`,
      source: 'Titan Striker'
    }
  ]
  abilityProficiencies = [
    Ability.Strength,
    Ability.Constitution
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor,
    ArmorItemType.HeavyArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    WeaponItemType.MartialWeapon,
    WeaponItemType.PrimaryWeapon,
    WeaponItemType.HeavyWeapon
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle
  ]
  lightModifier = Ability.Constitution

  getPath (id: string|null) {
    switch (id) {
      case Striker.Juggernaut.id: return Striker.Juggernaut
      case Striker.Engineer.id: return Striker.Engineer
      case Striker.Gladiator.id: return Striker.Gladiator
      default: return null
    }
  }
}

export class Defender implements Subclass {
  static Light: SubclassPath = {
    id: 'light',
    name: 'Light',
    super: SuperAbility.WardOfDawn,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Calling of the Light',
        description: `Those Defender Titans who would directly protect the weak and downtrodden, and keep their allies safe in battle often answer the Calling of the Light. Whether leading at the front line or serving as an indomitable backstop, Defenders of this calling stand as unflappable bulwarks against the Darkness.`,
        source: 'Titan Defender Light'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Unbreakable',
        description: `At 3rd level, you wear protective Light like hardened bunker armor. While you have an overshield you have resistances to bludgeoning, kinetic, piercing, and slashing damage.`,
        source: 'Titan Defender Light'
      },
      {
        _type: 'Feature',
        level: 7,
        name: 'Threat Management',
        description: `At 7th level, as an action you can attempt to draw an enemy's attention toward yourself, and away from your allies. Choose one creature you can see and who can clearly hear you. That creature must make a Charisma saving throw against your Light save DC. If it fails, it has disadvantage on any attacks it makes that are not directed at you until the start of your next turn and your Light wells up to form a protective skin, granting you an overshield that lasts for the next minute.`,
        source: 'Titan Defender Light',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Charisma }
        ]
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Illuminated',
        description: `When you reach 11th level your study and practice of casting Light into rigid structure enables you to strengthen your Ward of Dawn. Creatures who receive an overshield via your Ward of Dawn become Empowered (Stage 2) for the next minute. Beginning at 17th level, creatures become Empowered (Stage 3) instead.`,
        source: 'Titan Defender Light'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Relentless',
        description: `By the time you reach 15th level, you are unflagging and tireless in your defense of your allies, and staunch in the face of your own demise. If you are reduced to 0 hit points, you can choose to drop to 1 health point instead. You must complete a long rest before you can invoke this aspect of Relentless again.

Additionally, if you take damage from a creature within 5 feet of you, you can use your reaction to make a weapon attack against that creature.`,
        source: 'Titan Defender Light'
      }
    ]
  }
  static Shield: SubclassPath = {
    id: 'shield',
    name: 'Shield',
    super: SuperAbility.BannerShield,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Calling of the Shield',
        description: `If you yearn to lead your allies in the charge, protecting and empowering them as you surge forward together, then you have heard the Calling of the Shield. Your place is at the fore of the fighting, side by side with your staunch friends, where you can be their shield wall.`,
        source: 'Titan Defender Shield'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Void Detonators',
        description: `Beginning at 3rd level, you learn to arm yourself with explosive Light, which you channel into your core Light abilities. You have 3 void detonators. If a target takes void damage from your melee ability, grenade ability, or super ability, you can choose to attach one detonator to the target. For the next minute, if the target takes any further damage you can choose to cause all of your detonators on the target to explode. The target takes 1d8 explosive void damage per detonator.

The damage of your detonators increases as you gain levels in this class: to 2d8 at 5th level, 3d8 at 11th level, and 4d8 at 17th level. You regain all spent void detonators when you complete a brief rest.`,
        source: 'Titan Defender Shield',
        damage: [
          { formula: '(1 + (([level] + 1) / 6))d8', type: DamageType.ExplosiveVoid }
        ]
      },
      {
        _type: 'Feature',
        level: 7,
        name: 'Protection Of My Body',
        description: `When you reach 7th level, you are keenly aware your allies depend on your strength as a frontline Defender to stand with you in the thick of battle, and you throw your all into keeping them safe. When you invoke your Selfless feature, the attack must beat your AC instead of the target's AC, if your AC is higher than the target's.`,
        source: 'Titan Defender Shield'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Explosive Resupply',
        description: `When you reach 11th level, you refine the design of your void detonators to replenish your own Light when they go off. If a creature is reduced to 0 hit points either from the damage of the void detonator or while a void detonator is attached to it, you and all Risen creatures of your choice within 15 feet of you can make both a shield recharge roll and a grenade recharge roll.`,
        source: 'Titan Defender Shield'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Rallying Force',
        description: `At 15th level, you have refined your Banner Shield to the point of reclaiming the energy of outgoing attacks. If a creature Empowered by your Banner Shield kills a creature hostile to you, you and all other creatures of your choice within 15 feet of you can make a shield recharge roll. Once you use this feature, you must wait until the start of your next turn before you can use it again.`,
        source: 'Titan Defender Shield'
      }
    ]
  }
  static Sentinel: SubclassPath = {
    id: 'sentinel',
    name: 'Sentinel',
    super: SuperAbility.SentinelShield,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Calling of the Sentinel',
        description: `Those who heed the Calling of the Sentinel protect their allies by getting ahead of threats, and putting an end to the danger themselves. Your shield offers defense, yes, but in your hands it is also a devastating weapon. Anything that comes between your comrades and their objective will be pummeled, crushed, and broken upon your shield's edge.`,
        source: 'Titan Defender Sentinel'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'War Machine',
        description: `At 3rd level, you can reinforce your attacks with your Light. When you make a weapon attack on your turn, you can add a bonus to the damage roll equal to your Charisma modifier.`,
        source: 'Titan Defender Sentinel'
      },
      {
        _type: 'Feature',
        level: 7,
        name: 'Full Stop',
        description: `Starting at 7th level, when you land a hit on an unwary adversary, it brings them to a standstill. If you hit a creature with an attack of opportunity, you halt the creature in place. The creature's speed becomes 0 until the end of its turn.`,
        source: 'Titan Defender Sentinel'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Combat Superiority',
        description: `At 11th level, your fighting experience has grown quite extensive. Choose one of the following features.`,
        source: 'Titan Defender Sentinel',
        featureOptions: [
          {
            _type: 'Feature',
            level: 11,
            name: 'Advanced Training',
            description: `You can choose an additional Combat Specialty.`,
            source: 'Titan Defender Sentinel',
            charges: {
              type: ChargeType.Subclass,
              value: 1
            }
          },
          {
            _type: 'Feature',
            level: 11,
            name: 'In the Trenches',
            description: `If you start your turn with two or more hostile creatures within 15 feet of you, you have advantage on super recharge rolls you make until the end of your next turn.`,
            source: 'Titan Defender Sentinel'
          },
          {
            _type: 'Feature',
            level: 11,
            name: 'Superior Arsenal',
            description: `If you reduce a hostile creature to 0 hit points with the damage from your Light grenade, you can make a grenade recharge roll.`,
            source: 'Titan Defender Sentinel'
          }
        ]
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Sentinel\'s Strength',
        description: `At 15th level, your attacks are made with great aptitude and verve, and hit especially hard more often than would typically be expected. The critical hit range of all attack rolls you make is increased by 1.`,
        source: 'Titan Defender Sentinel'
      }
    ]
  }

  id = 'defender'
  class = Class.Titan
  element = LightElement.Void
  classAbility = ClassAbility.Barricade
  name = 'Titan Defender'
  shieldDie = 12
  perks = [
    ...TitanClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Defender',
      description: `As a Defender, you gain the following class features in addition to the features granted by the Titan superclass.      
### Health Points and Shields
- **Shield Die Size**: d12
- **Health Points at 1st level**: 12 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 12
- **Shield Points at higher levels**: 1d12 (or 7) for every level after 1st
### Proficiencies
- **Armor**: All armor
- **Weapons**: All weapons and firearms
- **Toolkit**: Medical toolkit
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Strength, Constitution
- **Skills (Choose 2)**: Arcana, History, Insight, Medicine, Perception, Persuasion, Religion, and Technology`,
      skillOptions: [{ choice: 2, skills: [Skill.Arcana, Skill.History, Skill.Insight, Skill.Medicine, Skill.Perception, Skill.Persuasion, Skill.Religion, Skill.Technology] }],
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Selfless',
      description: `You instinctively put others' safety before your own, and will do whatever is necessary to keep your allies from harm. When a creature you can see makes an attack roll against a target other than yourself that is within 5 feet of you, you may use your reaction to impose disadvantage on the attack roll. If the attack still hits, you can choose to take the damage of the attack instead.

If the target you want to defend is not within 5 feet of you, but is within your movement, you can still invoke this feature if, as part of your reaction, you move up to your movement toward the target. This reduces your movement on your next turn by the same amount. You can only do this if the movement would put you within 5 feet of the target, only if you are not Restrained, and only if you would still have at least 5 feet of movement for your next turn.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Defender\'s Voice',
      description: `Even at 1st level, your voice carries a certain authoritative weight, making those willing to listen predisposed to heed you. You have advantage on Charisma (Persuasion) checks you make when speaking to creatures that are not hostile to you.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Light Affinity',
      description: `Beginning at 2nd level, you know how to channel the Light you possess, and you shape it in the form of void Light. Charisma is your Light ability score, and your Light Ability modifier is your Charisma modifier. You use your Charisma modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Charisma saving throw.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Melee Ability',
      description: `Starting at 2nd level, you learn to strike out with your void Light. You gain a new Light ability, Disintegrate, which you cast using your melee ability charge. You have one melee ability charge. You regain spent melee ability charges when you complete a brief rest.`,
      source: 'Titan Defender',
      abilityOptions: [
        MeleeAbility.Disintegrate
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Titan Defender',
      abilityOptions: [
        GrenadeAbility.Magnetic,
        GrenadeAbility.Spike,
        GrenadeAbility.Suppressor
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Combat Specialty',
      description: `At 2nd level, you have developed a fighting specialization with a distinct style. Choose one of the following options. You can't select a Combat Specialty option more than once, even if you later get to choose again.`,
      source: 'Titan Defender',
      abilities: [
        OtherAbility.TitanDefense,
        OtherAbility.TitanDualWielder,
        OtherAbility.TitanGeneralist,
        OtherAbility.TitanHeavyWeaponsExpert,
        OtherAbility.TitanLoadedForBear,
        OtherAbility.TitanMasterAtArms
      ]
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Defender\'s Calling',
      description: `Upon reaching 3rd level, you begin to follow your specific calling as a Defender. Choose from the Calling of the Light, Calling of the Shield, or Calling of the Sentinel. This choice grants you features at 3rd level, and again at 7th, 11th, and 15th levels.`,
      source: 'Titan Defender',
      pathOptions: [
        Defender.Light,
        Defender.Shield,
        Defender.Sentinel
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 6,
      name: 'Protective Light',
      description: `Starting at 6th level, whenever you or another creature of your choice within 15 feet of you must make a saving throw, you can extend a defensive envelope of your Light to grant a bonus to the saving throw equal to your Charisma modifier (minimum +1 bonus). You must be conscious to grant this bonus.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 10,
      name: 'Radiant Light',
      description: `Starting at 10th level, you and nearby allies take comfort and strength from the reassuring radiance of the Light emanating from you. You and any other creatures of your choice within 15 feet of you can't be Charmed or Frightened. You must be conscious to grant the bonuses of this feature. If a creature is Charmed or Frightened when they start their turn within your Radiant Light, the effect ends early for them.`,
      source: 'Titan Defender',
      condition: ['Charmed', 'Frightened']
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 14,
      name: 'Bastion',
      description: `At 14th level, your protection of your allies is solid as a plasteel buttress. When you invoke your Selfless Defense feature and take the damage yourself, you halve the amount of damage the attack does.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Titan Defender'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Champion of Light',
      description: `When you reach 20th level, your Light is unshakeable, like the roots of the Last City's mighty walls. If you fail a concentration check, you can choose to succeed instead. You must complete a brief rest before you can invoke this feature again.`,
      source: 'Titan Defender'
    }
  ]
  charges: SubclassCharge = {
    name: 'Combat Specialties',
    type: 'feature',
    values: {
      2: 1,
      3: 1,
      4: 1,
      5: 1,
      6: 1,
      7: 1,
      8: 1,
      9: 1,
      10: 1,
      11: 1,
      12: 1,
      13: 1,
      14: 1,
      15: 1,
      16: 1,
      17: 1,
      18: 1,
      19: 1,
      20: 1
    }
  }
  abilityProficiencies = [
    Ability.Strength,
    Ability.Constitution
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor,
    ArmorItemType.HeavyArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    WeaponItemType.MartialWeapon,
    WeaponItemType.PrimaryWeapon,
    WeaponItemType.HeavyWeapon
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle,
    ToolItemType.MedicalTools
  ]
  lightModifier = Ability.Charisma

  getPath (id: string|null) {
    switch (id) {
      case Defender.Light.id: return Defender.Light
      case Defender.Sentinel.id: return Defender.Sentinel
      case Defender.Shield.id: return Defender.Shield
      default: return null
    }
  }
}

export class Sunsinger implements Subclass {
  static Flame: SubclassPath = {
    id: 'flame',
    name: 'Flame',
    super: SuperAbility.Radiance,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Ballad of Flame',
        description: `All Guardians cross back and forth over the
        threshold between life and death on a regular basis,
        but Sunsingers who choose the Ballad of Flame
        recognize a deeper harmony in the crossing, and
        thus find ways to glean knowledge from beyond
        death. The most powerful are said to be capable of
        returning from the other side by the sheer strength
        of their own will.`,
        source: 'Warlock Sunsinger Flame'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Behond the Veil',
        description: `The constant refrain of the Ballad of Flame is illumination of the dark beyond living consciousness. During deep sleep or when returning from death, Sunsingers of this ballad often receive Lightmediated visions of critical moments yet to come. Learning to recognize and remember what these portents may foretell is a key aspect of their practice.

At 2nd level you gain two Visions, each of which is the result of a hypothetical d20 roll. Whenever you complete a long rest, or whenever you are resurrected from death, roll on the table below to learn what your Visions are. From then on, once on a turn, if a creature you are aware of makes an attack roll, saving throw, or ability check, you can choose to replace their roll of the d20 with one of your Visions. If you do, the creature treats their roll of the d20 as equal to your Vision. Critical success and critical failure is determined as normal, and the creature adds any relevant modifiers to the Vision.

| d100   | Vision                 |
| :----: | :--------------------- |
| 1-16   | Two natural 1s         |
| 17-33  | One natural 1, one 5   |
| 34-50  | One natural 1, one 15  |
| 51-67  | One natural 20, one 15 |
| 68-84  | One natural 20, one 5  |
| 85-100 | Two natural 20s        |

If the creature would normally have advantage or disadvantage when you choose to replace their roll, they no longer do. 

Each of your Visions can only be used once. You can only replace a roll with a Vision if you choose to do so before you learn the result of the creature's attack roll, saving throw, or ability check. If you complete a long rest with unspent Visions, or if you are killed with unspent Visions, you lose those unspent Visions.`,
        source: 'Warlock Sunsinger Flame'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Flame Shield',
        description: `When you reach 6th level, you develop a way to channel some of your restorative Light into a protective resonance. If you deal damage to a hostile creature with Scorch, you can grant yourself an overshield which lasts for up to 1 minute.`,
        source: 'Warlock Sunsinger Flame'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Radiant Will',
        description: `At 11th level, the blaze of your radiant Light burns even brighter when you unleash it. Choose one of the following features to bolster your Radiance.

**Song of Flame.** For the duration of your Radiance, all Risen creatures of your choice who are within 15 feet of you make grenade ability recharge rolls with advantage. Beginning at 18th level, the radius of this effect increases to 40 feet.

**Radiant Skin.** When you cast Radiance, you also gain an overshield that lasts for the next minute. While concentrating on Radiance, you have advantage on melee recharge rolls.

**Searing Effulgence.** You become Empowered (Stage 2) while concentrating on Radiance. Beginning at 17th level, you become Empowered (Stage 3) instead.`,
        source: 'Warlock Sunsinger Flame'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Rising Sun',
        description: `When you reach 18th level, you can call on your Light even after death. On your turn, if you have 0 health points and have the option to make a death saving throw, you can choose to use your action to cast Radiance instead, returning to life in a blaze of solar Light. You return with your hit points completely restored and begin the duration of your Radiance super ability. You can only use this feature if you have a super ability charge remaining, and you spend a super ability charge when you use this feature.

You must complete a long rest before you can invoke this feature again.`,
        source: 'Warlock Sunsinger Flame'
      }
    ]
  }
  static Grace: SubclassPath = {
    id: 'grace',
    name: 'Grace',
    super: SuperAbility.Radiance,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Ballad of Grace',
        description: `In the Ballad of the Grace, the strongest chords are those of family, camaraderie, and unity. Sunsingers of this ballad practice harmony with these chords, strengthening themselves by sharing their Light to bolster allies.`,
        source: 'Warlock Sunsinger Grace'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Solar Prominence',
        description: `At 2nd level, the warm strength of your Light is a reassurance to those around you. Sunsinger grenades you are attuned to have their recharge value reduced by one. In addition, when you use your Divine Blessing, you can spend a grenade ability charge in order to also grant the creature an amount of overshield points equal to your maximum for overshields. These overshields last for the next minute. Beginning at 9th level, the range of this feature increases to 60 feet.`,
        source: 'Warlock Sunsinger Grace'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Guiding Flame',
        description: `At 6th level, a new verse of your inner song enables you to project your Light to all those close around you. As either an action or bonus action on your turn, you can spend a melee ability charge to grant all creatures of your choice within 15 feet of you the Empowered (Stage 1) condition, which lasts until the start of your next turn. At 11th Sunsinger level you grant Empowered (Stage 2) instead, and at 17th Sunsinger level you grant Empowered (Stage 3).`,
        source: 'Warlock Sunsinger Grace'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Grace of the Sun',
        description: `At 11th level, the Light you share with allies flows back to you and restores your own strength. Once, at the end of your turn, you can make a melee, grenade, and superclass ability recharge roll if a creature other than yourself is Empowered from one of your Sunsinger features.`,
        source: 'Warlock Sunsinger Grace'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Transcendence',
        description: `At 18th level, the Light of your Well of Radiance has grown to a veritable geyser of restorative power. The radius of your Well of Radiance increases to 15 feet. If a Risen creature makes a shield recharge roll while within the area of your Well of Radiance, the amount they roll can be applied to their health points first, with any remaining going into their shield points. If there is still a remainder, the amount can go into the creature's overshield points.`,
        source: 'Warlock Sunsinger Grace'
      }
    ]
  }
  static Dawn: SubclassPath = {
    id: 'dawn',
    name: 'Dawn',
    super: SuperAbility.Dawnblade,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Ballad of Dawn',
        description: `Sunlight can be harsh as well as nurturing. Sunsingers who choose the Ballad of Dawn embody this aspect of the Sun. Singing a song of righteous retribution, they take to the skies to scourge the darkness with fiery swords.`,
        source: 'Warlock Sunsinger Dawn'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Feedback',
        description: `At 2nd level, you learn to overcharge the Light of your hand-to-hand strikes, imbuing them with searing heat. The damage die size of Scorch is increased by one, your Scorch deals explosive solar damage, and you can cast Scorch to a range of 30 feet.

You can also move vertically while Warlock gliding, and while Warlock gliding, you do not have disadvantage on weapon attack rolls due to being airborne, and Aiming costs you no movement.

Finally, if you reduce a hostile creature to 0 hit points while airborne, you can make a melee ability recharge roll.`,
        source: 'Warlock Sunsinger Dawn'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Bonus Proficiencies',
        description: `When you choose this ballad at 2nd level, you also become proficient with all martial weapons and martial firearms except rocket launchers.
        In addition, you can always use your Charisma modifier when determining a weapon's attack rolls, damage rolls, and weapon DC if you are proficient with the weapon. If the weapon has the Payload property, you can use your Charisma modifier to determine its payload DC.`,
        source: 'Warlock Sunsinger Dawn'
        //TODO: Add proficiencies written above
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Heat Rises',
        description: `Beginning at 6th level, you learn to rechannel the explosive power of your concentrated Light to give yourself a burst of lift. You can spend a grenade ability charge to grant yourself a flying speed equal to your base walking speed until the end of your next turn. While you have this flying speed, you have advantage on attack rolls you make while airborne.`,
        source: 'Warlock Sunsinger Dawn'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Icarus Dash',
        description: `At 11th level, your practice at airborne maneuvers has made you quite nimble. If you are airborne when you are subjected to a Dexterity saving throw to prevent yourself from taking damage, you can use your reaction to automatically succeed on the saving throw.`,
        source: 'Warlock Sunsinger Dawn'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Glorious Dawn',
        description: `At 18th level you have mastered the sword of Light and honed its edge to impossible brilliance. When you draw your Dawnblade, you draw forth the very sunrise. While concentrating on Dawnblade, you have the following benefits:

- You cannot lose concentration on your Dawnblade as a result of taking damage. 
- You shine bright light in a 30 foot radius, and dim light 60 feet beyond.
- Targets within the bright light of your Dawnblade have disadvantage on their saving throws against your Blade Salvo.
- Reducing a hostile creature to 0 hit points with the damage of your Dawnblade allows you to make a shield recharge roll. The amount you roll can be applied as health first, with any remainder going into energy shields.`,
        source: 'Warlock Sunsinger Dawn'
      }
    ]
  }

  id = 'sunsinger'
  class = Class.Warlock
  element = LightElement.Solar
  classAbility = ClassAbility.RiftOfLight
  name = 'Warlock Sunsinger'
  shieldDie = 8
  perks = [
    ...WarlockClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Sunsinger',
      description: `As a Stormcaller, you gain the following class features in addition to the features granted by the Warlock superclass.
### Health Points and Shields
- **Shield Die Size**: d8
- **Health Points at 1st level**: 8 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 8
- **Shield Points at higher levels**: Add 1d8 (or 5) for every level after 1st
### Proficiencies
- **Armor**: All armor
- **Weapons**: Simple melee weapons, longswords, shortswords, smallswords, warhammers, combat bows, simple firearms, fusion rifles
- **Toolkit**: Medical toolkit
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Constitution, Charisma
- **Skills (Choose 2)**: Acrobatics, Athletics, Insight, Intimidation, Medicine, Perception, Performance, Persuasion, and Religion`,
      skillOptions: [{ choice: 2, skills: [Skill.Acrobatics, Skill.Athletics, Skill.Insight, Skill.Intimidation, Skill.Medicine, Skill.Perception, Skill.Performance, Skill.Persuasion, Skill.Religion] }],
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Light Affinity',
      description: `You know how to harness the Light you possess, and you shape it in the form of solar Light. Charisma is your Light ability score, and your Charisma modifier is your Light ability modifier. You use your Charisma modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Charisma saving throw.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Divine Blessing',
      description: `Your abundant Light is a protective balm you can share with your allies. As a bonus action on your turn you can choose one creature you can see within 15 feet of you and grant them the blessing of your Light. The creature recovers 2d4 shield points.

When you invoke this feature at a Light level of 2nd or higher, the amount the creature recovers increases by 2d4 for every Light level above 1st.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Melee Ability',
      description: `Even at 1st level, your Light is strong enough, and your attunement to it clear enough that you are able to use a new Light ability, Scorch. You cast this Light ability by spending a melee ability charge. You have a single melee ability charge, which you regain when you complete a brief rest.`,
      source: 'Warlock Sunsinger',
      abilityOptions: [
        MeleeAbility.Scorch
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Warlock Sunsinger',
      abilityOptions: [
        GrenadeAbility.Firebolt,
        GrenadeAbility.Fusion,
        GrenadeAbility.Solar
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Detect Light and Dark',
      description: `At 2nd level, your refined sensitivity to the ebb and flow of the Light keeps you cognizant of subtle changes in the space around you. As an action, you can take one or two deep breaths, centering yourself and opening your awareness fully to the paracausal forces around you. For the next 10 minutes, you can determine what active modifiers there are in the area, if there are any weapons within 60 feet of you that can deal Light or Darkness damage, and you can sense if there are any creatures of the Light or the Darkness within the same range. You learn the creature's level (or individual CR) when you detect them in this way.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Sunsinger Ballad',
      description: `At 2nd level you choose the form of your Sunsinger ballad, which will shape the progression of your abilities. Choose from the Ballad of Flame, the Ballad of Grace, or the Ballad of Dawn, all detailed at the end of this class description. Your choice grants you a feature at 2nd level, and additional features at 3rd, 6th, 11th, and 18th level.`,
      source: 'Warlock Sunsinger',
      pathOptions: [
        Sunsinger.Flame,
        Sunsinger.Grace,
        Sunsinger.Dawn
      ]
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Patron of the Arts',
      description: `At 3rd level, choose an artisan toolset, or choose one of the following skills: Acrobatics, Animal Handling, Athletics, Performance, or Sleight of Hand. You gain proficiency with your chosen toolset or skill, if you weren't proficient already, and you can double your proficiency bonus when making ability checks with that toolset or skill if it doesn't already benefit from this bonus.`,
      skillOptions: [{ choice: 1, skills: [Skill.Acrobatics, Skill.AnimalHandling, Skill.Athletics, Skill.Performance, Skill.SleightOfHand] }],
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Blessed Repose',
      description: `When you reach 5th level, your restorative Light is so abundant that you regain shield points when you heal others. Whenever you use your Divine Blessing on a creature other than yourself, you also regain a number of shield points equal to 5 + your Light level.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Righteous Surge',
      description: `At 7th level, you learn to imbue your attacks with a pulse of Light, magnifying their impact. Once on each of your turns when you hit a creature with a weapon attack, you can deal an additional 1d8 damage of the weapon's type to the creature. When you reach 14th level in this class, this bonus increases to 2d8.`,
      source: 'Warlock Sunsinger',
      damage: [
        { formula: '([level] / 7)d8', type: DamageType.Kinetic }
      ]
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 9,
      name: 'Blessed Presence',
      description: `At 9th level, you no longer have to be close to a creature to grant them the benefit of your Divine Blessing. You can use your Divine Blessing up to a range of 40 feet.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 13,
      name: 'Touch of the Sun',
      description: `At 13th level, whenever a creature benefits from your Divine Blessing, you can choose to cause them to emit a warm glow. For the next minute they shed bright light in a 5-foot radius, and dim light 5 feet beyond that.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 15,
      name: 'Heliocentric',
      description: `At 15th level, the brilliant blaze of your Light grants you great inner clarity. You gain proficiency in Wisdom saving throws.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 17,
      name: 'Nobility of the Sun',
      description: `At 17th level, your noble nature suffuses your Light, and you can add your Charisma modifier to the healing a creature receives from Divine Blessing.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Sunsinger'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Boundless Energy',
      description: `By the time you reach 20th level, you are overflowing with restorative Light, and it streams forth like sunshine. When you use your Divine Blessing, you can allow any other creatures of your choice within range to heal the same amount. The healing creatures receive can be applied to health first, with any leftover going to energy shields. Once you use this feature, you must complete a short or long rest before you can use it again.`,
      source: 'Warlock Sunsinger'
    }
  ]
  abilityProficiencies = [
    Ability.Constitution,
    Ability.Charisma
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor,
    ArmorItemType.HeavyArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    MartialWeaponItemType.Longsword,
    MartialWeaponItemType.Shortsword,
    MartialWeaponItemType.Smallsword,
    MartialWeaponItemType.Warhammer,
    MartialWeaponItemType.CombatBow,
    WeaponItemType.PrimaryWeapon,
    HeavyWeaponItemType.FusionRifle
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle,
    ToolItemType.MedicalTools
  ]
  lightModifier = Ability.Charisma

  getPath (id: string|null) {
    switch (id) {
      case Sunsinger.Dawn.id: return Sunsinger.Dawn
      case Sunsinger.Flame.id: return Sunsinger.Flame
      case Sunsinger.Grace.id: return Sunsinger.Grace
      default: return null
    }
  }
}

export class Stormcaller implements Subclass {
  static Lightning: SubclassPath = {
    id: 'Lightning',
    name: 'Lightning',
    super: SuperAbility.Stormtrance,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Master of Lightning',
        description: `You embody the raw destructive power of lightning. From a distance your stormtrance is a beautiful, awe-inspiring spectacle, but anything in reach of the lightning surging through your fingers beholds only crackling terror. You channel the fury of your summoned storm from an epicenter of unswerving focus, guiding its wrath in a wide swath.`,
        source: 'Warlock Stormcaller Lightning'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Arc Web',
        description: `At 3rd level, all arc Light grenades you cast chain arc Light to other nearby creatures. After resolving the effect(s) of the grenade, choose a number of targets up to your Wisdom modifier to take 2d4 arc damage. This damage increases as your stormcaller level increases: it becomes 3d4 at 11th level and 4d4 at 17th level.

The targets you choose must be within 5 feet of any creature originally damaged by your arc Light grenade. You can choose targets that were originally damaged by the grenade, but you cannot choose the same target multiple times for this feature.`,
        source: 'Warlock Stormcaller Lightning',
        damage: [
          { formula: '(1 + (4 / [level]) + (([level] + 1) / 6))d4', type: DamageType.Arc }
        ]
      },
      {
        _type: 'Feature',
        level: 7,
        name: 'Evasion',
        description: `At 7th level, you have learned to emulate the quickness of the lightning you channel, heightening your reflexes. If you are subject to an effect that allows you to make a Dexterity saving throw to take only half damage from the effect, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.`,
        source: 'Warlock Stormcaller Lightning'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Ionic Blink',
        description: `When you reach 11th level, your mastery of the lightning has deepened, and the path of the Stormcaller widens to reveal new ways to channel the storm. While concentrating on your Stormtrance, you can use a bonus action to teleport to an unoccupied space you can see within 15 feet, taking all carried and worn equipment of your choice with you when you do this.`,
        source: 'Warlock Stormcaller Lightning'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Electrostatic Mind',
        description: `At 15th level, you have advantage on super ability recharge rolls you make when two or more allied creatures are within 15 feet of you.`,
        source: 'Warlock Stormcaller Lightning'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Supercell',
        description: `At 18th level, the arc storms you summon are superlatively energetic, wreathing you with an overflow of Light-laced arc. On each of your turns during your Stormtrance, you can choose to use a bonus action to let loose a barrage of arc Light, causing all creatures within 15 feet of you to take 10 arc damage. This counts as super ability damage for the purpose of determining the effects of your Destroy Creature superclass feature.`,
        source: 'Warlock Stormcaller Lightning'
      }
    ]
  }
  static Wind: SubclassPath = {
    id: 'wind',
    name: 'Wind',
    super: SuperAbility.ChaosReach,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Master of Wind',
        description: `You embody the shattering power of gale-force winds. Your stormtrance takes the form of a brief, overwhelmingly intense concentration of arc Light, as if all the wind and thunder of a howling hurricane followed the single line of your will. Whether pointedly directed or swept wide to wipe clean a whole battlefield, your Light is a mighty tempest.`,
        source: 'Warlock Stormcaller Wind'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Voltaic Residue',
        description: `At 3rd level, your mastery of the storm's winds has deepened, and your advancement along the path of the Stormcaller brings you to a new understanding of the current. Whenever you spend at least 2 arc charges on your turn, you gain the benefit of the Disengage action, and your movement increases by 10 feet until the end of your turn.`,
        source: 'Warlock Stormcaller Wind'
      },
      {
        _type: 'Feature',
        level: 7,
        name: 'Pulsewave',
        description: `At 7th level, you can invoke your Light with the quickness of stormwinds in moments of need. If you take an amount of damage that reduces you to half your shield points or less, but you still have at least 1 health point remaining, you can use your reaction to trigger a pulsewave of wind-heightened speed. All creatures of your choice within 30 feet of you can double their base walking speed on their next turn. You must regain at least 1 energy shield point before you can use this feature again.`,
        source: 'Warlock Stormcaller Wind'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Ionic Trace',
        description: `Starting at 11th level, your grasp on the flow of arc energies grows firm, enabling you to allow your lightning to return to you. If a creature is reduced to 0 hit points within 30 feet of you, you can regain a number of arc charges equal to your Wisdom modifier.`,
        source: 'Warlock Stormcaller Wind'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Ball Lightning',
        description: `When you reach 15th level, the subtleties of the storm are entirely within your grasp—including some of its more peculiar phenomena. When you cast Thunderstrike, instead of its normal effects you can hurl a sphere of arc Light with a 1-foot diameter along a 30-foot line in a direction of your choosing, originating from you. The sphere can pass through openings large enough for a Tiny object, can phase through creatures, and can pass through non-metal barriers up to half an inch thick. Every creature of your choice that the sphere passes through must make a Constitution saving throw against your Light save DC. They take the damage of your Thunderstrike on a failed save, or half as much on a success. If the sphere passes through a creature, they make this saving throw with disadvantage.`,
        source: 'Warlock Stormcaller Wind',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Transcendence',
        description: `Beginning at 18th level, summoning a storm is like remembering your true self. It feels restorative and invigorating, even in especially perilous moments. If you cast your super ability on your turn, you can make a shield recharge roll. The amount you roll can be applied as health points first, with any leftover going to energy shield points.

In addition, whenever you invoke your Pulsewave feature, you and all creatures affected by Pulsewave can make a shield recharge roll.`,
        source: 'Warlock Stormcaller Wind'
      }
    ]
  }
  static Thunder: SubclassPath = {
    id: 'thunder',
    name: 'Thunder',
    super: SuperAbility.Landfall,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Master of Thunder',
        description: `You embody the bone-shaking power of rolling thunder. Your stormtrance focuses the might of a tempest into a single, devastating thunderclap around you. Where you step, the thunder descends—and when you let fall those crackling bolts, even an army can't stand against you.`,
        source: 'Warlock Stormcaller Thunder'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Feedback',
        description: `At 3rd level, you learn to let your Light flow around those that surround you, like thunderbolts finding their path through the sky. If you start your turn with at least two creatures within 15 feet of you, you can immediately regain a melee ability charge, and your Thunderstrike gains an additional damage die until the end of your turn. In addition, if you start your turn with at least two hostile creatures within 15 feet of you, your AC increases by 2 until the start of your next turn.`,
        source: 'Warlock Stormcaller Thunder'
      },
      {
        _type: 'Feature',
        level: 7,
        name: 'Arc Soul',
        description: `At 7th level, you develop a technique to externalize a portion of your Light into a stable, independent construct to aid you and your allies. When you cast Rift of Light, all creatures of your choice in the area of your Rift of Light gain an arc soul. In addition, until your Rift of Light ends, if a creature starts their turn within your Rift of Light, or enters your Rift of Light for the first time on a turn, you can choose to grant them an arc soul.

**Arc Soul.** An arc soul is a tiny, glowing, blue orb of Light no bigger than a small coin. It hovers over a creature's shoulder and, once on the creature's turn, it will deal 4 arc damage to one target within 15 feet of the creature. The creature decides who the target is and when this damage occurs. After dealing damage, or at the end of the creature's turn, the arc soul dissipates.

The arc soul's damage increases as the caster of the Rift of Light's level increases. It increases to 5 at 10th level, 6 at 15th level, and 7 at 20th level.`,
        source: 'Warlock Stormcaller Thunder'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Ionic Surge',
        description: `At 11th level, you are adept at channeling and redirecting your Light when you extend it to strike. Each time an arc soul of yours applies damage to a creature that is hostile to you, you can make a melee and grenade recharge roll.`,
        source: 'Warlock Stormcaller Thunder'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Echoing Thunder',
        description: `Starting at 15th level, you are so attuned to your inner thunder that you can lash out with it like a natural reflex. If a creature you can see hits you with an attack and they are within the reach of your Thunderstrike, you can use your reaction to cast Thunderstrike against that creature.`,
        source: 'Warlock Stormcaller Thunder'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Thunderstruck',
        description: `At 18th level, you have mastered the thunder and extended its reach through you. When you cast Landfall, all creatures within 40 feet of you must also make a Constitution saving throw against your Light save DC. Creatures are Blinded and Deafened for 1 minute on a failed save. A creature that has been Blinded or Deafened can repeat the Constitution saving throw at the end of each of their turns, ending the effect early on a success. A creature who succeeds on their saving throw, or for whom the effect ends, becomes immune to being Blinded or Deafened by Landfall for 24 hours.`,
        source: 'Warlock Stormcaller Thunder',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      }
    ]
  }

  id = 'stormcaller'
  class = Class.Warlock
  element = LightElement.Arc
  classAbility = ClassAbility.RiftOfLight
  name = 'Warlock Stormcaller'
  shieldDie = 8
  perks = [
    ...WarlockClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Stormcaller',
      description: `As a Stormcaller, you gain the following class features in addition to the features granted by the Warlock superclass.
### Health Points and Shields
- **Shield Die Size**: d8
- **Health Points at 1st level**: 8 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 8
- **Shield Points at higher levels**: Add 1d8 (or 5) for every level after 1st
### Proficiencies
- **Armor**: Light armor, medium armor
- **Weapons**: Simple melee weapons, longswords, smallswords, shortswords, warhammers, combat bows, simple firearms, fusion rifles, grenade launchers, linear fusion rifles
- **Toolkit**: Electronics toolkit
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Dexterity, Wisdom
- **Skills (Choose 2)**: Acrobatics, Animal Handling, Insight, Nature, Perception, Persuasion, Religion, Technology`,
      skillOptions: [{ choice: 2, skills: [Skill.Acrobatics, Skill.AnimalHandling, Skill.Insight, Skill.Nature, Skill.Perception, Skill.Persuasion, Skill.Religion, Skill.Technology] }],
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Light Affinity',
      description: `Even at 1st level, you know how to harness the Light you possess, and you shape it in the form of arc Light. Wisdom is your Light ability score, and your Wisdom modifier is your Light ability modifier. You use your Wisdom modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Wisdom saving throw.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Arc Charges',
      description: `You are a natural conduit for arc Light, and you store that excess Light in the form of arc charges. Once on your turn, you can spend these charges to empower yourself through arc actions, your options for which are detailed at the end of this class description.
      
You cannot perform an arc action if you don't have enough arc charges, or if you don't meet the level prerequisite. Unless the arc action describes otherwise, you spend arc charges immediately when you perform the arc action.

You have a maximum number of charges based on your level in the Stormcaller class, as shown in the Max Arc Charges column of the Stormcaller table. At the start of each of your turns, you regain one arc charge. If you end your turn without having spent any arc charges, you regain one additional arc charge. You regain all arc charges when you complete a brief rest.

If you use an arc action that requires concentration, you regain no arc charges at the start of your turn while concentrating on it. You cannot begin concentration on an arc action if doing so would reduce you to 0 arc charges, and if you fall to 0 arc charges while concentrating on an arc action, you immediately lose concentration.`,
      source: 'Warlock Stormcaller',
      abilities: [
        OtherAbility.StormcallerAlternatingCurrent,
        OtherAbility.StormcallerBolt,
        OtherAbility.StormcallerDischarge,
        OtherAbility.StormcallerInduction,
        OtherAbility.StormcallerOvercharged,
        OtherAbility.StormcallerOverride,
        OtherAbility.StormcallerRecharge,
        OtherAbility.StormcallerRefresh,
        OtherAbility.StormcallerWindswift,
        OtherAbility.StormcallerZaplight
      ]
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Harmony Within',
      description: `You have a natural inner calm and peace of mind that keeps you centered and focused. Instead of sleeping, you can enter a deep meditation for 4 hours each day. While meditating, you may experience dreamlike visions or practice mental exercises. When you complete your meditation you gain the same benefit as a human does from 8 hours of sleep, and you have advantage on saving throws to end conditions causing you to be Blinded, Burning, Charmed, Deafened, Frightened, or Poisoned for the next 24 hours.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Melee Ability',
      description: `At 2nd level you gain your melee ability option, Thunderstrike. You cast this Light ability using a melee ability charge, of which you only have one. When you complete a brief rest, you can regain spent melee ability charges.`,
      source: 'Warlock Stormcaller',
      abilityOptions: [
        MeleeAbility.Thunderstrike
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Warlock Stormcaller',
      abilityOptions: [
        GrenadeAbility.Arcbolt,
        GrenadeAbility.Pulse,
        GrenadeAbility.Storm
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Living Arc',
      description: `Starting at 2nd level, your Light develops a sensitivity to the arc energy that animates living things. When you focus, you can perceive subtle fluctuations in the living arc within creatures and which naturally permeates spaces. As an action, you can hone in on these fluctuations, granting yourself advantage on Wisdom (Perception) checks to find living creatures or living machines for the next hour (requires concentration).`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Stormcaller Mastery',
      description: `At 3rd level, you must decide which fork of the Stormcaller's path you wish to walk. Choose Master of Lightning, Master of Wind, or Master of Thunder, which are detailed at the end of this class description. Your choice grants you your super ability at 3rd level, and additional features at 7th, 11th, 15th, and 18th level.`,
      source: 'Warlock Stormcaller',
      pathOptions: [
        Stormcaller.Lightning,
        Stormcaller.Wind,
        Stormcaller.Thunder
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 6,
      name: 'Thunderstrike Attunement',
      description: `At 6th level, you learn one of the following ways to modify your Light abilities. Choose one of the following features to gain. You can alter your choice by meditating on this feature exclusively for at least 1 hour (this meditation can be done during a short or long rest). If you complete your meditation, you lose the effects of your previous choice and gain the effects of your new choice.

### Amplitude
The reach of your Thunderstrike increases to 10 feet, and it is considered as a melee weapon for the purpose of determining if you can make an attack of opportunity with it. The damage die size of Thunderstrike increases by one.

### Chain Lightning
Once on your turn, if the target of your Thunderstrike fails their saving throw against it, you can choose one additional target within 15 feet of your original target to strike with a chain-bolt of arc Light. This additional target must also make a saving throw against your Thunderstrike.

### Rising Storm
When you use your action to cast Thunderstrike, you can cause the target to make a Constitution saving throw, instead of a Dexterity saving throw, against Thunderstrike. If they fail their saving throw, the target takes the damage of your Thunderstrike and is Paralyzed for the next minute. The target can repeat the saving throw at the end of each of their turns, ending the effect early on a success. If they succeed, they take half the damage of your Thunderstrike and are not Paralyzed.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 10,
      name: 'Galvanism',
      description: `Starting at 10th level, the amplitude of your internally circulating arc Light surges in protective resonance at moments of danger. When you fail a saving throw, you can spend 2 arc charges to re-roll that saving throw once. You can choose to use either result.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 11,
      name: 'Perpetual Charge',
      description: `At 11th level, the lightning residing within you has grown abundant and comes to you quickly and readily. Your Bolt arc action costs no arc charges to use, and you can add your Wisdom modifier to the damage of Bolt. Using Bolt still counts as using your arc action for your turn.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 14,
      name: 'Elementary Particles',
      description: `When you reach 14th level, your ability to remain clear-minded in any situation grants you proficiency in all saving throws.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Stormcaller'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Boundless Energy',
      description: `At 20th level, you are a perpetual conduit for the natural arc energy that pervades space. You can draw upon it to replenish your Light almost like drawing breath to fill your lungs. As a bonus action on your turn, you can regain all spent arc charges. Once you use this feature, you must complete a short or long rest before you can use it again.`,
      source: 'Warlock Stormcaller'
    }
  ]
  charges: SubclassCharge = {
    name: 'Arc Charges',
    type: 'ability',
    values: {
      1: 2,
      2: 2,
      3: 4,
      4: 4,
      5: 6,
      6: 6,
      7: 8,
      8: 8,
      9: 8,
      10: 10,
      11: 10,
      12: 10,
      13: 12,
      14: 12,
      15: 12,
      16: 14,
      17: 14,
      18: 14,
      19: 16,
      20: 16
    }
  }
  abilityProficiencies = [
    Ability.Dexterity,
    Ability.Wisdom
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    MartialWeaponItemType.Longsword,
    MartialWeaponItemType.Shortsword,
    MartialWeaponItemType.Smallsword,
    MartialWeaponItemType.Warhammer,
    MartialWeaponItemType.CombatBow,
    WeaponItemType.PrimaryWeapon,
    HeavyWeaponItemType.FusionRifle,
    HeavyWeaponItemType.GrenadeLauncher,
    HeavyWeaponItemType.LinearFusionRifle
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle,
    ToolItemType.ElectronicsTools
  ]
  lightModifier = Ability.Wisdom

  getPath (id: string|null) {
    switch (id) {
      case Stormcaller.Lightning.id: return Stormcaller.Lightning
      case Stormcaller.Thunder.id: return Stormcaller.Thunder
      case Stormcaller.Wind.id: return Stormcaller.Wind
      default: return null
    }
  }
}

export class Voidwalker implements Subclass {
  static Destruction: SubclassPath = {
    id: 'Destruction',
    name: 'Destruction',
    super: SuperAbility.NovaBomb,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Harbinger of Destruction',
        description: `You manifest the destructive aspect of the void, drawing deeply on its power to wreak widespread devastation. You shape your Light into the Nova Bomb, an impossible singularity with the power of a stellar collapse. Your training teaches you to take hold of this power and push yourself to make it yield even mightier destruction.`,
        source: 'Warlock Voidwalker Destruction'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Strength of the Void',
        description: `At 2nd level you are well versed in the use of mundane wargear as well as reality-defying arcana. You gain proficiency in medium armor, all martial weapons, and all martial firearms except rocket launchers.`,
        source: 'Warlock Voidwalker Destruction'
      },
      {
        _type: 'Feature',
        level: 5,
        name: 'Extra Attack',
        description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
        source: 'Warlock Voidwalker Destruction'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Nova Bomb Attunement',
        description: `At 11th level, your diligent study and carefully refined Light-wielding technique enable you to enhance the destructive yield of the Nova Bomb. Choose one of the following ways to alter your Nova Bomb.
        
**Lance.** Your Nova Bomb travels faster and in a straighter line, and hits harder on impact. You lose the Mortar effect of your Nova Bomb, the range of your Nova Bomb increases by 100 feet, and you roll d10s for the damage of your Nova Bomb instead of d8s.

**Shatter.** You hurl three projectiles when you cast your Nova Bomb instead of only one. The area of your Nova Bomb now consists of up to three 5-foot cubes, which you can arrange as your wish when you cast your Nova Bomb. The spot you choose for each cube must be within 10 feet of another cube. Every creature within 5 feet of any of the cubes must make a Dexterity saving throw, taking the damage of your Nova Bomb on a failed save, or half as much on a success.`,
        source: 'Warlock Voidwalker Destruction'
      },
      {
        _type: 'Feature',
        level: 14,
        name: 'War Caster',
        description: `At 14th level, your sharpened situational awareness and heightened reaction speed when drawing upon the void make you a lethally effective mixedtechnique combatant. When you use your action to cast a melee or grenade ability, you can use your bonus action to make one weapon attack.`,
        source: 'Warlock Voidwalker Destruction'
      }
    ]
  }
  static Knowledge: SubclassPath = {
    id: 'knowledge',
    name: 'Knowledge',
    super: SuperAbility.NovaBomb,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Harbinger of Knowledge',
        description: `You manifest the mysterious aspect of the void, seeking knowledge of the unseen in the hidden recesses and interstices below the surface of apparent reality. You concentrate your Light to form the Nova Bomb, and work tirelessly to understand and master the intricacies of its ethereal explosion.`,
        source: 'Warlock Voidwalker Knowledge'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Knowledge of the Void',
        description: `Already at 2nd level, you possess a sufficient aptitude with the workings of void Light to infuse a weapon with its energies, binding it to yourself and endowing it with capabilities beyond typical equipment. You can choose one weapon you are proficient with and tie it to the void within you, turning it into a Voidbonded weapon. You must use an action to do this. Once you do, you gain the following benefits with that weapon:

- You can always use your Intelligence modifier when determining that weapon's attack rolls, damage rolls, and weapon DC. If the weapon has the Payload property, you can use your Intelligence modifier to determine its Payload DC.
- You are always aware of the location of your Voidbonded weapon if it is within 60 feet of you.
- You are instantly aware if any creature other than yourself is holding your Voidbonded weapon.

If you are ever more than 5 feet away from your Voidbonded weapon for 1 minute or longer, or if your Voidbonded weapon is stored in the memory of a Ghost other than your own, your Voidbonded weapon loses its connection. You cannot have more than one Voidbonded weapon at a time. If you create a new Voidbonded weapon, your previous Voidbonded weapon loses its connection to you.`,
        source: 'Warlock Voidwalker Knowledge'
      },
      {
        _type: 'Feature',
        level: 5,
        name: 'Heightened Voidbond',
        description: `Beginning at 5th level, you improve your ability to augment your chosen weapon, further enhancing its capabilities. Once on your turn when you deal damage with your Voidbonded weapon, its damage is increased by 1d8.`,
        source: 'Warlock Voidwalker Knowledge',
        damage: [
          { formula: '1d8', type: DamageType.Kinetic }
        ]
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Nova Bomb Attunement',
        description: `At 11th level, your thorough research and unrelenting exploration of void effects enable you to augment the secondary properties of the Nova Bomb. Choose one of the following ways to alter your Nova Bomb.`,
        featureOptions: [
          {
            _type: 'Feature',
            level: 11,
            name: 'Vortex',
            description: `After casting and resolving the effects of your Nova Bomb, it creates a 5-foot radius of void Light, swirling in a vortex centered on the point of impact for your Nova Bomb. You can concentrate on this vortex for up to 1 minute. While concentrating on this vortex, a creature that starts their turn within the vortex, or which enters the vortex for the first time on a turn, must make a Strength saving throw against your Light save DC. On a failed save, the creature is Restrained until the start of its next turn. A creature that ends their turn within the vortex takes half the damage of your Nova Bomb. This damage can invoke the effects of Destroy Creature (see the Risen superclass).`,
            source: 'Warlock Voidwalker Knowledge',
            savingThrowFormula: [
              { dc: '8 + [prof] + [lightMod]', ability: Ability.Strength }
            ]
          },
          {
            _type: 'Feature',
            level: 11,
            name: 'Cataclysm',
            description: `When you cast your Nova Bomb, you unleash a sphere of churning void Light that appears in a location within 5 feet of you, centered on that location. The sphere has a diameter of 5 feet and, upon casting, you can instruct the sphere to hunt down a target of your designation, which can be as vague or as specific as you like. If the designated target exists within 120 feet of the sphere, the sphere will immediately begin to seek it out, moving up to 45 feet on each of your turns, starting on the turn you cast it, dodging around obstacles and moving through any opening big enough for a Medium object.

The Light in the sphere can detect invisible targets and targets that are behind most barriers, but the detection abilities of the sphere are blocked by 2 feet of stone, 2 inches of common metal, or 6 feet of wood or dirt, as well as any barrier that prevents divination. If the sphere's ability to detect the target fails, or if the designated target does not exist within 120 feet of the sphere, it will travel in a straight line for 30 feet and then detonate.
            
If the sphere enters the target's space, it detonates on the target. If the path to the target is ever completely blocked, the sphere will detonate on the obstruction closest to the target.
            
Upon detonation, all creatures within 5 feet of the sphere must succeed on a Dexterity saving throw against your Light save DC. Increase your DC by an amount equal to your Light level for this. Targets take the damage of your Nova Bomb on a failed save, or half as much on a success. If the sphere detonates due to entering a target's space, that target must instead make a Constitution saving throw. Increase your DC by an amount equal to your Light level for this. The target takes the damage of your Nova Bomb on a failed save, or half as much on a success. In addition, if the target fails their saving throw, they become Incapacitated for the next minute. A creature Incapacitated in this way may reattempt the saving throw at the end of each of its turns, ending the effect on itself early on a success.`,
            source: 'Warlock Voidwalker Knowledge',
            savingThrowFormula: [
              { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity },
              { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Constitution }
            ]
          }
        ],
        source: 'Warlock Voidwalker Knowledge'
      },
      {
        _type: 'Feature',
        level: 14,
        name: 'Knowledge is Power',
        description: `When you reach 14th level, the bonus damage from Heightened Voidbond increases to 2d8.`,
        source: 'Warlock Voidwalker Knowledge',
        damage: [
          { formula: '2d8', type: DamageType.Kinetic }
        ]
      }
    ]
  }
  static Chaos: SubclassPath = {
    id: 'chaos',
    name: 'Chaos',
    super: SuperAbility.NovaWarp,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Harbinger of Chaos',
        description: `You manifest the unstable aspect of the void, embracing the eternal hiss of fluctuation that underlies the illusion of solid substance. Rather than externalize the energies of the void into a bomb in your hands, you perform the Nova Warp, concentrating your Light inward and channeling the void through yourself, making your own body into a roving bomb.`,
        source: 'Warlock Voidwalker Chaos'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Depths of the Void',
        description: `Already at 2nd level, the void has shown you the true path to power, and you are not afraid to plunge into its depths to attain it. You gain the ability to channel the power of void Light almost at whim, and you use it to achieve paracausal effects. You gain two effects to start: Overchannel Light, described at the end of this feature, and one other effect granted by the Twist the Real feature. Some Marks of the Void can grant you additional options for this feature.

You have two uses of your Depths of the Void. When you use your Depths of the Void feature, you choose which effect to create. When you finish a short or long rest you regain your uses of Depths of the Void.

**Chaos Ensues.** This power comes at a cost. When you choose to spend a use of your Depths of the Void feature, you must make a Chaos Roll (a single roll of a d20, no modifiers). If you roll a 1 or a 2, you must roll on the Chaos table, listed at the end of this class description.

Unless stated otherwise, any effect from the Chaos table lasts until you complete a rest in which you regain all uses of your Depths of the Void. If the result requires a saving throw, the DC is equal to 10 + your Light level.

**Overchannel Light.** Once on your turn, when you would have to roll void damage, you can choose to use your Depths of the Void feature to deal maximum damage instead of rolling.`,
        source: 'Warlock Voidwalker Chaos'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Pulsewave',
        description: `At 2nd level, choose one of the following additional options for your Depths of the Void feature.

**Strike From the Void.** Once on your turn when you make an attack roll, you can spend a use of your Depths of the Void to grant yourself a bonus +10 on the roll. You can choose to do this before or after you learn the results of your attack roll. If you choose to do this after you learn the results, you have disadvantage on your Chaos roll.

**Grace of the Void.** When you make a shield recharge roll, you can choose to spend a use of your Depths of the Void to heal for the maximum amount instead of making a roll. In addition, you can choose to apply the amount you roll to your health first, with any leftover going to your energy shield points, at the cost of having disadvantage on your Chaos roll.`,
        source: 'Warlock Voidwalker Chaos'
      },
      {
        _type: 'Feature',
        level: 5,
        name: 'Delve Deeper',
        description: `Beginning at 5th level, you become more at ease calling upon the reality-bending powers of the void. You can use your Depths of the Void feature three times between rests. You regain all uses when you complete a short or long rest.

Additionally, when you choose to spend a use of your Depths of the Void, you can choose to do so once without having to make a Chaos roll. Once you choose to do this, you must complete a long rest before you can choose to do this again.`,
        source: 'Warlock Voidwalker Chaos'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Nova Warp Attunement',
        description: `At 11th level, your ceaseless experimentation and unfettered communion with the void's power leadyou to intensify the behavior of the Nova Warp. Choose one of the following ways to bolster your Nova Warp.

**Arcane Distortion.** You can choose to displace the origin point of your Nova Warp's Void Eruption to be a space you can see within 15 feet of you, instead of centered on yourself. Once on your turn, you can spend a use of your Depths of the Void to center your Void Eruption on any point within 30 feet of you, even one you cannot see.

**Arcane Strength.** Your AC increases by 4 while concentrating on your Nova Warp. When you cast Nova Warp, you can spend a use of your Depths of the Void to make it so you cannot lose concentration on Nova Warp as a result of taking damage. This effect lasts for the next minute.

**Arcane Force.** The radius of your Nova Warp's Void Eruption increases to 15 feet. Once on your turn, you can spend a use of your Depths of the Void to increase the radius to 30 feet instead.`,
        source: 'Warlock Voidwalker Chaos'
      },
      {
        _type: 'Feature',
        level: 14,
        name: 'Plumb the Depths',
        description: `At 14th level, you are so accustomed to staring into the abyss of the void's power that the normal world appears strange to your eyes. You can use your Depths of the Void feature four times between rests, and whenever you have to roll on the Chaos table, you can roll twice and choose either result.`,
        source: 'Warlock Voidwalker Chaos'
      }
    ]
  }

  id = 'voidwalker'
  class = Class.Warlock
  element = LightElement.Void
  classAbility = ClassAbility.RiftOfLight
  name = 'Warlock Voidwalker'
  shieldDie = 6
  perks = [
    ...WarlockClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Voidwalker',
      description: `As a Stormcaller, you gain the following class features in addition to the features granted by the Warlock superclass.
### Health Points and Shields
- **Shield Die Size**: d6
- **Health Points at 1st level**: 6 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 6
- **Shield Points at higher levels**: Add 1d6 (or 4) for every level after 1st
### Proficiencies
- **Armor**: Light armor
- **Weapons**: Simple melee weapons, shortswords, smallswords, combat bows, simple firearms, fusion rifles
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Intelligence, Wisdom
- **Skills**: Choose any 3`,
      skillOptions: [{ choice: 3, skills: getAllSkills() }],
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Light Affinity',
      description: `Even at 1st level, you know how to harness the Light you possess, and you shape it in the form of void Light. Intelligence is your Light ability score, and your Intelligence modifier is your Light ability modifier. You use your Intelligence modifier to determine your Light attack modifier and to set the save DC against your Light abilities. When you are told to make a Light ability saving throw, you make an Intelligence saving throw.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Enhanced Lightcasting',
      description: `At 1st level, you already understand some of the deeper workings of the Traveler's Light, and can manipulate it more subtly than others. When you cast a Light ability and roll a 1 or a 2 on any of the dice in the damage or healing roll, you may choose to re-roll those dice. You must use the new rolls, even if they are 1s or 2s.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Melee Ability',
      description: `At 1st level you gain your melee ability, Energy Drain. Whereas others merely strike at the substance of the hostile other, you pull at the other's vital energy, drawing it away and into yourself. You cast your Energy Drain using a melee ability charge, of which you have a single one. If you spend your melee ability charge, you regain it when you complete a brief rest.`,
      source: 'Warlock Voidwalker',
      abilityOptions: [
        MeleeAbility.EnergyDrain
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Warlock Voidwalker',
      abilityOptions: [
        GrenadeAbility.AxionBolt,
        GrenadeAbility.Scatter,
        GrenadeAbility.Vortex
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Marked by the Void',
      description: `After reaching 2nd level, the first time you complete a long rest in which you sleep or meditate, the void reaches out to you, resonating with your Light. Perhaps you are visited by visions of greatness, or perhaps you dream of drowning in an incomprehensibly dark and cold lake. Perhaps you wake with a scream. Perhaps you wake in a cold sweat. Or maybe you wake with only the quiet realization of what you will become. However the void reaches for you, it leaves you irrevocably marked by its presence.

You gain one Mark of the Void of your choice, the options for which are detailed at the end of this class description. When you gain certain Voidwalker levels, you gain additional Marks, as shown in the Total Marks column of the Voidwalker table. Furthermore, whenever you gain a level in this class, you can choose one of the Marks the Void has left you with and replace it with another Mark that you could have at your level. If the Mark you choose to remove is a prerequisite for any other Marks you have, you can also replace those Marks.

If you do not meet the prerequisite for a Mark, you do not gain its benefits, even if it is a Mark you have.`,
      source: 'Warlock Voidwalker',
      abilities: [
        OtherAbility.VoidwalkerAbyssalStalwart,
        OtherAbility.VoidwalkerAngryMagic,
        OtherAbility.VoidwalkerAnnihilate,
        OtherAbility.VoidwalkerAtMySummons,
        OtherAbility.VoidwalkerBlink,
        OtherAbility.VoidwalkerBombsAway,
        OtherAbility.VoidwalkerChaosAccelerant,
        OtherAbility.VoidwalkerComprehensionOfTheVoid,
        OtherAbility.VoidwalkerDarkMatter,
        OtherAbility.VoidwalkerDetectLightAndDark,
        OtherAbility.VoidwalkerEmbraceTheVoid,
        OtherAbility.VoidwalkerEyesOfTheVoid,
        OtherAbility.VoidwalkerFeedTheVoid,
        OtherAbility.VoidwalkerGazeOfTheVoid,
        OtherAbility.VoidwalkerGiftOfTheVoid,
        OtherAbility.VoidwalkerGraduateStudent,
        OtherAbility.VoidwalkerHandheldSupernova,
        OtherAbility.VoidwalkerIdentifyExotic,
        OtherAbility.VoidwalkerInsatiable,
        OtherAbility.VoidwalkerItFollows,
        OtherAbility.VoidwalkerLongShot,
        OtherAbility.VoidwalkerMasterOfLight,
        OtherAbility.VoidwalkerOfferingToTheVoid,
        OtherAbility.VoidwalkerOuroboros,
        OtherAbility.VoidwalkerPantologist,
        OtherAbility.VoidwalkerPeerless,
        OtherAbility.VoidwalkerSuperlativeSurge,
        OtherAbility.VoidwalkerSurgingProtection,
        OtherAbility.VoidwalkerTeachingsOfTheVoid,
        OtherAbility.VoidwalkerUnbound,
        OtherAbility.VoidwalkerVigilantScholar,
        OtherAbility.VoidwalkerVortexMastery,
        OtherAbility.VoidwalkerWarpAmmo,
        OtherAbility.VoidwalkerWhispersOfTheVoid
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Voidwalker Harbinger',
      description: `When you reach 2nd level, you choose the aspect of the void you will manifest in the material world, which will determine the progression of your abilities. You may become a Harbinger of Destruction, Harbinger of Knowledge, or Harbinger of Chaos, all detailed below in this class description. Your choice grants you a feature at 2nd level, and additional features at 3rd, 5th, 11th, and 14th level.`,
      source: 'Warlock Voidwalker',
      pathOptions: [
        Voidwalker.Destruction,
        Voidwalker.Knowledge,
        Voidwalker.Chaos
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 6,
      name: 'Void Praxis',
      description: `At 6th level, your research into potential applications of your void Light yields practical results. Choose one of the following features to enhance yourself. You can alter your choice by communing with the void for at least 1 hour (this can be done during a short or long rest). If you complete your communion, you lose the effects of your previous choice and gain the effects of your new choice.`,
      featureOptions: [
        {
          _type: 'Feature',
          level: 6,
          name: 'Atomic Breach',
          description: `The reach of your Energy Drain increases to 10 feet, and it is considered a melee weapon for the purpose of determining if you can make an attack of opportunity with it. The damage die size of Energy Drain increases by one.`,
          source: 'Warlock Voidwalker'
        },
        {
          _type: 'Feature',
          level: 6,
          name: 'Devour',
          description: `As a bonus action on your turn, you can spend your melee ability charge to begin a Devour effect. Make a shield recharge roll, and for the next minute, whenever a hostile creature is reduced to 0 hit points within 15 feet of you, you can make another shield recharge roll. Once you have made a shield recharge roll with this feature (this includes the initial shield recharge roll), you must wait until the start of your next turn before you can make a shield recharge roll with this feature again.`,
          source: 'Warlock Voidwalker'
        },
        {
          _type: 'Feature',
          level: 6,
          name: 'Surge',
          description: `As a bonus action on your turn, you can spend a melee ability charge to begin a Surge effect. For the next minute, it costs no movement for you to begin Aiming, you can make attacks of opportunity with firearms while Aiming, and your base walking speed increases by 10 feet.`,
          source: 'Warlock Voidwalker'
        }
      ],
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 9,
      name: 'Scarred by the Void',
      description: `At 9th level, your long immersion in the yawning depths of the void has changed you. Choose one Mark of the Void that you could learn at 2nd level. This becomes a permanent Mark whose effects you always have, and it doesn't count against the total for the number of Marks you have.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 13,
      name: 'Etheral Gait',
      description: `At 13th level, your Light can exert a repulsive force, and it is no longer strictly necessary for you to walk on solid surfaces. You gain a hover speed equal to your base walking speed.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 17,
      name: 'Adept of Light',
      description: `When you reach 17th level, your Light is wound taut, its pathways within you coiled efficiently. Your melee, grenade, and superclass ability recharge values are all reduced by one. Your super ability recharge value is reduced by two.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Warlock Voidwalker'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Boundless Energy',
      description: `At 20th level, you have gazed so long into the abysses below the veneer of apparent reality that you feel you know every impossible corner of the vastness beneath. So well do you know the void that when you look into it, it seems like merely looking at yourself. Whenever you complete a long rest, you can replace one Mark of the Void you have with another Mark. If the Mark you choose to remove is a prerequisite for any other Marks you have, you can also replace those Marks. Like normal, if you do not meet the prerequisite for a Mark, you do not gain its benefits, even if it is a Mark you have.

Additionally, you now have access to certain Marks that have the prerequisite of 20th level. These are your pinnacle Mark options. You may only have one pinnacle Mark at any time.`,
      source: 'Warlock Voidwalker'
    }
  ]
  abilityProficiencies = [
    Ability.Intelligence,
    Ability.Wisdom
  ]
  armor = [
    ArmorItemType.LightArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    MartialWeaponItemType.Shortsword,
    MartialWeaponItemType.Smallsword,
    MartialWeaponItemType.CombatBow,
    WeaponItemType.PrimaryWeapon,
    HeavyWeaponItemType.FusionRifle
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle
  ]
  lightModifier = Ability.Intelligence
  charges: SubclassCharge = {
    name: 'Marks of the Void',
    type: 'feature',
    values: {
      2: 1,
      3: 2,
      4: 2,
      5: 3,
      6: 3,
      7: 4,
      8: 4,
      9: 5,
      10: 5,
      11: 5,
      12: 6,
      13: 6,
      14: 6,
      15: 7,
      16: 7,
      17: 7,
      18: 8,
      19: 8,
      20: 8
    }
  }

  getPath (id: string|null) {
    switch (id) {
      case Voidwalker.Chaos.id: return Voidwalker.Chaos
      case Voidwalker.Destruction.id: return Voidwalker.Destruction
      case Voidwalker.Knowledge.id: return Voidwalker.Knowledge
      default: return null
    }
  }
}

export class Gunslinger implements Subclass {
  static Sharpshooter: SubclassPath = {
    id: 'sharpshooter',
    name: 'Sharpshooter',
    super: SuperAbility.Deadeye,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: The Sharpshooter',
        description: `As a sharpshooter, you live for landing your shots precisely where you call them. You prefer to view the battlefield through a scope, where your eagle eye can pick out the sweet spots from long range. If things get too close for comfort, your Light provides an ace up your sleeve in the form of a wheelgun made of sunshine that never fails to impress.`,
        source: 'Hunter Gunlinger Sharpshooter'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Over The Horizon',
        description: 'Also at 3rd level, you possess stupendously sharp aim. While you are Aiming with a firearm that has a medium or long range band, taking a shot against a target within the firearm\'s maximum range does not impose disadvantage on your attack roll.',
        source: 'Hunter Gunlinger Sharpshooter'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Sharpshooter\'s Tools',
        description: 'When you choose this style at 3rd level, you gain proficiency with sniper rifles and linear fusion rifles. Whatever kinds of weapons you like best, you can hardly call yourself a sharpshooter without knowing your way around long-range firearms.',
        source: 'Hunter Gunlinger Sharpshooter'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Weighted Knife',
        description: 'At 6th level, it occurs to you your knife tricks would be even more impressive if you could hit a target from a great range. The range of your Throwing Knife becomes 50/100. If you hit a target with your Throwing Knife while Aiming, the damage die size of your Throwing Knife increases by one.',
        source: 'Hunter Gunlinger Sharpshooter'
      },
      {
        _type: 'Feature',
        level: 10,
        name: 'Chain of Woe',
        description: `When you reach 10th level, you have developed a feel for zeroing in on the precise point of your aim, and sending successive shots to exactly that spot. On your turn, you can choose to begin a Chain of Woe. Starting from when you choose to do this, for every successful firearm attack you make while Aiming, you increase your critical hit range by 1. When you roll a critical hit, your critical hit range returns to its original state.

Your Chain of Woe ends after a minute, if you roll a critical hit, or if you roll a natural 1 on a firearm attack. Once you use this feature, you must complete a long rest before using it again.`,
        source: 'Hunter Gunlinger Sharpshooter'
      },
      {
        _type: 'Feature',
        level: 14,
        name: 'Keyhole',
        description: 'At 14th level, you place your shots so well that you could send a bullet up the barrel of the gun your enemy points at you. If a target is behind anything less than full cover, it gains no benefits from its cover against your firearm attack rolls.',
        source: 'Hunter Gunlinger Sharpshooter'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Grim Reaper',
        description: 'At 18th level, landing chained precision shots is as natural as breathing. You regain use of your Chain of Woe feature when you complete a short or long rest, rolling a natural 1 does not end your Chain of Woe, and neither does rolling a critical hit. However, like normal, your critical hit range returns to its original state when you roll a critical hit.',
        source: 'Hunter Gunlinger Sharpshooter'
      }
    ]
  }
  static Outlaw: SubclassPath = {
    id: 'outlaw',
    name: 'Outlaw',
    super: SuperAbility.SixShooter,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: The Outlaw',
        description: `Nomad, wanderer, lone wolf—whatever they call you, as an outlaw, you know how to survive on your own, and you have the skills to see yourself through. You prefer to keep on the move, but like to stop to tell a good story, even if its truthfulness depends on the credulence of the listeners. One thing is indisputably true, though: you carry a golden gun in your heart that never lets you down so long as you aim quick and sharp.`,
        source: 'Hunter Gunlinger Outlaw'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Sixth Sense',
        description: 'Also at 3rd level, your sense of your surroundings has grown uncannily keen. If you are Blinded, but still able to hear, creatures within 30 feet of you do not gain advantage on their attacks against you. Also, even while Blinded, you can make attacks with a firearm or your Golden Gun normally against targets that are within 30 feet of you.',
        source: 'Hunter Gunlinger Outlaw'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Proximity Explosive Knife',
        description: 'At 6th level, you start thinking about all the things you could do if your throwing knives could also explode. When you cast Throwing Knife, the knife you create sticks to the surface it hits and can linger there for up to 1 minute. During this time, if a hostile creature moves within 5 feet of the knife it will detonate. All creatures within 5 feet of the Throwing Knife must make a Dexterity saving throw against your Light save DC. They take the damage of your Throwing Knife as explosive solar damage on a failed save, or half as much on a success.',
        source: 'Hunter Gunlinger Outlaw',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
        ]
      },
      {
        _type: 'Feature',
        level: 10,
        name: 'Fan The Hammer',
        description: `Beginning at 10th level, you become adept at popping off quick shots against multiple targets in rapid succession. You can use your action to take a shot with a firearm against a number of creatures within 15 feet of a point you can see. This point must be within the firearm's effective range, you must have enough ammunition in your firearm's magazine for each target you choose, and you cannot benefit from the Aiming condition when you do this. You make a separate attack roll for each target you choose, and you can only choose a target once.

If taking a shot with the firearm is normally an action, such as via the effect of a weapon perk or the Cumbersome property, you cannot use this feature with that firearm.`,
        source: 'Hunter Gunlinger Outlaw'
      },
      {
        _type: 'Feature',
        level: 14,
        name: 'Gunfighter',
        description: 'Starting at 14th level, calling upon your Light strengthens the resolve and focus of your already unswerving aim. You cannot have disadvantage on an attack roll made with your Golden Gun if you are holding it in two hands.',
        source: 'Hunter Gunlinger Outlaw'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Circle of Life',
        description: `At 18th level, you have learned to channel the residual solar fire of foes burned away by your Light back into your Golden Gun. If you kill a hostile creature with a shot from your Golden Gun, two shots are refunded to your Golden Gun's magazine and you can make one additional shot with your Golden Gun on your turn.`,
        source: 'Hunter Gunlinger Outlaw'
      }
    ]
  }
  static Showman: SubclassPath = {
    id: 'showman',
    name: 'Showman',
    super: SuperAbility.BladeBarrage,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: The Showman',
        description: `People know Hunters like knives, but they expect Gunslingers to focus on their guns. As a showman, though, you delight in delivering the unexpected. You appreciate a good shooter as much as the next Gunslinger, but when the pressure is on, enemies box you in, and ammo runs out, you reach for your blades—and what a show you can put on with knives in your hands.`,
        source: 'Hunter Gunlinger Showman'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Decisive Edge',
        description: 'Even at 3rd level, your knife skills are already frighteningly impressive. Your Throwing Knife\'s recharge value is reduced by one. You can also choose to make your Throwing Knife your weapon of choice. If you do so, you can cast Throwing Knife as an action or bonus action on your turn.',
        source: 'Hunter Gunlinger Showman'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Fiery Fan',
        description: 'Beginning at 6th level, you are finally dextrous enough to throw multiple knives at once while keeping all your fingers. When you cast your Throwing Knife, you can fling a trio of knives at once. Choose up to three targets in a 15-foot cone (you cannot choose the same target multiple times). Make separate attack and damage rolls for each target.',
        source: 'Hunter Gunlinger Showman'
      },
      {
        _type: 'Feature',
        level: 10,
        name: 'Burning Edge',
        description: `At 10th level, it occurs to you that your knife tricks would be more impressive if things caught fire, too. If you hit a creature with your Throwing Knife, they begin Burning for the next minute. Creatures Burning from your Throwing Knife can make a Constitution saving throw at the end of their turn, ending the effect early on a success. Creatures who succeed on their saving throw, or for whom the effect ends, become immune to Burning from your Throwing Knife for 24 hours.

While any creature is Burning from your Throwing Knife, you have advantage on superclass ability recharge rolls you make.`,
        source: 'Hunter Gunslinger Showman',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      },
      {
        _type: 'Feature',
        level: 14,
        name: 'Playing with Fire',
        description: 'At 14th level, you are a master showman, and you love a flawless execution just as much as the crowd does. If a creature is reduced to 0 hit points while Burning from your Throwing Knife, you immediately regain one melee ability charge.',
        source: 'Hunter Gunlinger Showman'
      },
      {
        _type: 'Feature',
        level: 18,
        name: 'Dodge This',
        description: `When you reach 18th level, creatures can't have advantage on their saving throws against your Blade Barrage.`
      }
    ]
  }

  id = 'gunslinger'
  class = Class.Hunter
  element = LightElement.Solar
  classAbility = ClassAbility.HuntersDodge
  name = 'Hunter Gunslinger'
  shieldDie = 8
  perks = [
    ...HunterClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Gunslinger',
      description: `As a Gunslinger, you gain the following class features in addition to the features granted by the Hunter superclass.
### Health Points and Shields
- **Shield Die Size**: d8
- **Health Points at 1st level**: 8 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 8
- **Shield Points at higher levels**: Add 1d8 (or 5) for every level after 1st

### Proficiencies
- **Armor**: Light armor, medium armor
- **Weapons**: Simple melee weapons, simple firearms, shotguns, light machine guns
- **Toolkit**: Weaponsmithing toolkit
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Dexterity, Charisma
- **Skills (Choose 2)**: Acrobatics, Athletics, Insight, Intimidation, Medicine, Perception, Performance, Persuasion, and Stealth`,
      skillOptions: [{ choice: 2, skills: [Skill.Acrobatics, Skill.Athletics, Skill.Insight, Skill.Intimidation, Skill.Medicine, Skill.Perception, Skill.Performance, Skill.Persuasion, Skill.Stealth] }],
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Weapon of Choice',
      description: `Even at 1st level, you intuitively recognize the central importance of your relationship to your gun. You study the guns you use with intensive, loving care, and come to innately understand the individual characteristics and quirks of each piece. By studying a single firearm with which you are proficient for an uninterrupted period of 1 hour, you come to know every detail of its workings, feel, and character, developing an almost supernatural familiarity with it. This firearm then becomes your weapon of choice, and you know all the properties and perks of the weapon if you did not know them before.

You can only have one weapon of choice at a time. Your weapon of choice cannot have the Payload property. Once you've turned a firearm into your weapon of choice, you cannot be disarmed of that weapon unless you are Incapacitated.

Other class features grant additional benefits to your weapon of choice as you gain Gunslinger levels.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Exotic Weapon of Choice',
      description: 'In order to make an exotic weapon your weapon of choice, you need to study it over a period of one week and train with it for at least 4 hours a day during this time (this can be done during a long rest). At the end of the week, it becomes your weapon of choice. If you fail to train with it for at least 4 hours in a day during the week, you will need to begin the process all over again.',
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Uncanny Hits',
      description: 'Your ready familiarity with your weapon of choice allows you to hit the weak points of a target with uncanny precision. When you deal damage with a shot from your weapon of choice, you add a bonus to your damage roll as shown in the Uncanny Hits column of the Gunslinger table.',
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Weaponized Personality',
      description: 'Gunslingers know a well-placed word can do as much damage or win as much acclaim as a well-placed shot. You have a natural ability to draw on your own personality as a kind of weapon, whether you pull intimidating stony silence, unassuming roguish charm, or jovial arrogance. You can either gain proficiency in a Charisma-based skill of your choice, or you can choose to double your proficiency bonus for a Charisma-based skill you are already proficient in. The skill you choose can only benefit from this bonus if it is not already benefiting from a feature or trait that doubles your proficiency bonus for it.',
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Hunter Gunslinger',
      abilityOptions: [
        GrenadeAbility.Incendiary,
        GrenadeAbility.Swarm,
        GrenadeAbility.Tripmine
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Light Affinity',
      description: `Beginning at 2nd level, you've learned to harness the Light you possess, and you shape it in the form of solar Light. Charisma is your Light ability score, and when called for, your Charisma modifier is your Light ability modifier. You use your Charisma modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Charisma saving throw.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Melee Ability',
      description: `When you reach 2nd level you gain your melee Light ability, the Throwing Knife. Every Hunter carries knives, but Gunslingers fling knives of searing solar Light, sharp as sunbeams. You cast your Throwing Knife using a melee ability charge, of which you have a single one. You regain spent melee ability charges when you complete a brief rest.`,
      source: 'Hunter Gunslinger',
      abilityOptions: [
        MeleeAbility.ThrowingKnife
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Jack Of All Guns',
      description: 'Even at 2nd level, you have handled more guns than you can count, and accordingly know your way around everything from boot-holstered sidearms to anti-armor heavy sniper rifles. You can add half your proficiency bonus, rounded down, to any attack roll you make with a firearm that does not already benefit from your proficiency bonus.',
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Gunslinger Style',
      description: 'At 3rd level, you settle into a combat style that best suits your tastes as a Gunslinger, and determines how your skills will develop. Choose the Sharpshooter, the Outlaw, or the Showman style, all detailed at the end of this class description. The style you choose grants you features at 3rd, 6th, 10th, 14th, and 18th levels.',
      source: 'Hunter Gunslinger',
      pathOptions: [
        Gunslinger.Outlaw,
        Gunslinger.Sharpshooter,
        Gunslinger.Showman
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Weaponsmithing Expertise',
      description: `At 7th level, your study of firearms allows you to double your proficiency bonus when you make an ability check with your weaponsmithing toolkit. You can only do this if the ability check is not already benefiting in a way that doubles your proficiency bonus.

Additionally, if your weapon of choice ever becomes damaged or destroyed, you can repair or recreate it over the course of 8 hours, assuming you have adequate materials and glimmer to do so (confer with your Architect).`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Weapon Modification',
      description: 'Also at 7th level, you have devoted considerable thought to how you might modify your weapon of choice to improve its performance and suit your needs. Choose one of the following features. Whenever you choose a new weapon of choice, or whenever you complete a 1-hour study of your weapon of choice, you can change which feature to apply to your weapon of choice. Once you modify your weapon of choice in any way, you become linked to the weapon and are always aware of its location if it is within 30 feet of you.',
      source: 'Hunter Gunslinger',
      perks: [
        {
          level: 7,
          name: 'Hot Shot',
          description: 'When you make a successful attack with your weapon of choice, you can always choose to deal solar damage instead of its normal damage. Lucky Hit. If you roll a 1 or a 2 on any of thedamage dice for your weapon of choice, you can roll an additional damage die and add it to the total. You can only do this once for your damage roll, and only once on your turn.',
          source: 'Hunter Gunslinger'
        },
        {
          level: 7,
          name: 'Stopping Power',
          description: 'Once on your turn, if you hit a Large or smaller creature with an attack from your weapon of choice, you can cause that creature to make a Strength saving throw (DC = 8 + your proficiency bonus + your Strength or Dexterity modifier) or its movement becomes 0 until the end of its next turn.',
          source: 'Hunter Gunslinger',
          savingThrowFormula: [
            { dc: '8 + [prof]', ability: Ability.Wisdom }
          ]
        },
        {
          level: 7,
          name: 'Uncanny Aim',
          description: 'It costs no movement to begin Aiming with your weapon of choice. You can make an attack of opportunity with your weapon of choice even if you are Aiming.',
          source: 'Hunter Gunslinger'
        }
      ]
    },
    {
      _type: 'Feature',
      level: 9,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 11,
      name: 'Extra Attack Improvement',
      description: 'The number of attacks by attack action increases to three when you reach 11th level in this class.',
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 15,
      name: 'Watchful Eye',
      description: 'Starting at 15th level, your gunfighting focus has developed into an all-pervasive awareness of your surroundings in a fight. You can use your reaction to grant a creature disadvantage on one attack roll it makes against you, or to grant yourself advantage on a payload saving throw you make.',
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Gunslinger'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Legerdemain',
      description: 'By 20th level, your long experience handling all kinds of weapons—as well as performing other tricks requiring a delicate touch or a firm grip—has endowed you with exceptionally skillful hands. Choose either Strength or Dexterity. Your chosen score increases by 4, and your maximum possible for that score increases to 24.',
      source: 'Hunter Gunslinger'
    }
  ]
  abilityProficiencies = [
    Ability.Dexterity,
    Ability.Charisma
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    WeaponItemType.PrimaryWeapon,
    HeavyWeaponItemType.Shotgun,
    HeavyWeaponItemType.LightMachineGun
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle,
    ToolItemType.WeaponsmithingTools
  ]
  lightModifier = Ability.Charisma

  getPath (id: string|null) {
    switch (id) {
      case Gunslinger.Outlaw.id: return Gunslinger.Outlaw
      case Gunslinger.Sharpshooter.id: return Gunslinger.Sharpshooter
      case Gunslinger.Showman.id: return Gunslinger.Showman
      default: return null
    }
  }
}

export class Bladedancer implements Subclass {
  static Swiftcutter: SubclassPath = {
    id: 'swiftcutter',
    name: 'Swiftcutter',
    super: SuperAbility.RazorsEdge,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Way of the Swiftcutter',
        description: `It does not matter that your foes can see you coming if you are too fast for them to do anything to stop you. As a swiftcutter, you charge directly into the fray with unmatched quickness to cut down enemies with your trenchant blade of Light.`,
        source: 'Hunter Bladedancer Swiftcutter'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Melee Ability',
        description: `Starting at 2nd level, your practice harnessing the flow of your Light endows you with the ability to channel it into an overcharged strike. You have the Electrifying Strike melee ability, which you cast using melee ability charges. You have a single melee ability charge. If you spend your melee ability charge, you regain it when you make a successful recharge roll or complete a brief rest.`,
        source: 'Hunter Bladedancer Swiftcutter',
        abilityOptions: [
          MeleeAbility.ElectrifyingStrike
        ]
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'The More the Merrier',
        description: `At 3rd level, you see being outnumbered as an opportunity to test your skill. You don't need advantage on your attack roll to invoke Expert Attack if two or more hostile creatures are within 15 feet of you, and you don't have disadvantage on the attack roll. All other rules for Expert Attack still apply. Additionally, if you start your turn and benefit from this feature, your AC increases by 2 until the start of your next turn.`,
        source: 'Hunter Bladedancer Swiftcutter'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Relentless Style',
        description: `At 6th level, your tireless pursuit of perfection in your training has imbued your physicality with an impressive grace. Even your regular movements cannot help but show a stylish flair. If you make an ability check that uses your Charisma modifier, you can add a bonus to your ability check equal to your Dexterity modifier (minimum bonus +1).`,
        source: 'Hunter Bladedancer Swiftcutter'
      },
      {
        _type: 'Feature',
        level: 9,
        name: 'Ebb and Flow',
        description: `At 9th level, you perfect your awareness of the currents of Light within you. The recharge value of your melee ability is reduced by one. In addition, while a creature is Electrified, you have advantage on melee, grenade, and superclass ability recharge rolls you make.`,
        source: 'Hunter Bladedancer Swiftcutter'
      },
      {
        _type: 'Feature',
        level: 17,
        name: 'Overflow',
        description: `At 17th level, your crackling Light is strong enough to leap from one foe to the next. If a hostile creature is reduced to 0 hit points while Electrified from one of your features or core Light abilities, all other creatures within 5 feet must make a Constitution saving throw against your Light save DC. A creature becomes Electrified on a failed save.`,
        source: 'Hunter Bladedancer Swiftcutter',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      }
    ]
  }
  static Arcstrider: SubclassPath = {
    id: 'arcstrider',
    name: 'Arcstrider',
    super: SuperAbility.ArcStaff,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Way of the Arcstrider',
        description: `Arcstriders operate on the tension between delicacy and force, elegance and might, flexibility and implacability. You recognize these apparent opposites merely represent spectra, and you know how to fight most effectively by occupying the spaces between, gracefully adapting to the situation at hand.`,
        source: 'Hunter Bladedancer Arcstrider'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Melee Ability',
        description: `Starting at 2nd level, your understanding of the Light's current allows you to incapacitate those you strike with it. You have the Disorienting Blow melee ability, which you cast using melee ability charges. You have a single melee ability charge. If you spend your melee ability charge, you regain it when you make a successful recharge roll or complete a brief rest.`,
        source: 'Hunter Bladedancer Arcstrider',
        abilityOptions: [
          MeleeAbility.DisorientingBlow
        ]
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Open-hand Techniques',
        description: `Also at 2nd level, you are adept in specialized unarmed fighting techniques for close-quarters combat. Your unarmed strikes are considered to have the Finesse property, and you can roll a d4 in place of the normal damage die of your unarmed strikes. The damage die you can use increases to a d6 at 5th level, d8 at 11th level, and d10 at 17th level. In addition, when you make an unarmed strike, you can choose to deal bludgeoning or arc damage on a hit.`,
        source: 'Hunter Bladedancer Arcstrider'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Fleet-footed',
        description: `Starting at 3rd level, you learn to apply the principles underlying your combat stances to your regular walking gait. Your base walking speed increases by 5 feet, and the first 5 feet of movement on your turn doesn't provoke attacks of opportunity.`,
        source: 'Hunter Bladedancer Arcstrider'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Combat Flow',
        description: `At 6th level, you move freely from one strike to the next, allowing your momentum to carry you through. Killing a creature with an unarmed strike or Disorienting Blow allows you to regain a superclass ability charge. If you kill a creature with Disorienting Blow, you can also make a shield recharge roll.`,
        source: 'Hunter Bladedancer Arcstrider'
      },
      {
        _type: 'Feature',
        level: 9,
        name: 'Combat Meditation',
        description: `At 9th level, your focus is sharpest in the most heated moment of a fight. If you take an amount of damage that reduces you to half your shield points or less, you can immediately regain one melee ability charge and one superclass ability charge. You must regain at least 1 shield point before you can invoke this feature again.`,
        source: 'Hunter Bladedancer Arcstrider'
      },
      {
        _type: 'Feature',
        level: 17,
        name: 'Whirlwind Guard',
        description: `At 17th level, you have learned or devised new staff moves. While concentrating on your Arc Staff super ability, the following effects apply:

- Your AC increases by 4.
- Your concentration on Arc Staff can't be broken as a result of taking damage.`,
        source: 'Hunter Bladedancer Arcstrider'
      }
    ]
  }
  static Whisper: SubclassPath = {
    id: 'whisper',
    name: 'Whisper',
    super: SuperAbility.VanishingAct,
    perks: [
      {
        _type: 'Feature',
        level: 2,
        name: 'Subclass Path: Way of the Whisper',
        description: `You are the shadow of death, and your foes fall without even noticing the killing blow—much less who delivered it. To achieve this perfect stealth, you are devoted to constant improvement and refinement of your techniques, till you move as a whisper on the wind.`,
        source: 'Hunter Bladedancer Whisper'
      },
      {
        _type: 'Feature',
        level: 2,
        name: 'Melee Ability',
        description: `Starting at 2nd level, your practice attuning your movements to your Light endows you with the ability to close into melee range not merely by moving through space, but instantaneously translocating across it. As a bonus action you can spend your melee ability charge to teleport up to 10 feet to an unoccupied space you can see, taking all carried and worn equipment of your choice with you when you do.

You have a single melee ability charge. If you spend your melee ability charge, you regain it when you make a successful recharge roll (d6, 5-6) or complete a brief rest.`,
        source: 'Hunter Bladedancer Whisper',
        abilityOptions: [
          MeleeAbility.Blink
        ]
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Infiltrator',
        description: `At 3rd level, you are already capable of fast, nimble movement, no matter the direction. You have a climbing speed equal to your base walking speed. When traveling alone, you can move stealthily at a normal pace. It costs 10 less feet of movement for you to become Combat-Prone, or to stand from being Combat-Prone.`,
        source: 'Hunter Bladedancer Whisper'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Escape Artist',
        description: `When you reach 6th level, you develop an effective method to induce Light-actuated paracausal invisibility from the action of stabbing someone. When you hit with a melee attack, you can spend a grenade ability charge to gain active camouflage that lasts until the end of your next turn.`,
        source: 'Hunter Bladedancer Whisper'
      },
      {
        _type: 'Feature',
        level: 9,
        name: 'Enhanced Blink',
        description: `At 9th level, you become capable of focusing your Light to teleport longer distances. The range of your Blink feature increases to 15 feet. If you cast Blink, you also gain active camouflage for the next minute.`,
        source: 'Hunter Bladedancer Whisper'
      },
      {
        _type: 'Feature',
        level: 17,
        name: 'Backstap',
        description: `At 17th level, you combine your skill at sensing and exploiting your enemies' tactical weaknesses with your paracausal ability to suddenly be where you weren't. When you use your Blink feature and immediately make a successful attack against a target that is Surprised, you can double the damage of your attack.`,
        source: 'Hunter Bladedancer Whisper'
      }
    ]
  }

  id = 'bladedancer'
  class = Class.Hunter
  element = LightElement.Arc
  classAbility = ClassAbility.HuntersDodge
  name = 'Hunter Bladedancer'
  shieldDie = 8
  perks = [
    ...HunterClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Bladedancer',
      description: `As a Bladedancer, you gain the following class features in addition to the features granted by the Hunter superclass.
### Health Points and Shields
- **Shield Die Size**: d8
- **Health Points at 1st level**: 8 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 8
- **Shield Points at higher levels**: 1d8 (or 5) for every level after 1st

### Proficiencies
- **Armor**: Light armor
- **Weapons**:  Simple melee weapons, longswords, shortswords, smallswords, combat bows, simple firearms, fusion rifles, linear fusion rifles, sniper rifles
- **Toolkit**: Thieves' toolkit
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Dexterity, Intelligence
- **Skills (Choose 4)**:  Acrobatics, Deception, Insight, Intimidation, Investigation, Perception, Persuasion, Sleight of Hand, Stealth, Technology`,
      skillOptions: [{ choice: 4, skills: [Skill.Acrobatics, Skill.Deception, Skill.Insight, Skill.Intimidation, Skill.Investigation, Skill.Perception, Skill.Persuasion, Skill.SleightOfHand, Skill.Stealth, Skill.Technology] }],
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Tricks of the Trade',
      description: `Your inclination toward careful practice has made you quite adept in a variety of skills. You also recognize others don't so readily grasp how to do things best—but when it comes to whatever is in your wheelhouse, you can show them how it's done.

Choose two of your skill proficiencies, or one of your skill proficiencies and your proficiency with a toolkit or a vehicle. These are your Tricks of the Trade, and your proficiency bonus is doubled for any ability check you make that uses either of your Tricks of the Trade, if it does not already benefit from this effect.

When another creature makes an ability check using either of your Tricks of the Trade, you can use your reaction to add your proficiency bonus to the creature's roll. You can only do this if the creature does not have a trait, feature, or other source that allows them to double their own proficiency bonus for the ability check. A creature can only benefit from Tricks of the Trade once on their ability check.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Expert Attack',
      description: `Even at 1st level, you make your strikes carefully, with expert precision. You can invoke this feature to increase the damage you deal with a successful attack by 1d6. You can only do this if you had advantage on the attack roll and if, in the case of weapon attacks, the attack used a finesse or ranged weapon, or a firearm that doesn't have the High Recoil or Payload properties.

You don't need advantage on the attack roll if another enemy of the target is within 5 feet of it, that enemy is not Incapacitated, and you don't have disadvantage on the attack roll.

The amount of the extra damage increases as you gain levels in this class, as shown in the Expert Attack column of the Bladedancer table.

Once you invoke this feature, you cannot invoke it again until the start of your next turn.`,
      damage: [
        { formula: '(([level] + 1)/2)d6', type: DamageType.Kinetic }
      ],
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Light Affinity',
      description: `When you reach 2nd level, you learn to harness the Light you possess, and you shape it in the form of arc Light. Intelligence is your Light ability score, and your Intelligence modifier is your Light ability modifier. You use your Intelligence modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make an Intelligence saving throw.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Fast-acting',
      description: `At 2nd level, you are light on your feet and move with precision and alacrity. You can use the Aim, Dash, or Hide action as a bonus action on each of your turns. If you begin Aiming as a bonus action with this feature, you spend no movement to begin Aiming, and you grant yourself the ability to invoke Expert Attack on the next shot you take with a firearm or ranged weapon even if you don't have advantage on the attack roll. However, you must take that shot before the start of your next turn and you must meet all other requirements of Expert Attack.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Hunter Bladedancer',
      abilityOptions: [
        GrenadeAbility.Arcbolt,
        GrenadeAbility.Flux,
        GrenadeAbility.Skip
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Bladedancer Ways',
      description: `At 2nd level, you commit yourself to a Bladedancer methodology: the Way of the Swiftcutter, the Way of the Arcstrider, or the Way of the Whisper, all detailed at the end of this class description. The path you choose grants you features at 2nd level, and again at 3rd, 6th, 9th, and 17th levels.`,
      source: 'Hunter Bladedancer',
      pathOptions: [
        Bladedancer.Swiftcutter,
        Bladedancer.Arcstrider,
        Bladedancer.Whisper
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Opportunity Strikes',
      description: `Starting at 5th level, you gain a heightened sensitivity to the narrow openings created by enemies' mistakes in combat. Your skill and quickness enable you to seize the opportunities created by their imprecision. If a creature you can see misses with an attack, you can choose to use your reaction to make an attack of opportunity against it, even if you're Aiming. The creature must be within range of your currently held weapon in order for you to do this.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Arc Twitch',
      description: `Beginning at 7th level, you can use your arc Light to grant yourself certain reflexive bonuses, helping you to nimbly dodge out of the way of area effects, such as a walker's main cannon or a colossus' missile swarm. Whenever you are subjected to an effect that allows you to make a Dexterity saving throw and you aren't Restrained, you can grant yourself one of the following benefits for that saving throw. You must choose a benefit before you make your saving throw.`,
      source: 'Hunter Bladedancer',
      featureOptions: [
        {
          _type: 'Feature',
          level: 7,
          name: 'Elegant Maneuver',
          description: `Arc Light surges through your body in an attempt to help you completely dodge the effect. If you succeed on your Dexterity saving throw, you take no damage from the effect, and if you fail, you only take half damage.`,
          source: 'Hunter Bladedancer'
        },
        {
          _type: 'Feature',
          level: 7,
          name: 'Lightning Strike',
          description: `After resolving your Dexterity saving throw, you can use your reaction to immediately make an attack of opportunity against the source of the effect, if the source is within range of your currently held weapon.`,
          source: 'Hunter Bladedancer'
        }
      ]
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 11,
      name: 'Field Expert',
      description: `By 11th level, experience has taught you much, and diligently practicing skills that interest you has expanded your expertise. You can choose two additional skills, toolkit proficiencies, or vehicle proficiencies you have to add to your Tricks of the Trade. If you make an ability check with one of your Tricks of the Trade and roll a 9 or lower on the d20, you can treat your roll as a 10 instead.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 13,
      name: 'Strength in Flexibility',
      description: `Beginning at 13th level, your capacity for flexible movement enables you to perform feats beyond the normal extent of your physical strength. If you are forced to make a Strength saving throw, you can make a Dexterity saving throw instead.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 14,
      name: 'Never Not Ready',
      description: `Starting at 14th level, you abide in a constant state of keen readiness. You have advantage on initiative rolls.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 15,
      name: 'Master Tradesman',
      description: `At 15th level, you are a true master of the skills in which you've chosen to specialize. If you invoke Tricks of the Trade to assist another creature with their ability check and they roll a 9 or lower on the d20, they can treat the roll as a 10 instead.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 18,
      name: 'Evasive',
      description: `By 18th level, your reaction times are so fast as to appear like well-honed natural reflexes. No attack roll can have advantage against you if you aren't Incapacitated.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Bladedancer'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Perfectionist',
      description: `At 20th level, you simply do not allow yourself to make mistakes in critical moments. If you make an attack roll or ability check, you can treat the d20 roll as a 20. You can do this before or after rolling the d20, but it must always be before you learn the result of your roll. Once you use this feature, you must complete a long rest before you can use it again.`,
      source: 'Hunter Bladedancer'
    }
  ]
  abilityProficiencies = [
    Ability.Dexterity,
    Ability.Intelligence
  ]
  armor = [
    ArmorItemType.LightArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    MartialWeaponItemType.Longsword,
    MartialWeaponItemType.Shortsword,
    MartialWeaponItemType.Smallsword,
    MartialWeaponItemType.CombatBow,
    WeaponItemType.PrimaryWeapon,
    HeavyWeaponItemType.FusionRifle,
    HeavyWeaponItemType.LinearFusionRifle,
    HeavyWeaponItemType.SniperRifle
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle,
    ToolItemType.ThievesTools
  ]
  lightModifier = Ability.Intelligence

  getPath (id: string|null) {
    switch (id) {
      case Bladedancer.Arcstrider.id: return Bladedancer.Arcstrider
      case Bladedancer.Swiftcutter.id: return Bladedancer.Swiftcutter
      case Bladedancer.Whisper.id: return Bladedancer.Whisper
      default: return null
    }
  }
}

export class Nightstalker implements Subclass {
  static Trapper: SubclassPath = {
    id: 'trapper',
    name: 'Trapper',
    super: SuperAbility.Shadowshot,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Trapper',
        description: `Nightstalkers are famous for the Dusk Bow they wield, and the soaring arrows of concentrated void Light it sends forth. As a professional Trapper, you wreak stunning devastation by loosing a single such arrow, tethering whole packs of enemies to the point it strikes. Thus weakened, your foes are ripe prey for your allies—or for you to wipe out alone.`,
        source: 'Hunter Nightstalker Trapper'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Keen Scout',
        description: `At 3rd level, you are already an accomplished tracker with impressive situational awareness and perceptive skills. You gain the following benefits:
- Your base walking speed increases by 5 feet.
- When moving stealthily, creatures cannot rely on their passive Perception score to notice you.
- If a creature is Tethered by your Shadowshot super ability, you always know where they are for the next minute, if they are within 60 feet of you.`,
        source: 'Hunter Nightstalker Trapper'
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Ensnaring Smoke',
        description: `Also at 6th level, you adjust the formulation of your Smoke bomb to add an enervating effect. When you cast Smoke, all creatures of your choice who are in the area of your Smoke cloud, or who enter the area for the first time on a turn, must succeed on a Charisma saving throw against your Light save DC or become Restrained for the next minute. A creature can repeat the saving throw at the end of each of their turns, ending the effect early on itself on a success. A creature who succeeds on their saving throw, or for which the effect ends, becomes immune to being Restrained in this way for 24 hours.`,
        source: 'Hunter Nightstalker Trapper',
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Charisma }
        ]
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Vanishing Step',
        description: `At 11th level, you've come to understand the best way to keep out of sight is to use the Light to simply become invisible. When you spend a superclass ability charge on your Hunter's Dodge, you are also granted active camouflage that lasts until the start of your next turn. Additionally, when you cast Hunter's Dodge while two or more hostile creatures are within 15 feet of you, you can regain a melee ability charge. If there are 3 or more hostile creatures within 15 feet of you, you can regain both a melee and grenade ability charge instead.`,
        source: 'Hunter Nightstalker Trapper'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Black Hole',
        description: `Beginning at 15th level, you have attained a deeper attunement with the void, allowing you to draw upon more of its energy when you use your Dusk Bow. The duration of your Shadowshot increases to a number of minutes equal to your Wisdom modifier, and the range of its Void Anchor increases to 40 feet. You cannot lose concentration on the void anchor of your Shadowshot as a result of taking damage.`,
        source: 'Hunter Nightstalker Trapper'
      }
    ]
  }
  static Pathfinder: SubclassPath = {
    id: 'pathfinder',
    name: 'Pathfinder',
    super: SuperAbility.MoebiusQuiver,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Pathfinder',
        description: `The Nightstalker's Dusk Bow is a weapon, and as a pathfinder, you specialize in its use as a fighting tool. You learn to nock, draw, and loose a rapid barrage of arrows, each a death sentence written in void Light. Whether your sparkling darts bring down big game or strike multiple smaller foes, every one clears a path through the night for your allies.`,
        source: 'Hunter Nightstalker Pathfinder'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Shadestep',
        description: `At 3rd level, you already know how to slip into the hidden spaces behind the superficially visible. You gain the Shadestep focus action.`,
        source: 'Hunter Nightstalker Pathfinder',
        abilities: [
          OtherAbility.NightstalkerShadestep
        ]
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Vanish In Smoke',
        description: `At 6th level, you perfect a modification to your Smoke that endows it with short-term camouflaging capabilities for you and others. On the turn that the cloud from your Smoke appears, all creatures of your choice within the area gain active camouflage that lasts until the end of their next turn.`,
        source: 'Hunter Nightstalker Pathfinder'
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Combat Provision',
        description: `When you reach 11th level, your connection to the void has grown stronger, giving you greater insight into channeling your Light. If a creature takes damage from your Light grenade, you have advantage on melee recharge rolls you make until the end of your next turn. When you invoke Vanish in Smoke, you can make a grenade recharge roll once for each allied creature that gains active camouflage.`,
        source: 'Hunter Nightstalker Pathfinder'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Heart Of The Pack',
        description: `At 15th level, you deeply value those you allow to become your comrades. When you strike a killing blow, it is as much for them as for yourself. Once on your turn, if you reduce a creature to 0 hit points with the damage from your Moebius Quiver, you and all other Risen creatures of your choice within 40 feet of you can make a shield recharge roll.`,
        source: 'Hunter Nightstalker Pathfinder'
      }
    ]
  }
  static Wraith: SubclassPath = {
    id: 'wraith',
    name: 'Wraith',
    super: SuperAbility.SpectralBlades,
    perks: [
      {
        _type: 'Feature',
        level: 3,
        name: 'Subclass Path: Wraith',
        description: `Not all Nightstalkers prefer to unleash their Light from a distance. Under the profession of the wraith, you call upon the void not for arrows, but for blades, and a supernatural awareness of your enemies that allows you to stalk any battlefield with impunity and strike with deadly accuracy.`,
        source: 'Hunter Nightstalker Wraith'
      },
      {
        _type: 'Feature',
        level: 3,
        name: 'Truesight',
        description: `Also at 3rd level, your sensory perception is conducted by all-pervasive void energy, endowing you with a kind of second sight. You gain the Truesight focus action.`,
        source: 'Hunter Nightstalker Wraith',
        abilities: [
          OtherAbility.NightstalkerTruesight
        ]
      },
      {
        _type: 'Feature',
        level: 6,
        name: 'Corrosive Smoke',
        description: `At 6th level, you concoct a modified formula for the composition of your Smoke. When you cast Smoke, you may choose to make it so that all creatures other than you within the area of the cloud must make a Constitution saving throw against your Light save DC. On a failed save, they become Poisoned for 1 minute. Creatures Poisoned in this way take 2d6 void damage at the start of each of their turns. A creature can repeat the saving throw at the end of each of their turns, ending the effect on itself early on a success.

Until the cloud dissipates, if a creature enters the cloud for the first time on a turn and isn't already Poisoned, they must make the same saving throw against this effect. 

The damage of this feature increases as you gain Nightstalker levels. It becomes 3d6 at 11th level, and 4d6 at 17th level.`,
        source: 'Hunter Nightstalker Wraith',
        damage: [
          { formula: '(1 + (([level] + 1) / 6))d6', type: DamageType.Void}
        ],
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      },
      {
        _type: 'Feature',
        level: 11,
        name: 'Flawless Execution',
        description: `Beginning at 11th level, with a deepening connection to the void, invoking its power through your Light heightens your perception even more readily. While you are not missing any health points, killing a creature on your turn while Aiming grants you active camouflage for 1 minute. In addition, while you have active camouflage from any source, you are also granted the benefit of your Truesight focus action.`,
        source: 'Hunter Nightstalker Wraith'
      },
      {
        _type: 'Feature',
        level: 15,
        name: 'Unseen Strike',
        description: `When you reach 15th level, you have perfected the devastating technique of an invisible surprise strike. If you hit a creature with a melee attack while under the effects of active camouflage, they must also make a Constitution saving throw against your Light save DC. They become Weakened for the next minute on a failed save. A creature Weakened in this way has any damage they deal reduced by 1d10 + your Wisdom modifier. A Weakened creature can repeat the saving throw at the end of each of their turns, ending the effect early on a success. A creature who succeeds on the saving throw, or for which the effect ends, becomes immune to being Weakened by this feature for 24 hours.`,
        source: 'Hunter Nightstalker Wraith',
        damage: [
          { formula: '1d10 + [wisMod]', type: DamageType.Kinetic }
        ],
        savingThrowFormula: [
          { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
        ]
      }
    ]
  }

  id = 'nightstalker'
  class = Class.Hunter
  element = LightElement.Void
  classAbility = ClassAbility.HuntersDodge
  name = 'Hunter Nightstalker'
  shieldDie = 8
  perks = [
    ...HunterClassFeatures,
    {
      _type: 'Feature',
      level: 1,
      name: 'Subclass: Nightstalker',
      description: `As a Nightstalker, you gain the following class features in addition to the features granted by the Hunter superclass.
### Health Points and Shields
- **Shield Die Size**: d8
- **Health Points at 1st level**: 8 + your Constitution modifier
- **Health Points at higher levels**: Add your Constitution modifier for every level after 1st, minimum 0
- **Shield Points at 1st level**: 8
- **Shield Points at higher levels**: 1d8 (or 5) for every level after 1st

### Proficiencies
- **Armor**: Light armor, medium armor
- **Weapons**: Simple melee weapons, longswords, shortswords, smallswords, combat bows, simple firearms, fusion rifles, linear fusion rifles, sniper rifles
- **Vehicles**: Jumpships, sparrows
- **Saving Throws**: Strength, Dexterity
- **Skills (Choose 3)**: Acrobatics, Animal Handling, Arcana, Athletics, Insight, Investigation, Perception, and Stealth`,
      skillOptions: [{ choice: 3, skills: [Skill.Acrobatics, Skill.AnimalHandling, Skill.Arcana, Skill.Athletics, Skill.Insight, Skill.Investigation, Skill.Perception, Skill.Stealth] }],
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Nightstalker\'s Focus',
      description: `Even at 1st level, your preternatural focus grants you heightened awareness, including keen sensitivity to the weaknesses of your enemies. Once on your turn, you may spend focus to perform one of the focus actions detailed at the end of this class description. You cannot perform a focus action if you don't have enough focus points, or if you don't meet the level prerequisite. Unless the focus action describes otherwise, you spend focus immediately when you perform the focus action.

You have a maximum amount of focus determined by your Nightstalker class level, as shown in the Max Focus column of the Nightstalker table. At the start of each of your turns, you regain one point of focus. If you end your turn without having spent any focus, you regain one additional point of focus. You regain all focus when you complete a brief rest.
      
If you use a focus action that requires concentration, you regain no focus at the start of your turn while concentrating on it. You cannot begin concentration on a focus action if doing so would reduce you to 0 focus, and if you fall to 0 focus while concentrating on a focus action, you immediately lose concentration.`,
      source: 'Hunter Nightstalker',
      abilities: [
        OtherAbility.NightstalkerAmplifiedHits,
        OtherAbility.NightstalkerFlatline,
        OtherAbility.NightstalkerStudy,
        OtherAbility.NightstalkerFarsight,
        OtherAbility.NightstalkerAtTheReady,
        OtherAbility.NightstalkerPhantomStride,
        OtherAbility.NightstalkerPredatorsEye,
        OtherAbility.NightstalkerStabilize,
        OtherAbility.NightstalkerSpectersShroud,
        OtherAbility.NightstalkerStunningBlow,
        OtherAbility.NightstalkerRefresh
      ]
    },
    {
      _type: 'Feature',
      level: 1,
      name: 'Born of the Wild',
      description: `The void calls to you, drawing you to the quietude of empty spaces. You typically feel more at home in the wilds than within the confines of the City walls, and as such you are particularly familiar with natural environments.
      
You have the following benefits:
- You become proficient in the Nature skill if you weren't already.
- You gain a climbing speed and swimming speed equal to your base walking speed.
- You have superior vision in dim and dark conditions, granting you darkvision. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. If you already had darkvision, the range of your darkvision increases by 30 feet.
- Nonmagical and non-paracausal difficult terrain doesn't slow your movement.
- If you are traveling alone, you can move stealthily at a normal pace.`,
      skillOptions: [Skill.Nature],
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Light Affinity',
      description: `When you reach 2nd level, you know how to harness the Light you possess, and you shape it in the form of void Light. Wisdom is your Light ability score, and your Wisdom modifier is your Light ability modifier. You use your Wisdom modifier to determine your Light attack modifier and to set the save DC against your Light abilities, as shown below. When you are told to make a Light ability saving throw, you make a Wisdom saving throw.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Melee Ability',
      description: `At 2nd level you gain your melee ability option, Smoke, which you cast using melee ability charges. You have a single melee ability charge. You regain spent melee ability charges when you complete a brief rest.`,
      source: 'Hunter Nightstalker',
      abilityOptions: [
        MeleeAbility.Smoke
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: LightGrenade.name,
      description: LightGrenade.description,
      source: 'Hunter Nightstalker',
      abilityOptions: [
        GrenadeAbility.Spike,
        GrenadeAbility.Voidwall,
        GrenadeAbility.Vortex
      ]
    },
    {
      _type: 'Feature',
      level: 2,
      name: 'Draw From The Void',
      description: `At 2nd level, your communion with the void gives you an atypical perspective on things, and a tendency to see what others do not. You can always choose to use your Wisdom ability score when determining your modifiers for Arcana, Investigation, and Nature checks.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 3,
      name: 'Nightstalker Profession',
      description: `At 3rd level, you choose to focus on and train in a Nightstalker profession: the Trapper, the Pathfinder, or the Wraith, all detailed at the end of this class description. Your choice grants you features at 3rd level and again at 6th, 11th, and 15th levels.`,
      source: 'Hunter Nightstalker',
      pathOptions: [
        Nightstalker.Trapper,
        Nightstalker.Pathfinder,
        Nightstalker.Wraith
      ]
    },
    {
      _type: 'Feature',
      level: 4,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 5,
      name: 'Extra Attack',
      description: `Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 7,
      name: 'Stalwart',
      description: `At 7th level, experience working with the energies of the void has taught you to keep on your toes. You gain one of the following features of your choice.`,
      source: 'Hunter Nightstalker',
      featureOptions: [
        {
          _type: 'Feature',
          level: 7,
          name: 'Evasion',
          description: `If you are subject to a Dexterity saving throw to only take half damage from an effect, you instead take no damage if you succeed, and only half damage if you fail.`,
          source: 'Hunter Nightstalker'
        },
        {
          _type: 'Feature',
          level: 7,
          name: 'Defensive Rebound',
          description: `If you take damage from a creature's attack, until the end of the creature's turn, you have a bonus +4 to your AC against any subsequent attacks the creature makes against you, and a bonus +4 to any subsequent saving throws the creature causes you to make.`,
          source: 'Hunter Nightstalker'
        },
        {
          _type: 'Feature',
          level: 7,
          name: 'Steel Will',
          description: `You have advantage on all saving throws you make to prevent yourself from becoming Frightened, and on Constitution saving throws you make to maintain concentration.`,
          source: 'Hunter Nightstalker'
        }
      ]
    },
    {
      _type: 'Feature',
      level: 8,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 10,
      name: 'Specter\'s Skills',
      description: `At 10th level, you have mastered conventional and paracausal stealth techniques, using the Light to become part of the darkness when you move in the shadows, undetectable by eyes and scanners alike. If you are in darkness and moving stealthily, you are invisible to any creature that relies on darkvision or scanners to see you, and creatures have disadvantage on Wisdom (Perception) checks to notice you otherwise.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 11,
      name: 'Sure Strikes',
      description: `At 11th level, you are accustomed to striking killing blows with no hesitation. Your Amplified Hits focus action costs no focus to use, and you can add your Wisdom modifier to the damage of Amplified Hits. Using Amplified Hits still counts as using your focus action for your turn.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 12,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 14,
      name: 'Silent Step',
      description: `At 14th level, after so long practicing evasion and disappearance, becoming unseen is second nature to you. You can use the Hide action as a bonus action on your turn.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 16,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 18,
      name: 'Unearthly Senses',
      description: `At 18th level, your perceptive senses are supernaturally acute, to the point that you can practically see the invisible. When you attack a creature you can't see, your inability to see it doesn't impose disadvantage on your attack rolls against it. You are also aware of the exact location of any invisible creature within 30 feet of you, provided you are aware that the creature is within 30 feet of you at all, and you aren't Blinded or Deafened.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 19,
      name: 'Ability Score Improvement',
      description: `You can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.`,
      source: 'Hunter Nightstalker'
    },
    {
      _type: 'Feature',
      level: 20,
      name: 'Lucid Hunter',
      description: `At 20th level, your unbreakable connection to the all-pervasive void keeps your mind clear as an interstellar vacuum, and replenishes you in an instant when you call upon it. As a bonus action on your turn, you can regain all spent focus. Once you use this feature, you must complete a short or long rest before you can use it again.`,
      source: 'Hunter Nightstalker'
    }
  ]
  charges: SubclassCharge = {
    name: 'Focus',
    type: 'ability',
    values: {
      1: 2,
      2: 2,
      3: 4,
      4: 4,
      5: 6,
      6: 7,
      7: 8,
      8: 8,
      9: 8,
      10: 10,
      11: 10,
      12: 10,
      13: 12,
      14: 12,
      15: 12,
      16: 14,
      17: 14,
      18: 14,
      19: 16,
      20: 16
    }
  }
  abilityProficiencies = [
    Ability.Strength,
    Ability.Dexterity
  ]
  armor = [
    ArmorItemType.LightArmor,
    ArmorItemType.MediumArmor
  ]
  weapons = [
    WeaponItemType.SimpleWeapon,
    MartialWeaponItemType.Longsword,
    MartialWeaponItemType.Shortsword,
    MartialWeaponItemType.Smallsword,
    MartialWeaponItemType.CombatBow,
    WeaponItemType.PrimaryWeapon,
    HeavyWeaponItemType.FusionRifle,
    HeavyWeaponItemType.LinearFusionRifle,
    HeavyWeaponItemType.SniperRifle
  ]
  tools = [
    VehicleItemType.AirVehicle,
    VehicleItemType.LandVehicle
  ]
  lightModifier = Ability.Wisdom

  getPath (id: string|null) {
    switch (id) {
      case Nightstalker.Trapper.id: return Nightstalker.Trapper
      case Nightstalker.Pathfinder.id: return Nightstalker.Pathfinder
      case Nightstalker.Wraith.id: return Nightstalker.Wraith
      default: return null
    }
  }
}

export const allSubclasses: Subclass[] = [
  new Sunbreaker,
  new Striker,
  new Defender,
  new Sunsinger,
  new Stormcaller,
  new Voidwalker,
  new Gunslinger,
  new Bladedancer,
  new Nightstalker
]
export const subclassIndex = allSubclasses.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<string, Subclass>)
export const getSubclass = (id: string) => subclassIndex[id]
