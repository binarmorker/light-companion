import { Ability, Size, Race, Feature, DamageType, Action, Enemy } from '@/models/destiny'

export class AwokenCorsair implements Enemy {
  name = 'Awoken Corsair'
  size = Size.Medium
  species = Race.Awoken
  speciesText = 'Medium human (Awoken)'
  description = `Awoken Corsairs are a detachment of the Royal Armada of the Reef. They serve as combat pilots, scouts, and special forces infantry for the Awoken Queen. Because of the disproportionate gender distribution among the Awoken, most (though not all) Corsairs are women.

The Corsair uniform is a lightly armored, formfitting pressure suit designed for mobility in space. The its headgear is usually worn open, partly revealing the wearer's face, but it seals for operation in vacuum. Standard armament is a Reefmade sidearm and a long, curved knife, but Corsairs sometimes carry a variety of other small arms.

Although usually more or less on the same side, relations between Corsairs and Guardians are often somewhat tense. Lacking the Traveler's gift and its benefit of resurrection, many Corsairs feel Guardians take their immortality too lightly.`
  ac = 12
  armor = 'light armor'
  healthPoint = 33
  healthPointRoll = '6d8+5'
  speed = 30
  climb = 20
  abilities = {
    [Ability.Strength]: 12,
    [Ability.Dexterity]: 14,
    [Ability.Constitution]: 13,
    [Ability.Intelligence]: 14,
    [Ability.Wisdom]: 12,
    [Ability.Charisma]: 13
  }
  savingThrows = [Ability.Dexterity, Ability.Wisdom]
  skills = 'Athletics +3, Perception +3, Survival +3, and any three others'
  vehiclesProficiency = 'jumpships, sparrows'
  senses = 'passive Perception 13'
  languages = 'Risen, RSL, Awoken Speech'
  crIndividual = 0.25
  crClassification = 1
  crText = 'Individual CR 1/4 (50 XP), Classification CR 1 Elite (200 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Paracausal Resistance',
      description: `The Corsair has advantage on saving throws against spells, Light abilities, and Darkness effects.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Sidearm',
      description: `Firearm Weapon Attack: +4 to hit, scope 20/30/40 (close), one target. Hit: 5 (1d6+2) kinetic damage.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1d6 + [dexMod]', type: DamageType.Kinetic }
      ]
    },
    {
      _type: 'Action',
      name: 'Shortsword',
      description: `Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) piercing damage.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1d6 + [dexMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class Batdactyl implements Enemy {
  name = 'Batdactyl'
  size = Size.Tiny
  species = Race.Beast
  speciesText = 'Tiny beast'
  description = `Batdactyl is the colloquial name for a species of seemingly reptilian flying creatures found on Venus. They have brown bodies and light tan wings like a bat, but a tail that more resembles a lizard's. They stand upright and hop along the ground like birds when not in flight. Another very similar species has been identified on Nessus, though it has a more colorful pink and purple body with a head that resembles a gecko with goat eyes.`
  ac = 12
  armor = 'natural armor'
  healthPoint = 3
  healthPointRoll = '1d6-1'
  speed = 5
  fly = 30
  abilities = {
    [Ability.Strength]: 3,
    [Ability.Dexterity]: 15,
    [Ability.Constitution]: 8,
    [Ability.Intelligence]: 2,
    [Ability.Wisdom]: 12,
    [Ability.Charisma]: 4
  }
  skills = 'Perception +3'
  senses = 'passive Perception 13'
  crIndividual = 0
  crClassification = 0
  crText = ' Individual CR 0 (10 XP), Classification CR 0 Minion (10 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Keen Sight',
      description: `The batdactyl has advantage on Wisdom (Perception) checks that rely on sight.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Swoop',
      description: `The batdactyl doesn't provoke opportunity attacks when it flies into or out of an enemy's reach.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Beak',
      description: `Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) piercing damage.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1d4 + [dexMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class Bee implements Enemy {
  name = 'Bee'
  size = Size.Tiny
  species = Race.Beast
  speciesText = 'Tiny beast'
  description = ``
  ac = 16
  armor = 'natural armor'
  healthPoint = 1
  healthPointRoll = '1d1'
  fly = 40
  abilities = {
    [Ability.Strength]: 1,
    [Ability.Dexterity]: 23,
    [Ability.Constitution]: 8,
    [Ability.Intelligence]: 2,
    [Ability.Wisdom]: 13,
    [Ability.Charisma]: 5
  }
  skills = 'Nature +0, Perception +3'
  damageImmunities = [DamageType.Psychic]
  conditionImmunities = ['Charmed', 'Frightened']
  senses = 'passive Perception 13'
  crIndividual = 0.125
  crClassification = 1
  crText = 'Individual CR 1/8 (25 XP), Classification CR 1 Minion (200 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Allergic Reaction',
      description: `If the bee stings a creature, that creature must roll a d20. On a 1 or 2, the creature develops an allergy to bee stings and takes an additional 25 (10d4) poison damage.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Unimposing Size',
      description: `The bee can occupy the space of another creature, and vice versa. It costs no additional movement for the bee to move through the space of another creature.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Sting',
      description: `Melee Weapon Attack: +8 to hit, reach 0 ft., one target. Hit: 1 piercing damage. The bee also deals 1 piercing damage to itself on a hit.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class CityMilitiaman implements Enemy {
  name = 'City Militiaman'
  size = Size.Medium
  species = Race.Human
  speciesText = 'Medium human (any race)'
  description = ``
  ac = 14
  armor = 'light armor'
  healthPoint = 16
  healthPointRoll = '3d8+3'
  speed = 30
  climb = 20
  swim = 20
  abilities = {
    [Ability.Strength]: 14,
    [Ability.Dexterity]: 12,
    [Ability.Constitution]: 12,
    [Ability.Intelligence]: 11,
    [Ability.Wisdom]: 12,
    [Ability.Charisma]: 10
  }
  skills = 'Athletics +4, Perception +3, and any three others'
  vehiclesProficiency = 'jumpships'
  senses = 'passive Perception 13'
  languages = 'Risen, RSL, and City common'
  crIndividual = 0.25
  crClassification = 2
  crText = 'Individual CR 1/2 (100 XP), Classification CR 2 Soldier (450 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Squad Tactics',
      description: `The City militiaman has advantage on weapon attacks it makes against targets that have been attacked by an ally of the City militiaman before the start of the City militiaman's turn.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Auto Rifle',
      description: `Firearm Weapon Attack: +4 to hit, scope 30/40/75 (close), one target. Hit: 7 (2d6d1+2) kinetic damage.`,
      attackFormula: '1d20 + [strMod] + [prof]',
      damage: [
        { formula: '2d6d1 + [strMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class Frame implements Enemy {
  name = 'Frame'
  size = Size.Medium
  species = Race.Construct
  speciesText = 'Medium construct (VI, City tech)'
  description = `Frames are humanoid, non-sentient machines built to serve in a variety of roles, mostly acting as assistants, janitors, or secretaries. They often have rudimentary ‘personalities' due to accretive quirks in their programming that naturally develop over time. Frames that work with a particular person or in a single role for extended periods, such as the Crucible quartermaster Arcite 99-40, tend to develop the strongest expression of these traits.`
  ac = 13
  armor = 'natural armor'
  healthPoint = 5
  healthPointRoll = '1d8+1'
  speed = 30
  abilities = {
    [Ability.Strength]: 13,
    [Ability.Dexterity]: 9,
    [Ability.Constitution]: 12,
    [Ability.Intelligence]: 12,
    [Ability.Wisdom]: 12,
    [Ability.Charisma]: 9
  }
  damageImmunities = [DamageType.Poison, DamageType.Psychic]
  conditionImmunities = ['Blinded', 'Burning', 'Charmed', 'Deafened', 'Exhaustion', 'Frightened', 'Petrified', 'Poisoned']
  senses = 'darkvision 30 ft., passive Perception 13'
  languages = 'Risen, RSL, and all modern languages of humanity'
  crIndividual = 0.125
  crClassification = 1
  crText = 'Individual CR 1/8 (25 XP), Classification CR 1 Minion (200 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Vanguard Network (range 30 ft)',
      description: `The frame can create a wireless link between itself and other creatures with the Vanguard Network feature that are within range. Linked creatures can share information such as their operational status, the friendly/hostile status of other creatures, and the location of creatures they can see.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Fists',
      description: `Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4+1) bludgeoning damage.`,
      attackFormula: '1d20 + [strMod] + [prof]',
      damage: [
        { formula: '1d4 + [strMod]', type: DamageType.Kinetic }
      ]
    },
    {
      _type: 'Action',
      name: 'Broom',
      description: `Melee Weapon Attack: +3 to hit, reach 10 ft., one target. Hit: 4 (1d6+1) bludgeoning damage.`,
      attackFormula: '1d20 + [strMod] + [prof]',
      damage: [
        { formula: '1d4 + [strMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class CombatFrame implements Enemy {
  name = 'Combat Frame'
  size = Size.Medium
  species = Race.Construct
  speciesText = 'Medium construct (VI, City tech)'
  description = ``
  ac = 14
  armor = 'natural armor'
  healthPoint = 26
  healthPointRoll = '4d8+8'
  speed = 30
  abilities = {
    [Ability.Strength]: 14,
    [Ability.Dexterity]: 9,
    [Ability.Constitution]: 14,
    [Ability.Intelligence]: 14,
    [Ability.Wisdom]: 14,
    [Ability.Charisma]: 9
  }
  damageImmunities = [DamageType.Poison, DamageType.Psychic]
  conditionImmunities = ['Blinded', 'Burning', 'Charmed', 'Deafened', 'Exhaustion', 'Frightened', 'Petrified', 'Poisoned']
  senses = 'darkvision 30 ft., passive Perception 13'
  languages = 'Risen, RSL, and all modern languages of humanity'
  crIndividual = 0.5
  crClassification = 2
  crText = 'Individual CR 1/2 (100 XP), Classification CR 2 Soldier (450 XP)'
  proficiencyBonus = 2
  hackDevice = 27
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Vanguard Network (range 30 ft)',
      description: `The frame can create a wireless link between itself and other creatures with the Vanguard Network feature that are within range. Linked creatures can share information such as their operational status, the friendly/hostile status of other creatures, and the location of creatures they can see.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Auto Rifle',
      description: `Firearm Weapon Attack: +6 to hit, scope 30/40/75 (close), one target. Hit: 7 (2d6d1+2) kinetic damage.`,
      attackFormula: '1d20 + [strMod] + [strMod] + [prof]',
      damage: [
        { formula: '2d6d1 + [strMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class IronWolf implements Enemy {
  name = 'Iron Wolf'
  size = Size.Medium
  species = Race.Beast
  speciesText = 'Medium beast'
  description = `The iron wolves are a particular lineage of wolf, bred and raised by the ancient Iron Lords, the first of the Risen to join together to protect the remnants of Humanity. They are tough and fearless creatures, unswayed by the sound of gunfire or the howls of their enemies.`
  ac = 13
  armor = 'natural armor'
  healthPoint = 18
  healthPointRoll = '4d8'
  speed = 30
  abilities = {
    [Ability.Strength]: 14,
    [Ability.Dexterity]: 13,
    [Ability.Constitution]: 11,
    [Ability.Intelligence]: 7,
    [Ability.Wisdom]: 14,
    [Ability.Charisma]: 8
  }
  skills = 'Athletics +4, Perception +4, Stealth +4'
  senses = 'passive Perception 13'
  languages = 'understands Risen commands'
  crIndividual = 0.25
  crClassification = 2
  crText = 'Individual CR 1/4 (50 XP), Classification CR 2 Minion (450 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Keen Hearing and Smell',
      description: `The iron wolf has advantage on Wisdom (Perception) checks that rely on hearing or scent.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Pack Tactics',
      description: `The iron wolf has advantage on an attack roll against a creature if at least one of the iron wolf's allies is within 5 feet of the creature and the ally isn't incapacitated.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Takedown',
      description: `If the iron wolf has Grappled a Medium or smaller creature, it can use its bonus action to cause the creature to make a DC 13 Strength saving throw. The creature is knocked Prone on a failed save. The DC increases by 2 for each iron wolf already grappling the creature.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Bite',
      description: `Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) piercing damage and the target is Grappled by the iron wolf (escape DC 13). A Medium or smaller creature Grappled in this way also becomes Restrained.`,
      attackFormula: '1d20 + [strMod] + [prof]',
      damage: [
        { formula: '1d4 + [strMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class NessianToad implements Enemy {
  name = 'Nessian Toad'
  size = Size.Tiny
  species = Race.Beast
  speciesText = 'Tiny beast'
  description = ``
  ac = 9
  armor = 'natural armor'
  healthPoint = 1
  healthPointRoll = '1d1'
  speed = 10
  climb = 10
  abilities = {
    [Ability.Strength]: 1,
    [Ability.Dexterity]: 16,
    [Ability.Constitution]: 7,
    [Ability.Intelligence]: 1,
    [Ability.Wisdom]: 8,
    [Ability.Charisma]: 2
  }
  senses = 'passive Perception 9'
  crIndividual = 0
  crClassification = 0
  crText = 'Individual CR 0 (10 XP), Classification CR 0 Minion (10 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Poisonous Flesh',
      description: `If a creature consumes the Nessian toad, it must succeed on a DC 9 Constitution saving throw or become Poisoned for 8 (2d4+3) days.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Sticky Feet',
      description: `The Nessian toad can move up, down, and across vertical surfaces and upside-down along ceilings without having to make an ability check.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Standing Leap',
      description: `The Nessian toad's long jump is up to 10 ft. and its high jump is up to 5 ft., with or without a running start.`,
      level: 0
    }
  ]
}

export class Pilgrim implements Enemy {
  name = 'Pilgrim'
  size = Size.Medium
  species = Race.Human
  speciesText = 'Medium human (any race)'
  description = ``
  ac = 12
  armor = 'light armor'
  healthPoint = 11
  healthPointRoll = '2d8+2'
  speed = 30
  climb = 20
  swim = 20
  abilities = {
    [Ability.Strength]: 11,
    [Ability.Dexterity]: 12,
    [Ability.Constitution]: 12,
    [Ability.Intelligence]: 10,
    [Ability.Wisdom]: 10,
    [Ability.Charisma]: 10
  }
  skills = 'Perception +2, Survival +2, and any three others'
  senses = 'passive Perception 12'
  languages = 'any one modern human language'
  crIndividual = 0.125
  crClassification = 1
  crText = 'Individual CR 1/8 (25 XP), Classification CR 1 Minion (200 XP)'
  proficiencyBonus = 2
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Dagger',
      description: `Melee Weapon Attack: +3 to hit, reach 5 ft., thrown 20/30 ft., one target. Hit: 3 (1d4+1) piercing damage.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1d4 + [dexMod]', type: DamageType.Kinetic }
      ]
    },
    {
      _type: 'Action',
      name: 'Sidearm',
      description: `Firearm Weapon Attack: +3 to hit, scope 20/30/40 (close), one target. Hit: 4 (1d6+1) kinetic damage.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1d6 + [dexMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class SwarmOfBatDactyls implements Enemy {
  name = 'Swarm of Batdactyls'
  size = Size.Large
  species = Race.Beast
  speciesText = 'Large swarm of Tiny beasts'
  description = ``
  ac = 12
  armor = 'natural armor'
  healthPoint = 77
  healthPointRoll = '14d10'
  speed = 10
  fly = 30
  abilities = {
    [Ability.Strength]: 4,
    [Ability.Dexterity]: 14,
    [Ability.Constitution]: 10,
    [Ability.Intelligence]: 3,
    [Ability.Wisdom]: 14,
    [Ability.Charisma]: 3
  }
  savingThrows = [Ability.Dexterity]
  skills = 'Perception +4'
  senses = 'passive Perception 14'
  crIndividual = 1
  crClassification = 1
  crText = 'Individual CR 1 (200 XP), Classification CR 1 Major (200 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Keen Sight',
      description: `The swarm has advantage on Wisdom (Perception) checks that rely on sight.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Swoop',
      description: `The swarm doesn't provoke opportunity attacks when it flies into or out of an enemy's reach.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Swarm',
      description: `The swarm can occupy another creature's space and vice versa, and the swarm can move through a space small enough for a Tiny beast. The swarm can't regain hit points or gain temporary hit points.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Multiattack',
      description: `The swarm makes four attacks with its beak. If the swarm is at half health points or less, it only makes two beak attacks instead.`
    },
    {
      _type: 'Action',
      name: 'Beak',
      description: `Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d4+2) piercing damage.`,
      attackFormula: '1d20 + [dexMod] + [prof]',
      damage: [
        { formula: '1d4 + [dexMod]', type: DamageType.Kinetic }
      ]
    }
  ]
}

export class SwarmOfBees implements Enemy {
  name = 'Swarm of Bees'
  size = Size.Medium
  species = Race.Beast
  speciesText = 'Medium swarm of Tiny beasts'
  description = ``
  ac = 17
  armor = 'natural armor'
  healthPoint = 36
  healthPointRoll = '8d8'
  fly = 40
  abilities = {
    [Ability.Strength]: 1,
    [Ability.Dexterity]: 24,
    [Ability.Constitution]: 10,
    [Ability.Intelligence]: 2,
    [Ability.Wisdom]: 14,
    [Ability.Charisma]: 5
  }
  skills = 'Nature +0, Perception +4'
  damageVulnerabilities = [DamageType.Stasis, DamageType.Solar]
  damageImmunities = [DamageType.Psychic]
  conditionImmunities = ['Blinded', 'Charmed', 'Frightened', 'Grappled', 'Paralyzed', 'Petrified', 'Poisoned', 'Prone', 'Restrained', 'Stunned']
  senses = 'passive Perception 13'
  crIndividual = 3
  crClassification = 3
  crText = 'Individual CR 3 (700 XP), Classification CR 3 Major (700 XP)'
  proficiencyBonus = 2
  features: Feature[] = [
    {
      _type: 'Feature',
      name: 'Allergic Reaction',
      description: `If the swarm stings a creature, that creature must roll a d20. On a 1 or 2, the creature develops an allergy to bee stings and takes an additional 25 (10d4) poison damage.`,
      level: 0
    },
    {
      _type: 'Feature',
      name: 'Swarm',
      description: `The swarm can occupy another creature's space and vice versa, and the swarm can move through a space small enough for a Tiny beast. The swarm can't regain hit points or gain temporary hit points.`,
      level: 0
    }
  ]
  actions: Action[] = [
    {
      _type: 'Action',
      name: 'Multiattack',
      description: `The swarm attacks four times with its sting. If the swarm has half its hit points or fewer, it makes two attacks instead.`
    },
    {
      _type: 'Action',
      name: 'Sting',
      description: `Melee Weapon Attack: +9 to hit, reach 0 ft., one target. Hit: 1 piercing damage and the swarm also deals 4 piercing damage to itself.`,
      attackFormula: '1d20 + [dexMod] + [prof] + 1',
      damage: [
        { formula: '1', type: DamageType.Kinetic }
      ]
    }
  ]
}
