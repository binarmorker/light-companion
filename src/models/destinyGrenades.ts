import { Ability, ChargeType, DamageType, GrenadeAbility, LightAbility, LightElement } from '@/models/destiny'

export class ArcboltGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Arcbolt
  element = LightElement.Arc
  name = 'Arcbolt Grenade'
  description = `You throw this grenade at a hard surface within range and the Light nestled within bursts out, striking at up to three targets of your choice within 15 feet of its impact point. Each target must make a Constitution saving throw. A target takes 3d6 arc damage on a failed save, or half as much on a success. Targets that fail their saving throw also become Electrified for the next minute. A target can make a Constitution saving throw at the end of each of its turns, ending the effect on itself early on a success.

**At Higher Levels.** The damage of this Light ability increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 7'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d6', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class AxionBoltGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.AxionBolt
  element = LightElement.Void
  name = 'Axion Bolt Grenade'
  description = `You throw this grenade of void Light onto a hard surface within range, and the Light in the grenade attempts to find a nearby target of your designation, which can simply be any hostile creature or as specific as you like. If the designated target exists within 60 feet of the surface the grenade impacts, the grenade turns into an axion seeker. The Light in the grenade can detect invisible targets, or targets that are behind most barriers, but the detection abilities of this grenade are blocked by 1 foot of stone, 1 inch of common metal, a thin sheet of lead, or 3 feet of wood or dirt, as well as any barrier that prevents divination. If the grenade's ability to detect the target fails, or if the designated target does not exist within 60 feet of the grenade, the Light dissipates into nothing.

**Axion Seeker.** An axion seeker is a Tiny arcane construct made entirely of void Light that has one job: find the target its caster specified. Starting on the turn the axion seeker appeared, if there is any path that leads toward the target, regardless of the distance of that path, the axion seeker will fly up to 30 feet on each of your turns along that path to reach the specified target, moving around obstacles and going through any opening big enough for a Tiny construct.

If the seeker enters the target's space, it detonates on the target. The target must make a Dexterity saving throw. Add a bonus to your DC equal to your Light level. The target takes 2d10 + your Light ability modifier in void damage on a failed save, or half as much on a success.

If, while the seeker is active, the path to the target is ever completely blocked, the seeker will detonate on the obstruction closest to the target.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 7'
  duration = 'Up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(1 + [light])d10 + [lightMod]', type: DamageType.Void }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class FireboltGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Firebolt
  element = LightElement.Solar
  name = 'Firebolt Grenade'
  description = `You create a palm-sized ball of Light and throw it at a hard surface within range. Upon impact, three bolts of solar Light lash out from the grenade at nearby targets. Choose up to three unique targets within 15 feet of your grenade, that do not have half or greater cover from your grenade, to make a Dexterity saving throw. Targets take 3d6 solar damage on a failed save or half as much on a success. Targets that fail their saving throw also begin Burning for the next minute. A target can make a Constitution saving throw at the end of each of its turns, ending the effect on itself early on a success.

**At Higher Levels.** The damage of this Light ability increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 7'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d6', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class FlashbangGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Flashbang
  element = LightElement.Arc
  name = 'Flashbang Grenade'
  description = `You throw a grenade packed with arc Light at a hard surface within range, and the grenade detonates on impact. All creatures within 5 feet of the detonation point must make a Constitution saving throw. Creatures who fail their saving throw take 3d6 explosive arc damage and become Blinded for the next minute. Creatures who succeed take half as much damage and do not become Blinded.

A creature can repeat the saving throw at the end of each of their turns, ending the effect on itself early on a success. Creatures who succeed on their saving throw, or for which the effect ends, become immune to being Blinded by this grenade for 24 hours.

**At Higher Levels.** The damage of this Light ability increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d6', type: DamageType.ExplosiveArc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class FluxGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Flux
  element = LightElement.Arc
  name = 'Flux Grenade'
  description = `You channel your Light into a grenade charged with arc energy, which you throw at a target you can see within range. Make a ranged Light attack roll, and add a bonus to your attack roll equal to your Light level. On a hit, the target takes 2d10 + your Light ability modifier in explosive arc damage. If you miss, the target must make a Dexterity saving throw. They take half the damage of this grenade on a failed save.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '20/60 feet'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(1 + [light])d10 + [lightMod]', type: DamageType.ExplosiveArc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class FusionGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Fusion
  element = LightElement.Solar
  name = 'Fusion Grenade'
  description = `You focus your Light into a palm-sized grenade brimming with solar energy, which you throw at a target you can see within range. Make a ranged Light attack roll, and add a bonus to your attack roll equal to your Light level. On a hit, the target takes 2d10 + your Light ability modifier in explosive solar damage. If you miss, the target must make a Dexterity saving throw. They take half the damage of this grenade on a failed save.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '20/60 feet'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(1 + [light])d10 + [lightMod]', type: DamageType.ExplosiveSolar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class IncendiaryGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Incendiary
  element = LightElement.Solar
  name = 'Incendiary Grenade'
  description = `You hurl a grenade packed with solar Light at a hard surface within range. All creatures within 5 feet of the location must make a Constitution saving throw. Creatures who fail their save take 3d6 explosive solar damage and begin Burning for one minute. If a creature succeeds on their saving throw, they take half as much damage and do not begin Burning.

Creatures who are Burning can repeat the saving throw at the end of each of their turns, ending the effect on itself on a success. Creatures who succeed on their saving throw, or for whom the effect ends, become immune to Burning from this grenade for 24 hours.

**At Higher Levels.** The damage of this Light ability increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d6', type: DamageType.ExplosiveSolar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class LightningGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Lightning
  element = LightElement.Arc
  name = 'Lightning Grenade'
  description = `You hurl an arc-infused grenade onto a non-living, hard surface within range. This grenade sticks to the surface and creates an active area in a 15-foot cone perpendicular to that surface. All creatures in the area must make a Dexterity saving throw. They take 3d8 + your Light ability modifier in arc damage on a failed save, or half as much on a success.

For the duration, creatures who end their turn within the area, or who enter the area for the first time on a turn, must also make a Dexterity saving throw against this grenade's effects. 

**Device.** This grenade can be destroyed if it takes any amount of damage. For this purpose, the grenade has an AC equal to 13 + your Light level, and fails all saving throws.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 7'
  duration = 'Up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d8 + [lightMod]', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class MagneticGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Magnetic
  element = LightElement.Void
  name = 'Magnetic Grenade'
  description = `You focus your Light into a fist-sized grenade made of void energy, which you throw at a target you can see within range. Make a ranged Light attack roll, and add a bonus to your attack roll equal to your Light level. On a hit, the target takes 2d10 + your Light ability modifier in explosive void damage. If you miss, the target must make a Dexterity saving throw. They take half the damage of this grenade on a failed save.

**At Higher Levels.** The damage of this Light ability increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '20/60 feet'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  attackFormula = '1d20 + [lightMod] + [light]'
  damage = [
    { formula: '(1 + [light])d10 + [lightMod]', type: DamageType.ExplosiveVoid }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class PulseGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Pulse
  element = LightElement.Arc
  name = 'Pulse Grenade'
  description = `You throw a ball of arc Light onto a hard surface within range and, upon impact, the ball bursts into a 10-foot diameter sphere of sparking arc Light, centered on the spot it hit the surface. All creatures in the area take 4d4 arc damage, and for the duration, all creatures that end their turn within the area, or who enter the area for the first time on a turn, take 4d4 arc damage.

A creature that takes damage from this grenade must also make a Constitution saving throw, becoming Electrified for the next minute on a failed save. An Electrified creature can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success. A creature that succeeds on the saving throw, or for whom the effect ends, becomes immune to being Electrified by a Pulse grenade for 24 hours.

**At Higher Levels.** The damage of this Light ability increases by 1d4 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(3 + [light])d4', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class ScatterGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Scatter
  element = LightElement.Void
  name = 'Scatter Grenade'
  description = `You toss a grenade of void Light onto a hard surface within range. On impact it breaks apart into a shower of explosive sparks. All creatures within 5 feet of the grenade must make a Dexterity saving throw. Creatures who fail their saving throw take 3d8 explosive void damage and are knocked Prone. Creatures who succeed take half as much and are not knocked Prone.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d8', type: DamageType.ExplosiveVoid }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class SkipGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Skip
  element = LightElement.Arc
  name = 'Skip Grenade'
  description = `You concentrate your arc Light into a grenade and throw it at a hard surface within range. On impact, the grenade breaks apart into fragments of arc Light that cover a 10-foot line on the ground. For the duration, should a hostile creature start their turn on the line, or move within 5 feet of these fragments, that creature must make a Dexterity saving throw. Add a bonus to your DC equal to your Light level. The creature takes 3d6 arc damage on a failed save, or half as much on a success. Once this grenade damages a target its duration ends.

**At Higher Levels.** The damage of this Light ability increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 7'
  duration = 'Up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d6', type: DamageType.Arc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod] + [light]', ability: Ability.Dexterity }
  ]
}

export class SolarGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Solar
  element = LightElement.Solar
  name = 'Solar Grenade'
  description = `You toss a compacted ball of solar Light onto a hard surface within range and, upon impact, the ball bursts into a 10-foot diameter sphere of flaming solar Light, centered on the spot it hit the surface. All creatures in the area take 4d4 solar damage, and for the duration, all creatures that end their turn within the area, or who enter the area for the first time on a turn, take 4d4 solar damage.

A creature that takes damage from this grenade must also succeed on a Constitution saving throw or begin Burning for the next minute on a failed save. A Burning creature can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success. A creature that succeeds on the saving throw, or for whom the effect ends, becomes immune to being Burning by a Solar grenade for 24 hours.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d4 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(3 + [light])d4', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class SpikeGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Spike
  element = LightElement.Void
  name = 'Spike Grenade'
  description = `You throw a void-laced grenade onto a non-living, hard surface within range. This grenade sticks to the surface and creates an active area in a 15-foot cone perpendicular to that surface. All creatures in the area must make a Charisma saving throw. They take 3d8 + your Light ability modifier in void damage on a failed save, or half as much on a success.

For the duration, creatures who end their turn within the area, or who enter the area for the first time on a turn, must also make a Charisma saving throw against this grenade's effects.

**Device.** This grenade can be destroyed if it takes any amount of damage. For this purpose, the grenade has an AC equal to 13 + your Light level, and fails  all saving throws.

**At Higher Levels.** The damage of this Light ability increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 7'
  duration = 'Up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d8 + [lightMod]', type: DamageType.Void }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Charisma }
  ]
}

export class StormGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Storm
  element = LightElement.Arc
  name = 'Storm Grenade'
  description = `You lob a storm-filled grenade at a hard surface within range. On impact with the surface, the grenade bursts apart into crackling arc Light that fills a sphere with a 10-foot radius. All creatures in the area must make a Dexterity saving throw, taking 3d8 explosive arc damage on a failed save, or half as much on a success.

**At Higher Levels.** The damage of this Light ability increases by 1d8 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d8', type: DamageType.ExplosiveArc }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class SuppressorGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Suppressor
  element = LightElement.Void
  name = 'Suppressor Grenade'
  description = `You pitch a fist-sized grenade of void Light at a hard surface within range. On impact it explodes, and all creatures within 5 feet must make a Charisma saving throw. Creatures who fail their save take 3d6 explosive void damage and become Suppressed for one minute. If a creature succeeds on their saving throw, they take half as much damage and do not become Suppressed.

Creatures who are Suppressed can repeat the saving throw at the end of each of their turns, ending the effect on itself on a success. Creatures who succeed on their saving throw, or for which the effect ends, become immune to being Suppressed by this grenade for 24 hours.

**At Higher Levels.** The damage of this Light ability increases by 1d6 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Instantaneous'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d6', type: DamageType.ExplosiveVoid }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Charisma }
  ]
}

export class SwarmGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Swarm
  element = LightElement.Solar
  name = 'Swarm Grenade'
  description = `You concentrate your Light into a grenade and throw it onto a hard surface within range. On impact, the grenade breaks apart into several seeking fragments that occupy a 5-foot cube, with at least one face of the cube centered on the location of impact.

This grenade's damage is represented by a dice pool, which consists of six d4s. After casting this grenade, you can choose a creature within 5 feet of the cube. Then choose any number of dice from the grenade's damage dice pool and roll them. The creature you chose takes that much solar damage, and the grenade's damage dice pool is reduced by the number of dice you rolled for the remainder of the grenade's duration.

For the duration, so long as you have at least one damage die remaining in this grenade's damage pool, should a creature end their turn within 5 feet of the cube, or should a creature move within 5 feet of the cube, you can also choose to have that creature take an amount of solar damage equal to however many dice from the pool you want to roll. The number of available dice in this grenade's damage pool is reduced by however many dice you roll to damage a creature. This reduction lasts for the remaining duration.

**At Higher Levels.** This grenade's dice pool gains two additional d4s for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(4 + (2 * [light]))d4', type: DamageType.Solar }
  ]
}

export class ThermiteGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Thermite
  element = LightElement.Solar
  name = 'Thermite Grenade'
  description = `You fling a Light grenade onto a hard surface within range. On impact with the surface, the grenade instantly creates a burning wall of solar Light that is 25 feet long, 5 feet high, and 5 feet thick. The wall is opaque and lasts for the duration. All creatures in the area must make a Constitution saving throw, taking 2d10 + your Light ability modifier in solar damage on a failed save, or half as much on a success.

For the duration, all creatures that end their turn in the area of the wall, or who move through the area for the first time on a turn, must also make a Constitution saving throw against this grenade's damage.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(1 + [light])d10 + [lightMod]', type: DamageType.Solar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export class TripmineGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Tripmine
  element = LightElement.Solar
  name = 'Tripmine Grenade'
  description = `You throw or place a solar-charged grenade onto a non-living, hard surface within range. This grenade sticks to the surface and creates an active area in a 15-foot cone perpendicular to that surface. For the duration, if at any point there is a Small or larger creature within the active area, the grenade will detonate. All creatures in the area must make a Dexterity saving throw. They take 3d10 + your Light ability modifier in explosive solar damage on a failed save, or half as much on a success.

**Device.** This grenade can be destroyed if it takes any amount of damage. For this purpose, the grenade has an AC equal to 13 + your Light level, and fails all saving throws.

**At Higher Levels.** When you cast this grenade at a Light level of 2nd or higher, its damage increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(2 + [light])d10 + [lightMod]', type: DamageType.ExplosiveSolar }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity }
  ]
}

export class VoidwallGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Voidwall
  element = LightElement.Void
  name = 'Voidwall Grenade'
  description = `You toss an oblong Light grenade onto a hard surface within range. Upon impact with the surface, it instantly creates a roiling wall of void Light that is 25 feet long, 5 feet high, and 5 feet thick. The wall is opaque and lasts for the duration. All creatures in the area must make a Charisma saving throw, taking 2d10 + your Light ability modifier in void damage on a failed save, or half as much on a success.

For the duration, all creatures that end their turn in the area of the wall, or who move through the area for the first time on a turn, must also make a Charisma saving throw against this grenade's damage.

**At Higher Levels.** The damage of this Light ability increases by 1d10 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(1 + [light])d10 + [lightMod]', type: DamageType.Void }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Charisma }
  ]
}

export class VortexGrenade implements LightAbility {
  _type = 'LightAbility'
  id = GrenadeAbility.Vortex
  element = LightElement.Void
  name = 'Vortex Grenade'
  description = `You lob a tightly packed ball of void Light onto a hard surface within range and, upon impact, the ball bursts into a 10-foot diameter sphere of churning void Light, centered on the spot it hit the surface. All creatures in the area take 4d4 void damage, and for the duration, all creatures that end their turn within the area, or who enter the area for the first time on a turn, take 4d4 void damage.

A creature that takes damage from this grenade must also succeed on a Constitution saving throw or begin Burning for the next minute on a failed save. A Burning creature can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success. A creature that succeeds on the saving throw, or for whom the effect ends, becomes immune to being Burning by a Vortex grenade for 24 hours.

**At Higher Levels.** The damage of this Light ability increases by 1d4 for each Light level you are above 1st.`
  castingTime = '1 action'
  range = '60 feet, mortar'
  cost = 1
  resource = ChargeType.Grenade
  recharge = 'd8, 8'
  duration = 'Concentration, up to 1 minute'
  prerequisiteLevel = 2
  damage = [
    { formula: '(3 + [light])d4', type: DamageType.Void }
  ]
  savingThrowFormula = [
    { dc: '8 + [prof] + [lightMod]', ability: Ability.Constitution }
  ]
}

export const allGrenades: LightAbility[] = [
  new ArcboltGrenade,
  new AxionBoltGrenade,
  new FireboltGrenade,
  new FlashbangGrenade,
  new FluxGrenade,
  new FusionGrenade,
  new IncendiaryGrenade,
  new LightningGrenade,
  new MagneticGrenade,
  new PulseGrenade,
  new ScatterGrenade,
  new SkipGrenade,
  new SolarGrenade,
  new SpikeGrenade,
  new StormGrenade,
  new SuppressorGrenade,
  new SwarmGrenade,
  new ThermiteGrenade,
  new TripmineGrenade,
  new VoidwallGrenade,
  new VortexGrenade
]
export const grenadeIndex = allGrenades.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<GrenadeAbility, LightAbility>)
export const getGrenade = (id: GrenadeAbility, level: number) => (grenadeIndex[id]?.prerequisiteLevel || 0) <= level ? grenadeIndex[id] : null
