import { Ability, DamageType, Feature, GuardianRace, Race, Skill } from '@/models/destiny'
import { getAllSkills } from '@/utils'

const AwokenFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Awoken Traits',
    description: `**Ability Score Increase.** One ability score of your choice increases by 2, and a different ability score of your choice increases by 1.

**Age.** Awoken attain physical maturity at the same rate as humans, and appear as developed adults at 20-30 years of age. Although they are no longer immortal, Awoken lifespans can still be extraordinarily long.

**Size.** Like humans, Awoken stature tends to fall between 5-7 feet, with weight varying accordingly. Of course, smaller and larger individuals are not unheard of. Your size is Medium.

**Speed.** Your base walking speed is 30 feet.

**Languages.** Awoken in the Distributary simply called their language Speech, but they subsequently relearned human languages as well. You can speak, read, and write City common.`,
    source: 'Awoken',
    languages: ['City Common']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Always a Way Home',
    description: `You are always at least vaguely aware of Mara Sov, the Awoken Queen, so long as both you and she are in the same plane of existence. Even when lost, you can use this sense to direct yourself or a vehicle you are operating along a straight path toward her`,
    source: 'Awoken'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Paracausal Resistance',
    description: `You have advantage on saving throws against spells, Light effects, and Darkness effects.`,
    source: 'Awoken'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Paracausal Sensitivity',
    description: `You gain the effects of the Sunsinger's Detect Light and Dark feature. Your refined sensitivity to the ebb andflow of the Light keeps you cognizant of subtle changes in the space around you. As an action, you can take one or two deep breaths, centering yourself and opening your awareness fully to the paracausal forces around you. For the next 10 minutes, you can determine what active modifiers there are in the area, if there are any weapons within 60 feet of you that can deal Light or Darkness damage, and you can sense if there are any creatures of the Light or the Darkness within the same range. You learn the creature's level (or individual CR) when you detect them in this way.`,
    source: 'Awoken'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Psychokinesis',
    description: `You have the ability to move or manipulate objects by thought. As your action, you can exert your will on one object that you can see that is within a number of feet equal to 5 times your proficiency bonus, and that doesn't weigh more than 10 pounds. If the object isn't being worn or carried, you can automatically move it up to 30 feet in any direction, but not beyond the range of this trait.

If the object is worn or carried by a creature, you must make a Charisma ability check contested by that creature's Strength check. If you succeed, you pull the object away from that creature and can move it up to 30 feet in any direction but not beyond the range of this trait.

You can exert fine control on objects with your psychokinetic grip, such as manipulating a simple tool, opening a door or a container, stowing or retrieving an item from an open container, or pouring the contents from a vial.

You can affect the same object round after round, or choose a new one at any time. If you switch objects, the prior object is no longer affected by your psychokinesis.`,
    source: 'Awoken'
  }
]

const ExoFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Exo Traits',
    description: `**Age.** To all outside observation, Exos are ageless. However, Exos whose minds have been reset many times exhibit symptoms of senility.

**Size.** Exos are typically the same size as humans, though an Exo will weigh 20-30 pounds more than a human with a similar height and build. Your size is Medium.

**Speed.** Exos are built for locomotion at least on par with natural human mobility. Your base walking speed is 30 feet.

**Languages.** Exo vocal modulators are not simple speakers, but sophisticated electromechanical organs capable of producing all the sounds of human language. You can speak, read, and write City common.`,
    source: 'Exo',
    languages: ['City common']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Darkvision',
    description: `Exo optical sensors are tuned to the human visible spectrum by default, but can also perceive additional wavelengths to achieve superior vision in dark and dim conditions. You can see in dim light within 30 feet of you as if it were bright light, and in darkness as if it were dim light.`,
    source: 'Exo'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Living Machine',
    description: `Even though you are a machine, your mind and much of the functionality of your body is no different from the average human. As such, you are considered a living creature, and you appear on scanners as one. You are immune to disease. You do not need to eat or drink, but you can if you want to. You do still need to sleep each day, but you typically only sleep for 4-6 hours, gaining the same benefit that a human does from 8 hours of sleep when you do. During your sleep, you sometimes experience dreams that feel like snatches of memory. In these dreams, you witness hideous, neverending battles in dark, lifeless places, and see yourself fighting in them. Many other Exos have described similar dreams, and the experience, though unsettling, is considered normal.`,
    source: 'Exo'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Resistant Design',
    description: `You resist poison damage, and you have advantage on saving throws you make to prevent yourself from Burning or becoming Poisoned. You also have advantage on saving throws you make to end an effect on yourself that is causing a Burning or Poisoned condition.`,
    source: 'Exo',
    resistances: [DamageType.Poison]
  }
]

const HumanFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Human Traits',
    description: `**Ability Score Increase.** One ability score of your choice increases by 2, and a different ability score of your choice increases by 1.

**Age.** Human longevity increased massively during the Golden Age, but since the Collapse, they have reverted to a natural lifespan of anywhere between 60-90 years, depending on living conditions. Humans are generally considered adults at 20 years of age, but the wilds often require them to undertake mature responsibilities sooner than that.

**Size.** Typical human height is between 5-7 feet, with average weight varying accordingly, but of course smaller and larger individuals are not uncommon. Your size is Medium.
    
**Speed.** Your base walking speed is 30 feet.

**Languages.** Humans still use a bewildering variety of languages. You speak, read, and write City common, and may choose one other modern human language you can speak, read, and write.`,
    source: 'Human',
    languages: ['City common']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Adrenaline Rush',
    description: `. If you take damage that would reduce you to half hit points or less, but leaves you with at least 1 hit point remaining, you can use your reaction to begin an adrenaline rush. For the next minute, the following benefits apply:

- You ignore the effects of the first three levels of exhaustion, and cannot accumulate any additional levels of exhaustion.
- You have advantage on Strength saving throws and Strength (Athletics) checks you make.
- The rush of adrenaline allows you to ignore small portions of the damage you take. Whenever you take damage, you reduce the damage you take by an amount equal to 1d6 + your Constitution modifier. If the amount that would be reduced is 0 or less, this does nothing instead.
- If you take damage that would reduce you to 0 hit points or less, you can choose to drop to 1 hit point instead. You can only do this once during your adrenaline rush.

This effect ends early if you are Incapacitated or if you receive healing. Upon ending, you immediately gain two levels of exhaustion, and you take an amount of damage equal to the total amount you ignored during your adrenaline rush. You must complete a long rest in order to invoke this trait again.`,
    source: 'Human'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'The Strength of Humanity',
    description: ` If a creature you can see is reduced to 0 hit points, you can choose to use your reaction to begin the effects of your adrenaline rush.`,
    source: 'Human'
  }
]

const CabalFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Cabal Traits',
    description: `**Ability Score Increase.** One ability score of your choice increases by 2, and a different ability score of your choice increases by 1.

**Age.** Cabal maturation periods and lifespan are unknown, but the empire possesses sophisticated biogenic technologies that enable full-grown adult clones to be rapidly produced. It is thus possible that some Cabal may not be born and raised at all.

**Size.** Typical Cabal individuals are about 7 feet tall and weigh around 800 pounds. Even so, your size is Medium.

**Speed.** Cabal are strong, but their bulky bodies are not suited to rapid locomotion. Your base walking speed is 25 feet. 

**Languages.** You can speak, read, and write Ulurant. Cabal are capable of speaking other languages, including human ones.`,
    source: 'Cabal',
    languages: ['Ulurant']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Heavyweight',
    description: `You count as one size larger when determining your weight limit, when determining the result of effects that target creatures of specific sizes, and for determining what you can grapple, shove, or ride as a mount`,
    source: 'Cabal'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Powerful Stature',
    description: ` You have advantage on saving throws you make to prevent yourself from being knocked back or knocked prone, as well as on Strength saving throws or Strength (Athletics) checks you make to prevent yourself from being grappled or shoved.`,
    source: 'Cabal'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Imperial Era',
    description: `Cabal society changed dramatically after Emperor Calus was overthrown and Dominus Ghaul took control of the empire. Choose whether you were raised under Emperor Calus' reign, or under Dominus Ghaul's rule.`,
    source: 'Cabal'
  }
]

const EliksniFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Eliksni Traits',
    description: `**Ability Score Increase.** One ability score of your choice increases by 2, and one other ability score of your choice increases by 1.

**Age.** The duration of Eliksni maturation from their larval stage is unknown, but once fully grown, their lifespan can be immensely long. Some kells, archons, and other high-ranking Fallen are firsthand survivors of the Whirlwind, which occurred long before the Traveler even arrived in our solar system.

**Size.** Typical, adequately fed Eliksni are around 7-8 feet tall when standing at full height. Your size is Medium.

**Speed.** Eliksni walk bipedally, but can drop into a low, crawling dash using their lower arms. Your base walking speed is 30 feet.

**Languages.** You can speak, read, and write Eliksni, and may also speak City common with the assistance of a voice synth. A rare piece of Eliksni technology called a glossator would allow you to speak with others in any language, but few possess such devices.`,
    source: 'Eliksni',
    languages: ['Eliksni']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Darkvision',
    description: `Eliksni's four eyes see beyond the spectrum visible to humans, and are well adapted for dark environments. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.`,
    source: 'Eliksni'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Enhanced Sense',
    description: `You have advantage on Wisdom (Perception) checks you make that rely on scent.`,
    source: 'Eliksni'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Multi-Limbed',
    description: `You have four arms: two right arms and two left arms. When you hold a twohanded weapon, you must use one right hand and one left hand in order to do so. You cannot hold more than one two-handed weapon at a time. For additional rules on dual-wielding, see Chapter 7`,
    source: 'Eliksni'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Ether Diet',
    description: `The form your body takes is dependent on the amount of ether you consume. No matter how little ether you consume, you cannot be below the vandal form of Eliksni. If you manage to maintain a steady diet so as to meet the dietary requirements of other Eliksni forms, you may obtain one of those forms. You cannot skip forms: you must advance from one form to the other, following the guidelines for form advancement.`,
    source: 'Eliksni'
  }
]

const KrillFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Krill Traits',
    description: `**Age.** Krill lifespan was about ten years. Individuals reached maturity at around two years old, and would choose which morph to assume, if any, by middle age.

**Size.** Typical krill were around 5-6 feet tall, though different morphs might grow larger. Your size is Medium.

**Speed.** Krill people's springy limbs allow for fairly nimble movement. Your base walking speed is 30 feet.

**Languages.** You can speak, read, and write krill.`,
    source: 'Krill',
    languages: ['Krill']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Darkvision',
    description: `The krill's three eyes were adapted for the murky conditions of living inside a gas giant, and see well in low-light conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.`,
    source: 'Krill'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Wary Hypnogogia',
    description: `Constantly threatened by predators, krill cannot afford unconscious rest. Instead, you enter a shallow trance, remaining semiconscious for 4 hours a day. While in this restful trance, you sometimes experience fleeting dreamlike episodes. Such dreams are the result of your subconscious manifesting itself, similar to the dreams a human may have. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep.`,
    source: 'Krill'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Morphology',
    description: `You start as the acolyte morph. Whenever you increase in level, so long as you meet the level requirement of a different morph, you can choose to alter yourself to adopt it. This change is permanent and cannot be reversed. If you choose to do this, you lose the traits of the acolyte morph and gain the traits of the new morph you chose.`,
    source: 'Krill'
  }
]

const PsionFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Psion Traits',
    description: `**Ability Score Increase.** Your Intelligence score increases by 2, and one other ability score of your choice increases by 1.

**Age.** The psion life cycle is mysterious, but it seems psions can be exceptionally long-lived, whether naturally or by dint of advanced Cabal technology.

**Size.** Typical psions stand no more than five feet tall, and average four feet tall. Your size is Small.

**Speed.** Though very agile and capable of quick movement, psions have a limited stride length. Your base walking speed is 25 feet.

**Languages.** You can speak, read, and write Ulurant. Additionally, psion telepathy allows you to communicate simple concepts with any intelligent creature without need for language, if the creature is within a number of feet equal to 5 times your level.`,
    source: 'Psion',
    languages: ['Ulurant']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Naturally Stealthy',
    description: `You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you.`,
    source: 'Psion',
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Psion Nimbleness',
    description: `You can move through the space of any creature that is of a larger size than you without it counting as difficult terrain. You can also take the Disengage action as a bonus action on your turn.`,
    source: 'Psion'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Psionics',
    description: `You have a number of psionic points equal to your proficiency bonus (minimum 2). You can spend these psionic points to grant yourself certain benefits. When you complete a long rest, you regain all spent psionic points. The DC of your psionic abilities that cause saving throws is equal to 8 + your Intelligence modifier + your proficiency bonus.`,
    source: 'Psion'
  }
]

const VexFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Vex Traits',
    description: `**Ability Score Increase.** One ability score of your choice increases by 2.

**Age.** Vex do not age in any meaningful way. Their consciousness exists effectively outside time. Even when residing in hulls for immensely long periods, they are not affected by the passage of time.

**Size.** Different Vex hulls are built in a wide range of sizes. Regardless of hull, your size is Medium.

**Languages.** You can speak, read, and write hexinary. You speak other languages flawlessly when you learn them.`,
    source: 'Vex',
    languages: ['Hexinary']
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Blink',
    description: `As either an action or bonus action on your turn, you can teleport to an unoccupied space you can see that is within a distance equal to 5 times your proficiency bonus. You take all carried and worn equipment of your choice with you when you do this.`,
    source: 'Vex'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Darkvision',
    description: `Vex monocular sensors can be tuned to perceive various electromagnetic spectra. You can see in dim light within 30 feet of you as if it were bright light, and in darkness as if it were dim light.`,
    source: 'Vex'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Living Machine',
    description: `Even though you are part of a machine, your consciousness itself is processed in a substrate of living radiolarian organisms suspended in fluid. You are considered a living creature, and you appear on scanners as one. You are immune to disease, and you do not eat or breathe. Instead of sleeping, you enter an inactive state for 4 hours each day. While inactive, you experience dreamlike sensory episodes. These dreams are a result of memory defragmentation in your machine hull's data core. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep.`,
    source: 'Vex'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Natural Armor',
    description: `Due to your machine body, your AC cannot be less than 15, even when unarmored.`,
    source: 'Vex',
    ac: '15'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Superior Memory',
    description: `You are naturally proficient in the History skill, and you can double your proficiency bonus for it when you make an Intelligence (History) check to recall information you have learned, or to recall the details of your experiences. Your Intelligence (History) check can only benefit from this if it does not already benefit from a feature or trait that allows you to double your proficiency bonus for it.`,
    skillOptions: [Skill.History],
    source: 'Vex'
  }
]

export class ReefbornAwokenRace implements GuardianRace {
  perks = [
    ...AwokenFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Agile Movement',
      description: `Moving through an opening one size smaller than you does not count as difficult terrain for you. You have advantage on Dexterity (Acrobatics) checks you make in zero gravity.`,
      source: 'Reefborn Awoken'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Additional Language Proficiencies',
      description: `You can speak, read, and write Awoken Speech, and you can choose whether or not you can also speak, read, and write Eliksni.`,
      source: 'Reefborn Awoken',
      languages: ['Awoken Speech']
    }
  ]
  race = Race.Awoken
  name = 'Reefborn Awoken'
  id = 'reefborn-awoken'
}

export class EarthbornAwokenRace implements GuardianRace {
  perks = [
    ...AwokenFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Natural Athlete',
      description: `You have your choice of either a swimming speed or climbing speed equal to your base walking speed.`,
      source: 'Earthborn Awoken'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Additional Language Proficiencies',
      description: `You can speak, read, and write Awoken Speech, and you can choose whether or not you can also speak, read, and write Eliksni.`,
      source: 'Earthborn Awoken',
      languages: ['Awoken Speech']
    }
  ]
  race = Race.Awoken
  name = 'Earthborn Awoken'
  id = 'earthborn-awoken'
}

export class ExoABURace implements GuardianRace {
  perks = [
    ...ExoFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Ability Score Increase',
      description: `One ability score of your choice increases by 2.`,
      source: 'Exo AB-U'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Built',
      description: `Your hit point maximum increases by 1, and it increases by 1 every time you gain a level.`,
      source: 'Exo AB-U'
      //TODO: Add the case where the level is added to the maximum hit points
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Natural Armor',
      description: `You have tough, heavy-duty plating. You have disadvantage on Dexterity (Stealth) checks. When not wearing armor, your AC is equal to 13 + your Constitution modifier. Even while wearing armor, you can use this natural armor to determine your AC if the armor you are wearing would leave you with a lower AC. You are still subject to the effects of the armor you are wearing if you do this.`,
      source: 'Exo AB-U',
      ac: '13 + Con mod'
    }
  ]
  race = Race.Exo
  name = 'Exo AB-U'
  id = 'exo-abu'
}

export class ExoCTHDRace implements GuardianRace {
  perks = [
    ...ExoFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Ability Score Increase',
      description: `One ability score of your choice increases by 2, and a different ability score of your choice increases by 1.`,
      source: 'Exo CT-HD'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Competitive',
      description: `If you make an attack roll, saving throw, or ability check that fails, you can choose to add a bonus to your attack roll, saving throw, or ability check that is equal to the number of creatures who can see you and that you are aware of (maximum bonus of +5). If using this trait causes you to succeed on your attack roll, ability check, or saving throw, you cannot use this trait again until you complete a short or long rest.`,
      source: 'Exo CT-HD'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Natural Armor',
      description: `When you aren't wearing armor, your AC is equal to 12 + your Dexterity modifier. Even while wearing armor, you can use this natural armor to determine your AC if the armor you are wearing would leave you with a lower AC. You are still subject to the effects of the armor you are wearing if you do this.`,
      source: 'Exo CT-HD',
      ac: '12 + Dex mod'
    }
  ]
  race = Race.Exo
  name = 'Exo CT-HD'
  id = 'exo-cthd'
}

export class ExoJSYKRace implements GuardianRace {
  perks = [
    ...ExoFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Ability Score Increase',
      description: 'Choose three of your ability scores. Each of those scores increases by 1. Alternatively, you can choose one ability score to increase by 2, and a different ability score to increase by 1.',
      source: 'Exo JSYK'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Keen Eye',
      description: 'If you make a ranged attack roll and miss, you can choose to re-roll your attack once. You must use the new roll, and you must complete a short or long rest before you can use this trait again.',
      source: 'Exo JSYK'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Natural Armor',
      description: 'When you aren\'t wearing armor, your AC is equal to 15. Even while wearing armor, you can use this natural armor to determine your AC if the armor you are wearing would leave you with a lower AC. You are still subject to the effects of the armor you are wearing if you do this.',
      source: 'Exo JSYK',
      ac: '15'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Superior Sensors',
      description: 'The range of your darkvision increases to 60 feet. When you make a Wisdom (Perception) check, you can roll a d4 and add it to your total.',
      source: 'Exo JSYK'
    }
  ]
  race = Race.Exo
  name = 'Exo JSYK'
  id = 'exo-jsyk'
}

export class HumanCitizenRace implements GuardianRace {
  perks = [
    ...HumanFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'School Studies',
      description: `You are proficient with one skill of your choice.`,
      skillOptions: [{ choice: 1, skills: getAllSkills() }],
      source: 'Human Citizen'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Urban Hobbies',
      description: `You can choose one toolkit, vehicle, modern human language, or ancient human language to become proficient with. Proficiency with a language means you can speak, read, and write that language.`,
      source: 'Human Citizen'
    }
  ]
  race = Race.Human
  name = 'Human Citizen'
  id = 'human-citizen'
}

export class HumanPilgrimRace implements GuardianRace {
  perks = [
    ...HumanFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Survivor\'s Luck',
      description: `If you make an attack roll, saving throw, or ability check and roll a 1 on the d20, you can choose to re-roll once. You must use the new roll. You regain use of this trait when you complete a brief rest.`,
      source: 'Human Pilgrim'
    }
  ]
  race = Race.Human
  name = 'Human Pilgrim'
  id = 'human-pilgrim'
}

export class HumanWayfarerRace implements GuardianRace {
  perks = [
    ...HumanFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Ship Mechanic',
      description: `You are proficient with jumpships and other spacefaring vehicles, and can double your proficiency bonus when making an ability check to operate, repair, hide within, or recall information about spaceships. Your ability check can only benefit from this bonus if it is not already benefiting from a feature or trait that doubles your proficiency bonus for it.`,
      source: 'Human Wayfarer'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Alien Language Comprehension',
      description: `You have a working knowledge of one alien language of your choice. You can convey simple ideas and concepts when speaking this language, and you understand the most commonly written phrases in this language, enough to be able to read signs or common labels in vehicles made by creatures who natively speak the language.`,
      source: 'Human Wayfarer'
    }
  ]
  race = Race.Human
  name = 'Human Wayfarer'
  id = 'human-wayfarer'
}

export class CalusCabalRace implements GuardianRace {
  perks = [
    ...CabalFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Built',
      description: `Your hit point maximum increases by 1, and it increases by 1 every time you gain a level.`,
      source: 'Calus\'s Cabal'
      //TODO: Add the case where the level is added to the maximum hit points
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Skill Training',
      description: `You are proficient with one skill of your choice.`,
      skillOptions: [{ choice: 1, skills: getAllSkills() }],
      source: 'Calus\'s Cabal'
    }
  ]
  race = Race.Cabal
  name = 'Calus\'s Cabal'
  id = 'calus-cabal'
}

export class GhaulCabalRace implements GuardianRace {
  perks = [
    ...CabalFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Strong Grip',
      description: `You can hold a two-handed weapon in one hand without penalty due to the weapon's bulk, and holding non-weapon objects do not count as duel-wielding for you unless you use the object as a weapon. If you try to hold multiple weapons or use a non-weapon object as a weapon, the bulk of the items affects your ability to attack effectively as normal.`,
      source: 'Ghaul\'s Cabal'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Unshakable',
      description: `If you fail an ability check to prevent yourself from becoming Frightened, you can choose to succeed instead. Once you use this trait you must complete a long rest before you can use it again.`,
      source: 'Ghaul\'s Cabal'
    }
  ]
  race = Race.Cabal
  name = 'Ghaul\'s Cabal'
  id = 'ghaul-cabal'
}

export class EliksniVandalRace implements GuardianRace {
  perks = [
    ...EliksniFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Natural Climber',
      description: `You have a climbing speed of 30 feet. You only need to use two of your hands to climb, including while moving up vertical surfaces and upside-down on ceilings.`,
      source: 'Eliksni Vandal'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Vandal Evasion',
      description: `If you fail a Dexterity saving throw, you can choose to succeed instead. Once you use this trait, you must complete a long rest before you can use it again.`,
      source: 'Eliksni Vandal'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Form Advancement',
      description: `If you meet the dietary requirements of the captain form for a consecutive period of 30 days, at the end of the 30 days, you finish growing into the captain form. You lose the traits of the vandal form and gain the traits of the captain form.`,
      source: 'Eliksni Vandal'
    }
  ]
  race = Race.Eliksni
  name = 'Eliksni Vandal'
  id = 'eliksni-vandal'
}

export class EliksniCaptainRace implements GuardianRace {
  perks = [
    ...EliksniFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Dietary Requirements',
      description: `You must consume twice as much ether a day (2 rations/day) to maintain a captain form. If you fail to meet this requirement for any given 15 days in a 30-day period, at the end of the 30-day period you finish reducing to the vandal form. You lose the traits of the captain form and gain the traits of the vandal form when you do this.`,
      source: 'Eliksni Captain'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Presence',
      description: `You are naturally proficient with your choice of either the Intimidation or Persuasion skill while maintaining the captain form. Once you make this choice you cannot change it, regardless of how many times you lose or gain this form.`,
      skillChoices: [{ choice: 1, skills: [Skill.Intimidation, Skill.Persuasion] }],
      source: 'Eliksni Captain'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Relentless',
      description: `If you take damage that would reduce you to 0 hit points or less, you can instead drop to 1 hit point. Once you use this trait, you must complete a long rest before you can use it again.`,
      source: 'Eliksni Captain'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Form Adancement',
      description: `If you meet the dietary requirements of the baron form for a consecutive period of 30 days, at the end of the 30 days, you finish growing into the baron form. You gain the traits of the baron form in addition to the traits of the captain form.`,
      source: 'Eliksni Captain'
    }
  ]
  race = Race.Eliksni
  name = 'Eliksni Captain'
  id = 'eliksni-captain'
}

export class EliksniBaronRace implements GuardianRace {
  perks = [
    ...EliksniFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Dietary Requirements',
      description: `You must consume eight times as much ether a day (8 rations/day) to maintain a baron form. If you fail to meet this requirement for any given 15 days in a 30-day period, at the end of the 30-day period you finish reducing to the captain form. You lose the traits of the baron form when you do this.`,
      source: 'Eliksni Baron'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Size',
      description: 'Your size becomes Large.',
      source: 'Eliksni Baron'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Conspicuous',
      description: `You have disadvantage on Dexterity (Stealth) checks.`,
      source: 'Eliksni Baron'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Strength of Size',
      description: `Your Strength is increased by 2.`,
      source: 'Eliksni Baron'
      //TODO: Add 2 to your strength
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'The Bigger They Are',
      description: `You are a much easier target to hit on the battlefield now, and your increased size hinders your movement. While maintaining this form, your AC is reduced by 1, and your Dexterity score is reduced by 2.`,
      source: 'Eliksni Baron'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Commanding Presence',
      description: `You gain one of the following traits, depending on whether you chose Intimidation or Persuasion for the Presence trait in captain form. If you chose Intimidation, you gain Bellow. If you chose Persuasion, you gain Embolden.

- Bellow: As an action on your turn you can let out a bellowing howl that can be heard up to 300 feet away. All hostile creatures who can hear you must make a Wisdom saving throw (DC = 8 + your proficiency bonus + your Charisma modifier). On a failed save, they become Frightened of you for the next minute. A creature can repeat the saving throw at the end of each of their turns, ending the effect on itself early on a success. If a creature succeeds on their saving throws, if you are Incapacitated, or if the effect ends for them, they become immune to being Frightened by you for 24 hours. Once you use this trait, you must complete a long rest before you can use it again.
- Embolden: As an action on your turn you can shout words of encouragement, which can be heard clearly up to 60 feet away. For the next minute, creatures who clearly heard you and who consider you their ally cannot be Charmed or Frightened, and if they are already Charmed or Frightened, the condition ends early for them. In addition, once on each of your allies' next turn, they can add a d4 to an ability check, saving throw, or attack roll they make. This effect ends early if you are Incapacitated. Once you use this trait, you must complete a long rest before you can use it again.`,
      source: 'Eliksni Baron'
    }
  ]
  race = Race.Eliksni
  name = 'Eliksni Baron'
  id = 'eliksni-baron'
}

export class AcolyteKrillRace implements GuardianRace {
  perks = [
    ...KrillFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Ability Score Increase',
      description: `One ability score of your choice increases by 1. When you adopt a new morph, this score is reduced by 1.

Instead of choosing a new morph, you can choose to make the acolyte morph your permanent form, which you can do at any time. If you make the acolyte morph permanent, you can choose one ability score to increase by 2.`,
      source: 'Acolyte Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Cunning Movement',
      description: `Moving through an opening one size smaller than you does not count as difficult terrain for you. You can take the Disengage action as a bonus action on your turn.`,
      source: 'Acolyte Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Keen Eye',
      description: `If you make a ranged attack roll and miss, you can choose to re-roll your attack roll once. You must use the new roll, and you must complete a short or long rest before you can use this trait again.`,
      source: 'Acolyte Krill'
    }
  ]
  race = Race.Krill
  name = 'Acolyte Krill'
  id = 'acolyte-krill'
}

export class KnightKrillRace implements GuardianRace {
  perks = [
    ...KrillFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Level Requirements',
      description: `You can choose the knight morph beginning when you reach 2nd level.`,
      source: 'Knight Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Ability Score Increase',
      description: `Choose either your Strength, Constitution, or Charisma score to increase by 2. You can increase one other ability score by 1.`,
      source: 'Knight Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Chitinous Plating',
      description: `You have natural chitinous armor. You have disadvantage on Dexterity (Stealth) checks. While unarmored, your AC is equal to 13 + your Constitution modifier. Even while wearing armor, you can use this natural armor to determine your AC if the armor you are wearing would leave you with a lower AC. You are still subject to the effects of the armor you are wearing if you do this.`,
      source: 'Knight Krill',
      ac: '13 + Con mod'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Fortified',
      description: `If a creature scores a critical hit against you, you can cause the critical hit to become a normal hit instead. Once you use this trait, you must complete a long rest before you can use it again.`,
      source: 'Knight Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Heavyweight',
      description: `You count as one size larger when determining your weight limit, when determining the result of effects that target creatures of specific sizes, and for determining what you can grapple, shove, or ride as a mount.`,
      source: 'Knight Krill'
    }
  ]
  race = Race.Krill
  name = 'Knight Krill'
  id = 'knight-krill'
}

export class MotherKrillRace implements GuardianRace {
  perks = [
    ...KrillFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Level Requirements',
      description: `You can choose the mother morph beginning when you reach 4th level.`,
      source: 'Mother Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Ability Score Increase',
      description: `Choose either your Dexterity, Wisdom, or Intelligence score to increase by 2. You can increase one other ability score by 1.`,
      source: 'Mother Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Evasive Flight',
      description: `Your ability to move freely while airborne grants you the effects of a natural armor equal to 12 + your Dexterity modifier. You can use this in place of your normal armor while airborne, if this would grant you a higher AC. You are still subject to the effects of the armor you are wearing when you do this.`,
      source: 'Mother Krill',
      ac: '12 + Dex mod'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Flight',
      description: `You have a flying speed of 30 feet in addition to your base walking speed, and you can hover in place while flying. You cannot begin flying if you are carrying a weight that is greater than half your weight limit, and if you are flying when you acquire this weight, you begin to fall. If you are incapacitated while airborne, you begin to fall.`,
      source: 'Mother Krill'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Paperweight',
      description: `You count as one size smaller when determining your weight limit, and for determining what you can grapple, shove, or ride as a mount.`,
      source: 'Mother Krill'
    }
  ]
  race = Race.Krill
  name = 'Mother Krill'
  id = 'mother-krill'
}

export class SoldierPsionRace implements GuardianRace {
  perks = [
    ...PsionFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Keen Eye',
      description: `If you make a ranged attack roll and miss, you can choose to re-roll your attack roll once. You must use the new roll, and you must complete a short or long rest before you can use this trait again.`,
      source: 'Soldier Psion'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Soldier\'s Psionics',
      description: `You can spend your psionic points to cause any of the following effects. The cost of each psionic effect is indicated in parentheses. If you don't have enough psionic points to cause an effect, you cannot choose that effect.

- Swift (1 psionic point). You double your movement until the end of your turn.
- Reflexive (3 psionic points). If a creature causes you to make a Dexterity saving throw, you can spend psionic points to grant yourself advantage on that saving throw.`,
      source: 'Soldier Psion'
    }
  ]
  race = Race.Psion
  name = 'Soldier Psion'
  id = 'soldier-psion'
}

export class MindFlayerPsionRace implements GuardianRace {
  perks = [
    ...PsionFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Mind Flayer\'s Psionics',
      description: `You can spend your psionic points to cause any of the following effects. The cost of each psionic effect is indicated in parentheses. If you don't have enough psionic points to cause an effect, you cannot choose that effect.
- Psionic Blast (1 psionic point). As an action you let loose a quick burst of psionic energy at a target within 5 feet of you. That target must make a Dexterity saving throw. On a failed save it takes 2d8 necrotic damage. On a success, it takes half as much. The damage of this psionic effect increases to 3d8 at 5th level, 4d8 at 11th level, and 5d8 at 17th level.
- Nova Rupture (3 psionic points). As an action you hurl a ball of psionic energy at a point you can see within 30 feet. All Medium or smaller creatures within 5 feet of that point must make a Strength saving throw. On a failed save they are shoved 20 feet in a direction of your choosing, which can be straight upward. If you shove a creature into the air like this, it becomes airborne, and it remains airborne until the start of its next turn when it begins to fall. If a creature who is airborne has a flying speed and is not incapacitated, it does not need to begin falling.`,
      source: 'Mind Flayer Psion',
      damage: [
        { formula: '(2 + (([level] + 1) / 6))d8', type: DamageType.Void }
      ],
      savingThrowFormula: [
        { dc: '8 + [prof] + [lightMod]', ability: Ability.Dexterity },
        { dc: '8 + [prof] + [lightMod]', ability: Ability.Strength }
      ]
    }
  ]
  race = Race.Psion
  name = 'Mind Flayer Psion'
  id = 'mindflayer-psion'
}

export class VexGoblinRace implements GuardianRace {
  perks = [
    ...VexFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Speed',
      description: `Your base walking speed is 20 feet.`,
      source: 'Vex Goblin'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Reactive Blink',
      description: `If a creature makes a weapon attack against you, or if you are forced to make a Dexterity saving throw, you can choose to use your Blink trait as a reaction. You can only choose to do this if you did not use Blink since the start of your previous turn, and if there is an unoccupied spot within range of your Blink. When you choose to do this, you cause disadvantage on the attack roll, or you grant yourself advantage on the Dexterity saving throw.`,
      source: 'Vex Goblin'
    }
  ]
  race = Race.Vex
  name = 'Vex Goblin'
  id = 'vex-goblin'
}

export class VexHobgoblinRace implements GuardianRace {
  perks = [
    ...VexFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Speed',
      description: `Your base walking speed is 15 feet.`,
      source: 'Vex Hobgoblin'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Aimbot',
      description: `Once on your turn, you can choose to begin Aiming without expending movement to do so.`,
      source: 'Vex Hobgoblin'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Scanners',
      description: `As an action you can open your conscious mind up to the extensive sensor array in your horns. For the next minute, you have scanners with a radius equal to 5 times your proficiency bonus. You must complete a short or long rest to use this trait again.`,
      source: 'Vex Hobgoblin'
    }
  ]
  race = Race.Vex
  name = 'Vex Hobgoblin'
  id = 'vex-hobgoblin'
}

export class VexMinotaurRace implements GuardianRace {
  perks = [
    ...VexFeatures,
    {
      _type: 'Feature',
      level: 0,
      name: 'Speed',
      description: `Your base walking speed is 20 feet.`,
      source: 'Vex Minotaur'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Extended Reach',
      description: `When you make an attack with a melee weapon on your turn, your reach for it is 5 feet greater than it is normally.`,
      source: 'Vex Minotaur'
    },
    {
      _type: 'Feature',
      level: 0,
      name: 'Heavyweight',
      description: `You count as one size larger when determining your weight limit, when determining the result of effects that target creatures of specific sizes, and for determining what you can grapple, shove, or ride as a mount.`,
      source: 'Vex Minotaur'
    }
  ]
  race = Race.Vex
  name = 'Vex Minotaur'
  id = 'vex-minotaur'
}

export const ghostFeatures: Feature[] = [
  {
    _type: 'Feature',
    level: 0,
    name: 'Ghost Traits',
    description: `**Ability Score Increase.** One of your Ghost's ability scores increases by 2, and a different ability score increases by 1.

**Size.** While Ghosts can vary in size depending on the type of shell they equip, the core of the Ghost can fit into the palm of a human hand. Your Ghost's size is Tiny.

**Speed.** Your Ghost only has 30 feet of flying speed, and it can hover in place.

**Languages.** Your Ghost can speak, read, and write City common, the Risen language, and one other language of its choice. It understands and can interpret RSL.

**Universal Interface.** Your Ghost is proficient in the Technology skill.

**Ultralight.** Your Ghost's weight limit is 7 lbs. It cannot lift, push, pull, or drag a weight in excess of this limit. If your Ghost is ever carrying a physical weight in excess of its weight limit, it is considered Restrained.

**Integrated Communications.** Your Ghost is able to detect, read, and communicate across most digital and quantum signals. They can record video and audio, and they can always communicate internally with their Guardian via neural symbiosis.`,
    skillOptions: [Skill.Technology],
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Living Machine',
    description: `Even though your Ghost is a construct, it has a personality and level of intelligence no different from other sentient biotic life. Your Ghost is considered a living creature, and it appears on scanners as one. It is immune to disease, and it does not need to eat, sleep, or drink, though it can choose to enter a rest state to defragment its digital memory if it wants to.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Crafted in Light',
    description: `Your Ghost is resistant to bludgeoning, kinetic, piercing, and slashing damage. However, your Ghost has a vulnerability to Darkness damage, and when your Ghost takes Darkness damage, its hit point maximum is reduced by the same amount.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Memory Bank',
    description: `Ghosts have a number of memory slots equal to 15 times their Intelligence score. Using a mind-bendingly sophisticated combination of the Traveler's technology and common transmat protocols, your Ghost can download items and store them in its memory slots. Every item the Ghost downloads requires at least one memory slot to store. Some items are large enough or complex enough that they require multiple memory slots in order for a Ghost to download them into its memory. Some items do not need multiple memory slots to store extra quantities of the item. These items can be compressed into a single memory slot, or into a single set of memory slots, which is called stacking the item. When reading an item's memory details, the first number is the number of slots the item requires, and the second number is the maximum number per stack of that item.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Naturally Stealthy',
    description: `A Ghost can attempt to hide even when it is only obscured by a creature or object that is at least one size larger than itself.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Ghost Features',
    description: `You and your Ghost are inseparable, but there will be times when your Ghost must act on its own. Your Ghost has its own hit points, set of skills, and bonuses to those skills, takes its initiative on your initiative, and can take its own actions, bonus actions, and reactions.

### Hit Points
- **Hit Dice**: 1d6 per Ghost level
- **Health Points at 1st level**: 6 + your Ghost's Constitution modifier
- **Health Points at higher levels**: 1d6 (or 4) + your Ghost's Constitution modifier per Ghost level after 1st
### Proficiencies
- **Armor**: Ghost shells
- **Vehicles**: Jumpships (autopilot programming only)
- **Saving Throws**: Dexterity, Intelligence
- **Skills**: Choose any 5`,
    skillOptions: [{ choice: 5, skills: getAllSkills() }],
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Restoration',
    description: `Your Ghost is able to use the Light to heal your injuries and revive you from almost any death, spending a use of this feature in order to take the Heal or Resurrect action, as described in Chapter 7. Your Ghost can invoke this feature a maximum number of times equal to its level.

When your Ghost completes a long rest, it regains a number of spent uses of Restoration, equal to half of the Ghost's maximum number of uses (minimum of one Restoration use recovered). For example, if your Ghost has a Restoration use maximum of four, it would regain up to two uses Restoration when it completes a long rest. A Ghost cannot have more uses of Restoration than its maximum.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Glimmer Programming',
    description: `Glimmer is a type of programmable matter that can be turned into any abiotic material, object, or construct, such as a pile of copper ore, a handful of weapon parts, or, with sufficient glimmer, even a fully functional Tower frame.

Your Ghost is capable of programming glimmer. As an action, it can program up to 2,000 bits of glimmer into an abiotic item that does not require attunement (see Architect's Guide). This glimmer is spent when your Ghost does this. How many bits of glimmer it takes to create a specific item is determined by the item's purchase cost, or by your Architect.

## Reverse Programming
If an item is made up of glimmer, either in part or in whole, your Ghost can use an action to reduce it back to raw glimmer. The amount of glimmer that is recovered from the item is equal to one third of the amount of glimmer originally in the item.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Pocket Backpack',
    description: `After it connects itself to you for the very first time, your Ghost gains access to a small pocket of extradimensional space attached to your person. Your Ghost can choose to use either its action or bonus action to enter or leave the pocket backpack on its turn, if it is within 5 feet of you. Your Ghost cannot be harmed while within its pocket backpack, and only your Ghost can enter or interact with its own pocket backpack.

If you ever die permanently, your Ghost is removed from the pocket backpack immediately, and cannot reenter it.

A Ghost's interaction with the outside world is limited while within its pocket backpack. It can still communicate with you, and it can interact with signals. A Ghost can read an incoming broadcast, for example, or speak over a local communication network, but if a Ghost wants to interact with a physical object, it must exit its pocket backpack.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Equipment Swap',
    description: `Even from within its pocket backpack, your Ghost is able to assist you in small ways. One of these ways is to switch out the items you have equipped with items it has stored in its memory. As an action, your Ghost can take one item you are carrying or wearing and store the item in its memory at the same time it withdraws one item from its memory and places it in your hands. For worn items, your Ghost is able to instantly transmat the item off of or onto your person. In order for your Ghost to store an item, it must have enough space in its memory.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Quick Thinking',
    description: `Your Ghost's computational mind and quick reflexes allows it to move and act quickly. It can take the Dash, Disengage, or Hide actions as bonus actions on each of its turns.

Additionally, if your Ghost is within 5 feet of you when it is forced to make a saving throw from an effect it can perceive, such as being forced to make a payload saving throw from a creature it can see, it can choose to use its reaction to disappear into its pocket backpack immediately, removing itself from the area of the effect and negating the need to make the saving throw at all.

If a creature makes an attack against your Ghost that misses, or if your Ghost takes damage, it can also choose to use its reaction to disappear into its pocket backpack, if it is within 5 feet of you.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 0,
    name: 'Scanners',
    description: `As an action your Ghost can scan the local area. It can choose to scan in a sphere with a radius of 5 feet, centered on itself, or it can choose to scan in a 15-foot cone originating from itself. It must be outside its pocket backpack to do this. Your Ghost learns the location of any living creatures within range, as well as any other information at the discretion of the Architect.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 4,
    name: 'Ability Score Increase',
    description: `Your Ghost's starting ability scores are determined using the same method that determined your own starting ability scores. Furthermore, when your Ghost reaches 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one of its ability scores of your choice by 2, or you can increase two of its ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.

Your Architect will determine what level your Ghost can start at, and how your Ghost gains experience points. Your Ghost's level does not need to be the same as your level, though typically, the Ghost does start at the same level as you, and it gains a level whenever you gain a level.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 8,
    name: 'Ability Score Increase',
    description: `Your Ghost's starting ability scores are determined using the same method that determined your own starting ability scores. Furthermore, when your Ghost reaches 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one of its ability scores of your choice by 2, or you can increase two of its ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.

Your Architect will determine what level your Ghost can start at, and how your Ghost gains experience points. Your Ghost's level does not need to be the same as your level, though typically, the Ghost does start at the same level as you, and it gains a level whenever you gain a level.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 12,
    name: 'Ability Score Increase',
    description: `Your Ghost's starting ability scores are determined using the same method that determined your own starting ability scores. Furthermore, when your Ghost reaches 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one of its ability scores of your choice by 2, or you can increase two of its ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.

Your Architect will determine what level your Ghost can start at, and how your Ghost gains experience points. Your Ghost's level does not need to be the same as your level, though typically, the Ghost does start at the same level as you, and it gains a level whenever you gain a level.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 16,
    name: 'Ability Score Increase',
    description: `Your Ghost's starting ability scores are determined using the same method that determined your own starting ability scores. Furthermore, when your Ghost reaches 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one of its ability scores of your choice by 2, or you can increase two of its ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.

Your Architect will determine what level your Ghost can start at, and how your Ghost gains experience points. Your Ghost's level does not need to be the same as your level, though typically, the Ghost does start at the same level as you, and it gains a level whenever you gain a level.`,
    source: 'Ghost'
  },
  {
    _type: 'Feature',
    level: 19,
    name: 'Ability Score Increase',
    description: `Your Ghost's starting ability scores are determined using the same method that determined your own starting ability scores. Furthermore, when your Ghost reaches 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one of its ability scores of your choice by 2, or you can increase two of its ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.

Your Architect will determine what level your Ghost can start at, and how your Ghost gains experience points. Your Ghost's level does not need to be the same as your level, though typically, the Ghost does start at the same level as you, and it gains a level whenever you gain a level.`,
    source: 'Ghost'
  }
]

export const allRaces: GuardianRace[] = [
  new ReefbornAwokenRace,
  new EarthbornAwokenRace,
  new ExoABURace,
  new ExoCTHDRace,
  new ExoJSYKRace,
  new HumanCitizenRace,
  new HumanPilgrimRace,
  new HumanWayfarerRace,
  new CalusCabalRace,
  new GhaulCabalRace,
  new EliksniVandalRace,
  new EliksniCaptainRace,
  new EliksniBaronRace,
  new AcolyteKrillRace,
  new KnightKrillRace,
  new MotherKrillRace,
  new SoldierPsionRace,
  new MindFlayerPsionRace,
  new VexGoblinRace,
  new VexHobgoblinRace,
  new VexMinotaurRace
]
export const allRaceNames = allRaces.map(x => ({ id: x.id, name: x.name }))
export const raceIndex = allRaces.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<string, GuardianRace>)
export const getRace = (id: string) => raceIndex[id]
