import { Ability, ArmorItemType, DamageType, GenericItemType, HeavyWeaponItemType, Item, ItemProperty, ItemRange,
  ItemRarity, Size, MartialWeaponItemType, MiscItemType, PrimaryWeaponItemType, SimpleWeaponItemType, VehicleItemType } from '@/models/destiny'
import * as WeaponPerks from '@/models/destinyWeaponPerks'

const uuidEmpty = '00000000-0000-0000-0000-000000000000'

/* Armors */
export class PaddedArmor implements Item {
  _type = 'Item'
  id = 'Armor.LightArmor.Padded'
  uuid = uuidEmpty
  type = ArmorItemType.LightArmor
  rarity = ItemRarity.Common
  name = 'Padded Armor'
  description = `Padded armor is merely thick layers of clothing on top of the skintight spacesuit under all Guardian armor. It is typically worn by Warlocks.`
  cost = 400
  ac = '11 + Dex mod'
  conditions = ['Stealth Disadvantage']
  memory: [number, number] = [4, 1]
  weight = 13
}

export class LeatherArmor implements Item {
  _type = 'Item'
  id = 'Armor.LightArmor.Leather'
  uuid = uuidEmpty
  type = ArmorItemType.LightArmor
  rarity = ItemRarity.Common
  name = 'Leather Armor'
  description = `Called leather mostly for its texture rather than composition, leather armor is woven with sapphire wire, a sturdy and silent material that provides additional protection around the chest, neck, and across the back.`
  cost = 1000
  ac = '11 + Dex mod'
  memory: [number, number] = [4, 1]
  weight = 15
}

export class SpinweaveArmor implements Item {
  _type = 'Item'
  id = 'Armor.LightArmor.Spinweave'
  uuid = uuidEmpty
  type = ArmorItemType.LightArmor
  rarity = ItemRarity.Common
  name = 'Spinweave Armor'
  description = `This armor is made by a special process that laces the sapphire wire with spinmetal, providing additional protection without sacrificing movement or stealth.`
  cost = 12500
  ac = '12 + Dex mod'
  memory: [number, number] = [5, 1]
  weight = 18
}

export class MakeshiftArmor implements Item {
  _type = 'Item'
  id = 'Armor.MediumArmor.Makeshift'
  uuid = uuidEmpty
  type = ArmorItemType.MediumArmor
  rarity = ItemRarity.Common
  name = 'Makeshift Armor'
  description = `This armor is mostly a hodgepodge of other armor pieces. It is commonly worn by folk who lack access to the tools and materials to create better armor—as well as lazy Hunters.`
  cost = 1000
  ac = '12 + Dex mod (max 2)'
  memory: [number, number] = [4, 1]
  weight = 17
}

export class SpinwireArmor implements Item {
  _type = 'Item'
  id = 'Armor.MediumArmor.Spinwire'
  uuid = uuidEmpty
  type = ArmorItemType.MediumArmor
  rarity = ItemRarity.Common
  name = 'Spinwire Armor'
  description = `Made of a sapphire wire undersuit and sparsely overlaid with spinmetal reinforcement, this light yet sturdy armor offers good protection without being noisy.`
  cost = 5000
  ac = '13 + Dex mod (max 2)'
  memory: [number, number] = [5, 1]
  weight = 25
}

export class ReinforcedArmor implements Item {
  _type = 'Item'
  id = 'Armor.MediumArmor.Reinforced'
  uuid = uuidEmpty
  type = ArmorItemType.MediumArmor
  rarity = ItemRarity.Common
  name = 'Reinforced Armor'
  description = `This armor uses a system of layering spinmetal plates in key locations, affixed to the undersuit with special pins. These pins leave less room on the undersuit for sapphire wire, which makes finesse movement more difficult to achieve. Civilians call it "squeaky armor."`
  cost = 5000
  ac = '14 + Dex mod (max 2)'
  conditions = ['Stealth Disadvantage']
  memory: [number, number] = [5, 1]
  weight = 50
}

export class PlastwireArmor implements Item {
  _type = 'Item'
  id = 'Armor.MediumArmor.Plastwire'
  uuid = uuidEmpty
  type = ArmorItemType.MediumArmor
  rarity = ItemRarity.Common
  name = 'Plastwire Armor'
  description = `This armor uses the spinwire model but almost entirely replaces the reinforcing components with plasteel. With a more sophisticated interleaving of supporting plating with sapphire wire, this armor is able to reduce almost all noise it makes.`
  cost = 10000
  ac = '14 + Dex mod (max 2)'
  memory: [number, number] = [5, 1]
  weight = 25
}

export class SpinplateArmor implements Item {
  _type = 'Item'
  id = 'Armor.MediumArmor.Spinplate'
  uuid = uuidEmpty
  type = ArmorItemType.MediumArmor
  rarity = ItemRarity.Common
  name = 'Spinplate Armor'
  description = `A weighty armor type that almost completely covers the wearer in spinmetal plating, though it leaves many of the joints free of plating for as much movement as possible.`
  cost = 25000
  ac = '15 + Dex mod (max 2)'
  conditions = ['Stealth Disadvantage']
  memory: [number, number] = [5, 1]
  weight = 45
}

export class HalfPlastArmor implements Item {
  _type = 'Item'
  id = 'Armor.HeavyArmor.HalfPlast'
  uuid = uuidEmpty
  type = ArmorItemType.HeavyArmor
  rarity = ItemRarity.Common
  name = 'Half-plast Armor'
  description = `The lightest of the heavy armors, the plating on this is made of plasteel, and the undersuit is a thick material that helps regulate the user's body temperature.`
  cost = 2500
  ac = '14'
  memory: [number, number] = [5, 1]
  weight = 45
}

export class PlasteelArmor implements Item {
  _type = 'Item'
  id = 'Armor.HeavyArmor.Plasteel'
  uuid = uuidEmpty
  type = ArmorItemType.HeavyArmor
  rarity = ItemRarity.Common
  name = 'Plasteel Armor'
  description = `A full set of plasteel armor, with overlapping plates to protect the joints.`
  cost = 7500
  ac = '16'
  prerequisite = '13 Strength'
  conditions = ['Stealth Disadvantage']
  memory: [number, number] = [5, 1]
  weight = 60
}

export class FortifiedArmor implements Item {
  _type = 'Item'
  id = 'Armor.HeavyArmor.Fortified'
  uuid = uuidEmpty
  type = ArmorItemType.HeavyArmor
  rarity = ItemRarity.Common
  name = 'Fortified Armor'
  description = `Like plasteel, but reinforced with bulky yet incredibly sturdy relic iron. The wearer might turn to mush inside the suit, but the suit itself is incredibly durable, stainless, and scratch-resistant.`
  cost = 15000
  ac = '17'
  prerequisite = '15 Strength'
  conditions = ['Stealth Disadvantage']
  memory: [number, number] = [5, 1]
  weight = 65
}

export class RelicArmor implements Item {
  _type = 'Item'
  id = 'Armor.HeavyArmor.Relic'
  uuid = uuidEmpty
  type = ArmorItemType.HeavyArmor
  rarity = ItemRarity.Common
  name = 'Relic Armor'
  description = `A large, cumbersome armor set covered in heavy plates of relic iron. The undersuit utilizes a higher-output thermoregulation system to prevent the wearer from overheating in even slightly warm conditions.`
  cost = 45000
  ac = '18'
  prerequisite = '15 Strength'
  conditions = ['Stealth Disadvantage']
  memory: [number, number] = [6, 1]
  weight = 70
}

export class NoShell implements Item {
  _type = 'Item'
  id = 'Armor.GhostShell.NoShell'
  uuid = uuidEmpty
  type = ArmorItemType.GhostShell
  rarity = ItemRarity.Common
  name = 'No Shell'
  description = `Without the protective enclosure of a shell, a Ghost is a vulnerable ball.`
  ac = '12 + Dex mod'
}

export class GeneralistShell implements Item {
  _type = 'Item'
  id = 'Armor.GhostShell.Generalist'
  uuid = uuidEmpty
  type = ArmorItemType.GhostShell
  rarity = ItemRarity.Common
  name = 'Generalist Shell'
  description = `A classic, lightweight Ghost shell made of spinmetal and plasteel.`
  cost = 300
  ac = '13 + Dex mod'
  memory: [number, number] = [3, 1]
  weight = 2
}

/* General Weapons */
export class Dagger implements Item {
  _type = 'Item'
  id = 'Weapon.General.SimpleMelee.Dagger'
  uuid = uuidEmpty
  type = SimpleWeaponItemType.Dagger
  rarity = ItemRarity.Common
  name = 'Dagger'
  cost = 500
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Agile, ItemProperty.Finesse, ItemProperty.Thrown]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.AdaptiveFrame]
}

export class ThrowingHammer implements Item {
  _type = 'Item'
  id = 'Weapon.General.SimpleMelee.ThrowingHammer'
  uuid = uuidEmpty
  type = SimpleWeaponItemType.ThrowingHammer
  rarity = ItemRarity.Common
  name = 'Throwing Hammer'
  cost = 500
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [2, 1]
  weight = 7
  properties = [ItemProperty.Agile, ItemProperty.Thrown]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = []
}

export class Greataxe implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.Greataxe'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Greataxe
  rarity = ItemRarity.Common
  name = 'Greataxe'
  cost = 2250
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d12 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 12
  properties = [ItemProperty.Heavy, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.HeavyFrame, new WeaponPerks.WhirlwindFrame]
}

export class Broadsword implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.Broadsword'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Broadsword
  rarity = ItemRarity.Common
  name = 'Broadsword'
  cost = 2250
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 11
  properties = [ItemProperty.Heavy, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.HeavyFrame, new WeaponPerks.WhirlwindFrame]
}

export class Longsword implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.Longsword'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Longsword
  rarity = ItemRarity.Common
  name = 'Longsword'
  cost = 1500
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 8
  properties = [ItemProperty.OneHanded, ItemProperty.Versatile]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.CasterFrame, new WeaponPerks.HeavyFrame]
}

export class HeavyMaul implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.HeavyMaul'
  uuid = uuidEmpty
  type = MartialWeaponItemType.HeavyMaul
  rarity = ItemRarity.Common
  name = 'Heavy Maul'
  cost = 2500
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Heavy, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.HeavyFrame, new WeaponPerks.WhirlwindFrame]
}

export class Shortsword implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.Shortsword'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Shortsword
  rarity = ItemRarity.Common
  name = 'Shortsword'
  cost = 1250
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d6 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 7
  properties = [ItemProperty.Agile, ItemProperty.Finesse]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.AdaptiveFrame]
}

export class Smallsword implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.Smallsword'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Smallsword
  rarity = ItemRarity.Common
  name = 'Smallsword'
  cost = 1500
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 5
  properties = [ItemProperty.Finesse, ItemProperty.OneHanded]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.AdaptiveFrame, new WeaponPerks.CasterFrame]
}

export class Warhammer implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialMelee.Warhammer'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Warhammer
  rarity = ItemRarity.Common
  name = 'Warhammer'
  cost = 1500
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 7
  properties = [ItemProperty.OneHanded, ItemProperty.Versatile]
  slot1Choices = [new WeaponPerks.ElementalShotCapacity]
  slot2Choices = [new WeaponPerks.EnduringEdge, new WeaponPerks.HonedEdge, new WeaponPerks.JaggedEdge, new WeaponPerks.TransmatHilt]
  slot3Choices = [new WeaponPerks.CasterFrame, new WeaponPerks.HeavyFrame]
}

export class CombatBow implements Item {
  _type = 'Item'
  id = 'Weapon.General.MartialRanged.Combatbow'
  uuid = uuidEmpty
  type = MartialWeaponItemType.CombatBow
  rarity = ItemRarity.Common
  name = 'Combat Bow'
  description = `The combat bow is designed for use in the modern theater of war. It has a scope of 20/100/200 (medium). The standard arrows it shoots deal kinetic damage on a hit. Unlike firearms, the combat bow is relatively silent.`
  cost = 3000
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 100, 200]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Ammunition, ItemProperty.Special, ItemProperty.TwoHanded]
  linkedConsumable = (new Arrow).id
  slot1Choices = [new WeaponPerks.FieldScout, new WeaponPerks.FullDraw, new WeaponPerks.NaturalString]
  slot2Choices = [new WeaponPerks.Elemental, new WeaponPerks.Rangefinder, new WeaponPerks.ShootToLoot, new WeaponPerks.Snapshot, new WeaponPerks.ThreatDetectorSights]
  slot3Choices = [new WeaponPerks.ArchersTempo, new WeaponPerks.FinalRound, new WeaponPerks.HiddenHand]
}

/* Simple Firearms */
export class AutoRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.AutoRifle'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.AutoRifle
  rarity = ItemRarity.Common
  name = 'Auto Rifle'
  cost = 1750
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [30, 40, 75]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 10
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FiringLine, new WeaponPerks.FocusedFire, new WeaponPerks.HipFire]
  slot2Choices = [new WeaponPerks.Exhumed, new WeaponPerks.ExplosiveRounds, new WeaponPerks.Lightweight, new WeaponPerks.Rangefinder, new WeaponPerks.Unflinching]
  slot3Choices = [new WeaponPerks.CrowdControl, new WeaponPerks.Demolitionist, new WeaponPerks.OneTwoPunch, new WeaponPerks.TapTheTrigger]
}

export class HandCannon implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.HandCannon'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Common
  name = 'Hand Cannon'
  cost = 1500
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.OneHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FocusedFire, new WeaponPerks.OpeningShot, new WeaponPerks.Outlaw]
  slot2Choices = [new WeaponPerks.Exhumed, new WeaponPerks.ExplosiveRounds, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.ArmyOfOne, new WeaponPerks.CrowdControl, new WeaponPerks.FinalRound, new WeaponPerks.LuckInTheChamber]
}

export class PulseRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.PulseRifle'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.PulseRifle
  rarity = ItemRarity.Common
  name = 'Pulse Rifle'
  cost = 2500
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d4 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 9
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FeatherweightBolt, new WeaponPerks.FocusedFire, new WeaponPerks.HeadSeeker]
  slot2Choices = [new WeaponPerks.Exhumed, new WeaponPerks.Lightweight, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot, new WeaponPerks.Unflinching]
  slot3Choices = [new WeaponPerks.Demolitionist, new WeaponPerks.HiddenHand, new WeaponPerks.LuckInTheChamber, new WeaponPerks.RicochetRounds]
}

export class ScoutRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.ScoutRifle'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.ScoutRifle
  rarity = ItemRarity.Common
  name = 'Scout Rifle'
  cost = 2000
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [15, 120, 140]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FieldScout, new WeaponPerks.TakeAKnee, new WeaponPerks.ZenMoment]
  slot2Choices = [new WeaponPerks.ExplosiveRounds, new WeaponPerks.Lightweight, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot, new WeaponPerks.ThreatDetectorSights]
  slot3Choices = [new WeaponPerks.ArmyOfOne, new WeaponPerks.LuckInTheChamber, new WeaponPerks.RapidHit, new WeaponPerks.VorpalWeapon]
}

export class Sidearm implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.Sidearm'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.Sidearm
  rarity = ItemRarity.Common
  name = 'Sidearm'
  cost = 1250
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 30, 40]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 3
  properties = [ItemProperty.Agile]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FeatherweightBolt, new WeaponPerks.PerfectBalance, new WeaponPerks.UnderPressure]
  slot2Choices = [new WeaponPerks.BattleRunner, new WeaponPerks.Elemental, new WeaponPerks.Lightweight, new WeaponPerks.Osmosis, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.Demolitionist, new WeaponPerks.RapidHit, new WeaponPerks.Swashbuckler, new WeaponPerks.TapTheTrigger, new WeaponPerks.VorpalWeapon]
}

export class SubmachineGun implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.SubmachineGun'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.SubmachineGun
  rarity = ItemRarity.Common
  name = 'Submachine Gun'
  cost = 1250
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [10, 15, 30]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 4
  properties = [ItemProperty.Agile, ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.HipFire, new WeaponPerks.LightweightFrame, new WeaponPerks.SecretRound]
  slot2Choices = [new WeaponPerks.BattleRunner, new WeaponPerks.ExplosiveRounds, new WeaponPerks.Lightweight, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.CrowdControl, new WeaponPerks.OneTwoPunch, new WeaponPerks.Surrounded, new WeaponPerks.Swashbuckler, new WeaponPerks.TapTheTrigger]
}

export class TraceRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Simple.SimpleFirearms.TraceRifle'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.TraceRifle
  rarity = ItemRarity.Common
  name = 'Trace Rifle'
  description = `Trace rifles always fire a line of energy up to the weapon's listed range, which quickly dissipates and becomes ineffective beyond that range. You cannot take a shot against a target beyond the weapon's listed range. When you take a shot with a trace rifle, you must always make your attack against the first target in the line of your attack.`
  cost = 2500
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d6 + [mod]', type: DamageType.Light }
  ]
  scope: [number, number, number] = [30, 40, 75]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.EnhancedBattery, new WeaponPerks.FiringLine, new WeaponPerks.Supercharger]
  slot2Choices = [new WeaponPerks.Exhumed, new WeaponPerks.ExtendedBarrel, new WeaponPerks.Lightweight, new WeaponPerks.Osmosis]
  slot3Choices = [new WeaponPerks.AugmentedFocus, new WeaponPerks.Persistence, new WeaponPerks.VorpalWeapon]
}

/* Martial Firearms */
export class FusionRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.FusionRifle'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.FusionRifle
  rarity = ItemRarity.Common
  name = 'Fusion Rifle'
  cost = 3000
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Light }
  ]
  scope: [number, number, number] = [15, 25, 30]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.EnergyProjectiles, ItemProperty.Finesse, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.AcceleratedCoils, new WeaponPerks.EnhancedBattery, new WeaponPerks.ParticleScattering]
  slot2Choices = [new WeaponPerks.BattleRunner, new WeaponPerks.Osmosis, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.Demolitionist, new WeaponPerks.OneTwoPunch, new WeaponPerks.ShieldBreaker, new WeaponPerks.Surrounded]
}

export class GrenadeLauncher implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.GrenadeLauncher'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.GrenadeLauncher
  rarity = ItemRarity.Common
  name = 'Grenade Launcher'
  description = `Rounds fired from a grenade launcher are reduced by 20 feet.`
  cost = 3500
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 50, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 4
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.PerfectBalance, new WeaponPerks.CrowdControl, new WeaponPerks.VolatileLaunch]
  slot2Choices = [new WeaponPerks.Elemental, new WeaponPerks.Exhumed, new WeaponPerks.Osmosis, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.FullCourt, new WeaponPerks.RicochetRounds, new WeaponPerks.StickyGrenades, new WeaponPerks.WaveGrenades]
}

export class LightMachineGun implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.LightMachineGun'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LightMachineGun
  rarity = ItemRarity.Common
  name = 'Light Machine Gun'
  cost = 3750
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 40, 80]
  scopeRange = ItemRange.Close
  memory: [number, number] = [5, 1]
  weight = 21
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.FiringLine, new WeaponPerks.FocusedFire, new WeaponPerks.TapTheTrigger]
  slot2Choices = [new WeaponPerks.ArmorPiercingRounds, new WeaponPerks.Elemental, new WeaponPerks.ExplosiveRounds, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot, new WeaponPerks.ThreatDetectorSights]
  slot3Choices = [new WeaponPerks.ArmyOfOne, new WeaponPerks.Demolitionist, new WeaponPerks.Persistence, new WeaponPerks.VorpalWeapon]
}

export class LinearFusionRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.LinearFusionRifle'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LinearFusionRifle
  rarity = ItemRarity.Common
  name = 'Linear Fusion Rifle'
  cost = 3250
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Light }
  ]
  scope: [number, number, number] = [15, 120, 340]
  scopeRange = ItemRange.Long
  memory: [number, number] = [4, 1]
  weight = 12
  properties = [ItemProperty.Loading, ItemProperty.EnergyProjectiles, ItemProperty.TwoHanded]
  shotCapacity = 3
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.AcceleratedCoils, new WeaponPerks.ShieldBreaker, new WeaponPerks.EnhancedBattery]
  slot2Choices = [new WeaponPerks.Lightweight, new WeaponPerks.Osmosis, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot, new WeaponPerks.ThreatDetectorSights]
  slot3Choices = [new WeaponPerks.FinalRound, new WeaponPerks.Mulligan, new WeaponPerks.VorpalWeapon]
}

export class RocketLauncher implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.RocketLauncher'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.RocketLauncher
  rarity = ItemRarity.Common
  name = 'Rocket Launcher'
  cost = 4000
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.ExplosiveKinetic }
  ]
  scope: [number, number, number] = [30, 50, 80]
  scopeRange = ItemRange.Close
  memory: [number, number] = [5, 1]
  weight = 25
  properties = [ItemProperty.HighRecoil, ItemProperty.Heavy, ItemProperty.Loading, ItemProperty.Payload, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new Rocket).id
  slot1Choices = [new WeaponPerks.DualBarrel, new WeaponPerks.HeavyPayload, new WeaponPerks.ImpactCasing, new WeaponPerks.Tracking]
  slot2Choices = [new WeaponPerks.BattleRunner, new WeaponPerks.Elemental, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.ArmyOfOne, new WeaponPerks.BlackPowder, new WeaponPerks.ClusterBombs, new WeaponPerks.Demolitionist, new WeaponPerks.Javelin]
}

export class Shotgun implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.Shotgun'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.Shotgun
  rarity = ItemRarity.Common
  name = 'Shotgun'
  cost = 3000
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [10, 15, 20]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.PrecisionFrame, new WeaponPerks.RifledBarrel, new WeaponPerks.ShotPackage]
  slot2Choices = [new WeaponPerks.ArmorPiercingRounds, new WeaponPerks.BattleRunner, new WeaponPerks.Elemental, new WeaponPerks.Exhumed, new WeaponPerks.Snapshot]
  slot3Choices = [new WeaponPerks.FinalRound, new WeaponPerks.OneTwoPunch, new WeaponPerks.Surrounded, new WeaponPerks.TrenchBarrel]
}

export class SniperRifle implements Item {
  _type = 'Item'
  id = 'Weapon.Martial.MartialFirearms.SniperRifle'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Common
  name = 'Sniper Rifle'
  cost = 3750
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [0, 300, 600]
  scopeRange = ItemRange.Long
  memory: [number, number] = [5, 1]
  weight = 12
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.BoxBreathing, new WeaponPerks.OpeningShot, new WeaponPerks.RifledBarrel]
  slot2Choices = [new WeaponPerks.ArmorPiercingRounds, new WeaponPerks.Elemental, new WeaponPerks.ExplosiveRounds, new WeaponPerks.Rangefinder, new WeaponPerks.Snapshot, new WeaponPerks.ThreatDetectorSights]
  slot3Choices = [new WeaponPerks.FinalRound, new WeaponPerks.RapidHit, new WeaponPerks.TakeAKnee]
}

/* Additional Gear */
export class Arrow implements Item {
  _type = 'Item'
  id = 'Consumable.Arrow'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Arrow'
  cost = 10
  memory: [number, number] = [1, 99]
  weight = 0.02
}

export class PrimaryAmmo implements Item {
  _type = 'Item'
  id = 'Consumable.PrimaryAmmo'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Primary Ammo'
  cost = 250
  memory: [number, number] = [1, 50]
  weight = 1
}

export class Rocket implements Item {
  _type = 'Item'
  id = 'Consumable.Rocket'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Rocket'
  cost = 1000
  memory: [number, number] = [4, 10]
  weight = 2
}

export class HeavyAmmo implements Item {
  _type = 'Item'
  id = 'Consumable.HeavyAmmo'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Heavy Ammo'
  cost = 100
  memory: [number, number] = [1, 50]
  weight = .5
}

export class BackPack implements Item {
  _type = 'Item'
  id = 'Container.Backpack'
  uuid = uuidEmpty
  type = GenericItemType.Container
  rarity = ItemRarity.Common
  name = 'Backpack'
  cost = 750
  memory: [number, number] = [3, 10]
  weight = 3
}

export class Battery implements Item {
  _type = 'Item'
  id = 'Generic.Battery'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Battery'
  description = `Most modern devices use a universal battery pack. Solar-charged batteries can be recharged by setting them out in full, bright sun for 8 hours. The lifespan of a battery is determined by the device it is used with.`
  cost = 200
  memory: [number, number] = [2, 50]
  weight = 0.01
}

export class Binoculars implements Item {
  _type = 'Item'
  id = 'Generic.Binoculars'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Binoculars'
  description = `An optical instrument used for viewing distant objects. When held to your eyes, you can use this device to view all objects within a 15-foot cube area that is up to 600 feet away from you as if it were only 20 feet away.`
  cost = 1500
  memory: [number, number] = [2, 50]
  weight = 2
}

export class BlanketHeated implements Item {
  _type = 'Item'
  id = 'Generic.BlanketHeated'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Heated Blanket'
  description = `Heated blankets are powered by a universal battery. While turned on, the blanket will keep a Medium creature warm for up to 8 hours.`
  cost = 1000
  memory: [number, number] = [4, 10]
  weight = 3
}

export class BlanketWool implements Item {
  _type = 'Item'
  id = 'Generic.BlanketWool'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Wool Blanket'
  cost = 300
  memory: [number, number] = [3, 50]
  weight = 7
}

export class Book implements Item {
  _type = 'Item'
  id = 'Generic.Book'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Book'
  cost = 50
  memory: [number, number] = [1, 99]
  weight = 1
}

export class BoxOfNails implements Item {
  _type = 'Item'
  id = 'Generic.BoxOfNails'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Box of 100 nails'
  cost = 50
  memory: [number, number] = [1, 1]
  weight = 3
}

export class Bucket implements Item {
  _type = 'Item'
  id = 'Generic.Bucket'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Bucket'
  cost = 250
  memory: [number, number] = [1, 50]
  weight = 2
}

export class BungieCord implements Item {
  _type = 'Item'
  id = 'Generic.BungieCord'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Bungie Cord'
  cost = 50
  memory: [number, number] = [1, 99]
  weight = 1
}

export class DigitalCamera implements Item {
  _type = 'Item'
  id = 'Generic.DigitalCamera'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Digital Camera'
  description = `A device that can be used to record high-definition images. They use a universal battery, which lasts 6 hours.`
  cost = 9000
  memory: [number, number] = [3, 1]
  weight = 5
}

export class CellPhone implements Item {
  _type = 'Item'
  id = 'Generic.CellPhone'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Cell Phone'
  description = `A complex, tiny device that allows for person-to-person communication when both the caller and receiver are within range of a cell tower, or a Vanguard relay beacon. A basic cell phone can only perform audio or text communication, while a smartphone can also perform all the functions of a tablet computer.`
  cost = 20000
  memory: [number, number] = [4, 1]
  weight = 2
}

export class Chalk implements Item {
  _type = 'Item'
  id = 'Generic.Chalk'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Chalk (1 stick)'
  cost = 10
  memory: [number, number] = [1, 99]
  weight = 0.01
}

export class HunterCloak implements Item {
  _type = 'Item'
  id = 'ClassItem.HunterCloak'
  uuid = uuidEmpty
  type = GenericItemType.ClassItem
  rarity = ItemRarity.Common
  name = 'Hunter Cloak'
  cost = 1000
  memory: [number, number] = [1, 10]
  weight = 3
}

export class TitanMark implements Item {
  _type = 'Item'
  id = 'ClassItem.TitanMark'
  uuid = uuidEmpty
  type = GenericItemType.ClassItem
  rarity = ItemRarity.Common
  name = 'Titan Mark'
  cost = 250
  memory: [number, number] = [1, 10]
  weight = 2
}

export class WarlockBond implements Item {
  _type = 'Item'
  id = 'ClassItem.WarlockBond'
  uuid = uuidEmpty
  type = GenericItemType.ClassItem
  rarity = ItemRarity.Common
  name = 'Warlock Bond'
  cost = 3000
  memory: [number, number] = [3, 1]
  weight = 3
}

export class CasualWear implements Item {
  _type = 'Item'
  id = 'Clothing.CasualWear'
  uuid = uuidEmpty
  type = GenericItemType.Clothing
  rarity = ItemRarity.Common
  name = 'Casual Wear'
  cost = 150
  memory: [number, number] = [3, 10]
  weight = 3
}

export class FormalWear implements Item {
  _type = 'Item'
  id = 'Clothing.FormalWear'
  uuid = uuidEmpty
  type = GenericItemType.Clothing
  rarity = ItemRarity.Common
  name = 'Formal Wear'
  cost = 750
  memory: [number, number] = [3, 10]
  weight = 4
}

export class RainPoncho implements Item {
  _type = 'Item'
  id = 'Clothing.RainPoncho'
  uuid = uuidEmpty
  type = GenericItemType.Clothing
  rarity = ItemRarity.Common
  name = 'Rain Poncho'
  cost = 30
  memory: [number, number] = [3, 10]
  weight = 1
}

export class WinterCoat implements Item {
  _type = 'Item'
  id = 'Clothing.WinterCoat'
  uuid = uuidEmpty
  type = GenericItemType.Clothing
  rarity = ItemRarity.Common
  name = 'Winter Coat'
  cost = 300
  memory: [number, number] = [3, 10]
  weight = 3
}

export class ComputerDesktop implements Item {
  _type = 'Item'
  id = 'Generic.ComputerDesktop'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Computer Desktop'
  description = `Computers can perform a variety of tasks. Desktop computers (includes the tower, monitor, keyboard, and mouse) require a power outlet or portable generator to function, and can perform intensive tasks such as image or video editing. Tablet computers can't perform intensive tasks but they are easily portable, using a universal battery that has a 3-hour lifespan. Tablet computers consume no battery while dormant.`
  cost = 52500
  memory: [number, number] = [7, 1]
  weight = 15
}

export class ComputerTablet implements Item {
  _type = 'Item'
  id = 'Generic.ComputerTablet'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Computer Tablet'
  description = `Computers can perform a variety of tasks. Desktop computers (includes the tower, monitor, keyboard, and mouse) require a power outlet or portable generator to function, and can perform intensive tasks such as image or video editing. Tablet computers can't perform intensive tasks but they are easily portable, using a universal battery that has a 3-hour lifespan. Tablet computers consume no battery while dormant.`
  cost = 20000
  memory: [number, number] = [4, 1]
  weight = 4
}

export class Crowbar implements Item {
  _type = 'Item'
  id = 'Generic.Crowbar'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Crowbar'
  cost = 25
  memory: [number, number] = [1, 99]
  weight = 5
}

export class DuctTape implements Item {
  _type = 'Item'
  id = 'Generic.DuctTape'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Duct tape, 100 ft.'
  cost = 50
  memory: [number, number] = [1, 50]
  weight = 3
}

export class ElectricDrill implements Item {
  _type = 'Item'
  id = 'Generic.ElectricDrill'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Electric Drill'
  description = ` A handheld device for drilling screws into surfaces. Comes with a standard set of drill bits. This device can be used continuously for up to 4 hours.`
  cost = 3000
  memory: [number, number] = [2, 1]
  weight = 7
}

export class LargeEtherContainer implements Item {
  _type = 'Item'
  id = 'Consumable.LargeEtherContainer'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Large Ether Container'
  description = `Special container device for carrying Eliksni ether. A small container can hold 10 rations of ether, a medium container can hold 25 rations, and a large container can hold 100 rations.`
  cost = 500
  memory: [number, number] = [1, 10]
  weight = 5
}

export class MediumEtherContainer implements Item {
  _type = 'Item'
  id = 'Consumable.MediumEtherContainer'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Medium Ether Container'
  description = `Special container device for carrying Eliksni ether. A small container can hold 10 rations of ether, a medium container can hold 25 rations, and a large container can hold 100 rations.`
  cost = 250
  memory: [number, number] = [1, 10]
  weight = 3
}

export class SmallEtherContainer implements Item {
  _type = 'Item'
  id = 'Consumable.SmallEtherContainer'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Small Ether Container'
  description = `Special container device for carrying Eliksni ether. A small container can hold 10 rations of ether, a medium container can hold 25 rations, and a large container can hold 100 rations.`
  cost = 100
  memory: [number, number] = [1, 10]
  weight = 2
}

export class FirewoodBundle implements Item {
  _type = 'Item'
  id = 'Generic.FirewoodBundle'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Firewood Bundle'
  cost = 1500
  memory: [number, number] = [3, 10]
  weight = 12
}

export class Floodlight implements Item {
  _type = 'Item'
  id = 'Generic.Floodlight'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Floodlight'
  description = `A portable device that shines bright light in a 60-foot cone, and dim light 100 feet beyond that. It uses a special battery that costs 1,000 glimmer and lasts for 8 hours of continuous use.`
  cost = 15000
  memory: [number, number] = [5, 1]
  weight = 10
}

export class FoldoutCot implements Item {
  _type = 'Item'
  id = 'Generic.FoldoutCot'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Fold-out Cot'
  cost = 6000
  memory: [number, number] = [4, 1]
  weight = 18
}

export class FoldingChair implements Item {
  _type = 'Item'
  id = 'Generic.FoldingChair'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Folding Chair'
  cost = 300
  memory: [number, number] = [3, 50]
  weight = 5
}

export class GasMask implements Item {
  _type = 'Item'
  id = 'Generic.GasMask'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Gas Mask'
  description = `This battery-powered device filters out most airborne toxins, poisons, viruses, and other contaminants. It can be used continuously for up to 24 hours.`
  cost = 3000
  memory: [number, number] = [2, 1]
  weight = 1
}

export class Gasoline implements Item {
  _type = 'Item'
  id = 'Generic.Gasoline'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Gasoline (1 gal)'
  cost = 2000
  memory: [number, number] = [2, 1]
  weight = 6
}

export class Hammock implements Item {
  _type = 'Item'
  id = 'Generic.Hammock'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Hammock'
  cost = 2000
  memory: [number, number] = [4, 1]
  weight = 4
}

export class Handcuffs implements Item {
  _type = 'Item'
  id = 'Generic.Handcuffs'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Handcuffs'
  description = `A pair of lockable linked metal rings for securing a creature's wrists in close proximity to each other, usually affixed in a way to hold the creature's arms behind their back. Each ring has a hinged ratchet arm, which prevents the ring from opening once it has been closed. Standard handcuffs can fit on the wrists of a Medium or smaller creature. DC 17 Strength check to break.`
  cost = 50
  memory: [number, number] = [1, 99]
  weight = 1
}

export class HumanVoiceSynth implements Item {
  _type = 'Item'
  id = 'Generic.HumanVoiceSynth'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Human Voice Synth'
  description = `A vocal modulator device worn over the mouth, used by Eliksni to speak human languages. It can be used continuously for up to 8 hours.`
  cost = 3000
  memory: [number, number] = [2, 1]
  weight = 1
}

export class HuntingTrap implements Item {
  _type = 'Item'
  id = 'Consumable.HuntingTrap'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Hunting Trap'
  description = `Colloquially called a bear trap, when you use your action to set it, this trap forms a saw-toothed steel ring that snaps shut when a creature steps on a pressure plate in the center. The trap is affixed by a heavy chain to another object, such as a tree or a spike driven into the ground. A creature that steps on the plate must succeed on a DC 13 Dexterity saving throw or take 1d4 piercing damage and stop moving. Thereafter, until the creature breaks free of the trap, its movement is limited by the length of the chain (typically 3 feet long). A creature can use its action to make a DC 13 Strength check, freeing itself or another creature within its reach on a success. Each failed check deals 1 piercing damage to the trapped creature.`
  cost = 750
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4', type: DamageType.Kinetic }
  ]
  savingThrowFormula = [
    { dc: '13', ability: Ability.Dexterity }
  ]
  memory: [number, number] = [1, 50]
  weight = 25
}

export class KnifeBelt implements Item {
  _type = 'Item'
  id = 'Generic.KnifeBelt'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Knife Belt'
  description = `Allows a creature to easily carry up to 6 daggers or knives. Optionally, the daggers or knives may be secured. It costs an action to unsecure a dagger or knife, but daggers and knives secured by the belt cannot be removed without alerting the wearer.`
  cost = 1500
  memory: [number, number] = [3, 50]
  weight = 4
}

export class LadderCollapsable implements Item {
  _type = 'Item'
  id = 'Generic.LadderCollapsable'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Ladder, Collapsable'
  description = `When fully extended this ladder is 15 feet tall and 1 foot wide. When collapsed, it is 2 feet tall and 2 feet wide.`
  cost = 5000
  memory: [number, number] = [5, 1]
  weight = 12
}

export class LEDLightHandheld implements Item {
  _type = 'Item'
  id = 'Consumable.LEDLightHandheld'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'LED Light, Handheld'
  description = `Also called a flashlight or torch, this device shines bright light in a 15-foot cone, and dim light 10 feet beyond that. It uses a universal battery, and can be left on for up to 8 hours.`
  cost = 2000
  memory: [number, number] = [2, 1]
  weight = 2
}

export class Lighter implements Item {
  _type = 'Item'
  id = 'Generic.Lighter'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Lighter'
  description = `As an action, a creature can use this tiny device to create a small flame (deals 1 fire damage). Ignites kindling and other sources of fuel for a fire.`
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d1', type: DamageType.Solar }
  ]
  cost = 2000
  memory: [number, number] = [2, 1]
  weight = 2
}

export class LockDigital implements Item {
  _type = 'Item'
  id = 'Generic.LockDigital'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Lock, Digital'
  description = `A mechanical lock requires a physical key to open it. A digital lock is a device, usually one built into the object it is locking, that can require anything from a specific biometric reading (retina, fingerprint, voice) to basic keycode input to open.`
  cost = 5000
  memory: [number, number] = [2, 1]
  weight = 1
}

export class LockMechanical implements Item {
  _type = 'Item'
  id = 'Generic.LockMechanical'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Lock, Mechanical'
  description = `A mechanical lock requires a physical key to open it. A digital lock is a device, usually one built into the object it is locking, that can require anything from a specific biometric reading (retina, fingerprint, voice) to basic keycode input to open.`
  cost = 3000
  memory: [number, number] = [2, 10]
  weight = 3
}

export class Megaphone implements Item {
  _type = 'Item'
  id = 'Generic.Megaphone'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Megaphone'
  description = `A device that, when spoken into, amplifies the volume of your voice so it can be heard clearly from up to 600 feet away. It also has a button that, while held down, will cause the device to make a rising siren noise that can be heard for the same distance. This device can be used continuously for 24 hours.`
  cost = 2000
  memory: [number, number] = [2, 1]
  weight = 3
}

export class MotionActivatedCamera implements Item {
  _type = 'Item'
  id = 'Generic.MotionActivatedCamera'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Motion-activated Camera'
  description = `A camera device that records a series of images when it detects the motion of a Small or larger creature in a 15-foot cone in front of it. It can transmit these images to a connected device within 200 feet of it that is capable of receiving image files.`
  cost = 6000
  memory: [number, number] = [2, 1]
  weight = 3
}

export class NightVisionArmorAugmentation implements Item {
  _type = 'Item'
  id = 'Generic.NightVisionArmorAugmentation'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Night Vision Armor Augmentation'
  description = `An augmentation for Guardian helmets that alters the visor. If you're wearing a helmet with this augmentation, you can see in dim light within 30 feet of you as if it were bright light, and in darkness as if it were dim light.`
  cost = 500
}

export class Oil implements Item {
  _type = 'Item'
  id = 'Consumable.Oil'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Oil (1 quart)'
  cost = 1000
  memory: [number, number] = [1, 1]
  weight = 2
}

export class Pillow implements Item {
  _type = 'Item'
  id = 'Generic.Pillow'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Pillow'
  cost = 250
  memory: [number, number] = [1, 50]
  weight = 1
}

export class PlasmaCutter implements Item {
  _type = 'Item'
  id = 'Generic.PlasmaCutter'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Plasma cutter'
  description = `Good for cutting through up to 3 inches of steel or weaker metals. Loud and bright when used. Burns wood, melts plastic. Deals 1d4 fire damage against a creature.`
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4', type: DamageType.Solar }
  ]
  cost = 5000
  memory: [number, number] = [2, 1]
  weight = 10
}

export class Pliers implements Item {
  _type = 'Item'
  id = 'Generic.Pliers'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Pliers'
  cost = 100
  memory: [number, number] = [1, 99]
  weight = 1
}

export class PortableGenerator implements Item {
  _type = 'Item'
  id = 'Generic.PortableGenerator'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Portable Generator'
  description = `A device roughly 2 feet wide and 2 feet tall. It acts as a power outlet, providing up to 3,000 watts to devices plugged into it. The generator requires gasoline to run, and it lasts for 6 hours per gallon of gasoline.`
  cost = 45000
  memory: [number, number] = [6, 1]
  weight = 30
}

export class PortableHeater implements Item {
  _type = 'Item'
  id = 'Generic.PortableHeater'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Portable Heater'
  description = `A device that looks like an oversized hooded lantern roughly 1 foot tall. When powered on, it can provide comfortable heating for all creatures within a 15-foot radius. It uses a special battery that costs 2,500 glimmer, and can be used continuously for up to 24 hours.`
  cost = 20000
  memory: [number, number] = [4, 1]
  weight = 25
}

export class Quiver implements Item {
  _type = 'Item'
  id = 'Container.Quiver'
  uuid = uuidEmpty
  type = GenericItemType.Container
  rarity = ItemRarity.Common
  name = 'Quiver'
  description = `A quiver for use with combat bows. It can hold up to 100 arrows. Its memory is 1/1 while it has any arrows in it.`
  cost = 50
  memory: [number, number] = [1, 99]
  weight = 1
}

export class Ration implements Item {
  _type = 'Item'
  id = 'Consumable.Ration'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Ration (1 day)'
  cost = 50
  memory: [number, number] = [1, 99]
  weight = 1
}

export class RelayBeacon implements Item {
  _type = 'Item'
  id = 'Generic.RelayBeacon'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Relay Beacon'
  description = `When activated, this device can connect anyone within 6 miles to the Vanguard network. This beacon can be used for communication or to transmat items to and from a Guardian's vault if the beacon, through one connection after the other, is able to link back to the Tower. These beacons can be blocked by conditions like dense physical terrain and signal jammers.`
  cost = 5000
  memory: [number, number] = [2, 1]
  weight = 10
}

export class RelayBeaconQuantum implements Item {
  _type = 'Item'
  id = 'Generic.RelayBeaconQuantum'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Relay Beacon Quantum'
  description = `Advanced relay beacon device that connects to Vanguard comm nets using quantum-entangled signaling. Ignores distance to other quantum beacons. Creatures within 6 miles of the beacon can connect to it.`
  cost = 37500
  memory: [number, number] = [5, 1]
  weight = 15
}

export class Scissors implements Item {
  _type = 'Item'
  id = 'Generic.Scissors'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Scissors'
  cost = 50
  memory: [number, number] = [1, 99]
  weight = 1
}

export class Shovel implements Item {
  _type = 'Item'
  id = 'Generic.Shovel'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Shovel'
  cost = 25
  memory: [number, number] = [1, 99]
  weight = 5
}

export class SignalBooster implements Item {
  _type = 'Item'
  id = 'Generic.SignalBooster'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Signal Booster'
  description = `A tiny device that amplifies signals it receives, extending the radius of signals by 500 feet.`
  cost = 3000
  memory: [number, number] = [2, 1]
  weight = 0.5
}

export class SignalFlareGun implements Item {
  _type = 'Item'
  id = 'Consumable.SignalFlareGun'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Signal Flare, Gun'
  description = `Device that produces a bright, and usually colorful, light. Signal flare sticks are used by twisting the cap; signal flare guns shoot a small ball of light that arcs through the air. Hitting a creature with a lit signal flare deals 1 fire damage.`
  cost = 3000
  memory: [number, number] = [2, 1]
  weight = 3
}

export class SignalFlareStick implements Item {
  _type = 'Item'
  id = 'Consumable.SignalFlareStick'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Signal Flare, Stick'
  description = `Device that produces a bright, and usually colorful, light. Signal flare sticks are used by twisting the cap; signal flare guns shoot a small ball of light that arcs through the air. Hitting a creature with a lit signal flare deals 1 fire damage.`
  cost = 2000
  memory: [number, number] = [2, 10]
  weight = 0.25
}

export class SignalJammer implements Item {
  _type = 'Item'
  id = 'Generic.SignalJammer'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Signal Jammer'
  description = ` Device that spams junk signals, corrupting all local signals and making it difficult or impossible to communicate via signal-based devices within 300 feet of it.`
  cost = 3000
  memory: [number, number] = [2, 1]
  weight = 1
}

export class SleepingBag implements Item {
  _type = 'Item'
  id = 'Generic.SleepingBag'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Sleeping Bag'
  description = `A comfortable, insulated zipper bag large enough for a Medium creature. Water-resistant, and provides ample protection against cold weather, but not below freezing temperatures.`
  cost = 750
  memory: [number, number] = [1, 50]
  weight = 5
}

export class Spacesuit implements Item {
  _type = 'Item'
  id = 'Generic.Spacesuit'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Spacesuit'
  description = `Lightweight space-grade, fully sealed suit. It has a 4-hour supply of oxygen. Additional oxygen sources can be attached or exchanged on the suit (500 glimmer per container). The suit is considered a device in and of itself, and is powered with a battery that can run continuously for 24 hours.`
  cost = 24000
  memory: [number, number] = [3, 1]
  weight = 10
}

export class SurveillanceDrone implements Item {
  _type = 'Item'
  id = 'Generic.SurveillanceDrone'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Surveillance Drone'
  description = `These Tiny constructs have 1 hit point, an AC of 18, and a flying speed of 40 feet. They can connect to a tablet computer and be used to gather visual information. They can only operate within 1 mile of the controller.`
  cost = 22500
  memory: [number, number] = [3, 1]
  weight = 8
}

export class Tarp10 implements Item {
  _type = 'Item'
  id = 'Generic.Tarp10'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Tarp, 10x10ft.'
  cost = 550
  memory: [number, number] = [11, 50]
  weight = 4
}

export class Tarp5 implements Item {
  _type = 'Item'
  id = 'Generic.Tarp5'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Tarp, 5x5ft.'
  cost = 200
  memory: [number, number] = [4, 50]
  weight = 4
}

export class Telescope implements Item {
  _type = 'Item'
  id = 'Generic.Telescope'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Telescope'
  description = `A device that uses a universal battery (6-hour lifespan) that allows an observer to view distant objects.`
  cost = 37500
  memory: [number, number] = [5, 1]
  weight = 25
}

export class TentTwoPerson implements Item {
  _type = 'Item'
  id = 'Generic.TentTwoPerson'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Tent, two-person'
  cost = 18000
  memory: [number, number] = [12, 1]
  weight = 40
}

export class WaterBottle implements Item {
  _type = 'Item'
  id = 'Consumable.WaterBottle'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Water Bottle'
  description = `Holds 4 days' worth of water. Selffilters most common contaminants. Dentproof, heatproof, bullet-resistant.`
  cost = 25
  memory: [number, number] = [1, 99]
  weight = 1
}

export class WaterFilter implements Item {
  _type = 'Item'
  id = 'Generic.WaterFilter'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Water Filter'
  description = `A small object that can be attached to hoses, water bottles, or faucets to filter out most common contaminants, including dirt or debris, from water that passes through the filter. Needs to be cleaned between uses.`
  cost = 1500
  memory: [number, number] = [2, 50]
  weight = 2
}

export class WaterproofBox implements Item {
  _type = 'Item'
  id = 'Container.WaterproofBox'
  uuid = uuidEmpty
  type = GenericItemType.Container
  rarity = ItemRarity.Common
  name = 'Waterproof Box'
  description = `A small box 1 foot wide, 6 inches deep, and 6 inches tall. When sealed, water cannot enter it.`
  cost = 250
  memory: [number, number] = [1, 10]
  weight = 18
}

export class WireCable implements Item {
  _type = 'Item'
  id = 'Consomable.Wirecable'
  uuid = uuidEmpty
  type = GenericItemType.Consumable
  rarity = ItemRarity.Common
  name = 'Wire Cable, 50 ft.'
  description = `Several individual steel wires wrapped in a helix pattern that resembles rope, and treated for durability. Can hold up to 14,000 pounds and doesn't corrode in outdoor conditions`
  cost = 100
  memory: [number, number] = [1, 99]
  weight = 12
}

export class Wrench implements Item {
  _type = 'Item'
  id = 'Generic.Wrench'
  uuid = uuidEmpty
  type = GenericItemType.Generic
  rarity = ItemRarity.Common
  name = 'Wrench'
  cost = 250
  memory: [number, number] = [1, 50]
  weight = 3
}

/* Toolkit */

export class Armorsmithing implements Item {
  _type = 'Item'
  id = 'Toolkit.Armorsmithing'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Armorsmithing toolkit'
  description = `A typical armorsmithing kit includes tools such as a screwdriver set, sewing needles of various sizes, a handheld spot welder, soldering rod, clamps, hyperglue, and replacement parts for circuitry. When used in conjunction with armor parts and appropriate resources, these kits can be used to fix most armor, including exotic armor. They can also be used to improve the AC of Guardian armor.`
  cost = 7500
  memory: [number, number] = [5, 1]
  weight = 25
}

export class Climbing implements Item {
  _type = 'Item'
  id = 'Toolkit.Climbing'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Climbing toolkit'
  description = `This toolkit consists of a climbing harness, bag of powdered chalk, 1,000 feet of 600-pound rope, a wide selection of carabiners, and a climbing pick.`
  cost = 1000
  memory: [number, number] = [3, 1]
  weight = 12
}

export class Cooking implements Item {
  _type = 'Item'
  id = 'Toolkit.Cooking'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Cooking toolkit'
  description = `A set of cooking utensils coupled with a portable induction burner. Requires a battery or portable charger to function. Commonly advertised as having enough cookware to feed a fireteam.`
  cost = 7500
  memory: [number, number] = [5, 1]
  weight = 32
}

export class Electronics implements Item {
  _type = 'Item'
  id = 'Toolkit.Electronics'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Electronics toolkit'
  description = `A toolbox with standard equipment for working with electronics such as LED lights, computers, and basic constructs. Contents include a multimeter, pliers, a small box of fuses, electrical tape, a screwdriver with interchangeable bits, an anti-static wrist wrap, and a small selection of commonly used screws, nuts, washers, and plastic clips. You can use the toolkit to, over the course of an hour, repair a damaged computer or VI construct, such as the dashboard of a jumpship or a scorpius turret, for an amount of health points equal to 2d4 + your Technology modifier. Most repairs can be performed solely using an electronics toolkit, though your Architect may rule that specialized equipment or additional resources are required for some kinds of repairs.`
  cost = 7500
  memory: [number, number] = [5, 1]
  weight = 10
}

export class Medical implements Item {
  _type = 'Item'
  id = 'Toolkit.Medical'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Medical toolkit'
  description = `Gauze, general-purpose
    antimicrobials, Exo-grade replacement plating, liquid welders, and more fill this handy, travel-sized box with a friendly green caduceus on the lid. This toolkit has 10 uses. If you are proficient with the toolkit, you can use your action to spend a use to stabilize a dying creature, or you can heal a creature for an amount of health points equal to 1d4 + your Medicine modifier. You cannot use this toolkit to heal constructs such as a shank, and you cannot use this toolkit to heal a creature that has not been stabilized first.

You can spend 100 glimmer to restore a use of your medical toolkit. Most wounds sustained in the field by humans and aliens alike can be treated with this toolkit, though your Architect may rule that a particularly severe or uncommon type of wound requires additional gear or resources to treat.`
  cost = 6000
  memory: [number, number] = [4, 1]
  weight = 10
}

export class SCUBA implements Item {
  _type = 'Item'
  id = 'Toolkit.SCUBA'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'SCUBA toolkit'
  description = `Wetsuit, rebreather, tool belt, waterproof pen and small whiteboard, and a single tank of oxygen good for approximately 2 hours of use.`
  cost = 24000
  memory: [number, number] = [5, 1]
  weight = 55
}

export class Sewing implements Item {
  _type = 'Item'
  id = 'Toolkit.Sewing'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Sewing toolkit'
  description = `A small portable sewing machine accompanies a selection of threads, needles, pins, and other assorted, easy-to-carry sewing supplies in this toolkit. This toolkit is sufficient to fix most minor rips and tears in clothing, or to make simple alterations.`
  cost = 4500
  memory: [number, number] = [3, 1]
  weight = 8
}

export class Thieves implements Item {
  _type = 'Item'
  id = 'Toolkit.Thieves'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Thieves\' toolkit'
  description = `While this set does include more traditional thieves' tools, such as a file, a set of lock picks, and a small mirror mounted on an extendable metal handle, it also includes less archaic instruments, including a small thumb drive loaded with a hacker's API and a bundle of cables for connecting any tablet computer to almost every conceivable type of device. Tablet sold separately. This toolkit can be used to pick mechanical locks, override digital locks, install keyloggers, rapidly copy server data, or to take the Hack action on applicable devices, to name a few uses. While the possible applications of this toolkit are varied, they are not limitless: your Architect may rule that you need more specialized or robust gear to perform the action you want to take.`
  cost = 5000
  memory: [number, number] = [5, 1]
  weight = 3
}

export class Vehicle implements Item {
  _type = 'Item'
  id = 'Toolkit.Vehicle'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Vehicle toolkit'
  description = `Often called a portable garage, a vehicle toolkit has everything from basic wrenches to a self-assembling collapsible engine hoist. Considered a must for anyone who owns a jumpship or sparrow. You can use the toolkit to, over the course of an hour, repair a damaged vehicle for an amount of health points equal to 2d4 + your Technology modifier. Common repairs can be performed in this way, but your Architect may rule that some form of specialized equipment or additional resources are required for some kinds of repairs.`
  cost = 52500
  memory: [number, number] = [6, 1]
  weight = 150
}

export class Weaponsmithing implements Item {
  _type = 'Item'
  id = 'Toolkit.Weaponsmithing'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Weaponsmithing toolkit'
  description = `Not too dissimilar from an armorsmithing toolkit, a weaponsmithing toolkit has one vital difference: a portable, glimmer-fed CNC printer and matching program for a tablet computer (though Ghosts can often act as a substitute and interface with the printer directly). This CNC printer is particularly required for weaponsmithing because while armor parts are generally interchangeable, firearm components tend to be highly specialized and unique from weapon to weapon. These toolkits can be used to add perks to certain kinds of weapons.`
  cost = 7500
  memory: [number, number] = [5, 1]
  weight = 35
}

export class Whittling implements Item {
  _type = 'Item'
  id = 'Toolkit.Whittling'
  uuid = uuidEmpty
  type = MiscItemType.Toolkit
  rarity = ItemRarity.Common
  name = 'Whittling toolkit'
  description = `Includes most basic tools for carving wood, such as a hook knife, sloyd knife, detail knife, polishing compound and strap, wood spoon blank, and a pair of cut-resistant gloves.`
  cost = 1000
  memory: [number, number] = [3, 1]
  weight = 5
}

/* Common Vehicles */
export class Cavalier implements Item {
  _type = 'Item'
  id = 'Vehicle.Sparrows.S20Cavalier'
  uuid = uuidEmpty
  type = VehicleItemType.LandVehicle
  rarity = ItemRarity.Common
  name = 'S-20 Cavalier'
  description = `Quick, lightweight, and relatively inconspicuous, sparrows are the bread and butter of Guardian ground transportation. They're cheap to build and easy to operate, perfect for the high-octane life of a Guardian. In addition, they come with a built-in boost function, which can significantly increase the sparrow's speed at the cost of maneuverability. Sparrows, unlike most other vehicles, are also small enough that they can be transmatted, and take up 50/1 memory.`
  cost = 15000
  size = Size.Medium
  speed = 140
  weightLimit = 520
}

export class Nomad implements Item {
  _type = 'Item'
  id = 'Vehicle.Sparrows.S20Nomad'
  uuid = uuidEmpty
  type = VehicleItemType.LandVehicle
  rarity = ItemRarity.Common
  name = 'S-20 Nomad'
  description = `Quick, lightweight, and relatively inconspicuous, sparrows are the bread and butter of Guardian ground transportation. They're cheap to build and easy to operate, perfect for the high-octane life of a Guardian. In addition, they come with a built-in boost function, which can significantly increase the sparrow's speed at the cost of maneuverability. Sparrows, unlike most other vehicles, are also small enough that they can be transmatted, and take up 50/1 memory.`
  cost = 12000
  size = Size.Medium
  speed = 160
  weightLimit = 320
}

export class Seeler implements Item {
  _type = 'Item'
  id = 'Vehicle.Sparrows.S20Seeker'
  uuid = uuidEmpty
  type = VehicleItemType.LandVehicle
  rarity = ItemRarity.Common
  name = 'S-20 Seeker'
  description = `Quick, lightweight, and relatively inconspicuous, sparrows are the bread and butter of Guardian ground transportation. They're cheap to build and easy to operate, perfect for the high-octane life of a Guardian. In addition, they come with a built-in boost function, which can significantly increase the sparrow's speed at the cost of maneuverability. Sparrows, unlike most other vehicles, are also small enough that they can be transmatted, and take up 50/1 memory.`
  cost = 13000
  size = Size.Medium
  speed = 150
  weightLimit = 380
}

export class Arcadia implements Item {
  _type = 'Item'
  id = 'Vehicle.Jumpships.Arcadia'
  uuid = uuidEmpty
  type = VehicleItemType.AirVehicle
  rarity = ItemRarity.Common
  name = 'Arcadia'
  description = `An expensive and precious commodity, jumpships are the fastest form of in-atmosphere travel and, should they be equipped with an NLS drive, they give Guardians access to the stars.

Most jumpships come with a suite of functionality, including compartment pressurization, air conditioning, autopilot, radio, basic scanners, anti-detection methods, and short-range transmat capabilities for shifting cargo or crew members in and out of the jumpship. When ascending into space, jumpships utilize a fractional portion of their equipped NLS drive's propulsion to achieve escape velocity.`
  cost = 600000
  size = Size.Gargantuan
  speed = 535
  weightLimit = 24250.85
}

export class Kestrel implements Item {
  _type = 'Item'
  id = 'Vehicle.Jumpships.Kestrel'
  uuid = uuidEmpty
  type = VehicleItemType.AirVehicle
  rarity = ItemRarity.Common
  name = 'Kestrel'
  description = `An expensive and precious commodity, jumpships are the fastest form of in-atmosphere travel and, should they be equipped with an NLS drive, they give Guardians access to the stars.

Most jumpships come with a suite of functionality, including compartment pressurization, air conditioning, autopilot, radio, basic scanners, anti-detection methods, and short-range transmat capabilities for shifting cargo or crew members in and out of the jumpship. When ascending into space, jumpships utilize a fractional portion of their equipped NLS drive's propulsion to achieve escape velocity.`
  cost = 900000
  size = Size.Gargantuan
  speed = 800
  weightLimit = 13227.74
}

export class Phaeton implements Item {
  _type = 'Item'
  id = 'Vehicle.Jumpships.Phaeton'
  uuid = uuidEmpty
  type = VehicleItemType.AirVehicle
  rarity = ItemRarity.Common
  name = 'Phaeton'
  description = `An expensive and precious commodity, jumpships are the fastest form of in-atmosphere travel and, should they be equipped with an NLS drive, they give Guardians access to the stars.

Most jumpships come with a suite of functionality, including compartment pressurization, air conditioning, autopilot, radio, basic scanners, anti-detection methods, and short-range transmat capabilities for shifting cargo or crew members in and out of the jumpship. When ascending into space, jumpships utilize a fractional portion of their equipped NLS drive's propulsion to achieve escape velocity.`
  cost = 700000
  size = Size.Gargantuan
  speed = 650
  weightLimit = 39683.21
}

export class Regulus implements Item {
  _type = 'Item'
  id = 'Vehicle.Jumpships.Regulus'
  uuid = uuidEmpty
  type = VehicleItemType.AirVehicle
  rarity = ItemRarity.Common
  name = 'Regulus'
  description = `An expensive and precious commodity, jumpships are the fastest form of in-atmosphere travel and, should they be equipped with an NLS drive, they give Guardians access to the stars.

Most jumpships come with a suite of functionality, including compartment pressurization, air conditioning, autopilot, radio, basic scanners, anti-detection methods, and short-range transmat capabilities for shifting cargo or crew members in and out of the jumpship. When ascending into space, jumpships utilize a fractional portion of their equipped NLS drive's propulsion to achieve escape velocity.`
  cost = 900000
  size = Size.Gargantuan
  speed = 600
  weightLimit = 66138.68
}

export class BulkFreighter implements Item {
  _type = 'Item'
  id = 'Vehicle.Other.BulkFreighter'
  uuid = uuidEmpty
  type = VehicleItemType.AirVehicle
  rarity = ItemRarity.Common
  name = 'Bulk Freighter'
  description = `Also called pilgrim cars, bulk freighters are large, box-like hovercraft used for transporting materials or people over land. They are slow but heavily armored, easy to make, and can be linked together to form long trains that only require a single driver or programmed autopilot function. Each individual freighter car is equipped with a safety hatch big enough for a Medium creature on the top and underside, as well as two large bay doors, one on each broadside.`
  cost = 65000
  size = Size.Huge
  speed = 12
  weightLimit = 4409.25
}

export class Hawk implements Item {
  _type = 'Item'
  id = 'Vehicle.Other.Hawk'
  uuid = uuidEmpty
  type = VehicleItemType.AirVehicle
  rarity = ItemRarity.Common
  name = 'Hawk'
  description = `A hawk is a troop carrier aircraft used by the Vanguard and others in and around the Last City. They are typically used to make cargo deliveries to and from the City and the Tower, or to transport Guardians to their assignments, if they have no transport of their own.`
  cost = 702000
  size = Size.Gargantuan
  speed = 560
  weightLimit = 44092.46
}

export const allItems = [
  new PaddedArmor,
  new LeatherArmor,
  new SpinweaveArmor,
  new MakeshiftArmor,
  new SpinwireArmor,
  new ReinforcedArmor,
  new PlastwireArmor,
  new SpinplateArmor,
  new HalfPlastArmor,
  new PlasteelArmor,
  new FortifiedArmor,
  new RelicArmor,
  new NoShell,
  new GeneralistShell,
  new Dagger,
  new ThrowingHammer,
  new Greataxe,
  new Broadsword,
  new Longsword,
  new HeavyMaul,
  new Shortsword,
  new Smallsword,
  new Warhammer,
  new CombatBow,
  new AutoRifle,
  new HandCannon,
  new PulseRifle,
  new ScoutRifle,
  new Sidearm,
  new SubmachineGun,
  new TraceRifle,
  new FusionRifle,
  new GrenadeLauncher,
  new LightMachineGun,
  new LinearFusionRifle,
  new RocketLauncher,
  new Shotgun,
  new SniperRifle,
  new Arrow,
  new PrimaryAmmo,
  new Rocket,
  new HeavyAmmo,
  new BackPack,
  new Battery,
  new Binoculars,
  new BlanketHeated,
  new BlanketWool,
  new Book,
  new BoxOfNails,
  new Bucket,
  new BungieCord,
  new DigitalCamera,
  new CellPhone,
  new Chalk,
  new HunterCloak,
  new TitanMark,
  new WarlockBond,
  new CasualWear,
  new FormalWear,
  new RainPoncho,
  new WinterCoat,
  new ComputerDesktop,
  new ComputerTablet,
  new Crowbar,
  new DuctTape,
  new ElectricDrill,
  new LargeEtherContainer,
  new MediumEtherContainer,
  new SmallEtherContainer,
  new FirewoodBundle,
  new Floodlight,
  new FoldoutCot,
  new FoldingChair,
  new GasMask,
  new Gasoline,
  new Hammock,
  new Handcuffs,
  new HumanVoiceSynth,
  new HuntingTrap,
  new KnifeBelt,
  new LadderCollapsable,
  new LEDLightHandheld,
  new Lighter,
  new LockDigital,
  new LockMechanical,
  new Megaphone,
  new MotionActivatedCamera,
  new NightVisionArmorAugmentation,
  new Oil,
  new Pillow,
  new PlasmaCutter,
  new Pliers,
  new PortableGenerator,
  new PortableHeater,
  new Quiver,
  new Ration,
  new RelayBeacon,
  new RelayBeaconQuantum,
  new Scissors,
  new Shovel,
  new SignalBooster,
  new SignalFlareGun,
  new SignalFlareStick,
  new SignalJammer,
  new SleepingBag,
  new Spacesuit,
  new SurveillanceDrone,
  new Tarp10,
  new Tarp5,
  new Telescope,
  new TentTwoPerson,
  new WaterBottle,
  new WaterFilter,
  new WaterproofBox,
  new WireCable,
  new Wrench,
  new Armorsmithing,
  new Climbing,
  new Cooking,
  new Electronics,
  new Medical,
  new SCUBA,
  new Sewing,
  new Thieves,
  new Vehicle,
  new Weaponsmithing,
  new Whittling,
  new Cavalier,
  new Nomad,
  new Arcadia,
  new Kestrel,
  new Phaeton,
  new Regulus,
  new BulkFreighter,
  new Hawk
]
export const itemIndex = allItems.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<string, Item>)
export const getItem = (id: string) => itemIndex[id]
