import { ChargeType, ClassAbility, DamageType, LightAbility } from '@/models/destiny'

export class DodgeAbility implements LightAbility {
  _type = 'LightAbility'
  id = ClassAbility.HuntersDodge
  name = 'Hunter\'s Dodge'
  description = `You gain the effects of the Dodge action. You can only cast this superclass ability while grounded. Choose one of the following additional effects to grant yourself when you cast this Light ability.

**Marksman's Dodge.** You dodge with a deft and steady hand. Choose one firearm you are holding. This weapon is reloaded instantly (this can overcome the Cumbersome property), and the next shot you take with it, if that shot is taken before the start of your next turn, has its critical hit range increased by one.

**Gambler's Dodge.** If you cast this superclass ability when two or more hostile creatures are within 15 feet of you, you can regain one melee ability charge.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Class
  recharge = 'd6, 6'
  duration = 'Instantaneous'
  prerequisiteLevel = 4
}

export class BarricadeAbility implements LightAbility {
  _type = 'LightAbility'
  id = ClassAbility.Barricade
  name = 'Barricade'
  description = `You create a wall of transparent Light centered in front of you that is no more than 1 foot away, with the bottom of the barricade touching the surface you are standing on. You can only cast this superclass ability while grounded. The barricade has an AC equal to 11 + your Light level (see your class table), and 20 health points per Light level. It succeeds on all Strength saving throws, fails on all Dexterity saving throws, and is immune to effects that would cause a Constitution, Intelligence, Wisdom, or Charisma saving throw. If the barricade runs out of health points it shatters, ending its duration early.

You can make superclass recharge rolls while your barricade is active, but you can only have one active barricade. If you cast a new barricade, the old one dissipates.

Choose one of the following barricades to make when you cast this Light ability.

**Towering Barricade.** The barricade you make is 5 feet tall, 15 feet wide, and 1 inch thick. While the barricade itself is considered full cover, creatures can pass through it.

**Rally Barricade.** The barricade you make is 3 feet tall, 15 feet wide, and 1 inch thick. The barricade is considered half-cover. Creatures of your choice within 5 feet of the barricade can choose to use a bonus action to reload a Cumbersome weapon.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Class
  recharge = 'd6, 6'
  duration = 'Up to 1 minute'
  prerequisiteLevel = 4
}

export class RiftOfLightAbility implements LightAbility {
  _type = 'LightAbility'
  id = ClassAbility.RiftOfLight
  name = 'Rift of Light'
  description = `You summon a vortex of Light that swirls in a flat 5-foot radius circle on the ground beneath your feet, centered on you. You can only cast this superclass ability while grounded. Choose one of the following rifts to create when you cast this ability. The rift dissipates at the start of your next turn.

**Healing Rift.** All Risen creatures of your choice within the area can immediately regain shield points equal to 2d8 + your Light ability modifier. Until the start of your next turn, Risen creatures of your choice who end their turn within the rift, or who enter the rift for the first time on a turn, may also regain shield points equal to 2d8 + your Light ability modifier. A creature can only benefit from any Healing Rift once on a turn. Your Healing Rift allows Risen creatures to regain an additional 1d8 shield points for each Light level you are above 1st.

**Empowering Rift.** Creatures of your choice standing within the rift become Empowered (Stage 1). Beginning at 11th Risen level, your Empowering Rift grants Empowered (Stage 2), and beginning at 17th Risen level, it grants Empowered (Stage 3). Creatures lose the Empowered condition from this Light ability at the start of your next turn.`
  castingTime = '1 bonus action'
  range = 'Self'
  cost = 1
  resource = ChargeType.Class
  recharge = 'd6, 6'
  duration = 'Instantaneous'
  prerequisiteLevel = 4
  damage = [
    { formula: '(1+[light])d8 + [lightMod]', type: DamageType.ShieldHealing }
  ]
}

export const allClassAbilities: LightAbility[] = [
  new DodgeAbility,
  new BarricadeAbility,
  new RiftOfLightAbility
]
export const classAbilityIndex = allClassAbilities.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<ClassAbility, LightAbility>)
export const getClassAbility = (id: ClassAbility, level: number) => (classAbilityIndex[id]?.prerequisiteLevel || 0) <= level ? classAbilityIndex[id] : null
