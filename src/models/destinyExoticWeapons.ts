import { Ability, DamageType, Item, ItemRarity, ItemProperty, ItemRange, HeavyWeaponItemType, PrimaryWeaponItemType, MartialWeaponItemType } from '@/models/destiny'
import * as WeaponPerks from '@/models/destinyWeaponPerks'
import { PrimaryAmmo, HeavyAmmo } from './destinyItems'

const uuidEmpty = '00000000-0000-0000-0000-000000000000'

export class FourthHorseman implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.4thHorseman'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.Shotgun
  rarity = ItemRarity.Exotic
  name = '4th Horseman'
  description = `Requires attunement by a Risen class.

An ornate shotgun with four barrels. It sports oversized ring sights, a wooden stock, and lavish engraving.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [5, 5, 10]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Cumbersome, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 4
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.FinalRound]
  slot2Choices = [new WeaponPerks.Thunderer]
  slot3Choices = [new WeaponPerks.ThePaleHorse]
  slot1 = new WeaponPerks.FinalRound
  slot2 = new WeaponPerks.Thunderer
  slot3 = new WeaponPerks.ThePaleHorse
}

export class AceOfSpades implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.AceOfSpades'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'Ace of Spades'
  description = `Requires attunement by a Risen class.

This hand cannon has strong, clean lines, and a striking black and white color scheme. The side of its frame is marked with a distinctive white spade.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Maverick]
  slot2Choices = [new WeaponPerks.Firefly]
  slot3Choices = [new WeaponPerks.MementoMori]
  slot1 = new WeaponPerks.Maverick
  slot2 = new WeaponPerks.Firefly
  slot3 = new WeaponPerks.MementoMori
}

export class Anarchy implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Anarchy'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.GrenadeLauncher
  rarity = ItemRarity.Exotic
  name = 'Anarchy'
  description = `Requires attunement by a Risen class.

A drum-loading grenade launcher constructed from Fallen weapon parts. Its frame is covered by the bulbous panels of brownish metal characteristic of Fallen technology, and its squarish muzzle bristles with spines and flanges.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4 + [mod]', type: DamageType.Kinetic }
  ]
  //TODO 60 feet
  scope: [number, number, number] = [60, 60, 60]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 4
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.LightningGrenades]
  slot2Choices = [new WeaponPerks.ArcTraps]
  slot3Choices = [new WeaponPerks.HighVoltageNodes]
  slot1 = new WeaponPerks.LightningGrenades
  slot2 = new WeaponPerks.ArcTraps
  slot3 = new WeaponPerks.HighVoltageNodes
}

export class Arbalest implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Arbalest'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LinearFusionRifle
  rarity = ItemRarity.Exotic
  name = 'Arbalest'
  description = `Requires attunement by a Risen class.

  An improvised, barebones weapon built from scavenged parts and spare tubing. It rattles slightly.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [15, 120, 340]
  scopeRange = ItemRange.Long
  memory: [number, number] = [4, 1]
  weight = 12
  properties = [ItemProperty.Loading, ItemProperty.EnergyProjectiles, ItemProperty.TwoHanded]
  shotCapacity = 3
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.Railgun]
  slot2Choices = [new WeaponPerks.CompoundingForce]
  slot3Choices = [new WeaponPerks.DisruptionBreak]
  slot1 = new WeaponPerks.Railgun
  slot2 = new WeaponPerks.CompoundingForce
  slot3 = new WeaponPerks.DisruptionBreak
}

export class BadJuju implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.BadJuju'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.PulseRifle
  rarity = ItemRarity.Exotic
  name = 'Bad Juju'
  description = `Requires attunement by a Risen class.

  An Earth-made pulse rifle with vertebrae bound to its stock and an avian skull mounted above its barrel. Something about it is unsettling.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 9
  properties = [ItemProperty.AutomaticFire, ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.HipFire]
  slot2Choices = [new WeaponPerks.Sunder]
  slot3Choices = [new WeaponPerks.StringOfCurses]
  slot1 = new WeaponPerks.HipFire
  slot2 = new WeaponPerks.Sunder
  slot3 = new WeaponPerks.StringOfCurses
}

export class Bastion implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Bastion'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.FusionRifle
  rarity = ItemRarity.Exotic
  name = 'Bastion'
  description = `Requires attunement by a Risen class.

  A most unusual rifle with an ornate casing of silver metal partly chased into a pattern of feathers. Five tassels of royal blue fabric are fastened to either side of the receiver.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [15, 25, 30]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Finesse, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.Breakthrough]
  slot2Choices = [new WeaponPerks.FinalStand]
  slot3Choices = [new WeaponPerks.SaintsFists]
  slot1 = new WeaponPerks.Breakthrough
  slot2 = new WeaponPerks.FinalStand
  slot3 = new WeaponPerks.SaintsFists
}

export class BlackTalon implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.BlackTalon'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Longsword
  rarity = ItemRarity.Exotic
  name = 'Black Talon'
  description = `Requires attunement by a Risen class.

An ornate, florid sword with an elaborate openwork blade and a hilt that resembles a backswept wing.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 8
  properties = [ItemProperty.OneHanded, ItemProperty.Versatile]
  slot1Choices = [new WeaponPerks.VoidShotCapacity]
  slot2Choices = [new WeaponPerks.CrowsWings]
  slot3Choices = [new WeaponPerks.CrowsReturn]
  slot1 = new WeaponPerks.VoidShotCapacity
  slot2 = new WeaponPerks.CrowsWings
  slot3 = new WeaponPerks.CrowsReturn
}

export class BoltCaster implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.BoltCaster'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Longsword
  rarity = ItemRarity.Exotic
  name = 'Bolt Caster'
  description = `Requires attunement by a Risen class.

A peculiar, straight-edged backsword built around a large hadium crystal situated above the hilt. The blade is lightened by a series of angled cutouts, and the crystal is imbued with arc Light.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 8
  properties = [ItemProperty.Cumbersome, ItemProperty.OneHanded, ItemProperty.Versatile]
  slot1Choices = [new WeaponPerks.AncientArcCore]
  slot2Choices = [new WeaponPerks.LiveByTheSword]
  slot3Choices = [new WeaponPerks.SwordOfThunder]
  slot1 = new WeaponPerks.AncientArcCore
  slot2 = new WeaponPerks.LiveByTheSword
  slot3 = new WeaponPerks.SwordOfThunder
}

export class BooleanGemini implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.BooleanGemini'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.ScoutRifle
  rarity = ItemRarity.Exotic
  name = 'Boolean Gemini'
  description = `Requires attunement by a Risen class.

A compact scout rifle painted in white, blue, and goldenrod. A small status display is built into the rounded shoulder of its stock. Its side bears the number 347.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 60, 120]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FiringMode]
  slot2Choices = [new WeaponPerks.Snapshot]
  slot1 = new WeaponPerks.FiringMode
  slot2 = new WeaponPerks.Snapshot
}

export class Borealis implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Borealis'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Exotic
  name = 'Borealis'
  description = `Requires attunement by a Risen class.

A prototype sniper rifle with a heavy body and plain beige casing. An underslung armature supports an array of three rings looped around the front of the barrel. Parts of its magazine, optics, and the center section of the barrel emit a glow corresponding to the currently selected elemental frequency.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Light }
  ]
  scope: [number, number, number] = [0, 300, 600]
  scopeRange = ItemRange.Long
  memory: [number, number] = [5, 1]
  weight = 12
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.TheFundamentals]
  slot2Choices = [new WeaponPerks.IonicReturn]
  slot1 = new WeaponPerks.TheFundamentals
  slot2 = new WeaponPerks.IonicReturn
}

export class Cerberus implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Cerberus'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.AutoRifle
  rarity = ItemRarity.Exotic
  name = 'Cerberus +1'
  description = `Requires attunement by a Risen class.

An improbable, slapdash collection of four disparate gun barrels somehow attached to a single rifle frame. A small, glowing brick is mounted on the side and wired into the chassis.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [30, 40, 50]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 10
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FourHeadedDog]
  slot2Choices = [new WeaponPerks.SpreadShotPackage]
  slot3Choices = [new WeaponPerks.FullBore]
  slot1 = new WeaponPerks.FourHeadedDog
  slot2 = new WeaponPerks.SpreadShotPackage
  slot3 = new WeaponPerks.FullBore
}

export class ColdHeart implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.ColdHeart'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.TraceRifle
  rarity = ItemRarity.Exotic
  name = 'Cold Heart'
  description = `Requires attunement by a Risen class.

The forward end of this sleek rifle splits into two arms above and below its firing aperture. The central drum of its beam generator is fed by four insulated tubes on each side. The coolant flowing through them chills the surrounding air, forming a thin film of ice on the gun's body.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [60, 60, 60]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.ColdFusion]
  slot2Choices = [new WeaponPerks.LongestWinter]
  slot1 = new WeaponPerks.ColdFusion
  slot2 = new WeaponPerks.LongestWinter
}

export class DarkDrinker implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.DarkDrinker'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Broadsword
  rarity = ItemRarity.Exotic
  name = 'Dark Drinker'
  description = `Requires attunement by a Risen class.

A peculiar, broad backsword with a curving, cleaver-like point, built around a large hadium crystal situated above the hilt. The blade is etched with a swirling pattern, and the crystal is imbued with void Light.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d12 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 11
  properties = [ItemProperty.Cumbersome, ItemProperty.Heavy, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.AncientVoidFuller]
  slot2Choices = [new WeaponPerks.DieByTheSword]
  slot3Choices = [new WeaponPerks.SupermassiveVortex]
  slot1 = new WeaponPerks.AncientVoidFuller
  slot2 = new WeaponPerks.DieByTheSword
  slot3 = new WeaponPerks.SupermassiveVortex
}

export class Deathbringer implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Deathbringer'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.RocketLauncher
  rarity = ItemRarity.Exotic
  name = 'Deathbringer'
  description = `Requires attunement by a Risen class.

A jagged-looking rocket launcher with a muzzle like a shrieking beak, built from unidentifiable scraps and bound with the paracausal sorcery of the Hive Deathsingers. Its grips appear to be mummified appendages from some enormous bird.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.ExplosiveVoid }
  ]
  //TODO 80 ft.
  scope: [number, number, number] = [80, 80, 80]
  memory: [number, number] = [5, 1]
  weight = 25
  properties = [ItemProperty.Cumbersome, ItemProperty.HighRecoil, ItemProperty.Heavy, ItemProperty.Loading, ItemProperty.Payload, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.DarkDeliverance]
  slot2Choices = [new WeaponPerks.DarkDescent]
  slot1 = new WeaponPerks.DarkDeliverance
  slot2 = new WeaponPerks.DarkDescent
}

export class DevilsRuin implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.DevilsRuin'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.Sidearm
  rarity = ItemRarity.Exotic
  name = 'Devil\'s Ruin'
  description = `Requires attunement by a Risen class.

This strange sidearm has a second underslung barrel extending well past the muzzle of the first.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [20, 30, 40]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 3
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Lightweight]
  slot2Choices = [new WeaponPerks.CloseTheGap]
  slot3Choices = [new WeaponPerks.ASymbol]
  slot1 = new WeaponPerks.Lightweight
  slot2 = new WeaponPerks.CloseTheGap
  slot3 = new WeaponPerks.ASymbol
}

export class Divinity implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Divinity'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.TraceRifle
  rarity = ItemRarity.Exotic
  name = 'Divinity'
  description = `Requires attunement by a Risen class.

This forked Vex weapon bears some resemblance to a trace rifle with a radiolarian core in its central emitter tine. It looks battered and ancient.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [60, 60, 60]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.Loading, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Judgment]
  slot2Choices = [new WeaponPerks.Penance]
  slot1 = new WeaponPerks.Judgment
  slot2 = new WeaponPerks.Penance
}

export class Drang implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Drang'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.Sidearm
  rarity = ItemRarity.Exotic
  name = 'Drang'
  description = `Requires attunement by a Risen class.

A light sidearm with a long, slender barrel protruding from its compact receiver. Its grips are inlaid with a golden commemorative medal.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 30, 40]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 3
  properties = [ItemProperty.Agile]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.CrowdControl]
  slot2Choices = [new WeaponPerks.Lightweight]
  slot3Choices = [new WeaponPerks.RifledBarrel]
  slot1 = new WeaponPerks.CrowdControl
  slot2 = new WeaponPerks.Lightweight
  slot3 = new WeaponPerks.RifledBarrel
}

export class FightingLion implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.FightingLion'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.GrenadeLauncher
  rarity = ItemRarity.Exotic
  name = 'Fighting Lion'
  description = `Requires attunement by a Risen class.

A breech-loading, single-shot grenade launcher, cast with exquisite and extensive decoration. Its muzzle is a stylized lion's head.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.ExplosiveSolar }
  ]
  scope: [number, number, number] = [30, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Finesse, ItemProperty.Payload, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 1
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.DelayedGratification]
  slot2Choices = [new WeaponPerks.ThinTheHerd]
  slot1 = new WeaponPerks.DelayedGratification
  slot2 = new WeaponPerks.ThinTheHerd
}

export class Gjallarhorn implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Gjallarhorn'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.RocketLauncher
  rarity = ItemRarity.Exotic
  name = 'Gjallarhorn'
  description = `Requires attunement by a Risen class.

A wondrous-looking rocket launcher completely covered in a lavish decorative motif of winged wolf heads, the most prominent of which gazes out above the barrel. The lustrous black of its launcher tube contrasts with the white finish of its housing-and both are richly overlaid with gleaming gold elements.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d8 + [mod]', type: DamageType.ExplosiveSolar }
  ]
  scope: [number, number, number] = [30, 50, 80]
  scopeRange = ItemRange.Close
  memory: [number, number] = [5, 1]
  weight = 25
  properties = [ItemProperty.HighRecoil, ItemProperty.Heavy, ItemProperty.Loading, ItemProperty.Payload, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.SmartDriftControl]
  slot2Choices = [new WeaponPerks.Tracking]
  slot3Choices = [new WeaponPerks.WolfpackRounds]
  slot1 = new WeaponPerks.SmartDriftControl
  slot2 = new WeaponPerks.Tracking
  slot3 = new WeaponPerks.WolfpackRounds
}

export class HardLight implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.HardLight'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.AutoRifle
  rarity = ItemRarity.Exotic
  name = 'Hard Light'
  description = `Requires attunement by a Risen class.

A stocky bullpup auto rifle with a streamlined, high-design profile. A round, translucent nodule on the side reveals the currently selected elemental charge of its liquid polymer ammunition.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Light }
  ]
  scope: [number, number, number] = [30, 60, 100]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 10
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.TheFundamentals]
  slot2Choices = [new WeaponPerks.OverpenetrationRounds]
  slot3Choices = [new WeaponPerks.VolatileLight]
  slot1 = new WeaponPerks.TheFundamentals
  slot2 = new WeaponPerks.OverpenetrationRounds
  slot3 = new WeaponPerks.VolatileLight
}

export class Hawkmoon implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Hawkmoon'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'Hawkmoon'
  description = `Requires attunement by a Risen class.

An elegant, robust hand cannon with a heavy triangular barrel and a remarkably large bore. A curved talon is fixed to the butt of its grip. The silvery barrel block is engraved with delicate feathers.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Lightweight]
  slot2Choices = [new WeaponPerks.LuckInTheChamber]
  slot3Choices = [new WeaponPerks.HoldingAces]
  slot1 = new WeaponPerks.Lightweight
  slot2 = new WeaponPerks.LuckInTheChamber
  slot3 = new WeaponPerks.HoldingAces
}

export class HeirApparent implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.HeirApparent'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LightMachineGun
  rarity = ItemRarity.Exotic
  name = 'HeirApparent'
  description = `Requires attunement by a Risen class.

The heavy, quad-barrelled chaingun of a Cabal colossus. Like most standard-issue Cabal military equipment, its design is sturdy and unembellished.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d8 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [20, 40, 80]
  scopeRange = ItemRange.Close
  memory: [number, number] = [5, 1]
  weight = 21
  properties = [ItemProperty.AutomaticFire, ItemProperty.Heavy, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.SpinningUp]
  slot2Choices = [new WeaponPerks.ArmorOfTheColossus]
  slot3Choices = [new WeaponPerks.HipFire]
  slot1 = new WeaponPerks.SpinningUp
  slot2 = new WeaponPerks.ArmorOfTheColossus
  slot3 = new WeaponPerks.HipFire
}

export class IzanagisBurden implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.IzanagisBurden'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Exotic
  name = 'Izanagi\'s Burden'
  description = `Requires attunement by a Risen class.

An understated sniper rifle with a broad, flat barrel assembly. The white paneling of its casing is decorated with a pattern of blue lines and the design of two leaping fish, curving away from each other.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [0, 450, 900]
  scopeRange = ItemRange.Long
  memory: [number, number] = [5, 1]
  weight = 12
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.ArmorPiercingRounds]
  slot2Choices = [new WeaponPerks.HonedEdge]
  slot3Choices = [new WeaponPerks.ElegantDesign]
  slot1 = new WeaponPerks.ArmorPiercingRounds
  slot2 = new WeaponPerks.HonedEdge
  slot3 = new WeaponPerks.ElegantDesign
}

export class Jotunn implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Jotunn'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.FusionRifle
  rarity = ItemRarity.Exotic
  name = 'Jötunn'
  description = `Requires attunement by a Risen class.

A very unusual box-shaped weapon with the grip situated inside its casing. Instead of a barrel, its front face presents emitter plates for its directed-energy projectiles. The dark metal of its utilitarian exterior is disrupted only by the curve of a partially exposed charging coil on its side, and the emblem of an open right hand emblazoned on the casing.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [20, 50, 100]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Loading, ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.ChargeShot]
  slot2Choices = [new WeaponPerks.OverheatedRounds]
  slot3Choices = [new WeaponPerks.Toasty]
  slot1 = new WeaponPerks.ChargeShot
  slot2 = new WeaponPerks.OverheatedRounds
  slot3 = new WeaponPerks.Toasty
}

export class Khvostov7G0X implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Khvostov7G0X'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.Other
  rarity = ItemRarity.Exotic
  name = 'Khvostov 7G-0X'
  description = `Requires attunement by a Risen class.

A no-frills battle rifle issued to soldiers in Old Russia during the Golden Age. Its unassuming design seems almost quaint, but belies the clever enhancements and infusion of Light it has received.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  //Todo See perks
  damage = [
    { formula: '1d6 + [mod]', type: DamageType.Kinetic }
  ]
  //Todo See perks
  scope: [number, number, number] = [0, 0, 0]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 15
  //Todo See perks
  properties = [ItemProperty.AutomaticFire]
  //Todo See perks
  shotCapacity = 1
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.ModularDesign]
  slot2Choices = [new WeaponPerks.PerfectBalance]
  slot3Choices = [new WeaponPerks.FiringMode]
  slot1 = new WeaponPerks.ModularDesign
  slot2 = new WeaponPerks.PerfectBalance
  slot3 = new WeaponPerks.FiringMode
}

export class LeMonarque implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.LeMonarque'
  uuid = uuidEmpty
  type = MartialWeaponItemType.CombatBow
  rarity = ItemRarity.Exotic
  name = 'Le Monarque'
  description = `Requires attunement by a Risen class.

An intricate compound bow with striking red and gold detailing on the riser, and a butterfly emblem on one of its cams. Its arrows' tips carry a toxic payload that emits a visible purple glow.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.Void }
  ]
  scope: [number, number, number] = [20, 100, 200]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Ammunition, ItemProperty.Special, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.PoisonArrows]
  slot2Choices = [new WeaponPerks.Hawkeye]
  slot3Choices = [new WeaponPerks.LeMonarque]
  slot1 = new WeaponPerks.PoisonArrows
  slot2 = new WeaponPerks.Hawkeye
  slot3 = new WeaponPerks.LeMonarque
}

export class LegendOfAcrius implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.LegendOfAcrius'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.Shotgun
  rarity = ItemRarity.Exotic
  name = 'Legend of Acrius'
  description = `Requires attunement by a Risen class.

A Cabal legionary's heavy scattergun, ponderous in its bulk. Its thick, squared-off body is typical of Cabal military aesthetics—or the lack thereof.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '4d10 + [mod]', type: DamageType.Arc }
  ]
  //Todo 20-foot line
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Cumbersome, ItemProperty.HighRecoil, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.ShockBlast]
  slot2Choices = [new WeaponPerks.LongMarch]
  slot3Choices = [new WeaponPerks.RecoilCompensators]
  slot1 = new WeaponPerks.ShockBlast
  slot2 = new WeaponPerks.LongMarch
  slot3 = new WeaponPerks.RecoilCompensators
}

export class LeviathansBreath implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.LeviathansBreath'
  uuid = uuidEmpty
  type = MartialWeaponItemType.CombatBow
  rarity = ItemRarity.Exotic
  name = 'Leviathan\'s Breath'
  description = `Requires attunement by a Risen class.

A massive, heavy bow with uncompromisingly solid and thick construction. Strung with a reinforced chain and actuated by piston joints, it propels harpoon-like arrows with titanic force.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d10 + [mod]', type: DamageType.Void }
  ]
  scope: [number, number, number] = [20, 100, 200]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Ammunition, ItemProperty.HighRecoil, ItemProperty.Loading, ItemProperty.Special, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.ChainBowstring]
  slot2Choices = [new WeaponPerks.LeviathansSigh]
  slot1 = new WeaponPerks.ChainBowstring
  slot2 = new WeaponPerks.LeviathansSigh
}

export class LordOfWolves implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.LordOfWolves'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.Shotgun
  rarity = ItemRarity.Exotic
  name = 'Lord of Wolves'
  description = `Requires attunement by a Risen class.

An Eliksni shrapnel launcher, the preferred firearm of Fallen captains. Its spindly stock and rounded receiver give way to the hard, direct lines and numerous small spines of the distinctive broad barrel, which ends in a cluster of angular prongs around the wide muzzle.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [15, 30, 45]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.ReleaseTheWolves]
  slot2Choices = [new WeaponPerks.HowlOfThePack]
  slot1 = new WeaponPerks.ReleaseTheWolves
  slot2 = new WeaponPerks.HowlOfThePack
}

/* export class Lumina implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Lumina'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'Lumina'
  description = `Requires attunement by a Risen class.

An ornate, flowery hand cannon crafted with advanced Light-forging techniques rather than mundane manufacturing. Its outer casing of swirling, elongated petals seems to sprout from a grip capped with a bud-shaped pommel. Golden thorns protrude through the casing from the weapon's slender body.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.NobleRounds]
  slot2Choices = [new WeaponPerks.BlessingOfTheSky]
  slot3Choices = [new WeaponPerks.ResonantVirtue]
}

export class Malfeasance implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Malfeasance'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'Malfeasance'
  description = `Requires attunement by a Risen class.

This bizarre hand cannon looks like little more than a jagged, gouged-out frame beaten from tortured metal, yet somehow it is a working weapon. Its uneven surfaces are crawling with webs of tangled inlay, and it exudes a sense of malignancy.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.ParacausalPredator]
  slot2Choices = [new WeaponPerks.OpeningShot]
  slot3Choices = [new WeaponPerks.ExplosiveShadow]
}

export class MIDAMultiTool implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.MIDAMultiTool'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.ScoutRifle
  rarity = ItemRarity.Exotic
  name = 'MIDA Multi-Tool'
  description = `Requires attunement by a Risen class.

A compact, lightweight scout rifle of rugged, utilitarian design. In addition to its scope, a small round display is mounted to the side, showing a compass or any number of other useful readouts.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [15, 120, 240]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.TheMIDATouch]
  slot2Choices = [new WeaponPerks.Multimode]
  slot3Choices = [new WeaponPerks.Toolkit]
}

export class MonteCarlo implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.MonteCarlo'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.AutoRifle
  rarity = ItemRarity.Exotic
  name = 'Monte Carlo'
  description = `Requires attunement by a Risen class.

A slick, extremely stylish auto rifle with a massive yet elegant bayonet built into the frame. The clean lines of its crisp, white casing are interspersed with understated patches of embossed scrollwork.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [30, 40, 75]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 10
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.MarkovChain]
  slot2Choices = [new WeaponPerks.MonteCarloMethod]
}

export class NoLandBeyond implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.NoLandBeyond'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Exotic
  name = 'No Land Beyond'
  description = `Requires attunement by a Risen class.

This extremely lengthy, heavy sniper rifle resembles an ancient, pre-Golden Age bolt-action long gun. Its carved wooden stock, weighty hexagonal barrel, and plain iron sights make it seem entirely out of place in a modern arsenal.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [0, 120, 300]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [5, 1]
  weight = 12
  properties = [ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.Snapshot]
  slot2Choices = [new WeaponPerks.Lightweight]
  slot3Choices = [new WeaponPerks.TheMaster]
}

export class OneThousandVoices implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.OneThousandVoices'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.FusionRifle
  rarity = ItemRarity.Exotic
  name = 'One Thousand Voices'
  description = `Requires attunement by a Risen class.

A grotesque contraption made of twisted bone and teeth, wired into the vague shape of a hand-carried weapon. Even without handling it, you have a bad feeling about it.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Kinetic }
  ]
  //Todo 90 feet
  scope: [number, number, number] = [90, 90, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.EnergyProjectiles, ItemProperty.Finesse, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.AhamkarasEye]
  slot2Choices = [new WeaponPerks.WishForDeath]
  slot3Choices = [new WeaponPerks.UnforeseenRepercussions]
}

export class OutbreakPrime implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.OutbreakPrime'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.PulseRifle
  rarity = ItemRarity.Exotic
  name = 'Outbreak Prime'
  description = `Requires attunement by a Risen class.

The product of advanced Golden Age manufacturing, this bullpup pulse rifle is all angles and sharp square panels of gleaming black plasteel and metamaterials. Its dark surfaces sharply contrast the numerous feedlines running back and forth through its length, carrying a glowing red stream of active SIVA nanomachines through the receiver.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '4d4 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 9
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Necrosis]
  slot2Choices = [new WeaponPerks.Virulence]
  slot3Choices = [new WeaponPerks.TheCorruptionSpreads]
}

export class PatienceAndTime implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.PatienceAndTime'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Exotic
  name = 'Patience and Time'
  description = `Requires attunement by a Risen class.

A rugged, unassuming sniper rifle with an integrated bipod, camo-patterned casing, and large optics covered with a scrap of ghillie netting.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [0, 300, 60]
  scopeRange = ItemRange.Long
  memory: [number, number] = [5, 1]
  weight = 12
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.TakeAKnee]
  slot2Choices = [new WeaponPerks.Unflinching]
  slot3Choices = [new WeaponPerks.PatienceAndTime]
}

export class PrometheusLens implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.PrometheusLens'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.TraceRifle
  rarity = ItemRarity.Exotic
  name = 'Prometheus Lens'
  description = `Requires attunement by a Risen class.

A trace rifle with clean, simple lines and a bright white and yellow casing. A perfectly planed octahedron of flawless, clear crystal is suspended in an aperture at the gun's center`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [40, 40, 40]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.AutomaticFire, ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.PrismaticInferno]
  slot2Choices = [new WeaponPerks.FlameRefraction]
}

export class RatKing implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.RatKing'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.Sidearm
  rarity = ItemRarity.Exotic
  name = 'Rat King'
  description = `Requires attunement by a Risen class.

A bizarre sidearm with strange decoration. Its heavy slide is cast to resemble a relief carving depicting several rats. Its grip is bound in red cord in an old tsukaito style.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 30, 40]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 3
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Lightweight]
  slot2Choices = [new WeaponPerks.Vermin]
  slot3Choices = [new WeaponPerks.RatPack]
}

export class RazeLighter implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.RazeLighter'
  uuid = uuidEmpty
  type = MartialWeaponItemType.Broadsword
  rarity = ItemRarity.Exotic
  name = 'Raze-Lighter'
  description = `Requires attunement by a Risen class.

A peculiar, straight-edged backsword built around a large hadium crystal situated above the hilt. The blade's spine is viciously serrated, and the crystal is imbued with solar Light.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Kinetic }
  ]
  memory: [number, number] = [3, 1]
  weight = 11
  properties = [ItemProperty.Cumbersome, ItemProperty.Heavy, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.AncientSolarEdge]
  slot2Choices = [new WeaponPerks.ThriveByTheSword]
  slot3Choices = [new WeaponPerks.PhoenixUppercut]
}

export class RedDeath implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.RedDeath'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.PulseRifle
  rarity = ItemRarity.Exotic
  name = 'Red Death'
  description = `Requires attunement by a Risen class.

A rugged, brutal-looking pulse rifle with iron sights and a large, vicious bayonet. The forward casing around the barrel is mounted with short spikes, and marked with several small skulls. The whole front end of the weapon is spattered in old blood.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 9
  properties = [ItemProperty.AutomaticFire, ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.OutlawedWeapon]
  slot2Choices = [new WeaponPerks.LightSap]
  slot3Choices = [new WeaponPerks.CruelRemedy]
}

export class Riskrunner implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Riskrunner'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.SubmachineGun
  rarity = ItemRarity.Exotic
  name = 'Riskrunner'
  description = `Requires attunement by a Risen class.

A very unusual, slick-looking submachine gun with three pointed prongs instead of a barrel. Three arc capacitor cells housed in the stock hum with carefully contained charge.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d4 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [10, 15, 30]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 4
  properties = [ItemProperty.Agile, ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.ArcConductor]
  slot2Choices = [new WeaponPerks.Superconductor]
}

export class RuinousEffigy implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.RuinousEffigy'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.TraceRifle
  rarity = ItemRarity.Exotic
  name = 'Ruinous Effigy'
  description = `Requires attunement by a Risen class.

A bizarre slab of otherworldly material that looks like wood hewn into the shape of a trace rifle. It is cold to the touch.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d10 + [mod]', type: DamageType.Void }
  ]
  scope: [number, number, number] = [40, 40, 40]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Trasmutation]
  slot2Choices = [new WeaponPerks.NoGoingBack]
  slot3Choices = [new WeaponPerks.Evolution]
}

export class SkyburnersOath implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.SkyburnersOath'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.ScoutRifle
  rarity = ItemRarity.Exotic
  name = 'Skyburner\'s Oath'
  description = `Requires attunement by a Risen class.

A Cabal slug rifle, the most widely used weapon among all types of Cabal ground troops. Its stocky, roughly triangular profile is a little unwieldy in human hands, but it is definitely usable.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.ExplosiveSolar }
  ]
  scope: [number, number, number] = [15, 120, 240]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FiringMode]
  slot2Choices = [new WeaponPerks.ForTheEmpire]
}

export class SleeperSimulant implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.SleeperSimulant'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LinearFusionRifle
  rarity = ItemRarity.Exotic
  name = 'Sleeper Simulant'
  description = `Requires attunement by a Risen class.

A remarkable weapon designed by a Golden Age warmind. Its elaborate yet spare construction is predominated by a precisely minimal openwork frame supporting square charge-coil modules along the barrel. It is cased in exactingly engineered triangular plates of carbon-fiber metamaterial. The angular warmind sigil is stamped just above the magazine.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '4d10 + [mod]', type: DamageType.Solar }
  ]
  //Todo 0 ft (see perks)
  scope: [number, number, number] = [0, 0, 0]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 0
  properties = [ItemProperty.Loading, ItemProperty.EnergyProjectiles, ItemProperty.TwoHanded]
  shotCapacity = 3
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.PolygonalRifling]
  slot2Choices = [new WeaponPerks.Dornroschen]
}

export class Sturm implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Sturm'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'Sturm'
  description = `Requires attunement by a Risen class.

A bulky, angular hand cannon with a box magazine instead of a cylinder, and a long tubular barrel. Its silvery metallic casing is detailed with brassy embellishments, and its wooden grips are inlaid with a golden commemorative medal.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Accomplice]
  slot2Choices = [new WeaponPerks.StormAndStress]
}

export class SUROSRegime implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.SUROSRegime'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.AutoRifle
  rarity = ItemRarity.Exotic
  name = 'SUROS Regime'
  description = `Requires attunement by a Risen class.

A sleek auto rifle with a retro-futuristic look, based on recovered Golden Age technology. Its clean, round lines and brightly colored casing panels are unmistakable. The Suros logo is prominently stamped on the side.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [30, 40, 75]
  scopeRange = ItemRange.Close
  memory: [number, number] = [4, 1]
  weight = 10
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.FiringMode]
  slot2Choices = [new WeaponPerks.LifeSupport]
  slot3Choices = [new WeaponPerks.SUROSLegacy]
}

export class Symmetry implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Symmetry'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.ScoutRifle
  rarity = ItemRarity.Exotic
  name = 'Symmetry'
  description = `Requires attunement by a Risen class.

A strange scout rifle with deeply swooping lines and aggressive, bladelike curves. Its shining surfaces are delicately etched with a fine pattern.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [15, 120, 240]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.Revolution]
  slot2Choices = [new WeaponPerks.DynamicFiringMode]
}

export class Tarrabah implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Tarrabah'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.SubmachineGun
  rarity = ItemRarity.Exotic
  name = 'Tarrabah'
  description = `Requires attunement by a Risen class.

A custom-built SMG wrought from blued metal, with dark wooden grips. Its frame is cast in the form of a snarling beast.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d6 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [10, 15, 30]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 4
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.RavenousBeast]
  slot2Choices = [new WeaponPerks.SharpFanged]
  slot3Choices = [new WeaponPerks.BottomlessAppetite]
}

export class TheColony implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TheColony'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.GrenadeLauncher
  rarity = ItemRarity.Exotic
  name = 'The Colony'
  description = `Requires attunement by a Risen class.

A particularly vicious-looking, aggressively angular grenade launcher manufactured by the Veist foundry. It has a box magazine instead of the drum typical of heavy grenade launchers. Through a translucent window into the breech, you can see a miniature insectoid drone loaded in place, its legs twitching.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d4 + [mod]', type: DamageType.Kinetic }
  ]
  //Todo 120 ft.
  scope: [number, number, number] = [120, 120, 120]
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Finesse, ItemProperty.Loading, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 4
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.InsectoidRobotGrenades]
  slot2Choices = [new WeaponPerks.ServeTheColony]
}

export class TheGunOfManyBullets implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TheGunOfManyBullets'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.Other
  rarity = ItemRarity.Exotic
  name = 'The Gun of Many Bullets'
  description = `Requires attunement by a Risen class.

An amalgamation of Vex tech, Hive magic, and industrial-strength tape, it bears no resemblance to any known firearm design. A large hopper is affixed to the top of what can generously be called its loading chamber.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  //Todo See perks
  damage = [
    { formula: '1d1 + [mod]', type: DamageType.Kinetic }
  ]
  //Todo See perks
  scope: [number, number, number] = [5, 5, 10]
  scopeRange = ItemRange.Close
  memory: [number, number] = [500, 1]
  weight = 20
  //Todo See perks for additional properties
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 0
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.GlimmerHopper]
  slot2Choices = [new WeaponPerks.UncertaintyCompensators]
  slot3Choices = [new WeaponPerks.WithHopeAndAPrayer]
}

export class TheHuckleberry implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TheHuckleberry'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.SubmachineGun
  rarity = ItemRarity.Exotic
  name = 'The Huckleberry'
  description = `Requires attunement by a Risen class.

A compact submachine gun with baroque construction, elaborate engraving, a stock carved of rare hardwood, and grips finished with mother of pearl. It features an unusual top-loading cylinder magazine.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d4 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [10, 15, 30]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 4
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.RicochetRounds]
  slot2Choices = [new WeaponPerks.Rampage]
  slot3Choices = [new WeaponPerks.RideTheBull]
}

export class TheLastWord implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TheLastWord'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'The Last Word'
  description = `Requires attunement by a Risen class.

A heavy hand cannon with a strong profile and a classical wooden grip. Its dark body is capped with a top plate of bright golden metal. The side of the barrel bears the legend ‘TEX MECHANICA' amid a course of fine gilded engraving.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d8 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Cumbersome, ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.HipFire]
  slot2Choices = [new WeaponPerks.FanFire]
  slot3Choices = [new WeaponPerks.TheLastWord]
}

export class TheQueenbreaker implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TheQueenbreaker'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LinearFusionRifle
  rarity = ItemRarity.Exotic
  name = 'The Queenbreaker'
  description = `Requires attunement by a Risen class.

An Eliksni wire rifle with swappable optics. Its long barrel extends from a flared, bulbous body cased in the brown plating typical of Eliksni technology.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d8 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [15, 120, 340]
  scopeRange = ItemRange.Long
  memory: [number, number] = [4, 1]
  weight = 12
  properties = [ItemProperty.Loading, ItemProperty.EnergyProjectiles, ItemProperty.TwoHanded]
  shotCapacity = 3
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.FiringMore]
  slot2Choices = [new WeaponPerks.WireRifle]
  slot3Choices = [new WeaponPerks.TheQueenbreaker]
}

export class Thorn implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Thorn'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.HandCannon
  rarity = ItemRarity.Exotic
  name = 'Thorn'
  description = `Requires attunement by a Risen class.

A dark, jagged hand cannon cursed by Hive sickness. Hive magics have transformed its frame into a tortured shadow of a human weapon, its edges grown sharp and its lines twisted. An unholy green glow shows forth through openings in its casing.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d8 + [mod]', type: DamageType.Darkness }
  ]
  scope: [number, number, number] = [25, 50, 90]
  scopeRange = ItemRange.Close
  memory: [number, number] = [2, 1]
  weight = 6
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.LightSap]
  slot2Choices = [new WeaponPerks.MarkOfTheDevourer]
  slot3Choices = [new WeaponPerks.SoulDevourer]
}

export class Thunderlord implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Thunderlord'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LightMachineGun
  rarity = ItemRarity.Exotic
  name = 'Thunderlord'
  description = `Requires attunement by a Risen class.

A sturdy-bodied, drum-fed machine gun with a ring sight and reinforced casing partly painted blue. A cable running from the receiver to the barrel assembly crackles with arc energy.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Arc }
  ]
  scope: [number, number, number] = [20, 40, 80]
  scopeRange = ItemRange.Close
  memory: [number, number] = [5, 1]
  weight = 21
  properties = [ItemProperty.AutomaticFire, ItemProperty.HighRecoil, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.FittedStock]
  slot2Choices = [new WeaponPerks.ArmorPiercingRounds]
  slot3Choices = [new WeaponPerks.ReignHavoc]
}

export class TouchOfMalice implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TouchOfMalice'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.ScoutRifle
  rarity = ItemRarity.Exotic
  name = 'Touch of Malice'
  description = `Requires attunement by a Risen class.

A mysterious scout rifle made of materials and artifacts stolen from the Hive. Most of its body is hidden in a cover of stitched hide, draped with a tattered cowl of Hive wormsilk. The only visible parts are the muzzle, a bayonet of Hive chitin, and a strange gyroscopic array in the center of what should be the receiver. Its nested rings are graven with Hive runes, and spin around a hunk of grimly dark stone suspended at their center.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d10 + [mod]', type: DamageType.Darkness }
  ]
  scope: [number, number, number] = [15, 120, 240]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.TwoHanded]
  shotCapacity = 6
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.TouchOfMalice]
  slot2Choices = [new WeaponPerks.EyeOfTheStorm]
  slot3Choices = [new WeaponPerks.TouchOfMercy]
}

export class TractorCannon implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TractorCannon'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.Shotgun
  rarity = ItemRarity.Exotic
  name = 'Tractor Cannon'
  description = `Requires attunement by a Risen class.

A gravitational grapple intended for moving cargo. Its bulky, rounded body fits over the hand, with a curved brace meant to sit on the forearm. The somewhat conical forward end presents a graviton field emitter in the center of three articulated claws.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Void }
  ]
  //Todo 5 ft.
  scope: [number, number, number] = [5, 5, 5]
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.HighRecoil, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.RepulsorForce]
  slot2Choices = [new WeaponPerks.ParticleRepeater]
  slot3Choices = [new WeaponPerks.TheScientificMethod]
}

export class TrinityGhoul implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TrinityGhoul'
  uuid = uuidEmpty
  type = MartialWeaponItemType.CombatBow
  rarity = ItemRarity.Exotic
  name = 'Trinity Ghoul'
  description = `Requires attunement by a Risen class.

An unusual compound combat bow with broad, flat outer limbs and deeply curved, thick inner limbs. The riser is quite stout, and seems to house atypical supplemental hardware, including a large, complex sight that collapses for stowage. It fires special flat, tripartite arrows designed to split on release.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Void }
  ]
  scope: [number, number, number] = [20, 100, 200]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Ammunition, ItemProperty.Special, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.FiringMode]
  slot2Choices = [new WeaponPerks.LightningRod]
}

export class TwoTailedFox implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.TwoTailedFox'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.RocketLauncher
  rarity = ItemRarity.Exotic
  name = 'TwoTailedFox'
  description = `Requires attunement by a Risen class.

An enormous rocket launcher with a massive, blocky body and a pair of offset bores for firing two rockets at once. The long rear portion of the launcher tube is grey and bears the logo of the Daito foundry, while the casing of the main body is finished in garish metallic purple, blue, and lavender, with a prominent pink emblem of a stylized fox's head.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d1 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [30, 50, 80]
  scopeRange = ItemRange.Close
  memory: [number, number] = [5, 1]
  weight = 2
  properties = [ItemProperty.Cumbersome, ItemProperty.HighRecoil, ItemProperty.Heavy, ItemProperty.Loading, ItemProperty.Payload, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.BlackPowder]
  slot2Choices = [new WeaponPerks.Twintails]
  slot3Choices = [new WeaponPerks.PlayWithYourPrey]
}

export class VexMythoclast implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.VexMythoclast'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.FusionRifle
  rarity = ItemRarity.Exotic
  name = 'Vex Mythoclast'
  description = `Requires attunement by a Risen class.

A unique fusion rifle obviously of Vex origin, yet designed for human operation. The streamlined curves of its brazen casing strongly resemble the form of Vex hulls, and its sight's lens seems almost identical to a Vex eye.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '4d6 + [mod]', type: DamageType.Solar }
  ]
  scope: [number, number, number] = [15, 25, 30]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.AutomaticFire, ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.RepetitiveBehavior]
  slot2Choices = [new WeaponPerks.TimelessMythoclast]
}

export class VigilanceWing implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.VigilanceWing'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.PulseRifle
  rarity = ItemRarity.Exotic
  name = 'Vigilance Wing'
  description = `Requires attunement by a Risen class.

A bulky pulse rifle with decorative fittings borrowed from ancient Egyptian designs. Its outer casing takes the shape of backswept golden wings, while its sight posts are formed as the jackal-headed god Anubis.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d6 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 9
  properties = [ItemProperty.Finesse, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.ThePack]
  slot2Choices = [new WeaponPerks.HarshTruths]
  slot3Choices = [new WeaponPerks.LastStand]
}

export class WardcliffCoil implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.WardcliffCoil'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.RocketLauncher
  rarity = ItemRarity.Exotic
  name = 'Wardcliff Coil'
  description = `Requires attunement by a Risen class.

A bulky, slapdash weapon that looks very little like a rocket launcher. Its clunky body consists of inscrutable machinery secured within a roll-cage frame from which protrudes a large, spheroid emitter coil of unknown provenance.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.ExplosiveArc }
  ]
  //Todo 20 ft. cone
  scope: [number, number, number] = [20, 20, 20]
  memory: [number, number] = [5, 1]
  weight = 25
  properties = [ItemProperty.HighRecoil, ItemProperty.Heavy, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 2
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.MadScientist]
  slot2Choices = [new WeaponPerks.SeekerRounds]
}

export class Wavesplitter implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Wavesplitter'
  uuid = uuidEmpty
  type = PrimaryWeaponItemType.TraceRifle
  rarity = ItemRarity.Exotic
  name = 'Wavesplitter'
  description = `Requires attunement by a Risen class.

A blocky trace rifle from the Omolon foundry, with a plain black casing. In the wide gap between the two field stabilizer arms of its emitter fork, its primary beam emitter is encased in a rounded cylindrical capsule of a glassy material.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d1 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [60, 60, 60]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 8
  properties = [ItemProperty.Finesse, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 8
  linkedConsumable = (new PrimaryAmmo).id
  slot1Choices = [new WeaponPerks.HarmonicLaser]
  slot2Choices = [new WeaponPerks.DarkFocus]
  slot3Choices = [new WeaponPerks.WhiteNail]
}

export class WhisperOfTheWorm implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.WhisperOfTheWorm'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Exotic
  name = 'Whisper of the Worm'
  description = `Requires attunement by a Risen class.

A ponderous, heavy sniper rifle whose muzzle bristles into the shape of a hungry, monstrous mouth. Its surfaces ripple with Taken energy.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [0, 300, 600]
  scopeRange = ItemRange.Long
  memory: [number, number] = [0, 0]
  weight = 12
  properties = [ItemProperty.Cumbersome, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 3
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.DarkFocus]
  slot2Choices = [new WeaponPerks.WhiteNail]
}

export class WillOfTheWisp implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.WillOfTheWisp'
  uuid = uuidEmpty
  type = MartialWeaponItemType.CombatBow
  rarity = ItemRarity.Exotic
  name = 'Will of the Wisp'
  description = `Requires attunement by a Risen class.

A recurve combat bow made of intricately carved wood, thrumming and glowing faintly with imbued Light.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 100, 200]
  scopeRange = ItemRange.Long
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Ammunition, ItemProperty.Loading, ItemProperty.Special, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.Snapshot]
  slot2Choices = [new WeaponPerks.FriarsLantern]
}

export class WishEnder implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.WishEnder'
  uuid = uuidEmpty
  type = MartialWeaponItemType.CombatBow
  rarity = ItemRarity.Exotic
  name = 'Wish Ender'
  description = `Requires attunement by a Risen class.

A massive longbow fashioned from a single solid piece of pearlescent material made by the Reefborn Awoken. The figure of a standing knight is carved in relief in the back of the upper limb, just above the riser. A simple ring sight is mounted on its stabilizer. Its arrows are fletched with amethyst and tipped with broad piercing points.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '3d10 + [mod]', type: DamageType.Kinetic }
  ]
  scope: [number, number, number] = [20, 100, 200]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 15
  properties = [ItemProperty.Ammunition, ItemProperty.Loading, ItemProperty.Special, ItemProperty.TwoHanded]
  slot1Choices = [new WeaponPerks.QueensWrath]
  slot2Choices = [new WeaponPerks.Broadhead]
  slot3Choices = [new WeaponPerks.FinalRound]
}

export class Witherhoard implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Witherhoard'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.GrenadeLauncher
  rarity = ItemRarity.Exotic
  name = 'Witherhoard'
  description = `Requires attunement by a Risen class.

A strange breechloading grenade launcher made of dark metal, meticulously worked together by hand. An undulating spine of slender tines runs along the top of the barrel.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '2d10 + [mod]', type: DamageType.Darkness }
  ]
  scope: [number, number, number] = [20, 50, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [4, 1]
  weight = 18
  properties = [ItemProperty.Cumbersome, ItemProperty.Finesse, ItemProperty.Loading, ItemProperty.Special, ItemProperty.TwoHanded]
  shotCapacity = 4
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.PrimevalsTorment]
  slot2Choices = [new WeaponPerks.SpreadTheHoard]
  slot3Choices = [new WeaponPerks.BreakTheBank]
}

export class Xenophage implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.Xenophage'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.LightMachineGun
  rarity = ItemRarity.Exotic
  name = 'Xenophage'
  description = `Requires attunement by a Risen class.

A heavily built machine gun with a massive foursided barrel assembly with flanged edges. Some curious custom wiring behind the sights connects to an unusual component integrated into the receiver: it appears to be a hunk of amber with a large, fearsome insect trapped within.`
  ability = Ability.Strength
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '5d10 + [mod]', type: DamageType.ExplosiveSolar }
  ]
  scope: [number, number, number] = [30, 60, 120]
  scopeRange = ItemRange.Medium
  memory: [number, number] = [5, 1]
  weight = 21
  properties = [ItemProperty.HighRecoil, ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 13
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.PyrotoxinRounds]
  slot2Choices = [new WeaponPerks.VengefulPersistence]
  slot3Choices = [new WeaponPerks.SplashDamage]
}

export class ZenMeteor implements Item {
  _type = 'Item'
  id = 'Weapon.Exotic.ZenMeteor'
  uuid = uuidEmpty
  type = HeavyWeaponItemType.SniperRifle
  rarity = ItemRarity.Exotic
  name = 'Zen Meteor'
  description = `Requires attunement by a Risen class.

A black sniper rifle with a robust profile and precision optics. Its receiver casing is engraved with floral scrollwork.`
  ability = Ability.Dexterity
  attackFormula = '1d20 + [mod] + [prof]'
  damage = [
    { formula: '1d10 + [mod]', type: DamageType.ExplosiveSolar }
  ]
  scope: [number, number, number] = [0, 300, 600]
  scopeRange = ItemRange.Long
  memory: [number, number] = [5, 1]
  weight = 12
  properties = [ItemProperty.Loading, ItemProperty.TwoHanded]
  shotCapacity = 5
  linkedConsumable = (new HeavyAmmo).id
  slot1Choices = [new WeaponPerks.WithALaserBeam]
  slot2Choices = [new WeaponPerks.ZenMeteor]
} */

export const allItems = [
  new FourthHorseman,
  new AceOfSpades,
  new Anarchy,
  new Arbalest,
  new BadJuju,
  new Bastion,
  new BlackTalon,
  new BoltCaster,
  new BooleanGemini,
  new Borealis,
  new Cerberus,
  new ColdHeart,
  new DarkDrinker,
  new Deathbringer,
  new DevilsRuin,
  new Divinity,
  new Drang,
  new FightingLion,
  new Gjallarhorn,
  new HardLight,
  new Hawkmoon,
  new HeirApparent,
  new IzanagisBurden,
  new Jotunn,
  new Khvostov7G0X,
  new LeMonarque,
  new LegendOfAcrius,
  new LeviathansBreath,
  new LordOfWolves,
  /* new Lumina,
  new Malfeasance,
  new MIDAMultiTool,
  new MonteCarlo,
  new NoLandBeyond,
  new OneThousandVoices,
  new OutbreakPrime,
  new PatienceAndTime,
  new PrometheusLens,
  new RatKing,
  new RazeLighter,
  new RedDeath,
  new Riskrunner,
  new RuinousEffigy,
  new SkyburnersOath,
  new SleeperSimulant,
  new Sturm,
  new SUROSRegime,
  new Symmetry,
  new Tarrabah,
  new TheColony,
  new TheGunOfManyBullets,
  new TheHuckleberry,
  new TheLastWord,
  new TheQueenbreaker,
  new Thorn,
  new Thunderlord,
  new TouchOfMalice,
  new TractorCannon,
  new TrinityGhoul,
  new TwoTailedFox,
  new VexMythoclast,
  new VigilanceWing,
  new WardcliffCoil,
  new Wavesplitter,
  new WhisperOfTheWorm,
  new WillOfTheWisp,
  new WishEnder,
  new Witherhoard,
  new Xenophage,
  new ZenMeteor */
]
export const itemIndex = allItems.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<string, Item>)
export const getItem = (id: string) => itemIndex[id]