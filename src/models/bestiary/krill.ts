import { Monster, Race } from '../destiny'

export const krill: Monster[] = [
  {
    id: 'Krill.Acolyte',
    name: 'Acolyte',
    race: Race.Krill,
    statblock: `## Acolyte
*Medium Hive*
___
**Armor Class** :: 13 (natural armor)
**Hit Points** :: 22 (4d8 + 4)
**Speed** :: 30 ft., climb 20 ft.

|STR|DEX|CON|INT|WIS|CHA|
|:---:|:---:|:---:|:---:|:---:|:---:|
|12 (+1)|14 (+2)|12 (+1)|11 (+0)|15 (+2)|12 (+1)|

**Skills** :: Perception +5, Religion +5, Stealth +5
**Senses** :: darkvision 120 ft., passive Perception 15
**Languages** :: krill
**Challenge** :: Individual CR 2 (450 XP), Classification CR 5 Minion (1,800 XP)

|Proficiency Bonus|Hack DC|
|:---:|:---:|
|+3|-|

***Shadowy Movement.*** When in dim light or darkness, the acolyte can take the Disengage or Hide action as a bonus action on its turn.
### Actions
___
***Multiattack.*** The acolyte takes three shots with its shredder rifle.
:
***Shredder Rifle.*** _Firearm Weapon Attack:_ +4 to hit, scope 30/40/75 (close), one target. _Hit:_ 7 (2d6d1 + 1) void damage.`,
    lore: `The fully developed but not yet matured form of the Hive, acolytes serve as its most common foot soldiers. They have grown to around six feet, and developed heavier chitinous plating, notably a protectively ridged cranial shell resembling a helm. The coloration of their chitin begins to vary between broods at this stage, though acolytes may also wear tattered raiments that can indicate their lineage. Most obviously, the membrane that covered their eyes during thrallhood has been shed, leaving acolytes' three green eyes clearly visible.

Acolytes are permitted small variants of Hive projectile weapons, most commonly the shredder. Though not skilled combatants, they have a simple and effective grasp of how to use cover to their advantage, and will dig in to fight with some patience rather than always attack recklessly. Each acolyte commands some number of thrall, and thus while acolytes often form bands of their own, they are also never far from thrall reinforcements.`
  },
  {
    id: 'Krill.Adherent',
    name: 'Adherent',
    race: Race.Krill,
    statblock: `## Adherent
*Medium Hive*
___
**Armor Class** :: 15 (natural armor)
**Hit Points** :: 44 (8d8 + 8)
**Speed** :: 30 ft., climb 30 ft.

|STR|DEX|CON|INT|WIS|CHA|
|:---:|:---:|:---:|:---:|:---:|:---:|
|12 (+1)|16 (+3)|12 (+1)|12 (+1)|16 (+3)|12 (+1)|

**Saving Throws** :: Str +4, Dex +6
**Skills** :: Arcana +6, Perception +6, Religion +6, Stealth +6
**Senses** :: blindsight 30 ft., darkvision 120 ft., passive Perception 16
**Languages** :: krill
**Challenge** :: Individual CR 4 (1,100 XP), Classification CR 7 Elite (2,900 XP)

|Proficiency Bonus|Hack DC|
|:---:|:---:|
|+3|-|

***Superior Tactics.*** The adherent can take the Aim, Dash, Disengage, or Hide action as a bonus action on its turn.
### Actions
___
***Soulfire Rifle.*** _Firearm Weapon Attack_: +8 to hit, scope 15/120/340 (long), one target. Hit: 10 (2d6+3) void damage and the target must succeed on a DC 14 Constitution saving throw, taking an additional 10 (3d6) void damage on a failed save.
### Spellcasting
___
The adherent is a 4th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 14, +6 to hit with spell attacks). It regains its expended spell slots when it finishes a long rest. It knows the following spells:

***Cantrips (at will):*** :: _message, minor illusion, true strike_*
***1st level (4 slots):*** :: _detect magic, hunter's mark, silence_
***2nd level (3 slots):*** :: _darkness, pass without trace_

*The adherent casts this spell on itself when it rolls initiative.`,
    lore: `Acolyte morphs armed with long-range precision weapons that fire single, concentrated bolts of condensed Hive soulfire. Aside from their armament, adherents are not physically different from other acolyte types. They stand around six feet tall, show the developing chitin plating of Hive maturity, and have lost the membranous caul that covered their three eyes in their early life as thrall.
  As one might expect, adherents are deployed as the Hive equivalent of snipers, taking positions with superior vantage to pick off targets from a distance. They use the same tactics whether fighting offensively or when stationed to protect an area from hostile approach. Like regular acolytes, they make frequent use of cover, which reduces their vulnerability to counter-sniping.

Adherents are less common than typically armed acolytes in most broods, perhaps because the soulfire rifle is more difficult to use. On the other hand, in some broods they appear almost as often as regular acolytes, leading some Hive scholars to propose their numbers simply reflect varying tactical preferences between different brood lineages.`
  },
  {
    id: 'Krill.AlakHul',
    name: 'Alak-Hul',
    race: Race.Krill,
    statblock: `## Alak-Hul
*Huge Hive*
___
**Armor Class** :: 18 (natural armor)
***Ultra Health Point Pools:***
||||
|:---:|:---:|:---:|
|103 (9d12+45)|103 (9d12+45)|92 (8d12+40)|

**Speed** :: 10 ft.

|STR|DEX|CON|INT|WIS|CHA|
|:---:|:---:|:---:|:---:|:---:|:---:|
|12 (+1)|16 (+3)|12 (+1)|12 (+1)|16 (+3)|12 (+1)|

**Saving Throws** :: Str +11, Cha +9
**Skills** :: Athletics +11, Deception +9, Intimidation +9, Perception +8, Religion +8, Stealth +4
**Damage Resistances** :: bludgeoning, kinetic, piercing, slashing
**Condition Immunities** :: Blinded, Charmed, Frightened, Incapacitated, Suppressed
**Senses** :: blindsight 120 ft., darkvision 300 ft., passive Perception 18
**Languages** :: krill
**Challenge** :: Individual CR 19 (22,000 XP), Classification CR 14 Ultra (22,000 XP)

|Proficiency Bonus|Hack DC|
|:---:|:---:|
|+5|-|

***Paracausal Resistance.*** Alak-Hul has advantage on saving throws against spells, Light effects, and Darkness effects.
:
***Terror of the Night.*** Alak-Hul has advantage on Dexterity (Stealth) checks while in dim light or darkness, and his darkvision can see in magical or paracausal darkness. Alak-Hul can use his bonus action to move up to 15 feet toward a Frightened creature without provoking opportunity attacks.
:
***Cloud of Darkness.*** While not Incapacitated, Alak-Hul radiates a cloud of magical darkness in an 60-foot spherical radius that is always centered on himself, even should he use his Dark Phasing legendary action. The darkness spreads around corners. A creature with darkvision can't see through this darkness, and nonmagical or nonparacausal light can't illuminate it.

Creatures of Alak-Hul's choice that start their turn in the cloud, or who enter the cloud for the first time on a turn, must succeed on a DC 18 Wisdom saving throw or become Frightened of the darkness. Creatures Frightened in this way are also Paralyzed. A Frightened creature can repeat the saving throw at the end of each of its turns, ending the effect on itself early on a success. Creatures that succeed on their saving throw, or for which the effect ends, are immune to being Frightened in this way for 24 hours.

Finally, while within the cloud, Risen creatures have disadvantage on core Light ability recharge rolls they make.
### Actions
___
***Multiattack.*** Alak-Hul can use his axe slam, then make two melee attacks.
:
***Double-Headed Axe.*** _Melee Weapon Attack_: +8 to hit, reach 10 ft., one target. Hit: 15 (2d8+6) slashing damage plus 18 (4d8) darkness damage.
:
***Axe Slam (range 15 ft., recharge d6 [4]).*** All targets of Alak-Hul's choice within 5 feet of a space within range must make a DC 18 Strength saving throw. A target takes 35 (10d6) darkness damage and is knocked Prone on a failed save. On a success, a target takes half as much damage and isn't knocked Prone.
### Legendary Actions
___
Alak-Hul can take 3 legendary actions, choosing from the options below. Only one legendary option can be used at a time and only at the end of another creature’s turn. Alak-Hul regains spent legendary actions at the start of his turn.
:
***Detect.*** Alak-Hul makes a Wisdom (Perception) check.
:
***Move.*** Alak-Hul moves up to 10 feet without provoking opportunity attacks.
:
***Dark Phasing.*** Alak-Hul phases into the Ascendant realm, becoming completely undetectable until the start of his next turn, at which point he returns to an unoccupied location within 60 feet of his original location. He cannot use legendary actions until then.
:
***Attack.*** Alak-Hul makes an attack with his double-headed axe.
### Ultra Actions
___
If one of Alak-Hul's health point pools is reduced to 0, and he has at least one health point pool remaining, he can immediately perform the following action.
:
***Summon the Swarm.*** Alak-Hul summons 7 (3d4) hallowed thrall within his cloud of Darkness, then uses his dark phasing legendary action.

If Alak-Hul only has one hit point pool remaining when he uses this ultra action, he gains the following additional benefits for the next minute:
- His speed is tripled.
- 5 (2d4) hallowed thrall are summoned within his cloud of darkness at the start of each of his turns, following the same rules as other summoned hallowed thrall.
- The number of legendary actions he can take increases by 1.`,
    lore: `A mighty Darkblade knight imprisoned for rebellion against Oryx. As a high-ranking knight morph, AlakHul stands around 12 feet tall, fully covered by a well-developed chitin exoskeleton. His broad, wedge-shaped cranial crest has grown into a nearly fully enclosed helm, which in Alak-Hul's case is a distinctive, bony white. He is still adorned with the wormsilk tassel-banners of his former station, and retains the massive axe that is the Darkblade's sole weapon.
  Alak-Hul has all the power and Hive magic of a Darkblade at his disposal, despite his confinement. He will attack in the same slow, deliberate manner at first, occasionally using a sorcerous ability to vanish by briefly stepping into or through the ascendant plane, then surprise his adversaries by reappearing in a different position. If hard pressed, he will adopt a markedly more aggressive offense.

Once, Alak-Hul held a favorable position as a foster son of Oryx. In this capacity, he stood with Oryx's true son, Crota, when the Hive prince raised the immense slaughter of Guardians on Luna that would come to be called the Great Disaster. Sometime afterward, feeling neglected by the King of the Hive despite his exalted position, Alak-Hul gathered an army and led an insurrection against Oryx. Despite the Darkblade's zeal, his uprising was defeated, and Alak-Hul himself was captured.

Overlooking his treachery, Oryx was pleased by Alak-Hul's demonstration of hungry ambition. Rather than destroy him utterly, Oryx merely killed Alak-Hul and elected to imprison him by interring his body in the dungeons aboard the Dreadnaught. Here, within the everted space of Oryx's throne world, the ascendant Darkblade could eventually reincarnate himself. Perhaps the Taken King hoped Alak-Hul would whet the edge of his hunger during this indefinite confinement, eventually to become worthy of returning to the King's service. However, even trapped in a sunless cell deep within the Dreadnaught, Alak-Hul could prove a dangerous contender in a struggle for leadership of the Hive.`
  },
  {
    id: 'Krill.Wizard',
    name: 'Wizard',
    race: Race.Krill,
    statblock: `## Wizard
*Medium Hive*
___
**Armor Class** :: 18 (natural armor + arcana armor)
**Energy Shields** :: 88 (16d8+16) solar
**Hit Points** :: 44 (8d8 + 8)
**Speed** :: 35 ft. (can hover)

|STR|DEX|CON|INT|WIS|CHA|
|:---:|:---:|:---:|:---:|:---:|:---:|
|8 (-1)|18 (+4)|12 (+1)|18 (+4)|16 (+3)|21 (+5)|

**Saving Throws** :: Dex +8, Int +8, Cha +9
**Skills** :: Acrobatics +8, Arcana +12, Deception +9, Perception +7, Religion +7, and any three others
**Vehicle Proficiencies** :: tomb ship
**Senses** :: darkvision 120 ft., passive Perception 17
**Languages** :: krill and one other
**Challenge** :: Individual CR 6 (2,300 XP), Classification CR 9 Elite (5,000 XP)

|Proficiency Bonus|Hack DC|
|:---:|:---:|
|+4|-|

***Arcana Armor.*** While the wizard has at least 1 energy shield point, it cannot fail concentration checks to maintain a spell it has cast, and its AC is increased by 4 (shown in stats).
### Actions
___
***Arc Bolt.*** _Ranged Spell Attack_: +9 to hit, range 90 ft., one target. Hit: 17 (7d4) arc damage.
### Spellcasting
___
The wizard is an 11th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following spells:

***Cantrips (at will):*** :: _dancing lights, mage hand, minor illusion_*
***1st-5th level (3 5th-level slots):*** :: _blight, fear, see invisibility, teleportation circle_
***1/brief rest:*** :: _shroud of darkness_
### Legendary Actions
___
The wizard can take 3 legendary actions, choosing from the options below. Only one legendary option can be used at a time and only at the end of another creature’s turn. The wizard regains spent legendary actions at the start of his turn.
:
***Detect.*** The wizard makes a Wisdom (Perception) check.
:
***Deft Movement.*** The wizard moves up to half its fly speed without provoking opportunity attacks.
:
***Bolt Barrage (costs 3 actions).*** The wizard casts arc bolt twice.`,
    lore: `Fully mature Hive that assume the mother morph become wizards, adepts of the Hive’s twisted paracausal sorcery. Wizards’ bodies do not grow much in size, and they only develop slightly more chitin plating than they had as acolytes, notably the ridged or crested cranial growths which often obscure their eyes. Their legs even atrophy, obliging wizards to master perpetual levitation. They clothe themselves with ragged robes of Hive-worm silk, and occasionally other adornments as well. They carry no weapons, but their arsenal of dark powers makes them more than lethal combatants.

Although individuals become wizards by taking on the Hive mother morph, not all wizards reproduce. One of the few things known about the Hive life cycle is that mother wizards can beget spawn with a mate or through parthenogenesis. It is additionally clear that regardless of whether or not they participate in spawning, wizards are most important to the Hive not for their reproductive capabilities, but for their practice of the arcane art commonly called Hive magic, for lack of a better description. The entire Hive tithing hierarchy, as well as more mundane aspects of their affairs, depends upon this so-called magic. As its foremost practitioners, wizards bear considerable responsibility for the maintenance and effective function of Hive operations and logistics.

Among Guardians who have spent the most time facing the Hive, particular fear of wizards is commonplace. As the Hive’s chief experimenters, wizards do not merely slaughter unquestioningly, but delight in capturing their enemies to indulge their gruesome curiosity. They are especially dreaded for their fascination with Ghosts, for the Traveler’s Light is extraordinarily rich nutriment to the Hive’s paracausal digestion.`
  },
]