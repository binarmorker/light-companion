export enum ActionType {
  Action = 1,
  BonusAction = 2,
  Reaction = 4,
  Movement = 8,
  Free = 16
}

export enum ActionTarget {
  Risen = 0,
  Ghost = 1,
  Warlock = 2,
  Titan = 3,
  Hunter = 4
}

export interface Action {
  name: string
  type: ActionType
  excludeTargets: ActionTarget[]
  description: string
  message: string
  cost: string
}

export const actions: Action[] = [
  {
    name: 'Aim',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Aiming movement

**You focus on your attack with a weapon.**

You gain the Aiming condition until the start of your next turn. You can't begin Aiming if you don't have enough movement left or if you don't have enough movement to do so.

If you take a shot with a weapon that has scope ranges, you can take that shot normally up to the weapon's extended range. You can also take a shot with the weapon against a target within its maximum range, but that attack is granted disadvantage.

You cannot make an attack of opportunity.`,
    message: 'is aiming',
    cost: '15ft. MOVEMENT'
  },
  {
    name: 'Attack',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Attack action

**Perform a melee or ranged attack with your weapon.**

Certain features, such as the *Extra Attack* feature, allow you to make more than one attack with this action. Each of these attacks is a separate roll and may target different creatures. You may move in between these attacks.

When you attack with a light melee weapon or an agile ranged weapon, you can use a bonus action to attack with your other hand with another light melee weapon or agile ranged weapon.

You may replace one of your melee attacks with a *Grapple* or a *Shove*.`,
    message: 'is attacking',
    cost: '1 ACTION'
  },
  {
    name: 'Grapple',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Grapple attack

**Attempt to grab a creature or wrestle with it.**

You can use the *Attack* action to make a special melee attack, a grapple. If you're able to make multiple attacks with the Attack action, this attack replaces one of them.

The target of your grapple must be no more than one size larger than you, and it must be within your reach.

Using at least one free hand, you try to seize the target by making a grapple check, a Strength (Athletics) check contested by the target's Strength (Athletics) or Dexterity (Acrobatics) check (the target chooses the ability to use).

If you succeed, you subject the target to the grappled condition (its speed is set to 0).`,
    message: 'is grappling',
    cost: '1 ATTACK'
  },
  {
    name: 'Shove',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Shoving attack

**Shove a creature, either to knock it prone or push it away from you.**

Using the *Attack* action, you can make a special melee attack to shove a creature. If you're able to make multiple attacks with the Attack action, this attack replaces one of them.

The target of your shove must be no more than one size larger than you, and it must be within your reach.

You make a Strength (Athletics) check contested by the target's Strength (Athletics) or Dexterity (Acrobatics) check (the target chooses the ability to use).

If you win the contest, you either knock the target prone or push it 5 feet away from you.`,
    message: 'is shoving',
    cost: '1 ATTACK'
  },
  {
    name: 'Boost',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost, ActionTarget.Warlock, ActionTarget.Hunter],
    description: `### Titan Boost

**Use the Light to give yourself a boost in the air.**

The Titan cannot use their boost to move downwards; glide can only be used to move horizontally or upwards.

It costs 2 feet of movement in order for the Titan to move upwards 1 foot.

When falling, the Titan can spend movement on their boost speed in order to slow their fall, but not completely stop it. The amount of movement spent on their boost is the amount of distance you ignore when calculating the falling damage to apply to the Titan once they stop falling. For example, if a Titan fell 80 feet but, before hitting the ground, spent 30 feet of their movement to boost, they would only take damage for 50 feet of falling. The Titan must not be Prone in order to do this.`,
    message: 'boost',
    cost: '5ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Cast Light Ability',
    type: ActionType.Action | ActionType.BonusAction | ActionType.Reaction,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Light Ability action
    
**You take on your light to use an ability.**

Casting a Light ability—either your grenade, melee, superclass, or super ability—is separate from the Attack action, even if the Light ability requires a Light attack roll.

The action cost of casting a Light ability is described by the Light ability.`,
    message: 'is casting a light ability',
    cost: 'VARIES'
  },
  {
    name: 'Climb/Swim/Crawl',
    type: ActionType.Movement,
    excludeTargets: [],
    description: `### Difficult movement

**May involve a Strength (Athletics) or a Dexterity (Acrobatics) check.**`,
    message: 'is climbing/swimming/crawling',
    cost: '10ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Dash',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Dash action

**Gain extra movement for the current turn.**

The increase equals your speed, after applying any modifiers.`,
    message: 'is dashing',
    cost: '1 ACTION'
  },
  {
    name: 'Dash',
    type: ActionType.Action | ActionType.BonusAction,
    excludeTargets: [ActionTarget.Risen],
    description: `### Dash action

**Gain extra movement for the current turn.**

The increase equals your speed, after applying any modifiers.`,
    message: 'is dashing',
    cost: 'VARIES'
  },
  {
    name: 'Disengage',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Ghost, ActionTarget.Hunter],
    description: `### Disengage action

**Your movement doesn't provoke opportunity attacks for the rest of the turn.**`,
    message: 'is disengaging',
    cost: '1 ACTION'
  },
  {
    name: 'Disengage',
    type: ActionType.Action | ActionType.BonusAction,
    excludeTargets: [ActionTarget.Titan, ActionTarget.Warlock],
    description: `### Disengage action

**Your movement doesn't provoke opportunity attacks for the rest of the turn.**`,
    message: 'is disengaging',
    cost: 'VARIES'
  },
  {
    name: 'Dodge',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Dodge action

**Focus entirely on avoiding attacks.**

Until the start of your next turn, any attack roll made against you has disadvantage if you can see the attacker, and you make Dexterity saving throws with advantage.`,
    message: 'is dodging',
    cost: '1 ACTION'
  },
  {
    name: 'Double Jump',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost, ActionTarget.Titan, ActionTarget.Warlock],
    description: `### Hunter Double Jump

**Use the Light to make up to two jumps in the air.**

Whether airborne or not, a Hunter can always choose to use either their Strength or Dexterity score to determine the distance they can cover with a jump. When performing a jump that calls for a Strength (Athletics) check, a Hunter can make a Dexterity (Acrobatics) check instead.

While falling, the Hunter can spend an amount of movement equal to half their base walking speed on an airborne jump in order to negate any falling damage they might take. The Hunter must not be Prone, and must have enough movement to spend, in order to do this.

Alternatively, a Hunter can spend their action to make a DC 12 Dexterity (Acrobatics) check in order to do the same thing without spending movement, though even still, the Hunter must not be Prone. The DC of this check may increase or decrease at the Architect's discretion.`,
    message: 'is double jumping',
    cost: '5ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Escape Grapple',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Escape Grapple action

**Escape a grapple.**

To escape a grapple, you must succeed on a Strength (Athletics) or Dexterity (Acrobatics) check contested by the grappler's Strength (Athletics) check.

Escaping other conditions that restrain you (such as manacles) may require a Dexterity or Strength check, as specified by the condition.`,
    message: 'is escaping a grapple',
    cost: '1 ACTION'
  },
  {
    name: 'Glide',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost, ActionTarget.Titan, ActionTarget.Hunter],
    description: `### Warlock Glide

**Use the Light to give yourself a boost in the air.**

The Warlock cannot use their glide to move downwards; glide can only be used to move horizontally or upwards.

It costs 2 feet of movement in order for the Warlock to move upwards 1 foot.

When falling, the Warlock can spend 5 feet of movement on their glide speed in order to completely halt their fall. The Warlock must not be Prone in order to do this.`,
    message: 'is gliding',
    cost: '5ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Grapple Move',
    type: ActionType.Movement,
    excludeTargets: [],
    description: `### Grapple movement

**Drag or carry the grappled creature with you.**

If you move while grappling another creature, your speed is halved, unless the creature is two or more sizes smaller than you.`,
    message: 'is moving while grappling',
    cost: '½ SPEED MOVEMENT'
  },
  {
    name: 'Hack Device',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Hack action

**You try to obtain access to a device.**

Make an Intelligence (Technology) check against a DC of 8 + the proficiency bonus of the device + the Intelligence modifier of the device.

On a successful check, you gain one hacking success. On a failed check, you gain one hacking failure. You must keep taking this action until you get three of a kind. If you have not yet accumulated three of a kind, and you do not take the Hack action on your turn, you automatically accumulate one hacking failure.

If you accumulate three hacking failures before you accumulate three hacking successes, the device is immune to the Hack action for the next 24 hours.

If you accumulate three hacking successes before accumulating three hacking failures, you assume control of the device.

Only devices can be hacked. An artifact, artificial intelligence (AI), or a 'dumb' device (a device without sufficiently sophisticated programming to be considered intelligent, such as an LED lamp or a megaphone) cannot be hacked.

You always need a thieves' toolkit or a Ghost to hack a virtual intelligence.`,
    message: 'is hacking a device',
    cost: 'VARIES'
  },
  {
    name: 'Help',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Help action

**Grant an ally advantage on an ability check or attack.**

The target gains advantage on the next avility check it makes to perform the task you are helping with.

Alternatively, the target gains advantage on the next attack roll against a creature within 5 feet of you.

The advantage lasts until the start of your next turn.`,
    message: 'is helping',
    cost: '1 ACTION'
  },
  {
    name: 'Hide',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Hide action

**Attempt to hide.**

You can't hide from a creature that can see you. You must have total cover, be in a heavily obscured area, be invisible, or otherwise block the enemy's vision.

If you make noise (such as shouting a warning or knocking over a vase), you give away your position.

When you try to hide, make a Dexterity (Stealth) check and note the result. Until you are discovered or you stop hiding, that check's total is contested by the Wisdom (Perception) check of any creature that actively searches for signs of your presence.

A creature notices you even if it isn't searching unless your Stealth check is higher than its Passive Perception.

Out of combat, you may also use a Dexterity (Stealth) check for acts like concealing yourself from enemies, slinking past guards, slipping away without being noticed, or sneaking up on someone without being seen or heard.`,
    message: 'is hiding',
    cost: '1 ACTION'
  },
  {
    name: 'Hide',
    type: ActionType.Action | ActionType.BonusAction,
    excludeTargets: [ActionTarget.Risen],
    description: `### Hide action

**Attempt to hide.**

You can't hide from a creature that can see you. You must have total cover, be in a heavily obscured area, be invisible, or otherwise block the enemy's vision.

If you make noise (such as shouting a warning or knocking over a vase), you give away your position.

When you try to hide, make a Dexterity (Stealth) check and note the result. Until you are discovered or you stop hiding, that check's total is contested by the Wisdom (Perception) check of any creature that actively searches for signs of your presence.

A creature notices you even if it isn't searching unless your Stealth check is higher than its Passive Perception.

Out of combat, you may also use a Dexterity (Stealth) check for acts like concealing yourself from enemies, slinking past guards, slipping away without being noticed, or sneaking up on someone without being seen or heard.`,
    message: 'is hiding',
    cost: 'VARIES'
  },
  {
    name: 'High Jump',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost],
    description: `### High Jump

**You leap into the air a number of feet equal to 3 + your Strength modifier.**

You must move 10 ft. on foot immediately before the jump. Otherwise, when you make a standing high jump, you can only jump half that distance.

You can extend your arms half your height above yourself during the jump.`,
    message: 'is high jumping',
    cost: '5ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Long Jump',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Long Jump

**You cover a number of feet up to your Strength score.**

You must move 10 ft. on foot immediately before the jump. Otherwise, when you make a standing long jump, you can only jump half that distance.

May involve a DC 10 Strength (Athletics) check to clear a low obstacle (no taller than a quarter of the jump's distance). You hit the obstacle on a failed check.

May involve a DC 10 Dexterity (Acrobatics) check to land on your feet in difficult terrain. You land prone on a failed check.`,
    message: 'is long jumping',
    cost: '5ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Move',
    type: ActionType.Movement,
    excludeTargets: [],
    description: `### Basic movement

**You may move anytime during your turn.**

If you have more than one speed, such as your walking speed and a flying speed, you can switch back and forth between your speeds during your move. Whenever you switch, subtract the distance you've already moved from the new speed.

You can move freely through a nonhostile creature's space. You can move through a hostile creature's space only if the creature is at least two sizes larger or smaller than you. Another creature's space is difficult terrain for you.

Whether a creature is a friend or an enemy, you can't willingly end your move in its space.`,
    message: 'is moving',
    cost: '5ft. / 5ft. MOVEMENT'
  },
  {
    name: 'Offhand Attack',
    type: ActionType.BonusAction,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Offhand Attack action

**Perform another melee or ranged attack with your offhand weapon.**

When you make an offhand weapon attack that hits, you do not add your ability modifier to the damage roll, unless the modifier is negative.`,
    message: 'is attacking',
    cost: '1 BONUS ACTION'
  },
  {
    name: 'Opportunity Attack',
    type: ActionType.Reaction,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Opportunity Attack reaction

**Perform a melee attack on an enemy leaving your reach.**

The attack interrupts the provoking creature's movement, occurring right before the creature leaves your reach.

Creatures don't provoke an opportunity attack when they teleport or when someone or something moves them without using their movement, action, or reaction.`,
    message: 'is attacking',
    cost: '1 REACTION'
  },
  {
    name: 'Prone',
    type: ActionType.Movement,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Prone

**You can drop prone without using any of your speed.**

To move while prone, you must crawl or use magic such as teleportation

You may move yourself to the Combat-Prone condition using 15ft. of your movement.

It takes 15ft. of your movement to bring yourself back up and end the Prone or Combat-Prone condition.

It costs no movement for a Combat-Prone creature to begin Aiming with a firearm that has a medium or long range band.`,
    message: 'is proning himself',
    cost: '15 ft. MOVEMENT'
  },
  {
    name: 'Ready',
    type: ActionType.Action | ActionType.Reaction,
    excludeTargets: [],
    description: `### Ready action

**Choose a trigger and a response reaction.**

First, you decide what perceivable circumstance will trigger your reaction.

Then, you choose the action you will take in response to that trigger, or you choose to move up to your speed in response to it.

When the trigger occurs, you can either take your reaction right after the trigger finishes or ignore the trigger.`,
    message: 'is readying',
    cost: '1 ACTION'
  },
  {
    name: 'Reload',
    type: ActionType.Action | ActionType.Free,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Reload action

**You replace a weapon's magazine, refilling the shot capacity of the weapon.**

You can reload weapons you are proficient with as your item interaction on your turn. If you are not proficient with a weapon that needs to be reloaded, you must use your action to reload it.

You can choose to reload a firearm before its shot capacity reaches 0, but you still spend a full magazine to reload it.`,
    message: 'is reloading',
    cost: 'VARIES'
  },
  {
    name: 'Revive',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Revive action

**You assist a Ghost unable to resurrect its Guardian.**

You channel your Light into a willing Ghost within 5 feet of you who has captured its dead Guardian's Light, and the Ghost of the dead Guardian can take the Resurrect action as a reaction, if it is not in an Oppressive Darkness zone.

You both then gain an overshield that lasts until the start of your—not the resurrected Guardian's—next turn.`,
    message: 'is reviving',
    cost: '1 ACTION'
  },
  {
    name: 'Search',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Search action

**Devote your attention to finding something.**

Depending on the nature of your search, you may have to make a Wisdom (Perception) check or an Intelligence (Investigation) check.`,
    message: 'is searching',
    cost: '1 ACTION'
  },
  {
    name: 'Use Object',
    type: ActionType.Free,
    excludeTargets: [],
    description: `### Free Object interaction

**Interact with a first object on your turn.**

You can interact with one object or feature of the environment for free, during either your move or your action. For example, you could open a door during your move as you stride toward a foe, or you could draw your weapon as part of the same action you use to attack.`,
    message: 'is using an object',
    cost: 'FREE'
  },
  {
    name: 'Use Object',
    type: ActionType.Action,
    excludeTargets: [],
    description: `### Use Object action

**Interact with a second object or use special object abilities.**

You can interact with one object for free during your turn (such as drawing a weapon or opening a door). If you want to interact with a second object, use this action.

When an object requires your action for its use, you also take this action.`,
    message: 'is using an object',
    cost: '1 ACTION'
  },
  {
    name: 'Capture Light',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Risen],
    description: `### Capture Light action

**You capture the Light of your Risen within 5ft. of you.**

This removes the Risen's body from the physical world and stores it, ending any condition and curing exhaustion.

You can only Capture Light outside of the pocket backpack, and cannot re-enter it until a Resurrect action has been performed on the Risen.

If the Risen accumulates three RTL saving throw failures, the Risen's Light has faded away and has died permanently.`,
    message: 'is capturing light',
    cost: '1 ACTION'
  },
  {
    name: 'Resurrect',
    type: ActionType.Action | ActionType.Reaction,
    excludeTargets: [ActionTarget.Risen],
    description: `### Resurrect action

**You spend a Restoration point to project your captured Light back into the physical world.**

Your Risen appears within 5ft. from you. They recover all health points but no shield, and can immediately take actions, bonus actions, and reactions on their initiative like normal.

You may use a reaction to perform a resurrection on your Risen if they are within 5ft. of you and another creature has performed a Revive action on them.

You can not perform a resurrection if you do not have a Restoration point to spend.`,
    message: 'is resurrecting',
    cost: 'VARIES'
  },
  {
    name: 'Heal',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Risen],
    description: `### Heal action

**You help your Risen recover from their injuries within 5ft. of you.**

Your Risen recovers all their health points but no shield.

You can only heal outside of the pocket backpack, and must be within 5ft. of the Risen.

You can not heal if you do not have a Restoration point to spend.`,
    message: 'is healing',
    cost: '1 ACTION'
  },
  {
    name: 'Glimmer Program',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Risen],
    description: `### Glimmer Program action

**You program glimmer into matter, or turn existing matter into piles of glimmer.**

As an action, you may program up to 2,000 bits of glimmer into an item that does not require attunement of that value.

Alternatively, if an item is made up of glimmer, you may use an action to reduce it back to raw glimmer equal to one third of its value in glimmer.`,
    message: 'is programming glimmer',
    cost: '1 ACTION'
  },
  {
    name: 'Pocket Backpack',
    type: ActionType.Action | ActionType.BonusAction | ActionType.Reaction,
    excludeTargets: [ActionTarget.Risen],
    description: `### Pocket Backpack action

**You go in the extradimensional space attached to your Risen.**

As an action or a bonus action, while you are within 5ft. of your Risen, you may enter or leave the pocket backpack.

You cannot be harmed while in the pocket backpack, and you may not enter other Risens' pocket backpacks.

You may still communicate with your Risen back and forth, and you may read or speak over local communication networks.`,
    message: 'is going into the pocket backpack',
    cost: 'VARIES'
  },
  {
    name: 'Equipment Swap',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Risen],
    description: `### Equipment Swap action
**You switch out an item with your Risen.**`,
    message: 'is transmatting an item',
    cost: '1 ACTION'
  },
  {
    name: 'Scanners',
    type: ActionType.Action,
    excludeTargets: [ActionTarget.Risen],
    description: `### Scanners action
**You scan the local area for creatures.**

You may choose to scan in a sphere of a radius of 5ft., or in a 15ft. cone.`,
    message: 'is scanning the area',
    cost: '1 ACTION'
  },
  {
    name: 'Brief Rest',
    type: ActionType.Free,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Brief Rest
**You take a breath and prepare for the next encounter.**

You may recharge your weapons or consume a ration. You also recover all your energy shield points.

All your Light abilities recharge, except for your super. Some feature uses also recharge on a brief rest.

You may take a brief rest even if you're walking to your next destination, but it must take at least 5 minutes and you must not engage in combat, forced march, or other strenuous activity.`,
    message: 'is taking a brief rest',
    cost: '5 MINUTES'
  },
  {
    name: 'Short Rest',
    type: ActionType.Free,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Short Rest
**You spend some quality time with your party.**

You may do light activities, including eating, drinking, reading, or reloading weapons.

All your Light abilities recharge. Some feature uses also recharge on a short rest.`,
    message: 'is taking a short rest',
    cost: '1 HOUR'
  },
  {
    name: 'Long Rest',
    type: ActionType.Free,
    excludeTargets: [ActionTarget.Ghost],
    description: `### Long Rest
**You spend the night and prepare for tomorrow.**

All your Light abilities recharge. Some feature uses also recharge on a long rest.

If you had any number of levels of exhaustion, you lose one upon completing your long rest.

You may spend up to 2 hours of your long rest performing extended actions, such as fixing broken equipment, spending glimmer to program a new gun, or standing watch. If you are interrupted by strenuous activity, such as combat, 1 hour of walking, or similar activity, you must begin the rest again to benefit from it.`,
    message: 'is taking a long rest',
    cost: '8 HOURS'
  },
  {
    name: 'Short Rest',
    type: ActionType.Free,
    excludeTargets: [ActionTarget.Risen],
    description: `### Short Rest
**You spend some quality time with your Risen.**

You may do light activities, including eating, drinking or reading.

You may also roll your hit dice to regain hit points.`,
    message: 'is taking a short rest',
    cost: '1 HOUR'
  },
  {
    name: 'Long Rest',
    type: ActionType.Free,
    excludeTargets: [ActionTarget.Risen],
    description: `### Long Rest
**You spend the night and prepare for tomorrow.**

Half your Restoration points recharge when completing a long rest.

You may spend up to 2 hours of your long rest performing light activities, like during a short rest. If you are interrupted by strenuous activity, such as combat, 1 hour of traveling, or similar activity, you must begin the rest again to benefit from it.`,
    message: 'is taking a long rest',
    cost: '8 HOURS'
  }
]
