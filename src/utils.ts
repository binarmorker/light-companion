import { micromark } from 'micromark'
import { gfmTable, gfmTableHtml } from 'micromark-extension-gfm-table'
import { Ability, Alignment, AllWeaponItemType, ArmorItemType, DamageType, GenericItemType, Ghost, Guardian, HeavyWeaponItemType, ItemProperty, ItemRarity, ItemType, MartialWeaponItemType, MiscItemType, PrimaryWeaponItemType, ProficiencyType, Race, SimpleWeaponItemType, Skill, ToolItemType, VehicleItemType, WeaponItemType } from '@/models/destiny'
import { getSubclass } from '@/models/destinySubclasses'

export function distinctBy<T>(array: T[], func: (arg: T) => number|string|boolean) {
  const map = new Map(array.map(item => [func(item), item]))
  return [...map.values()]
}

export function showNumber(number: number) {
  const formatter = new Intl.NumberFormat('en-CA')
  return formatter.format(number)
}

export function applyMarkdown(markdown: string) {
  return micromark(markdown, 'utf-8', {
    extensions: [gfmTable()],
    htmlExtensions: [gfmTableHtml()]
  })
}

export function clampAndRound(value: number, min: number, max: number) {
  return Math.round(Math.min(Math.max(value, min), max))
}

export function getProficiencyBonus(character: Guardian|Ghost, type: ProficiencyType|null = null) {
  const bonus = 2 + Math.floor(character.level / 4)

  switch (type) {
    case ProficiencyType.Expertise:
      return bonus * 2
    case ProficiencyType.Half:
      return Math.floor(bonus / 2)
    case ProficiencyType.None:
      return 0
    case ProficiencyType.Proficiency:
    case null:
    default:
      return bonus
  }
}

export function getAbilityModifier(character: Guardian|Ghost, ability: Ability) {
  return character ? Math.floor((character.abilities[ability] - 10) / 2) : 0
}

export function isProficientInSave(character: Guardian|Ghost, ability: Ability) {
  if (!('id' in character)) return ability === Ability.Dexterity || ability === Ability.Intelligence
  return character.subclass && getSubclass(character.subclass)?.abilityProficiencies.includes(ability)
}

export function getAbilitySave(character: Guardian|Ghost, ability: Ability) {
  return character
    ? (Math.floor((character.abilities[ability] - 10) / 2) +
      (isProficientInSave(character, ability) ? getProficiencyBonus(character) : 0))
    : 0
}

export function getSkillModifier(character: Guardian|Ghost, skill: Skill) {
  return character
    ? (Math.floor((character.abilities[getSkillAbility(skill)] - 10) / 2) +
      getProficiencyBonus(character, character.skillProficiencies[skill]))
    : 0
}

export function getSign(number: number) {
  return (number > 0 ? '+' : '') + number
}

export function getHealthPoints(dice: number, con: number, levels: number) {
  return dice + con + Math.max(0, con * (levels - 1))
}

export function getDiceAverageOrMaxOnFirstLevel(dice: number, levels: number) {
  let total = 0

  for (let level = 1; level <= levels; level++) {
    if (level === 1) {
      total += dice
    } else {
      total += (dice / 2) + 1
    }
  }

  return total
}

export function getHealthPointsDiceAverageOrMaxOnFirstLevel(dice: number, con: number, levels: number) {
  let total = 0

  for (let level = 1; level <= levels; level++) {
    if (level === 1) {
      total += dice + con
    } else {
      total += (dice / 2) + 1 + con
    }
  }

  return total
}

export function debounce(fn: (args?: unknown|undefined) => void, delay: number) {
  let timer: NodeJS.Timeout

  const debouncedFn = (args?: unknown|undefined): Promise<void> => new Promise((resolve) => {
    if (timer) clearTimeout(timer)

    timer = setTimeout(() => {
      resolve(fn(args))
    }, delay)
  })

  return debouncedFn
}

export function getAllRarities() {
  return [...Object.values(ItemRarity).filter(x => !isNaN(Number(x)))] as ItemRarity[]
}

export function enumerateEnum<T>(value: Record<string, string|number>) {
  return Object.values(value).filter(x => !isNaN(Number(x))) as T[]
}

export function getAllItemProperties() {
  return [...Object.values(ItemProperty).filter(x => !isNaN(Number(x)))] as ItemProperty[]
}

export function getAllAbilities() {
  return [...Object.values(Ability).filter(x => !isNaN(Number(x)))] as Ability[]
}

export function getAllSkills() {
  return [...Object.values(Skill).filter(x => !isNaN(Number(x)))] as Skill[]
}

export function getSkillAbility(skill: Skill) {
  switch (skill) {
    case Skill.Athletics:
      return Ability.Strength
    case Skill.Acrobatics:
    case Skill.SleightOfHand:
    case Skill.Stealth:
      return Ability.Dexterity
    case Skill.Arcana:
    case Skill.History:
    case Skill.Investigation:
    case Skill.Nature:
    case Skill.Religion:
    case Skill.Technology:
      return Ability.Intelligence
    case Skill.AnimalHandling:
    case Skill.Insight:
    case Skill.Medicine:
    case Skill.Perception:
    case Skill.Survival:
      return Ability.Wisdom
    case Skill.Deception:
    case Skill.Intimidation:
    case Skill.Performance:
    case Skill.Persuasion:
      return Ability.Charisma
  }
}

export function getAbilityName(ability: Ability) {
  switch (ability) {
    case Ability.Strength: return 'Strength'
    case Ability.Dexterity: return 'Dexterity'
    case Ability.Constitution: return 'Constitution'
    case Ability.Intelligence: return 'Intelligence'
    case Ability.Wisdom: return 'Wisdom'
    case Ability.Charisma: return 'Charisma'
  }
}

export function getSkillName(skill: Skill) {
  switch (skill) {
    case Skill.Athletics: return 'Athletics'
    case Skill.Acrobatics: return 'Acrobatics'
    case Skill.AnimalHandling: return 'Animal Handling'
    case Skill.Arcana: return 'Arcana'
    case Skill.Deception: return 'Deception'
    case Skill.History: return 'History'
    case Skill.Insight: return 'Insight'
    case Skill.Intimidation: return 'Intimidation'
    case Skill.Investigation: return 'Investigation'
    case Skill.Medicine: return 'Medicine'
    case Skill.Nature: return 'Nature'
    case Skill.Perception: return 'Perception'
    case Skill.Performance: return 'Performance'
    case Skill.Persuasion: return 'Persuasion'
    case Skill.Religion: return 'Religion'
    case Skill.SleightOfHand: return 'Sleight of Hand'
    case Skill.Stealth: return 'Stealth'
    case Skill.Survival: return 'Survival'
    case Skill.Technology: return 'Technology'
  }
}

export function getDamageTypeByName(name?: string) {
  switch (name?.trim().toLowerCase()) {
    case 'kinetic': return DamageType.Kinetic
    case 'poison': return DamageType.Poison
    case 'psychic': return DamageType.Psychic
    case 'light': return DamageType.Light
    case 'solar': return DamageType.Solar
    case 'arc': return DamageType.Arc
    case 'void': return DamageType.Void
    case 'darkness': return DamageType.Darkness
    case 'stasis': return DamageType.Stasis
    case 'strand': return DamageType.Strand
    case 'luster': return DamageType.Luster
    case 'explosive_kinetic': return DamageType.ExplosiveKinetic
    case 'explosive_poison': return DamageType.ExplosivePoison
    case 'explosive_psychic': return DamageType.ExplosivePsychic
    case 'explosive_light': return DamageType.ExplosiveLight
    case 'explosive_solar': return DamageType.ExplosiveSolar
    case 'explosive_arc': return DamageType.ExplosiveArc
    case 'explosive_void': return DamageType.ExplosiveVoid
    case 'explosive_darkness': return DamageType.ExplosiveDarkness
    case 'explosive_stasis': return DamageType.ExplosiveStasis
    case 'explosive_strand': return DamageType.ExplosiveStrand
    case 'explosive_luster': return DamageType.ExplosiveLuster
    case 'healing': return DamageType.Healing
    case 'regenerating': return DamageType.HealthHealing
    case 'recharging': return DamageType.ShieldHealing
    default: return undefined
  }
}

export function appendExplosive(damageTypes: DamageType[]) {
  const newDamageTypes = new Set<DamageType>()

  for (const damageType of damageTypes) {
    newDamageTypes.add(damageType)
    if (damageType < 900) newDamageTypes.add(damageType + 900)
    else newDamageTypes.add(damageType - 900)
  }

  return [...newDamageTypes]
}

export function getDamageTypeName(damageType: DamageType) {
  switch (damageType) {
    case DamageType.Kinetic: return 'Kinetic'
    case DamageType.Poison: return 'Poison'
    case DamageType.Psychic: return 'Psychic'
    case DamageType.Light: return 'Light'
    case DamageType.Solar: return 'Solar'
    case DamageType.Arc: return 'Arc'
    case DamageType.Void: return 'Void'
    case DamageType.Darkness: return 'Darkness'
    case DamageType.Stasis: return 'Stasis'
    case DamageType.Strand: return 'Strand'
    case DamageType.Luster: return 'Luster'
    case DamageType.ExplosiveKinetic: return 'Explosive Kinetic'
    case DamageType.ExplosivePoison: return 'Explosive Poison'
    case DamageType.ExplosivePsychic: return 'Explosive Psychic'
    case DamageType.ExplosiveLight: return 'Explosive Light'
    case DamageType.ExplosiveSolar: return 'Explosive Solar'
    case DamageType.ExplosiveArc: return 'Explosive Arc'
    case DamageType.ExplosiveVoid: return 'Explosive Void'
    case DamageType.ExplosiveDarkness: return 'Explosive Darkness'
    case DamageType.ExplosiveStasis: return 'Explosive Stasis'
    case DamageType.ExplosiveStrand: return 'Explosive Strand'
    case DamageType.ExplosiveLuster: return 'Explosive Luster'
    case DamageType.Healing: return 'Healing'
    case DamageType.HealthHealing: return 'Regenerating'
    case DamageType.ShieldHealing: return 'Recharging'
  }
}

export function getItemTypeName(itemType: AllWeaponItemType | ItemType, plural = true) {
  switch (itemType) {
    case WeaponItemType.SimpleWeapon: return plural ? 'Simple Weapons' : 'Simple Weapon'
    case WeaponItemType.MartialWeapon: return plural ? 'Martial Weapons' : 'Martial Weapon'
    case WeaponItemType.PrimaryWeapon: return plural ? 'Primary Weapons' : 'Primary Weapon'
    case WeaponItemType.HeavyWeapon: return plural ? 'Heavy Weapons' : 'Heavy Weapon'
    case WeaponItemType.RelicWeapon: return plural ? 'Relics' : 'Relic'
    case SimpleWeaponItemType.Dagger: return plural ? 'Daggers' : 'Dagger'
    case SimpleWeaponItemType.ThrowingHammer: return plural ? 'Throwing Hammers' : 'Throwing Hammer'
    case MartialWeaponItemType.Broadsword: return plural ? 'Broadswords' : 'Broadsword'
    case MartialWeaponItemType.CombatBow: return plural ? 'Combat Bows' : 'Combat Bow'
    case MartialWeaponItemType.Greataxe: return plural ? 'Greataxes' : 'Greataxe'
    case MartialWeaponItemType.HeavyMaul: return plural ? 'Heavy Mauls' : 'Heavy Maul'
    case MartialWeaponItemType.Longsword: return plural ? 'Longswords' : 'Longsword'
    case MartialWeaponItemType.Shortsword: return plural ? 'Shortswords' : 'Shortsword'
    case MartialWeaponItemType.Smallsword: return plural ? 'Smallswords' : 'Smallsword'
    case MartialWeaponItemType.Warhammer: return plural ? 'Warhammers' : 'Warhammer'
    case PrimaryWeaponItemType.AutoRifle: return plural ? 'Auto Rifles' : 'Auto Rifle'
    case PrimaryWeaponItemType.HandCannon: return plural ? 'Hand Cannons' : 'Hand Cannon'
    case PrimaryWeaponItemType.PulseRifle: return plural ? 'Pulse Rifles' : 'Pulse Rifle'
    case PrimaryWeaponItemType.ScoutRifle: return plural ? 'Scout Rifles' : 'Scout Rifle'
    case PrimaryWeaponItemType.Sidearm: return plural ? 'Sidearms' : 'Sidearm'
    case PrimaryWeaponItemType.SubmachineGun: return plural ? 'Submachine Guns' : 'Submachine Gun'
    case PrimaryWeaponItemType.TraceRifle: return plural ? 'Trace Rifles' : 'Trace Rifle'
    case HeavyWeaponItemType.FusionRifle: return plural ? 'Fusion Rifles' : 'Fusion Rifle'
    case HeavyWeaponItemType.GrenadeLauncher: return plural ? 'Grenade Launchers' : 'Grenade Launcher'
    case HeavyWeaponItemType.LightMachineGun: return plural ? 'Light Machine Guns' : 'Light Machine Gun'
    case HeavyWeaponItemType.LinearFusionRifle: return plural ? 'Linear Fusion Rifles' : 'Linear Fusion Rifle'
    case HeavyWeaponItemType.RocketLauncher: return plural ? 'Rocket Launchers' : 'Rocket Launcher'
    case HeavyWeaponItemType.Shotgun: return plural ? 'Shotguns' : 'Shotgun'
    case HeavyWeaponItemType.SniperRifle: return plural ? 'Sniper Rifles' : 'Sniper Rifle'
    case GenericItemType.ClassItem: return plural ? 'Class Items' : 'Class Item'
    case GenericItemType.Clothing: return plural ? 'Clothing' : 'Clothing'
    case GenericItemType.Consumable: return plural ? 'Consumables' : 'Consumable'
    case GenericItemType.Container: return plural ? 'Containers' : 'Container'
    case GenericItemType.Currency: return plural ? 'Currencies' : 'Currency'
    case GenericItemType.Trinket: return plural ? 'Trinkets' : 'Trinket'
    case GenericItemType.Generic: return plural ? 'Items' : 'Item'
    case ArmorItemType.LightArmor: return plural ? 'Light Armor' : 'Light Armor'
    case ArmorItemType.MediumArmor: return plural ? 'Medium Armor' : 'Medium Armor'
    case ArmorItemType.HeavyArmor: return plural ? 'Heavy Armor' : 'Heavy Armor'
    case ArmorItemType.GhostShell: return plural ? 'Ghost Shells' : 'Ghost Shell'
    case VehicleItemType.AirVehicle: return plural ? 'Jumpships' : 'Jumpship'
    case VehicleItemType.LandVehicle: return plural ? 'Sparrows' : 'Sparrow'
    case ToolItemType.ArmorsmithingTools: return plural ? 'Armorsmithing Toolkits' : 'Armorsmithing Toolkit'
    case ToolItemType.ClimbingTools: return plural ? 'Climbing Toolkits' : 'Climbing Toolkit'
    case ToolItemType.CookingTools: return plural ? 'Cooking Toolkits' : 'Cooking Toolkit'
    case ToolItemType.ElectronicsTools: return plural ? 'Electronics Toolkits' : 'Electronics Toolkit'
    case ToolItemType.MedicalTools: return plural ? 'Medical Toolkits' : 'Medical Toolkit'
    case ToolItemType.ScubaTools: return plural ? 'SCUBA Toolkits' : 'SCUBA Toolkit'
    case ToolItemType.SewingTools: return plural ? 'Sewing Toolkits' : 'Sewing Toolkit'
    case ToolItemType.ThievesTools: return plural ? 'Thieves\' Toolkits' : 'Thieves\' Toolkit'
    case ToolItemType.VehicleTools: return plural ? 'Vehicle Toolkits' : 'Vehicle Toolkit'
    case ToolItemType.WeaponsmithingTools: return plural ? 'Weaponsmithing Toolkits' : 'Weaponsmithing Toolkit'
    case ToolItemType.WhittlingTools: return plural ? 'Whittling Toolkits' : 'Whittling Toolkit'
    case MiscItemType.Vehicle: return plural ? 'All Vehicles' : 'Vehicle'
    case MiscItemType.Armor: return plural ? 'All Armor' : 'Armor'
    case MiscItemType.Toolkit: return plural ? 'All Tools' : 'Toolkit'
    case MiscItemType.Other: return plural ? 'Other Items' : 'Other'
  }
}

export function getRarityName(rarity: ItemRarity) {
  switch (rarity) {
    case ItemRarity.Common: return 'Common'
    case ItemRarity.Uncommon: return 'Uncommon'
    case ItemRarity.Rare: return 'Rare'
    case ItemRarity.Legendary: return 'Legendary'
    case ItemRarity.Exotic: return 'Exotic'
  }
}

export function getItemPropertyName(itemProperty: ItemProperty) {
  switch (itemProperty) {
    case ItemProperty.Agile: return 'Agile'
    case ItemProperty.Ammunition: return 'Ammunition'
    case ItemProperty.Cumbersome: return 'Cumbersome'
    case ItemProperty.Elemental: return 'Elemental'
    case ItemProperty.Finesse: return 'Finesse'
    case ItemProperty.Heavy: return 'Heavy'
    case ItemProperty.Loading: return 'Loading'
    case ItemProperty.OneHanded: return 'One-Handed'
    case ItemProperty.Ranged: return 'Ranged'
    case ItemProperty.Reach: return 'Reach'
    case ItemProperty.Special: return 'Special'
    case ItemProperty.Thrown: return 'Thrown'
    case ItemProperty.TwoHanded: return 'Two-Handed'
    case ItemProperty.Versatile: return 'Versatile'
    case ItemProperty.AutomaticFire: return 'Automatic Fire'
    case ItemProperty.EnergyProjectiles: return 'Energy Projectiles'
    case ItemProperty.HighRecoil: return 'High Recoil'
    case ItemProperty.Payload: return 'Payload'
  }
}

export function getItemPropertyDescription(itemProperty: ItemProperty) {
  switch (itemProperty) {
    case ItemProperty.Agile: return `An agile weapon is small and easy to handle, making it ideal for use when fighting with two weapons. An agile weapon can be held in one hand and used for an offhand weapon attack.`
    case ItemProperty.Ammunition: return `You can use a weapon that has the ammunition property to make a ranged attack only if you have ammunition to fire from the weapon. Each time you attack with the weapon, you expend one piece of ammunition. Drawing the ammunition from a quiver, case, or other container is part of the attack (you need a free hand to load a one-handed weapon). At the end of the battle, you can recover half your expended ammunition by taking a minute to search the battlefield.

If you use a weapon that has the ammunition property to make a melee attack, you treat the weapon as an improvised weapon.`
    case ItemProperty.Cumbersome: return `This weapon's magazine takes longer to reload than most weapons. If you run out of shots with this weapon, you must use an action to reload it. While no weapons inherently start with this property, some weapon perks grant this property.`
    case ItemProperty.Elemental: return `This weapon deals arc, solar, or void damage, which is determined at the time of the weapon's creation. The damage type this weapon deals cannot be altered once chosen.`
    case ItemProperty.Finesse: return `When making an attack with a finesse weapon, you use your choice of your Strength or Dexterity modifier for the attack and damage rolls. You must use the same modifier for both rolls.`
    case ItemProperty.Heavy: return `Small creatures have disadvantage on attack rolls with heavy weapons. A heavy weapon's size and bulk make it too large for a Small creature to use effectively.`
    case ItemProperty.Loading: return `When you use an action, bonus action, or reaction to take a shot with this weapon, you can only attack once with this weapon as your action, bonus action, or reaction, and your attack with this weapon must be the only attack you make using your action, bonus action, or reaction. This is due to the time required to load and/or make and attack with this weapon.

For example, you can not invoke the Extra Attack feature if you take a shot with this weapon, and you cannot take a shot with this weapon as part of the Extra Attack feature if you already took a shot with another weapon on your turn.`
    case ItemProperty.OneHanded: return `This weapon only requires one hand to hold. Even so, a one-handed weapon cannot be used for an offhand weapon attack. `
    case ItemProperty.Ranged: return `A weapon that can be used to make a ranged attack has a range in parentheses after the ammunition or thrown property. The range lists two numbers. The first is the weapon's effective range, and the second indicates the weapon's extended range. When attacking a target beyond a weapon's effective range, you have disadvantage on the attack roll. You can't attack a target beyond the weapon's extended range.`
    case ItemProperty.Reach: return `This weapon adds 5 feet to your reach when you attack with it, as well as when determining your reach for opportunity attacks with it.`
    case ItemProperty.Special: return `A weapon with the special property has unusual rules governing its use, explained in the weapon's description (see "Special Weapons").`
    case ItemProperty.Thrown: return `If a weapon has the thrown property, you can throw the weapon to make a ranged attack. If the weapon is a melee weapon, you use the same ability modifier for that attack roll and damage roll that you would use for a melee attack with the weapon. For example, if you throw a throwing hammer, you use your Strength, but if you throw a dagger, you can use either your Strength or your Dexterity, since the dagger has the finesse property.`
    case ItemProperty.TwoHanded: return `This weapon requires two hands when you attack with it. If you are not holding it with both of your hands, you cannot make an attack with it. A two-handed weapon cannot be used for an offhand weapon attack.`
    case ItemProperty.Versatile: return ` This weapon can be used with one or two hands. A damage value in parentheses appears with this property. This is the damage the weapon uses when two hands are used to make a melee attack with it.`
    case ItemProperty.AutomaticFire: return `This weapon fires multiple rounds per shot, granting it a higher than average chance of dealing damage in its upper ranges. When you deal damage with this weapon, roll an additional damage die and drop the lowest roll from your total for that attack. Do this after accounting for the effects of a critical hit.`
    case ItemProperty.EnergyProjectiles: return `This weapon fires coherent directed energy pulses or beams as projectiles, and can damage all creatures in a line up to this weapon's effective range while not Aiming, or up to the weapon's extended range while you are Aiming. In order to do this, your attack must have killed the previous target, your attack roll must beat the AC of the subsequent target, and the line of fire must not be blocked by 1 inch of metal, 2 inches of stone, or 3 feet of wood or dirt. Subsequent targets take the remaining damage from what killed the previous target.`
    case ItemProperty.HighRecoil: return ` The kickback on this weapon is severe enough that you must use your Strength modifier when determining the attack and damage rolls of this weapon, regardless whether it is a melee or ranged weapon.`
    case ItemProperty.Payload: return `Payload weapons fire explosive or highyield rounds that have a noticeable impact radius, indicated in parentheses after the payload property. When you make an attack with this weapon, instead of making an attack roll, you choose a space on a hard surface you can see within range of the weapon. All targets within the impact radius of the space you choose must make a payload saving throw, which is a Dexterity saving throw with the DC described below. For example, a Payload weapon with a 5-foot radius causes all targets within 5 feet of the impact point to make a payload saving throw.

Payload DC = 8 + your proficiency bonus + your Strength or Dexterity modifier, the same as the modifier you use for rolling damage for the weapon

You only add your proficiency bonus to the DC of the payload saving throw if you are proficient with the weapon. Targets take the damage of this weapon on a failed save, or half as much damage on a success.`
  }
}

export const conditions = [
  'Blinded',
  'Charmed',
  'Deafened',
  'Frightened',
  'Grappled',
  'Incapacitated',
  'Invisible',
  'Paralyzed',
  'Petrified',
  'Poisoned',
  'Prone',
  'Restrained',
  'Stunned',
  'Unconscious',
  'Active Camouflage',
  'Aiming',
  'Burning',
  'Combat-prone',
  'Electrified',
  'Empowered',
  'Empowered x2',
  'Empowered x3',
  'Suppressed',
  'Tethered',
  'Weakened',
  'Exhaustion',
  'Exhaustion x2',
  'Exhaustion x3',
  'Exhaustion x4',
  'Exhaustion x5',
  'Exhaustion x6'
]

export function experienceCapPerLevel(level: number) {
  switch (level) {
    case 1: return 300
    case 2: return 900
    case 3: return 2700
    case 4: return 6500
    case 5: return 14000
    case 6: return 23000
    case 7: return 34000
    case 8: return 48000
    case 9: return 64000
    case 10: return 85000
    case 11: return 100000
    case 12: return 120000
    case 13: return 140000
    case 14: return 165000
    case 15: return 195000
    case 16: return 225000
    case 17: return 265000
    case 18: return 305000
    case 19: return 355000
    case 20: return 355000
    default: return 0
  }
}

export function getAlignmentName(alignment: Alignment|null) {
  switch (alignment) {
    case Alignment.LawfulGood: return 'Lawful Good'
    case Alignment.LawfulNeutral: return 'Lawful Neutral'
    case Alignment.LawfulEvil: return 'Lawful Evil'
    case Alignment.NeutralGood: return 'Neutral Good'
    case Alignment.TrueNeutral: return 'True Neutral'
    case Alignment.NeutralEvil: return 'Neutral Evil'
    case Alignment.ChaoticGood: return 'Chaotic Good'
    case Alignment.ChaoticNeutral: return 'Chaotic Neutral'
    case Alignment.ChaoticEvil: return 'Chaotic Evil'
    default: return ''
  }
}

export function getRaceName(race: Race) {
  switch (race) {
    case Race.Awoken: return 'Awoken'
    case Race.Beast: return 'Beast'
    case Race.Cabal: return 'Cabal'
    case Race.Construct: return 'Construct'
    case Race.Eliksni: return 'Eliksni'
    case Race.Exo: return 'Exo'
    case Race.Human: return 'Human'
    case Race.Krill: return 'Hive'
    case Race.Psion: return 'Psion'
    case Race.Taken: return 'Taken'
    case Race.Vex: return 'Vex'
    default: return 'Unknown'
  }
}
