class Event {
  events: Record<string, ((data: never) => void)[]> = {}
  
  on<T>(eventName: string, fn: (data: T) => void) {
    this.events[eventName] = this.events[eventName] || []
    this.events[eventName].push(fn)
  }

  off<T>(eventName: string, fn: (data: T) => void) {
    if (this.events[eventName]) {
      for (let i = 0; i < this.events[eventName].length; i++) {
        if (this.events[eventName][i] === fn) {
          this.events[eventName].splice(i, 1)
          break
        }
      }
    }
  }

  trigger<T>(eventName: string, data: T) {
    if (this.events[eventName]) {
      this.events[eventName].forEach(function (fn) {
        fn(data as never)
      })
    }
  }
}

const eventBus = new Event()
export default eventBus
