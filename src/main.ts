import { createApp } from 'vue'
import { VueFire, VueFireAuth } from 'vuefire'
import 'vue-select/dist/vue-select.css'
import '@/style.css'
import App from '@/App.vue'
import router from '@/routes'
import { firebaseApp } from '@/firebase'
import { speedtest } from './utils/diceRoll'
import $bus from './event'
import { createTracker, tracker } from './tracker'
import * as Sentry from '@sentry/vue'

createTracker()

if (localStorage.getItem('emulatedUserId')) {
  window.__EMULATED_USER_ID__ = localStorage.getItem('emulatedUserId')
}

const app = createApp(App)

Sentry.init({
  app,
  dsn: 'https://e817463813174c29b46ddab4369dcd32@log.binar.ca/2',
  integrations: [
    Sentry.browserTracingIntegration({ router }),
    Sentry.replayIntegration()
  ],
  tracesSampleRate: 1.0,
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0,
})

app.config.globalProperties.$bus = $bus
app.config.globalProperties.tracker = tracker
app.use(router)
app.use(VueFire, {
  firebaseApp,
  modules: [
    VueFireAuth()
  ]
})

app.mount('#app')
console.log('Light Companion loaded!')
speedtest()
