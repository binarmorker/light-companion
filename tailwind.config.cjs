const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      screens: {
        xs: '480px'
      },
      textShadow: {
        sm: '0 1px 2px var(--tw-shadow-color)',
        DEFAULT: '0 2px 4px var(--tw-shadow-color)',
        lg: '0 8px 16px var(--tw-shadow-color)'
      },
      fontFamily: {
        serif: [
          'Neue Haas Grotesk Display Pro',
          ...defaultTheme.fontFamily.serif,
        ],
        sans: [
          'Neue Haas Grotesk Display Pro',
          ...defaultTheme.fontFamily.sans,
        ],
      }
    }
  },
  plugins: [
    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          'text-shadow': (value) => ({
            textShadow: value,
          })
        },
        {
          values: theme('textShadow')
        }
      )
    })
  ]
}
