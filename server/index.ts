import { WebSocket, WebSocketServer } from 'ws'
import * as Sentry from '@sentry/node'

interface Player {
  connection: WebSocket,
  name: string,
  colorset: string,
  texture: string,
  material: string
}

interface Room {
  name: string,
  players: Set<Player>,
  created: number,
  lastActivity: number
}

interface Message {
  method: 'join' | 'logout' | 'roll',
  user?: string,
  room?: string,
  notation?: string,
  vectors?: string
}

class DiceServer {
  private clients: Set<WebSocket>
  private rooms: Set<Room>
  private playerMap: Map<WebSocket, Player>

  constructor() {
    this.clients = new Set()
    this.rooms = new Set()
    this.playerMap = new Map()
    this.init()
  }

  private init() {
    const wss = new WebSocketServer({ port: 3000 })

    wss.on('connection', (ws: WebSocket) => {
      this.handleConnection(ws)

      ws.on('message', (data: string) => {
        this.handleMessage(ws, data)
      })

      ws.on('close', () => {
        this.handleDisconnection(ws)
      })
    })

    console.log('Dice server started on port 3000')
  }

  private handleConnection(ws: WebSocket) {
    const player = {
      connection: ws,
      name: '',
      colorset: '',
      texture: '',
      material: ''
    }

    this.clients.add(ws)
    this.playerMap.set(ws, player)

    this.log(`Client connected: ${this.clients.size} total clients`)
  }

  private handleMessage(ws: WebSocket, rawData: string) {
    try {
      const data = JSON.parse(rawData)
      const player = this.playerMap.get(ws)

      if (!player) return

      switch (data.method) {
        case 'join':
          this.handleJoin(player, data)
          break
        case 'roll':
          this.handleRoll(player, data)
          break
        case 'logout':
          this.handleLogout(player)
          break
      }
    } catch (err) {
      this.log(`An error occured: ${err}`)
    }
  }

  private handleJoin(player: Player, data: Message) {
    if (!data.user || !data.room) {
      this.sendError(player.connection, 'Invalid username or room name')
      return
    }

    // Sanitize inputs
    const username = this.sanitizeInput(data.user, 35)
    const roomName = this.sanitizeInput(data.room, 35)

    if (!username || !roomName) {
      this.sendError(player.connection, 'Invalid input')
      return
    }

    player.name = username

    let room = this.findRoom(roomName)
    if (!room) {
      room = this.createRoom(roomName)
      this.rooms.add(room)
    }

    this.addPlayerToRoom(player, room)
    this.notifyRoomUpdate(room)
  }

  private handleRoll(player: Player, data: Message) {
    const room = this.findPlayerRoom(player)
    if (!room) return

    const rollData = {
      action: 'roll',
      user: player.name,
      colorset: player.colorset,
      texture: player.texture,
      material: player.material,
      notation: data.notation,
      vectors: data.vectors,
      time: Date.now()
    }

    this.broadcast(room, rollData)
  }

  private handleLogout(player: Player) {
    const room = this.findPlayerRoom(player)
    if (room) {
      this.removePlayerFromRoom(player, room)
    }
  }

  private handleDisconnection(ws: WebSocket) {
    const player = this.playerMap.get(ws)
    if (player) {
      const room = this.findPlayerRoom(player)
      if (room) {
        this.removePlayerFromRoom(player, room)
      }
      this.playerMap.delete(ws)
    }
    this.clients.delete(ws)
    this.log(`Client disconnected: ${this.clients.size} total clients`)
  }

  private findRoom(name: string): Room | undefined {
    return Array.from(this.rooms).find(room =>
      room.name === name
    )
  }

  private createRoom(name: string): Room {
    return {
      name,
      players: new Set(),
      created: Date.now(),
      lastActivity: Date.now()
    }
  }

  private findPlayerRoom(player: Player): Room | undefined {
    return Array.from(this.rooms).find(room => room.players.has(player))
  }

  private addPlayerToRoom(player: Player, room: Room) {
    room.players.add(player)
    room.lastActivity = Date.now()

    this.log(`Player joined room ${room.name}: ${player.name}`)
    player.connection.send(JSON.stringify({
      action: 'joined',
      room: room.name
    }))
  }

  private removePlayerFromRoom(player: Player, room: Room) {
    room.players.delete(player)
    room.lastActivity = Date.now()

    if (room.players.size === 0) {
      this.rooms.delete(room)
    }
  }

  private notifyRoomUpdate(room: Room) {
    const update = {
      action: 'roomUpdate',
      players: Array.from(room.players).map(p => p.name)
    }
    this.broadcast(room, update)
  }

  private sanitizeInput(input: string, maxLength: number): string {
    return input.replace(/[^a-zA-Z0-9_\.-\s]/g, '').trim().substring(0, maxLength)
  }

  private broadcast(room: Room, data: any) {
    const message = JSON.stringify(data)
    room.players.forEach(player => {
      player.connection.send(message)
    })
  }

  private sendError(ws: WebSocket, message: string) {
    ws.send(JSON.stringify({ error: message }))
  }

  private log(message: string) {
    console.log(`${new Date().toISOString()}: ${message}`)
  }
}

Sentry.init({ dsn: 'https://5fab5a5941f74512a513936e12b1e8e9@log.binar.ca/1' })

new DiceServer()
