# Light Companion

Light Companion is a web application designed to enhance the gaming experience for Dungeons & Destiny (D&Destiny) players. It provides a convenient platform for players to access and manage their character sheets, collaborate with other players through sessions, and streamline dice rolling during their adventures.

## Features

- **Character Sheet Management:** Easily view and manage your D&Destiny character sheets in one place. Update and organize your character's vital information with ease.

- **Session Management:** Create, open, and share gaming sessions with your fellow players. Keep track of the progress of your campaigns and adventures effortlessly.

- **Dice Rolling:** Seamlessly exchange dice rolls with other players in real-time. Roll dice according to the rules of D&Destiny to determine the fate of your characters.

## Getting Started

1. Clone this repository:
   ```bash
   git clone https://gitlab.com/binarmorker/light-companion.git
   ```

2. Navigate to the project directory:
   ```bash
   cd light-companion
   ```

3. Install the required dependencies:
   ```bash
   npm install
   ```

4. Run the application locally:
   ```bash
   npm run dev
   ```

5. Access the application in your web browser at http://127.0.0.1:5173.

## Contributing
We welcome contributions from the D&Destiny community to make Light Companion even better. If you'd like to contribute, please follow these guidelines:

1. Fork the repository and create a new branch for your feature or bug fix.

2. Make your changes, and be sure to maintain code quality and follow best practices.

3. Create a clear and concise pull request with a detailed explanation of your changes.

4. Your pull request will be reviewed by the maintainers, and any necessary feedback will be provided.

5. Once approved, your changes will be merged into the main branch.

## Credits
Destiny, Destiny 2, and all its content and icons are property of Bungie.

Dungeons & Destiny, or D&Destiny, is a product of Velvet Fang.

Dungeons & Dragons, D&D, and the dragon head logo are trademarks of Wizards of the Coast.

Light Companion is the product of months of hard work from François Allard (lead developer), Nicolas Blier (data entry and developer), Ian Thibault (data entry), and all the previously mentioned entities.

The contents of the D&Destiny Player's Guidebook, Architect's Guide and Bestiary of the Wilds define their own Product Identity sections in their PDFs, available through their website.

Except for material designated as Product Identity (see above), all of the the content of Light Companion (this website) is Open Game Content as described in Section 1(d) of the License.

Light Companion is published under the Open Game License version 1.0a Copyright 2000 Wizards of the Coast, Inc.