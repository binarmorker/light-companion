import { sentryVitePlugin } from "@sentry/vite-plugin";
import { fileURLToPath, URL } from 'url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import obfuscatorPlugin from 'vite-plugin-javascript-obfuscator'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    obfuscatorPlugin({
      include: ["src/puzzles/**/*.ts"],
      apply: 'build',
      options: {
        optionsPreset: 'high-obfuscation'
      }
    }),
    sentryVitePlugin({
      org: "binar",
      project: "light-companion",
      url: "https://log.binar.ca/"
    })
  ],
  resolve: {
    alias: [
      { find: '@', replacement: fileURLToPath(new URL('./src', import.meta.url)) },
    ],
  },
  build: {
    sourcemap: true
  }
})